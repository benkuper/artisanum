set ANE_PATH="NativeSerial.ane"
set ANE_FILE=NativeSerial.ane
set LIB_NAME=NativeSerial

copy %ANE_PATH% lib\%LIB_NAME%.swc /Y

mkdir extension
mkdir extension\release
mkdir extension\debug

copy %ANE_PATH% extension\release\%ANE_FILE% /Y

copy %ANE_PATH% extension\debug\%ANE_FILE%.zip /Y

unzip -o extension\debug\%ANE_FILE%.zip -d extension\debug\%ANE_FILE% 

del "extension\debug\%ANE_FILE%.zip"

pause