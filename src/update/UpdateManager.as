﻿package update 
{
	import flash.desktop.NativeApplication;
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLStream;
	import flash.system.Capabilities;
	import flash.utils.ByteArray;
	import ui.Alert;
	import ui.Toaster;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class UpdateManager
	{
		
		public var appVersion:String;
		
		private var appIsInit:Boolean;
		private var checkOnInit:Boolean;
		
		public static var instance:UpdateManager;
		
		private static const UPDATE_DESCRIPTOR_URL:String = "http://www.bento-creations.com/artisanum/update.xml";
		private var updateDescLoader:URLLoader;
		
		private var updateUrl:String;
		private var updateFile:File;
		private var urlStream:URLStream;
		private var fileStream:FileStream;
		
		public function UpdateManager() 
		{
			instance = this;
			
			var appXML:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXML.namespace();
			appVersion = appXML.ns::versionNumber[0];
			
			
			trace("Artisanum version : "+appVersion);
			
			updateDescLoader = new URLLoader;
			updateDescLoader.addEventListener(Event.COMPLETE, updateDescLoader_completeHandler);
			updateDescLoader.addEventListener(IOErrorEvent.IO_ERROR, updateDescLoader_ioErrorHandler);
			
		}
		
		
		
		public function checkUpdate():void
		{
			Toaster.info("Vérification de mise à jour... (Version actuelle : "+appVersion+")");
			updateDescLoader.load(new URLRequest(UPDATE_DESCRIPTOR_URL));
			
		}
		
		private function updateDescLoader_ioErrorHandler(e:IOErrorEvent):void 
		{
			Toaster.error("Impossible de vérifier la mise à jour");
		}
		
		private function updateDescLoader_completeHandler(e:Event):void 
		{
			updateDescLoader.close();
				
			// Getting update descriptor XML from loaded data
			var updateDescriptor:XML;
			try
			{
				updateDescriptor = XML(updateDescLoader.data);
			}catch (e:Error)
			{
				Toaster.warning("Fichier de mise à jour à vérifier");
				return;
			}
			// Getting default namespace of update descriptor
			var udns:Namespace = updateDescriptor.namespace();
			// Getting versionNumber from update descriptor
			var updateVersion:String = updateDescriptor.udns::versionNumber.toString();
			
			
			// Comparing current version with update version
			if (appVersion != updateVersion)
			{
				// Getting update url
				if((Capabilities.os.indexOf("Windows") >= 0))
				{
					updateUrl = updateDescriptor.udns::url[0].udns::Windows[0].toString();
				}
				else if((Capabilities.os.indexOf("Mac") >= 0))
				{
					updateUrl = updateDescriptor.udns::url[0].udns::Mac[0].toString();
				}
				
				trace("update URL : " + updateUrl);
				
				// Downloading update file
				if (NativeApplication.nativeApplication.activeWindow != null)
				{
				Alert.show(NativeApplication.nativeApplication.activeWindow.stage, "Une nouvelle version est disponible : " + updateVersion + " (actuelle : "+appVersion+")\nVoulez-vous la télécharger ?", true, downloadUpdate);
				}
			}else
			{
				Toaster.success("L'application est à jour !");
			}
		}
		
		private function downloadUpdate():void 
		{
			Toaster.info("Téléchargement de la mise à jour...");
			// Parsing file name out of the download url
			var fileName:String = updateUrl.substr(updateUrl.lastIndexOf("/") + 1);
					
			// Creating new file ref in temp directory
			updateFile = File.createTempDirectory().resolvePath(fileName);
			
			// Using URLStream to download update file
			urlStream = new URLStream;
			urlStream.addEventListener(Event.OPEN, urlStream_openHandler);
			urlStream.addEventListener(ProgressEvent.PROGRESS, urlStream_progressHandler);
			urlStream.addEventListener(Event.COMPLETE, urlStream_completeHandler);
			urlStream.addEventListener(IOErrorEvent.IO_ERROR, urlStream_ioErrorHandler);
			urlStream.load(new URLRequest(updateUrl));
		}
		
		private function urlStream_ioErrorHandler(e:IOErrorEvent):void 
		{
			Toaster.error("Impossible de télécharger la nouvelle version :\n"+e.text);
		}
		
		protected function urlStream_openHandler(event:Event):void
		{
			// Creating new FileStream to write downloaded bytes into
			fileStream = new FileStream;
			fileStream.open(updateFile, FileMode.WRITE);
		}
		
		protected function urlStream_progressHandler(event:ProgressEvent):void
		{
			// ByteArray with loaded bytes
			var loadedBytes:ByteArray = new ByteArray;
			// Reading loaded bytes
			urlStream.readBytes(loadedBytes);
			// Writing loaded bytes into the FileStream
			fileStream.writeBytes(loadedBytes);
		}
		
		protected function urlStream_completeHandler(event:Event):void
		{
			// Closing URLStream and FileStream
			urlStream.close();
			fileStream.close();
					
			// Installing update
			installUpdate();
			
		}
		
		private function installUpdate():void 
		{
			trace("Installation de la nouvelle version...");
			if((Capabilities.os.indexOf("Windows") >= 0))
			{
				installWindows();
			}
			else if((Capabilities.os.indexOf("Mac") >= 0))
			{
				installMac();
			}
		}
		
		private function installFile(installFile:File):void
		{
			  // Running the installer using NativeProcess API
			var info:NativeProcessStartupInfo = new NativeProcessStartupInfo;
			info.executable = installFile;
				
			var process:NativeProcess = new NativeProcess;
			process.start(info);
				
			// Exit application for the installer to be able to proceed
			NativeApplication.nativeApplication.exit();
		}
		
		private function installWindows():void 
		{
			installFile(updateFile);
		}
		
		
		private function installMac():void 
		{
			var hdiutilHelper:HdiutilHelper = new HdiutilHelper(updateFile);
			hdiutilHelper.addEventListener(Event.COMPLETE, hdiutilHelper_completeHandler);
			hdiutilHelper.addEventListener(ErrorEvent.ERROR, hdiutilHelper_errorHandler);
			hdiutilHelper.attach();
		}
		
		 private function hdiutilHelper_errorHandler(event:ErrorEvent):void
		{
				var hdiutilHelper:HdiutilHelper = event.target as HdiutilHelper;
				hdiutilHelper.removeEventListener(Event.COMPLETE, hdiutilHelper_completeHandler);
				hdiutilHelper.removeEventListener(ErrorEvent.ERROR, hdiutilHelper_errorHandler);
				
				Toaster.error("Erreur lors de l'extration du fichier d'installation");
		}

		private function hdiutilHelper_completeHandler(event:Event):void
		{
			var hdiutilHelper:HdiutilHelper = event.target as HdiutilHelper;
			hdiutilHelper.removeEventListener(Event.COMPLETE, hdiutilHelper_completeHandler);
			hdiutilHelper.removeEventListener(ErrorEvent.ERROR, hdiutilHelper_errorHandler);
			
			var attachedDmg:File = new File(hdiutilHelper.mountPoint);
			var files:Array = attachedDmg.getDirectoryListing();
			
			if (files.length == 1)
			{
				var installFileFolder:File = File(files[0]).resolvePath("Contents/MacOS");
				var installFiles:Array = installFileFolder.getDirectoryListing();
				
				if (installFiles.length == 1)
				{
					installFile(installFiles[0]);
				}else
				{
					Toaster.error("Fichier d'installation invalide :\nContents/MacOS folder should contain only 1 install file!");
				}
			}else
			{
				Toaster.error("Fichier d'installation invalide :\nMounted volume should contain only 1 install file!");
			}
		}
	}
}