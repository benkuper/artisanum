package util 
{
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.text.TextField;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public function drawStringToBitmap(target:BitmapData,tf:TextField,x:Number,y:Number):void {
		var bmd:BitmapData = new BitmapData(tf.width,tf.height,true,0);
		bmd.draw(tf);
		var mat:Matrix = new Matrix();
		mat.translate(x, y);
		target.draw(bmd,mat);
		bmd.dispose();
	}
}
