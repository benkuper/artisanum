package util 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public function findIntersection(A:Point,B:Point,E:Point,F:Point,as_seg:Boolean=true):Point {
		var ip:Point;
		var a1:Number;
		var a2:Number;
		var b1:Number;
		var b2:Number;
		var c1:Number;
		var c2:Number;
	 
		a1= B.y-A.y;
		b1= A.x-B.x;
		c1= B.x*A.y - A.x*B.y;
		a2= F.y-E.y;
		b2= E.x-F.x;
		c2= F.x*E.y - E.x*F.y;
	 
		var denom:Number=a1*b2 - a2*b1;
		if (denom == 0) {
			return null;
		}
		
		ip=new Point();
		ip.x=(b1*c2 - b2*c1)/denom;
		ip.y=(a2*c1 - a1*c2)/denom;
	 
		if ( as_seg ) {
			if (  ( ip.x - A.x ) * ( ip.x - B.x ) > 0
			  ||  ( ip.y - A.y ) * ( ip.y - B.y ) > 0
			  ||  ( ip.x - E.x ) * ( ip.x - F.x ) > 0
			  ||  ( ip.y - E.y ) * ( ip.y - F.y ) > 0
			) return null ;
		}
		
		return ip;
	}
}