package ui 
{
	import ui.components.Panel;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Editor extends Panel 
	{
		
		public function Editor(title:String="Panel", panelWidth:Number=150, panelHeight:Number=200, titleIsEditable:Boolean=false, editionInContext:Boolean=false) 
		{
			super(title, panelWidth, panelHeight, titleIsEditable, editionInContext);
			
		}
		
	}

}