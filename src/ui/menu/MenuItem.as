package ui.menu 
{
	import flash.display.NativeMenuItem;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class MenuItem extends NativeMenuItem
	{
		private var isToggle:Boolean;
		public var path:String;
		public var callback:Function;
		public var id:String;
		
		public function MenuItem(id:String, path:String, callback:Function, shortcut:String = "", shortcutModifier:Array = null, isToggle:Boolean = false) 
		{
			this.isToggle = isToggle;
			
			this.id = id;
			this.callback = callback;
			this.path = path;
			
			var pSplit:Array = path.split("/");
			super(pSplit[pSplit.length - 1], false);
			
			if (shortcut != "") this.keyEquivalent = shortcut;
			if (shortcutModifier != null) this.keyEquivalentModifiers = shortcutModifier;
			
		}
		
		public function trigger():void 
		{
			if (isToggle) checked = !checked;
			if (callback == null) return;
			callback.apply();
			
		}
		
	}

}