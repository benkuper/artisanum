package ui.menu {
	import flash.desktop.NativeApplication;
	import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.system.Capabilities;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Menu 
	{
		private static var menu:NativeMenu;
		private static var isInit:Boolean;
		
		private static var items:Vector.<MenuItem>
		static private var stage:Stage;
		
		
		public function Menu() 
		{
			
		}
		
		public static function init(stage:Stage):void
		{
			
			if (isInit) return;
			
			trace("Menu Init");
			isInit = true;
			stage = stage;
			
			items = new Vector.<MenuItem>;
			menu = new NativeMenu();
			
			menu.addEventListener(Event.SELECT, menuSelect);
			
			if (Capabilities.os.indexOf("Windows") > -1)
			{
				stage.nativeWindow.menu = menu;
			} else
			{
				NativeApplication.nativeApplication.menu = menu;
			}
		}
		
		static private function testCallback():void 
		{
			trace("test callback");
		}
		
		static private function menuSelect(e:Event):void 
		{
			var mi:MenuItem = (e.target as MenuItem);
			
			//trace("Menu Select !", mi.path);
			mi.trigger();
		}
		
		
		public static function addMenu(id:String, path:String, callback:Function, shortcut:String = "", shortcutModifier:Array = null, enabled:Boolean = true,isToggle:Boolean = false ):void
		{
			var mi:MenuItem = new MenuItem(id, path, callback,shortcut,shortcutModifier,isToggle);
			mi.enabled = enabled;
			//on enleve le menu lui meme, on ne garde que le path
			var purePathSplit:Array = path.split("/");
			purePathSplit.pop();
			var purePath:String = purePathSplit.join("/");
			
			
			var curMenu:NativeMenu = goToMenu(purePath);
			
			items.push(mi);
			curMenu.addItem(mi);
		}
		
		
		static public function addSeparator(path:String):void 
		{
			var curMenu:NativeMenu = goToMenu(path);
			curMenu.addItem(new NativeMenuItem("",true));
		}
		
		public static function goToMenu(path:String):NativeMenu
		{
			var pathSplit:Array = path.split("/");
			var curMenu:NativeMenu = menu;
			
			
			for (var i:int = 0; i < pathSplit.length; i++)
			{
				var childMenu:NativeMenu = getSubMenuInMenu(pathSplit[i], curMenu);
				
				
				if (childMenu == null)
				{
					//trace("menu is null, creating one with name :" + pathSplit[i]);
					childMenu = new NativeMenu();
					curMenu.addSubmenu(childMenu, pathSplit[i]);
					
				}else
				{
					//trace("found subMenu : " + childMenu.items.lenght + " items in it");
				}
				
				curMenu = childMenu;
			}
			return curMenu;
		}
		
		public static function getSubMenuInMenu(subMenuName:String, parentMenu:NativeMenu ):NativeMenu
		{
			for each(var item:NativeMenuItem in parentMenu.items)
			{
				if (item.label == subMenuName) return item.submenu;
			}
			
			return null;
		}
		
		
		
		public static function removeMenu(id:String):void
		{
			var mi:MenuItem = getItem(id);
			if (mi == null) return;
			items.splice(items.indexOf(mi), 1);
			menu.removeItem(mi);
		}
		
		public static function setMenuCallback(id:String, callback:Function):void
		{
			var mi:MenuItem = getItem(id);
			if (mi == null) return
			mi.callback = callback;
		}
		
		static public function setEnabled(id:String, value:Boolean):void 
		{
			var mi:MenuItem = getItem(id);
			if (mi == null) return
			//trace("set mi enabled ("+id+")", value);
			mi.enabled = value;
		}
		
		
		static public function getChecked(id:String):Boolean
		{
			var mi:MenuItem = getItem(id);
			if (mi == null) return false;
			
			return mi.checked;
		}
		
		static public function setChecked(id:String, value:Boolean):void 
		{
			var mi:MenuItem = getItem(id);
			if (mi == null) return
			//trace("set mi enabled ("+id+")", value);
			mi.checked = value;
		}
		
		static private function getItem(id:String):MenuItem 
		{
			for each(var mi:MenuItem in items)
			{
				if (mi.id == id) return mi;
			}
			
			return null;
		}
		
	}

}