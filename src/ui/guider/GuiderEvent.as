package ui.guider 
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class GuiderEvent extends Event 
	{
		static public const SHOW:String = "show";
		static public const HIDE:String = "hide";
		
		public var guidedObject:DisplayObject;
		
		public function GuiderEvent(type:String, guidedObject:DisplayObject, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			this.guidedObject = guidedObject;
			
		} 
		
		public override function clone():Event 
		{ 
			return new GuiderEvent(type, guidedObject, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("GuiderEvent", "type", "guidedObject", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}