package ui.guider 
{
	import com.greensock.TweenLite;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import ui.fonts.Fonts;
	import ui.menu.Menu;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Guider extends Sprite 
	{
		private var shadowRect:Sprite;
		//private var highlightRect
		
		private static var instance:Guider;
		private static var stage:Stage;
		
		private static var isInit:Boolean;
		private static var active:Boolean;
		static private var guidedObject:DisplayObject;
		static private var guideText:String;
		static private var guideTF:TextField;
		static private var highlightColor:uint;
		
		
		public function Guider() 
		{
			alpha = 0;
			highlightColor = 0x1F9AF5;
			
			guideTF = Fonts.createTF("Guide", Fonts.guideTF);
			addChild(guideTF);
			
			
			
		}
		
		public static function init(stage:Stage):void
		{
			Guider.stage = stage;
			stage.stageFocusRect = false;
			
			if (instance == null) instance = new Guider();
			isInit = true;
			
		}
		
		public static function show(guidedObject:DisplayObject,guideText:String = "", hideOnFocusOut:Boolean = true,hideOnTime:Number = 0):void
		{
			if (!isInit)
			{
				trace("Guider is not init ! Must init with stage first");
				return;
			}
			
			Guider.guidedObject = guidedObject;
			active = true;
			
			var highlightRect:Rectangle = guidedObject.getRect(stage);
			var margin:Number = 15;
			var bigRect:Rectangle = new Rectangle(highlightRect.x - margin, highlightRect.y -margin, highlightRect.width + margin*2, highlightRect.height + margin*2);
			
			instance.graphics.clear();
			instance.graphics.beginFill(0, .6);
			instance.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight); //draw grey background
			instance.graphics.drawRoundRect(bigRect.x, bigRect.y, bigRect.width, bigRect.height, margin, margin); //draw hole
			instance.graphics.endFill();
			
			instance.graphics.lineStyle(4,highlightColor);
			instance.graphics.drawRoundRect(bigRect.x, bigRect.y, bigRect.width, bigRect.height, margin, margin); //draw hole
			
			guideTF.text = guideText;
			guideTF.x = (stage.stageWidth - guideTF.textWidth) / 2;
			guideTF.y = (stage.stageHeight - guideTF.textHeight) / 2;
			
			stage.addChild(instance);
			TweenLite.to(instance, .6, { alpha:1 } );
			stage.addEventListener(Event.RESIZE, stageResize);
			
			if (hideOnFocusOut)
			{
				stage.focus = instance;
				
				instance.addEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
			}
			
			if (hideOnTime > 0)
			{
				TweenLite.delayedCall(hideOnTime, hide);
			}
			
			instance.dispatchEvent(new GuiderEvent(GuiderEvent.SHOW,guidedObject));
		}
		
		static public function hide():void
		{
			instance.dispatchEvent(new GuiderEvent(GuiderEvent.HIDE,guidedObject));
			Guider.guidedObject = null;
			active =  false;
			TweenLite.to(instance, .2, { alpha:0, onComplete:clean } );
			
			stage.removeEventListener(Event.RESIZE, stageResize);
			instance.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
			
		}
		
		static public function clean():void 
		{
			stage.removeChild(instance);
			
		}
		
		
		static private function focusOutHandler(e:FocusEvent):void 
		{
			hide();
			
		}
		
		static private function stageResize(e:Event):void 
		{
			if (active) show(guidedObject);
		}
		
	}
}