package ui.assets 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Assets 
	{
		
		[Embed(source = "icons/warning.png")]
		public static var WARNING:Class;
		
		
		
		public static function getBitmap(bitmapClass:Class):Bitmap
		{
			return new bitmapClass() as Bitmap;
		}
		
		public static function getBitmapData(bitmapClass:Class):BitmapData
		{
			return (new bitmapClass() as Bitmap).bitmapData;
		}
	}

}