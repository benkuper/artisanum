package ui 
{
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Style 
	{
		public static const BG_COLOR:uint = 0x252525;
		
		public static const UI_NORMAL:uint = 0x404040;
		public static const UI_OVER:uint = 0x808080;
		public static const UI_HIGHLIGHT:uint = 0xFA7014;
		
		public static const UI_PANEL:uint = 0x303030;
		public static const UI_SUBPANEL:uint = 0x505050;
		static public const UI_LIGHT:uint = 0xc0c0c0;
		static public const UI_LINE:uint = 0xaaaaaa;
		static public const PANEL_ACTIVE:uint  = 0x476D83;
		static public const PANEL_PERMANENT:uint = 0xC1E76B;
		
		public static const UI_BLUE_FILL:uint = 0x4797EF;
		
		static public const GRID1:uint = 0x404040;
		static public const GRID2:uint = 0x353535;
		static public const GRID3:uint = 0x282828;
		
		static public const ORANGE:uint = 0xF77704;
		static public const BLUE:uint = 0x0C9FEF;
		static public const GREEN:uint = 0x91D511;
		static public const YELLOW:uint = 0xF9E600;
		static public const RED:uint = 0xE80909;
		
		
		
		
	}
}