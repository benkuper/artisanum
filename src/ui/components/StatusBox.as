package ui.components 
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import ui.fonts.Fonts;
	import ui.Style;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class StatusBox extends UIComponent 
	{
		//Static 
		static public const OK:String = "ok";
		static public const WARNING:String = "warning";
		static public const PENDING:String = "pending";
		static public const ERROR:String = "error";
		static public const NORMAL:String = "normal";
		
		static public const CUSTOM:String = "custom"; //customColor
		
		static public const CIRCLE:String = "circle";
		static public const RECTANGLE:String = "rectangle";
		
		//ui
		private var _status:String;
		
		private var _shape:String;
		private var _radius:Number;
		private var boxWidth:Number;
		private var boxHeight:Number;
		
		private var _customColor:uint;
		
		private var _label:String;
		private var labelTF:TextField;
		
		public function StatusBox(label:String = "") 
		{
			radius = 5;
			boxWidth = 10;
			boxHeight = 10;
			
			labelTF = Fonts.createTF(label,Fonts.normalTF)
			this._label = label;
			addChild(labelTF);
			labelTF.y = -2;
			
			_status = NORMAL;
			_shape = CIRCLE;
			
			draw();
		}
		
		public static function createCircle(label:String = "", radius:Number = 5):StatusBox
		{
			var s:StatusBox = new StatusBox(label);
			s.shape = CIRCLE;
			s.radius = radius;
			return s;
		}
		
		public static function createBox(label:String = "", width:Number = 10, height:Number = 10):StatusBox
		{
			var s:StatusBox = new StatusBox(label);
			s.shape = RECTANGLE;
			s.boxWidth = width;
			s.boxHeight = height;
			return s;
		}
		
		private function draw():void
		{
			graphics.clear();			
			graphics.beginFill(getColorForCurrentStatus());
			
			switch(shape)
			{
				case CIRCLE:
					graphics.drawCircle(labelTF.x+labelTF.textWidth + 10 + radius/2, radius/2+labelTF.y+labelTF.textHeight/2,radius);
					break;
				
				case RECTANGLE:
					graphics.drawRect(labelTF.x+labelTF.textWidth + 7, 0 , boxWidth, boxHeight);
					break;
			}
		}
		
		private function getColorForCurrentStatus():uint
		{
			
			switch(status)
			{
				case OK:
					return Style.GREEN;
					break;
					
				case WARNING:
					return Style.YELLOW;
					break;
					
				case PENDING:
					return Style.BLUE;
					break;
					
				case ERROR:
					return Style.RED;
					break;
					
				case NORMAL:
					return Style.UI_NORMAL;
					break;
				
				case CUSTOM:
					return customColor;
					break;
			}
			
			return 0;
		}
		
		public function get customColor():uint 
		{
			return _customColor;
		}
		
		public function set customColor(value:uint):void 
		{
			_customColor = value;
			status = CUSTOM;
			draw();
		}
		
		public function get status():String 
		{
			return _status;
		}
		
		public function set status(value:String):void 
		{
			_status = value;
			draw();
		}
		
		public function get shape():String 
		{
			return _shape;
		}
		
		public function set shape(value:String):void 
		{
			_shape = value;
			draw();
		}
		
		override public function set width(value:Number):void
		{
			boxWidth = value;
			draw();
		}
		
		override public function set height(value:Number):void
		{
			boxHeight = value;
			draw();
		}
		
		public function get radius():Number 
		{
			return _radius;
		}
		
		public function set radius(value:Number):void 
		{
			_radius = value;
			draw();
		}
		
		public function get label():String 
		{
			return _label;
		}
		
		public function set label(value:String):void 
		{
			_label = value;
			labelTF.text = value;
			draw();
		}
		
	}

}