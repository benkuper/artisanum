package ui.components
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import ui.fonts.Fonts;
	import ui.Style;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class CheckBox extends UIComponent
	{
		//ui
		public var box:Sprite;
		private var tf:TextField;
		
		//data
		private var _label:String;
		private var _selected:Boolean;
		private var toggle:Boolean;
		
		private var _readOnly:Boolean;
		
		public function CheckBox(label:String, toggle:Boolean = false, selected:Boolean = false)
		{
			this._label = label;
			this.toggle = toggle;
			this._selected = selected;
			
			box = new Sprite();
			addChild(box);
			box.filters = [new DropShadowFilter(2, 45, 0, 1, 4, 4, .3)];
			
			tf = Fonts.createTF(label, Fonts.normalTF);
			addChild(tf);
			tf.selectable = false;
			
			addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			addEventListener(MouseEvent.MOUSE_UP, mouseUp);
			
			
			draw();
		}
		
		
		
		private function mouseDown(e:MouseEvent):void
		{
			selected = toggle ? !selected : true;
			
			//if (!toggle) dispatchEvent(new UIEvent(UIEvent.CHECKBOX_SELECTED));
		
		}
		
		private function mouseUp(e:MouseEvent):void
		{
			if (!toggle) selected = false;			
		}
		
		private function draw():void
		{
			
			box.graphics.clear();
			box.graphics.beginFill(selected ? (readOnly?Style.GREEN:Style.UI_HIGHLIGHT) : Style.UI_NORMAL);
			box.graphics.drawRoundRect(0, 0, 20, 20, 3, 3);
			box.graphics.endFill();
			
			box.x = tf.textWidth + 10;
			box.y = (tf.textHeight - box.height) / 2 + 3;
		}
		
		override public function clean():void 
		{
			super.clean();
			removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
			
		}
		
		//getter / setter
		
		public function get label():String
		{
			return _label;
		}
		
		public function set label(value:String):void
		{
			_label = value;
			tf.text = label;
		}
		
		public function get selected():Boolean 
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void 
		{
			if (selected == value) return;
			_selected = value;
			draw();
			dispatchEvent(new UIEvent(selected ? UIEvent.CHECKBOX_SELECTED : UIEvent.CHECKBOX_DESELECTED));
		}
		
		public function get readOnly():Boolean 
		{
			return _readOnly;
		}
		
		public function set readOnly(value:Boolean):void 
		{
			_readOnly = value;
			mouseEnabled = !value;
			mouseChildren = !value;
		}
		
		//Silent
		public function setSilentSelected(value:Boolean):void 
		{
			_selected = value;
			draw();
		}
	
	}

}