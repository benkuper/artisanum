package ui.components 
{
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import ui.fonts.Fonts;
	import ui.Style;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class DropDown extends UIComponent 
	{
		
		//layout
		private var baseWidth:Number;
		private var baseHeight:Number;
		
		private var numColumns:Number;
		private var rowHeight:Number;
		private var optionsWidth:Number;
		
		private static var _currentOpenedDropDown:DropDown;
		private var _opened:Boolean;
		
		//data
		private var items:Vector.<DropDownItem>;
		private var _selectedItem:DropDownItem;
		private var _selectedIndex:int;
		
		//ui
		private var selectButton:Button;
		private var _enabled:Boolean;
		
		private var dropDown:Sprite;
		private var itemContainer:Panel;
		private var _noItemText:String;
		private var _shadowEditing:Boolean;
		
		public var instantClose:Boolean;
		
		
		public function DropDown(options:Array = null,noItemText:String = "Cliquer pour choisir") 
		{
			this._noItemText = noItemText;
			this.instantClose = true;
			
			selectButton = new Button(noItemText,true);
			addChild(selectButton);
			selectButton.addEventListener(UIEvent.BUTTON_SELECTED, buttonSelectChange);
			selectButton.addEventListener(UIEvent.BUTTON_DESELECTED, buttonSelectChange);
			selectButton.filters = [];
			//bg = new Shape();
			//selectButton.addChild(bg);
			
			numColumns = 3
			rowHeight = 25;
			
			baseWidth = 80;
			baseHeight = 20;
			
			optionsWidth = 350;
			
			//tf = Fonts.createTF("Module",Fonts.buttonTF);
			//selectButton.addChild(tf);
			//tf.selectable = false;			
			
			filters = [new DropShadowFilter(2, 45, 0, 1, 4, 4, .3)];
			
			dropDown = new Sprite();
			//addChild(dropDown);
			itemContainer = new Panel("");
			itemContainer.headerHeight = 5;
			itemContainer.scrollable = true;
			itemContainer.autoLayout = false;
			itemContainer.bgColor = 0xDFDFDF;
			//itemContainer.transparent = true;
			dropDown.addChild(itemContainer);
			
			
			
			this.items = new Vector.<DropDownItem>();
			if (options != null) setOptions(options);
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			dropDown.addEventListener(DropDownItem.ITEM_SELECTED, dropdownItemSelected);
			
			_opened = false;
			dropDown.scaleX = 0;
			dropDown.scaleY = 0;
			
		}
		
		//data
		
		public function setOptions(options:Array,selectFirst:Boolean = false):void 
		{
			
			clearOptions();
			//this.items = new Vector.<DropDownItem>();
			//itemContainer.removeAllEmenents();
			
			if (options != null)
			{
				var found:Boolean;
				
				for (var i:int = 0; i < options.length; i++)
				{
					var item:DropDownItem = addOption(options[i].id, options[i].label, options[i].value);
					
					if (selectedItem != null)
					{
						if (item.id == selectedItem.id) //item.label == selectedItem.label && item.value == selectedItem.value)
						{
							selectedItem = item;
							found = true;
						}
					}
				}
				
				if (!found) 
				{
					if (selectFirst && items.length > 0)
					{
						selectedItem = items[0];
					}else
					{
						selectedItem = null;
					}
				}
			}else
			{
				selectedItem = null;
			}
			
			drawOptions();
			
			//if (items.length > 0) selectedItem = items[0];
		}
		
		private function clearOptions():void 
		{
			if (items == null) return;
			while (numOptions > 0) removeOption(items[0]);
			
		}
		
		private function removeOption(item:DropDownItem):void 
		{
			item.clean();
			itemContainer.removeElement(item);
			items.splice(items.indexOf(item), 1);
		}
		
		public function getOptions():Array
		{
			var a:Array = new Array();
			for each(var i:DropDownItem in items)
			{
				a.push( {id:i.id, label:i.label, value:i.value } );
			}
			return a;
		}
		
		public function getOptionAt(index:int):*
		{
			if (index >= items.length) return null;
			
			return items[index];
		}
		
		private function addOption(id:String, label:String, value:*):DropDownItem
		{
			//trace("add option :", label, value);
			var item:DropDownItem = new DropDownItem(id,label, value);
			items.push(item);
			itemContainer.addElement(item);
			return item;
			
		}
		
		private function addedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			drawSelect();
			drawOptions();
			
			addEventListener(Event.REMOVED_FROM_STAGE, removeFromStage);
		}
		
		private function removeFromStage(e:Event):void 
		{
			//removeEventListener(Event.REMOVED_FROM_STAGE, removeFromStage);
			opened = false;
		}
		
		
		//handlers
		private function dropdownItemSelected(e:UIEvent):void 
		{
			opened = false;
			selectedItem = e.target as DropDownItem;
			selectButton.selected = false;
		}	
		
		private function buttonSelectChange(e:UIEvent):void 
		{
			opened = e.type == UIEvent.BUTTON_SELECTED;
		}
		
		
		//ui
		private function drawSelect():void 
		{
			//bg.graphics.clear();
			//bg.graphics.beginFill(Style.UI_NORMAL);
			//bg.graphics.drawRoundRect(0, 0, baseWidth, baseHeight, 5, 5);
			//bg.graphics.endFill();
			//
			//tf.x = (baseWidth-tf.textWidth)/2 - 2;
			//tf.y = 2;
			
			selectButton.width = baseWidth;
			selectButton.height = baseHeight;
			
			
		}
		
		private function drawOptions():void
		{
			var margin:Number = 10;
			var spacing:Number = 5;
			
			var curY:Number = margin;
			
			var lastItemLimit:Number = 0;
			
			for (var i:int = 0; i < items.length; i++)
			{
				var item:DropDownItem = items[i];
				item.x = 5 + ((i % numColumns)/numColumns) * (optionsWidth - margin * 2 + spacing);
				item.y = curY + Math.floor(i / numColumns) * (item.height + spacing);
				
				item.width = optionsWidth / numColumns - margin ;
				item.height = rowHeight;
				
				lastItemLimit = item.y + item.height;
			}
			
			itemContainer.width = optionsWidth;
			itemContainer.height = Math.min(lastItemLimit+20, 200);
			
			//curY = itemContainer.height + 20;
			
			//dropDown.graphics.clear();
			//dropDown.graphics.beginFill(Style.UI_LIGHT);
			//dropDown.graphics.drawRoundRect(0, 0, optionsWidth,curY,5,5);
			//dropDown.graphics.endFill();
			
		}
		
		public function setSelectedOption(id:String):Boolean
		{
			for each (var loopItem:DropDownItem in items)
			{
				if (loopItem.id == id)
				{
					selectedItem = loopItem;
					return true;
				}
			}
			return false;
		}
		
		public function setSelectedOptionWithValue(value:*):Boolean 
		{
			for each (var loopItem:DropDownItem in items)
			{
				if (loopItem.value == value)
				{
					selectedItem = loopItem;
					return true;
				}
			}
			return false;
		}
		
		public function setSelectedOptionWithLabel(label:String):Boolean 
		{
			for each (var loopItem:DropDownItem in items)
			{
				if (loopItem.label == label)
				{
					selectedItem = loopItem;
					return true;
				}
			}
			return false;
		}
		
		
		
		public function setOptionsLayout(numColumns:int = 0, optionsWidth:Number = 0, rowHeight:Number = 0):void
		{
			if (numColumns > 0) this.numColumns = numColumns;
			if (optionsWidth > 0)  this.optionsWidth = optionsWidth;
			if (rowHeight > 0) this.rowHeight = rowHeight;
			drawOptions();
		}
		
		public function resetSelection():void 
		{
			selectedItem = null;
		}
		
		
		
		//cleaning
		override public function clean():void 
		{
			super.clean();
			dropDown.removeEventListener(DropDownItem.ITEM_SELECTED, dropdownItemSelected);
			selectButton.removeEventListener(UIEvent.BUTTON_SELECTED, buttonSelectChange);
			selectButton.removeEventListener(UIEvent.BUTTON_DESELECTED, buttonSelectChange);
			selectButton.clean();
			for each(var di:DropDownItem in items) di.clean();
			
		}
		
		
		//Getter / setter
		
		
		override public function get width():Number
		{
			return baseWidth;
		}
		
		override public function set width(value:Number):void
		{
			baseWidth = value;
			drawSelect();
		}
		
		override public function get height():Number
		{
			return baseHeight;
		}
		
		override public function set height(value:Number):void
		{
			baseHeight = value;
			drawSelect();
		}
		
		public function get opened():Boolean 
		{
			return _opened;
		}
		
		public function set opened(value:Boolean):void 
		{
			if (opened == value) return;
			
			_opened = value;
			
			if (value)
			{
				dispatchEvent(new UIEvent(UIEvent.DROPDOWN_OPEN));
				currentOpenedDropDown = this;
				if (shadowEditing) selectButton.transparentBG = false;
			}else
			{
				dispatchEvent(new UIEvent(UIEvent.DROPDOWN_CLOSE));
				if (shadowEditing) selectButton.transparentBG = true;
				//currentOpenedDropDown = null;
			}
			
			selectButton.selected = value;
			
			if (stage == null) return;
			
			var tx:Number = 0;
			
			if (this.localToGlobal(new Point(this.x+optionsWidth, this.height)).x > stage.stageWidth)
			{
				tx = baseWidth - optionsWidth;
			}
			
			var stagePoint:Point = this.localToGlobal(new Point(0,baseHeight+5));
			var tPoint:Point = this.localToGlobal(new Point(tx,baseHeight+5));
			dropDown.y = stagePoint.y;
			dropDown.x = tPoint.x;
			
			//trace("OPen dropdown", this.height, this.baseHeight, selectButton.height, stagePoint );
			
			if (instantClose && !value)
			{
				TweenLite.killTweensOf(dropDown);
				dropDown.scaleX = 0;
				dropDown.scaleY = 0;
				if (!opened) stage.removeChild(dropDown);
			}else
			{
				if (opened) stage.addChild(dropDown);
				TweenLite.to(dropDown, .5, { x:value?tPoint.x:stagePoint.x, scaleX:value?1:0, scaleY:value?1:0, ease:Strong.easeOut,onComplete:finishDropDown } );
			}
			
		}
		
		public function finishDropDown():void 
		{
			if (!opened && stage != null && stage.contains(dropDown)) stage.removeChild(dropDown);
		}
		
		public function get selectedOption():Object
		{
			if (selectedItem == null) return null;
			return {id:selectedItem.id, label:selectedItem.label, value:selectedItem.value };
		}
		
		
		private function get selectedItem():DropDownItem 
		{
			return _selectedItem;
		}
		
		private function set selectedItem(value:DropDownItem):void 
		{
			if (selectedItem == value) return;
			
			_selectedItem = value;
			
			//trace("selected Item = " + value+"index = "+items.indexOf(selectedItem));
			if (selectedItem != null)
			{
				selectButton.label = value.label;
				_selectedIndex = items.indexOf(selectedItem);
			}else
			{
				selectButton.label = noItemText;
				_selectedIndex = -1;
			}
			
			dispatchEvent(new UIEvent(UIEvent.DROPDOWN_CHANGE));
		}
		
		
		override public function set enabled(value:Boolean):void 
		{
			super.enabled = value;
			
			if (!value) opened = false;
		}
		
		public function get selectedIndex():int 
		{
			return _selectedIndex;
		}
		
		public function set selectedIndex(value:int):void 
		{
			if (items.length == 0) return;
			if (value == -1) 
			{
				selectedItem = null;
			}else
			{
				_selectedIndex = Math.min(value, items.length - 1);
				selectedItem = items[selectedIndex];
			}
			
		}
		
		public function get numOptions():int
		{
			return items.length;
		}
		
		public function get shadowEditing():Boolean 
		{
			return _shadowEditing;
		}
		
		public function set shadowEditing(value:Boolean):void 
		{
			_shadowEditing = value;
			selectButton.transparentBG = !opened;
		}
		
		static public function get currentOpenedDropDown():DropDown 
		{
			return _currentOpenedDropDown;
		}
		
		static public function set currentOpenedDropDown(value:DropDown):void 
		{
			if (currentOpenedDropDown == value) return;
			if (currentOpenedDropDown != null) currentOpenedDropDown.opened = false; //Avoid multiple dropdown opened at the same time
			_currentOpenedDropDown = value;
			
			
			//if (currentOpenedDropDown != null && currentOpenedDropDown.parent != null) currentOpenedDropDown.parent.setChildIndex(currentOpenedDropDown, currentOpenedDropDown.parent.numChildren - 1);// = true;
		}
		
		public function get noItemText():String 
		{
			return _noItemText;
		}
		
		public function set noItemText(value:String):void 
		{
			_noItemText = value;
			if (selectedItem == null) selectButton.label = noItemText;
		}
		
		
	}

}