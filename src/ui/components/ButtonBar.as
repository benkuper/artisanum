package ui.components 
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import ui.fonts.Fonts;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ButtonBar extends UIComponent 
	{
		
		private var labelTF:TextField;
		private var _label:String;
		
		private var buttons:Vector.<Button>;
		private var buttonContainer:Sprite;
		private var options:Array;
		
		//layout
		private var _buttonWidth:Number;
		private var baseWidth:Number;
		private var baseHeight:Number;
		
		private var _selectedButton:Button;
		
		//paging
		private var buttonsPerPage:int;
		private var _curPage:int;
		private var prevBT:Button;
		private var nextBT:Button;
		
		
		public function ButtonBar(label:String = "", options:Array = null, buttonsPerPage:int = 0) 
		{
			super();
			
			this.buttonsPerPage = buttonsPerPage;
			_curPage = 0;
			
			prevBT = new Button("<", false, Button.ROUND_LEFT);
			prevBT.width = 15;
			nextBT = new Button(">", false, Button.ROUND_RIGHT);
			nextBT.width = 15;
			
			_label = label;
			labelTF = Fonts.createTF(label, Fonts.normalTF);
			addChild(labelTF);
			
			
			buttonContainer = new Sprite();
			addChild(buttonContainer);
			
			buttonContainer.x = labelTF.x + labelTF.width + 5;
			
			buttons = new Vector.<Button>();
			
			this.setOptions(options);
			
			addEventListener(UIEvent.BUTTON_SELECTED, buttonSelectHandler);
		}
		
		public function setOptions(options:Array):void
		{
			buttons = new Vector.<Button>();
			buttonContainer.removeChildren();
			this.options = options;
			if (options == null) 
			{
				options = new Array();
				return;
			}
			
			
			for (var i:int = 0; i < options.length;i++)
			{
				var label:String = options[i].label;
				var rc:String = (i == 0)?Button.ROUND_LEFT:((i == options.length - 1)?Button.ROUND_RIGHT:Button.ROUND_NONE);
				if (buttonsPerPage > 0) rc = Button.ROUND_NONE;
				
				var bt:Button = new Button(label, true, rc);
				buttons.push(bt);
				
			}
			
			if(buttonsPerPage > 0) curPage = 0;
			else placeElements();
		}
		
		private function placeElements():void
		{
			var bt:Button;
			var curX:int = 0;
			buttonContainer.removeChildren();
			
			if (buttonsPerPage == 0)
			{
				for each(bt in buttons)
				{
					buttonContainer.addChild(bt);
					bt.x = curX;
					curX += bt.width;
				}
			}else
			{
				buttonContainer.addChild(prevBT);
				curX += prevBT.width;
				prevBT.enabled = curPage > 0;
				
				for (var i:int = curPage * buttonsPerPage; i < (curPage+1) * buttonsPerPage && i < buttons.length; i++)
				{
					bt = buttons[i];
					buttonContainer.addChild(bt);
					bt.x = curX;
					curX += bt.width;
				}
				
				buttonContainer.addChild(nextBT);
				nextBT.x = curX;
				nextBT.enabled = (curPage+1) * buttonsPerPage < buttons.length;
			}
		}
		
		//handlers
		private function buttonSelectHandler(e:UIEvent):void 
		{
			if (e.target == prevBT || e.target == nextBT)
			{
				curPage += (e.target == nextBT)?1: -1;
			}else
			{
				selectedButton = e.target as Button;
			}
		}
		
		
		//cleaning
		override public function clean():void 
		{
			super.clean();
			for each(var b:Button in buttons) b.clean();
			buttons = null;
			removeEventListener(UIEvent.BUTTON_SELECTED, buttonSelectHandler);
		}
		
		//getter /setter
		public function get label():String 
		{
			return _label;
		}
		
		public function set label(value:String):void 
		{
			_label = value;
		}
		
		private function get selectedButton():Button 
		{
			return _selectedButton;
		}
		
		private function set selectedButton(value:Button):void 
		{
			if (selectedButton == value) return;
			if (selectedButton != null)
			{
				selectedButton.selected = false;
			}
			
			_selectedButton = value;
			
			if (selectedButton != null)
			{
				selectedButton.selected = true;
				dispatchEvent(new UIEvent(UIEvent.SELECTION_CHANGED));
			}
		}
		
		public function get selectedOption():Object
		{
			return options[buttons.indexOf(selectedButton)];
		}
		
		public function get value():*
		{
			if (selectedOption == null) return null;
			
			return selectedOption.value;
		}
		
		public function set selectedIndex(value:int):void
		{
			//trace("bar set selected index :", value);
			if (value >= 0 && value < buttons.length) selectedButton = buttons[value];
		}
		
		public function get buttonWidth():Number 
		{
			return _buttonWidth;
		}
		
		public function set buttonWidth(value:Number):void 
		{
			_buttonWidth = value;
			for each(var bt:Button in buttons) 
			{
				bt.width = value;
			}
			placeElements();
		}
		
		override public function set width(value:Number):void
		{
			baseWidth = value;
			buttonContainer.x = value - buttonContainer.width;
		}
		
		override public function get height():Number 
		{
			return baseHeight;
		}
		
		override public function set height(value:Number):void 
		{
			baseHeight = value;
			for each(var bt:Button in buttons) bt.height = value;
			prevBT.height = value;
			nextBT.height = value;
			placeElements();
		}
		
		public function get curPage():int 
		{
			return _curPage;
		}
		
		public function set curPage(value:int):void 
		{
			value = Math.max(value, 0);
			_curPage = value;
			
			placeElements();
		}
		
	}

}