package ui.components
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.ui.Keyboard;
	import ui.fonts.Fonts;
	import ui.Style;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TextBox extends UIComponent
	{
		
		//layout
		private var _autoSize:Boolean;
		private var baseWidth:Number;
		private var baseHeight:Number;
		
		//data
		private var _text:String;
		private var _label:String;
		private var _editable:Boolean;
		private var _editing:Boolean;
		private var _transparentBG:Boolean;
		public var opaqueBGOnEdit:Boolean;
		private var previousText:String;
		
		//ui
		private var textBox:Sprite;
		private var labelTF:TextField;
		private var textTF:TextField;
		
		private var _showLabel:Boolean;
		
		
		public function TextBox(label:String, text:String = "")
		{
			this._label = label;
			this._text = text;
			this._showLabel = (label != "");
			
			labelTF = Fonts.createTF(label, Fonts.normalTF);
			addChild(labelTF);
			labelTF.selectable = false;
			
			textBox = new Sprite();
			addChild(textBox);
			textTF = Fonts.createTF(text, Fonts.normalTF);
			textBox.addChild(textTF);
			textTF.selectable = false;
			editable = false;
			autoSize = true;
			
			opaqueBGOnEdit = true;
			
			filters = [new DropShadowFilter(2, 45, 0, 1, 4, 4, .3)];
			draw();
			
			textTF.addEventListener(FocusEvent.FOCUS_IN, textTFFocusIn);
			textTF.addEventListener(FocusEvent.FOCUS_OUT, textTFFocusOut);
			textTF.addEventListener(KeyboardEvent.KEY_DOWN, textTFKeyDown);
		}
		
		private function draw():void
		{
			var boxWidth:Number;
			var boxHeight:Number;
			
			if (autoSize)
			{
				boxWidth = textTF.textWidth + 8;
				boxHeight = textTF.textHeight + 8;
				baseWidth = labelTF.textWidth + 10 + boxWidth;
				baseHeight = boxHeight;
			}else
			{
				boxWidth = baseWidth - labelTF.textWidth - 10;
				boxHeight = baseHeight;
			}
			
			if (showLabel)
			{
				labelTF.x = 0;
				labelTF.y = (baseHeight - textTF.textHeight) / 2 - 3;
			}
			
			textBox.x = (showLabel?labelTF.textWidth:0) + 10;
			textTF.x = 2;
			textTF.y = (baseHeight - textTF.textHeight)/2-2;
			
			textBox.graphics.clear();
			if (!transparentBG)
			{
				textBox.graphics.beginFill(Style.UI_NORMAL);
				textBox.graphics.drawRoundRect(0, 0, boxWidth, boxHeight, 3, 3);
				textBox.graphics.endFill();
			}
		}
		
		public function setFocus():void 
		{
			stage.focus = textTF;
		}
		
		public function selectText():void 
		{
			textTF.setSelection(0, textTF.length);
		}
		
		//handlers
		private function tfChange(e:Event):void 
		{
			draw();
			text = textTF.text;
			dispatchEvent(new UIEvent(UIEvent.TEXTBOX_CHANGE));
		}
		
		private function textTFFocusIn(e:FocusEvent):void 
		{
			editing = true;
			previousText = text;
		}	
		
		private function textTFFocusOut(e:FocusEvent):void 
		{
			editing = false;
			//trace("focus out !");
		}
		
		private function textTFKeyDown(e:KeyboardEvent):void 
		{
			switch(e.keyCode)
			{
				case Keyboard.ENTER:
					text = textTF.text;
					editing = false;
					stage.focus = null;
					break;
					
				case Keyboard.ESCAPE:
					text = previousText;
					editing = false;
					stage.focus = null;
					break;
				
				default:
					break;
			}
			
		}
		
		//cleaning
		override public function clean():void 
		{
			super.clean();
			textTF.removeEventListener(FocusEvent.FOCUS_IN, textTFFocusIn);
			textTF.removeEventListener(FocusEvent.FOCUS_OUT, textTFFocusOut);
			textTF.removeEventListener(KeyboardEvent.KEY_DOWN, textTFKeyDown);
		}
		
		//Getter / setter
		
		public function get label():String
		{
			return _label;
		}
		
		public function set label(value:String):void
		{
			_label = value;
			labelTF.text = label;
			draw();
		}
		
		public function get text():String
		{
			return _text;
		}
		
		public function set text(value:String):void
		{
			var dispatch:Boolean = true;
			if (text == value) dispatch = false;
			
			_text = value;
			textTF.text = text;
			draw();
			
			
			if(dispatch) dispatchEvent(new UIEvent(UIEvent.TEXTBOX_CHANGE));
		}
		
		public function get editable():Boolean 
		{
			return _editable;
		}
		
		public function set editable(value:Boolean):void 
		{
			_editable = value;
			textTF.type = value?TextFieldType.INPUT:TextFieldType.DYNAMIC;
			textTF.selectable = value;
			
			if (value)
			{
				textTF.addEventListener(Event.CHANGE, tfChange);
			}else
			{
				textTF.removeEventListener(Event.CHANGE, tfChange);
			}
			//tmp
			autoSize = true;
		}
		
		
		
		public function get autoSize():Boolean 
		{
			return _autoSize;
		}
		
		public function set autoSize(value:Boolean):void 
		{
			_autoSize = value;
			draw();
		}
		
		override public function set width(value:Number):void
		{
			autoSize = false;
			baseWidth = value;
			draw();
		}
		
		override public function set height(value:Number):void
		{
			autoSize = false;
			baseHeight = value;
			draw();
		}
		
		public function get transparentBG():Boolean 
		{
			return _transparentBG;
		}
		
		public function set transparentBG(value:Boolean):void 
		{
			_transparentBG = value;
			draw();
		}
		
		public function get showLabel():Boolean 
		{
			return _showLabel;
		}
		
		public function set showLabel(value:Boolean):void 
		{
			_showLabel = value;
			labelTF.visible = value;
			draw();
		}
		
		public function get editing():Boolean 
		{
			return _editing;
		}
		
		public function set editing(value:Boolean):void 
		{
			//trace("set editing,", value, ", current :", editing,", editable : ",editable);
			if (!editable) value = false;
			
			if (editing && value) return;
			_editing = value;
			
			if (opaqueBGOnEdit) transparentBG = !value;
			dispatchEvent(new UIEvent(value?UIEvent.EDITING_START:UIEvent.EDITING_FINISH));
		}
	}

}