package ui.components 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class UIEvent extends Event 
	{
		static public const PANEL_OPENED:String = "panelOpened";
		static public const PANEL_CLOSED:String = "panelClosed";
		static public const PANEL_RESIZED:String = "panelResized";
		static public const PANEL_CLICK:String = "panelClick";
		
		static public const BUTTON_SELECTED:String = "buttonSelected";
		static public const BUTTON_DESELECTED:String = "buttonDeselected";
		
		static public const DROPDOWN_OPEN:String = "dropdownOpen";
		static public const DROPDOWN_CLOSE:String = "dropdownClose";
		static public const DROPDOWN_CHANGE:String = "dropdownChange";
		
		static public const SLIDER_CHANGE:String = "sliderChange";
		static public const SLIDER_SET:String = "sliderSet";
		
		static public const CHECKBOX_SELECTED:String = "checkboxSelected";
		static public const CHECKBOX_DESELECTED:String = "checkboxDeselected";
		
		static public const EDITING_START:String = "editingStart";
		static public const EDITING_FINISH:String = "editingFinish";
		static public const TEXTBOX_CHANGE:String = "textboxChange";
		
		static public const DRAGGING:String = "dragging";
		
		//multibar
		static public const SELECTION_CHANGED:String = "selectionChanged";
		
		public function UIEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new UIEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("UIEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}