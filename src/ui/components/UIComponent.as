package ui.components 
{
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class UIComponent extends Sprite 
	{
		
		private var _enabled:Boolean;
		
		public function UIComponent() 
		{
			_enabled = true;
			super();
			
		}
		
		//cleaning
		public function clean():void
		{
			//to be overriden;
		}
		
		public function get enabled():Boolean 
		{
			return _enabled;
		}
		
		public function set enabled(value:Boolean):void 
		{
			_enabled = value;
			
			mouseEnabled = value;
			mouseChildren = value;
			alpha = value?1:.5;
		}
	}

}