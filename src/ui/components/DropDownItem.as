package ui.components 
{
	import flash.display.Shape;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class DropDownItem extends UIComponent 
	{
		//layout
		//public static const WIDTH:Number = 50;
		//public static const HEIGHT:Number = 30;
		private var baseWidth:Number;
		private var baseHeight:Number;
		
		//data
		public var id:String;
		public var label:String;
		public var value:*;
		
		//ui
		private var button:Button;
		
		//event
		static public const ITEM_SELECTED:String = "dropdownItemSelected";
		
		public function DropDownItem(id:String,label:String, value:*)
		{
			this.id = id;
			this.label = label;
			this.value = value;
			
			baseWidth = 60;
			baseHeight = 30;
			
			button = new Button(label);
			addChild(button);
			button.filters = [];
			button.addEventListener(UIEvent.BUTTON_SELECTED, buttonSelected);
			
			draw();
		}
		
		private function buttonSelected(e:UIEvent):void 
		{
			dispatchEvent(new UIEvent(ITEM_SELECTED));
		}
		
		private function draw():void 
		{
			button.width = baseWidth;
			button.height = baseHeight;
		}
		
		//cleaning
		override public function clean():void 
		{
			super.clean();
			button.removeEventListener(UIEvent.BUTTON_SELECTED, buttonSelected);
			button.clean();
		}
		
		//getter / setter
		
		override public function set width(value:Number):void
		{
			baseWidth = value;
			draw();
		}
		
		override public function set height(value:Number):void
		{
			baseHeight = value;
			draw();
		}
		
	}

}