package ui.components 
{
	import benkuper.nativeExtensions.ExtendedMouse;
	import benkuper.util.KeyboardUtil;
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.ui.Mouse;
	import ui.fonts.Fonts;
	import ui.Style;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Slider extends UIComponent
	{
		//layout
		private var baseWidth:Number;
		private var baseHeight:Number;
		
		private var barWidth:Number; //keep here for stability when checking mouse pos over bar
		
		static public const HORIZONTAL:String = "horizontal";
		static public const VERTICAL:String = "vertical";
		
		//data
		private var _label:String;
		private var _value:Number;
		
		public var min:Number;
		public var max:Number;
		
		private var _steps:int;
		private var stepInterval:Number;
		private var _stepsObjects:Array;
		
		private var _readOnly:Boolean;
		
		private var _currentStepObject:Object;
		private var _currentStepIndex:int;
		
		private var _noDecimals:Boolean;
		private var _tolerance:Number;
		private var _useTolerance:Boolean;
		private var initTolerance:Number; //for relative
		private var initMouseX:Number; //for relative
		
		private var _useRef:Boolean;
		private var _refValue:Number;
		
		//ui
		private var labelTF:TextField;
		private var bar:Sprite;
		private var barBG:Shape;
		
		private var valueBar:Shape;
		private var toleranceBar:Shape;
		
		
		private var refPointer:Shape;
		
		private var handle:Sprite;
		private var valueBox:TextBox;
		private var zeroBarValue:Number;
		
		private var changing:Boolean;
		
		private var intSteps:Boolean;
		private var showStepsLabels:Boolean;
		private var _showLabel:Boolean;
		private var _showValue:Boolean;
		private var _valuePosition:String;
		
		private var draggingHandle:Sprite;
		
		
		private var _color:uint;
		private var _bgColor:uint;
		private var refNorm:Number;
		
		
		
		//ui-static
		static public const TOP:String = "top";
		static public const LEFT:String = "left";
		static public const RIGHT:String = "right";
		static public const BOTTOM:String = "bottom";
		static public const TOP_RIGHT:String = "topRight";
		static public const TOP_LEFT:String = "topLeft";
		static public const BOTTOM_LEFT:String = "bottomLeft";
		static public const BOTTOM_RIGHT:String = "bottomRight";
		static public const TOP_HANDLE:String = "topHandle";
		static public const BOTTOM_HANDLE:String = "bottomHandle";
		
		public function Slider(label:String, defaultValue:Number = 0,min:Number = 0,max:Number = 100, valuePosition:String = RIGHT ) 
		{
			
			baseWidth = 100;
			baseHeight = 10;
			
			this._value = defaultValue;
			this._label = label;
			this.min = min;
			this.max = max;
			this._showLabel = true;
			this._showValue = true;
			this._valuePosition = valuePosition;
			this._color = Style.UI_BLUE_FILL;
			this._bgColor = Style.UI_NORMAL;
			this.zeroBarValue = 0;
			this._tolerance = 0;
			
			_steps = 0;
			
			bar = new Sprite();
			addChild(bar);
			barBG = new Shape();
			bar.addChild(barBG);
			valueBar = new Shape();
			bar.addChild(valueBar);
			toleranceBar = new Shape();
			bar.addChild(toleranceBar);
			
			refPointer = new Shape();
			bar.addChild(refPointer);
			refPointer.x = 0;
			
			handle = new Sprite();
			bar.addChild(handle);
			
			
			labelTF = Fonts.createTF(label, Fonts.normalTF);
			addChild(labelTF);
			labelTF.selectable = false;
			
			valueBox = new TextBox("",defaultValue.toFixed(noDecimals?0:2));// , Fonts.normalTF);
			addChild(valueBox);
			valueBox.editable = true;
			
			valueBox.transparentBG = true;
			valueBox.addEventListener(UIEvent.EDITING_START, valueBoxEditingStart);
			valueBox.addEventListener(UIEvent.EDITING_FINISH, valueBoxEditingFinish);
			
			
			
			bar.filters = [new DropShadowFilter(2, 45, 0, 1, 4, 4, .5)];
			
			bar.addEventListener(MouseEvent.MOUSE_DOWN,mouseHandler);
			
			draw();	
		}
		
		
		private function valueBoxEditingStart(e:UIEvent):void 
		{
			valueBox.transparentBG = false;
		}
		
		private function valueBoxEditingFinish(e:UIEvent):void 
		{
			//trace("editing finish");
			valueBox.transparentBG = true;
			value = Number(valueBox.text);
			dispatchEvent(new UIEvent(UIEvent.SLIDER_SET));
		}
		
		private function draw():void
		{
			labelTF.y = (baseHeight - labelTF.textHeight) / 2;
			
			bar.x = showLabel?(labelTF.textWidth + 15):0;
			bar.y = baseHeight/2+3;
			
			barWidth =  baseWidth - bar.x - ((showValue && (valuePosition == RIGHT || valuePosition == LEFT))?50:0);
			if (noDecimals) barWidth += 20;
			
		
			
			barBG.graphics.clear();
			barBG.graphics.beginFill(bgColor);
			barBG.graphics.drawRoundRect(0, -baseHeight/2,barWidth, baseHeight, 10,10);
			barBG.graphics.endFill();
			
			
			var zeroX:Number = getPositionForValue(zeroBarValue);
			var tx:Number = normalizedValue * barWidth;
			
			if (zeroX > 0)
			{
				var tW:Number = tx - zeroX;
				var leftRad:Number = tW > 0?0:10;
				var rightRad:Number = tW < 0?0:10;
				
				valueBar.graphics.clear();
				valueBar.graphics.beginFill(color);
				valueBar.graphics.drawRoundRectComplex(Math.min(zeroX,zeroX+tW), -baseHeight / 2, Math.abs(tW), baseHeight,leftRad,rightRad,leftRad,rightRad);
				valueBar.graphics.endFill();
				
				valueBar.graphics.beginFill(Style.UI_OVER);
				valueBar.graphics.drawRect(zeroX-1,-baseHeight/2,2,baseHeight);
				valueBar.graphics.endFill();
			}else
			{
				valueBar.graphics.clear();
				valueBar.graphics.beginFill(color);
				valueBar.graphics.drawRoundRect(0, -baseHeight / 2, tx, baseHeight, 10, 10);
				valueBar.graphics.endFill();
			}
			
			handle.graphics.clear();
			handle.graphics.beginFill(changing?Style.UI_HIGHLIGHT:Style.UI_LIGHT);
			handle.graphics.drawCircle(0, 0, baseHeight /2+3);
			handle.graphics.endFill();
			handle.y = 0;
			handle.x = tx;
			
			toleranceBar.graphics.clear();
			if (tolerance > 0)
			{
				var barTolerance:Number = normalizedTolerance * barWidth;
				var toleranceStart:Number = Math.max(handle.x - barTolerance / 2, 0);
				var toleranceEnd:Number = Math.min(handle.x + barTolerance / 2, barWidth);
				var toleranceW:Number = toleranceEnd - toleranceStart;
				
				toleranceBar.graphics.beginFill(Style.YELLOW, .5);
				toleranceBar.graphics.drawRoundRect(toleranceStart, -baseHeight / 2, toleranceW, baseHeight, 10, 10);
				toleranceBar.graphics.endFill();
			}
			
			refPointer.graphics.clear();
			
			if (useRef)
			{
				refNorm = getNormalized(refValue);
				refPointer.graphics.lineStyle(2, Style.RED);
				refPointer.graphics.moveTo(0,-baseHeight/2);
				refPointer.graphics.lineTo(0, baseHeight/2);
				refPointer.x = refNorm * barWidth;
			}
			
			if (showValue)
			{
				switch(valuePosition)
				{
					case LEFT:
						valueBox.x = 0;
						valueBox.y = (baseHeight - valueBox.height) / 2;
						break;
						
					case RIGHT:
						valueBox.x = baseWidth - valueBox.width;
						valueBox.y = (baseHeight - valueBox.height) / 2;
						break;
						
					case TOP:
						valueBox.x = (baseWidth -valueBox.width) /2;
						valueBox.y = -valueBox.height -5;
						break;
						
					case BOTTOM:
						valueBox.x = (baseWidth -valueBox.width) /2;
						valueBox.y = baseHeight;
						break;
						
					case TOP_RIGHT:
						valueBox.x = bar.x + barWidth - valueBox.width;
						valueBox.y = -valueBox.height -5;
						break;
						
					case BOTTOM_RIGHT:
						valueBox.x = bar.x + barWidth - valueBox.width;
						valueBox.y = baseHeight;
						break;
						
					case TOP_LEFT:
						valueBox.x = 0;
						valueBox.y = -valueBox.height -5;
						break;
						
					case BOTTOM_LEFT:
						valueBox.x = 0
						valueBox.y = baseHeight;
						break;
						
					case TOP_HANDLE:
						valueBox.x = handle.x - valueBox.width/2;
						valueBox.y = -valueBox.height -5;
						break;
						
					case BOTTOM_HANDLE:
						valueBox.x = handle.x- valueBox.width/2;
						valueBox.y = baseHeight
						break;
				}
				
			}
			
		}
		
		
		
		override public function clean():void
		{
			bar.removeEventListener(MouseEvent.MOUSE_DOWN, mouseHandler);
			bar.removeEventListener(MouseEvent.RIGHT_MOUSE_DOWN, rightMouseHandler);
			valueBox.removeEventListener(UIEvent.EDITING_START, valueBoxEditingStart);
			valueBox.removeEventListener(UIEvent.EDITING_FINISH, valueBoxEditingFinish);
			valueBox.clean();
			
		}
		
		//handlers
		
		private function mouseHandler(e:MouseEvent):void
		{
			switch(e.type)
			{
				case MouseEvent.MOUSE_DOWN:
					Mouse.hide();
					stage.addEventListener(MouseEvent.MOUSE_UP,mouseHandler);
					stage.addEventListener(Event.ENTER_FRAME, mouseEnterFrame);
					stage.addEventListener(KeyboardEvent.KEY_DOWN, changingKeyHandler);
					stage.addEventListener(KeyboardEvent.KEY_UP, changingKeyHandler);
					
					changing = true;
					break;
				
				case MouseEvent.MOUSE_UP:
					Mouse.show();
					stage.removeEventListener(MouseEvent.MOUSE_UP,mouseHandler);
					stage.removeEventListener(Event.ENTER_FRAME, mouseEnterFrame);
					stage.removeEventListener(KeyboardEvent.KEY_DOWN, changingKeyHandler);
					stage.removeEventListener(KeyboardEvent.KEY_UP, changingKeyHandler);
					
					changing = false;
					intSteps = false;
					dispatchEvent(new UIEvent(UIEvent.SLIDER_SET));
					ExtendedMouse.setCursorRelativePos(handle.x, handle.y, bar);
					
					break;
			}
			
			draw();
		}
		
		private function rightMouseHandler(e:MouseEvent):void
		{
			switch(e.type)
			{
				case MouseEvent.RIGHT_MOUSE_DOWN:
					Mouse.hide();
					stage.addEventListener(MouseEvent.RIGHT_MOUSE_UP,rightMouseHandler);
					stage.addEventListener(Event.ENTER_FRAME, rightMouseEnterFrame);
					initTolerance = normalizedTolerance;
					initMouseX = bar.mouseX;
					changing = true;
					break;
				
				case MouseEvent.RIGHT_MOUSE_UP:
					Mouse.show();
					stage.removeEventListener(MouseEvent.RIGHT_MOUSE_UP,rightMouseHandler);
					stage.removeEventListener(Event.ENTER_FRAME, rightMouseEnterFrame);
					
					changing = false;
					intSteps = false;
					dispatchEvent(new UIEvent(UIEvent.SLIDER_SET));
					
					ExtendedMouse.setCursorRelativePos(initMouseX,handle.y, bar);
					break;
			}
			
			draw();
		}
		
		
		private function changingKeyHandler(e:KeyboardEvent):void 
		{
			intSteps = e.shiftKey;
			
			//force enterFrame for value
			if (intSteps) value = int(value);
			else mouseEnterFrame();
		}
		
		private function mouseEnterFrame(e:Event = null):void 
		{
			normalizedValue = bar.mouseX / barWidth;
		}		
		
		
		private function rightMouseEnterFrame(e:Event = null):void 
		{
			var val:Number = ((bar.mouseX - initMouseX) / barWidth);
			normalizedTolerance = Math.min(Math.max(initTolerance+val, 0), 1);
			
		}		
		
		//value
		public function toValue(val:Number, time:Number):void
		{
			TweenLite.to(this, time, { value:val,ease:Strong.easeOut } );
		}
		
		public function setSilentValue(value:Number):void 
		{
			_value = value;
			draw();
		}
		
		private function getPositionForValue(value:Number):Number 
		{
			return ((value-min) / (max - min)) * barWidth;
		}
		
		//util
		public function getNormalized(val:Number):Number 
		{
			return (val-min) / (max - min);
		}
		
		//getters / setters
		
		public function get label():String 
		{
			return _label;
		}
		
		public function set label(value:String):void 
		{
			_label = value;
			labelTF.text = value;
			draw();
		}
		
		public function get value():Number 
		{
			return _value;
		}
		
		public function set value(value:Number):void 
		{
			if (isNaN(value)) return;
			
			//trace("set value unfiltered :" + value+", previous value :",this._value);
			value = Math.max(min, Math.min(value, max));
			
			//trace("set value : 1 = ", value+" (max = "+max+")");
			
			if (intSteps) value = int(value);
			//trace("set value int = " + value);
			
			if (steps > 0)
			{
				var curStep:int = Math.round((value-min) / stepInterval);
				value = min + curStep * stepInterval;
				//trace("set value curStep = " + curStep + ", stepValue =" + value);
			}
			
			if (this._value == value) return;
			
			_value = value;
			
			if (showStepsLabels)
			{
				_currentStepIndex = Math.min(curStep, stepsObjects.length - 1);
				_currentStepObject = stepsObjects[currentStepIndex];
				
				if (currentStepObject != null)
				{
					valueBox.text = currentStepObject.label;
				}else
				{
					valueBox.text = "[Erreur]";
				}
				
			}else
			{
				//trace("int steps ?", intSteps);
				valueBox.text = value.toFixed(noDecimals?0:2).toString();
			}
			
			draw();
			
			dispatchEvent(new UIEvent(UIEvent.SLIDER_CHANGE));
		}
		
		
		public function get normalizedValue():Number 
		{
			return getNormalized(value);
		}
		
		
		public function set normalizedValue(value:Number):void 
		{
			this.value = min + value * (max - min);
		}
		
		public function get tolerance():Number 
		{
			return _tolerance;
		}
		
		public function set tolerance(value:Number):void 
		{
			_tolerance = value;
			draw();
			dispatchEvent(new UIEvent(UIEvent.SLIDER_CHANGE));
		}
		
		
		public function get normalizedTolerance():Number 
		{
			return getNormalized(tolerance);
		}
		
		public function set normalizedTolerance(value:Number):void 
		{
			this.tolerance = min + value * (max - min);
		}
		
		
		
		override public function set width(value:Number):void
		{
			baseWidth = value;
			
			
			draw();
		}
		
		override public function set height(value:Number):void
		{
			baseHeight = value;
			draw();
		}
		
		public function get readOnly():Boolean 
		{
			return _readOnly;
		}
		
		public function set readOnly(value:Boolean):void 
		{
			_readOnly = value;
			handle.visible = !value;
			mouseEnabled = !value;
			mouseChildren = !value;
		}
		
		public function get steps():int 
		{
			return _steps;
		}
		
		public function set steps(value:int):void 
		{
			if (value < 0)
			{
				if (value == -1) value = max - min;
				else value = 0;
			}
			
			//trace("set steps :", value);
			_steps = value;
			stepInterval = (max - min) / steps;
			value = value; //force updating value
		}
		
		
		public function get stepsObjects():Array
		{
			return _stepsObjects;
		}
		
		public function set stepsObjects(value:Array):void 
		{
			_stepsObjects = value;
			
			if (value == null) 
			{
				showStepsLabels = false;
				_steps = 0;
				return;
			}
			
			showStepsLabels = true;
			min = 0;
			max = stepsObjects.length-1;
			steps = -1;
			this.value = 0;
			
		}
		
		public function get showLabel():Boolean 
		{
			return _showLabel;
		}
		
		public function set showLabel(value:Boolean):void 
		{
			_showLabel = value;
			labelTF.visible = value;
			draw();
		}
		
		public function get currentStepObject():Object 
		{
			return _currentStepObject;
		}
		
		
		public function get showValue():Boolean 
		{
			return _showValue;
		}
		
		public function set showValue(value:Boolean):void 
		{
			_showValue = value;
			valueBox.visible = value;
			draw();
		}
		
		public function get valuePosition():String 
		{
			return _valuePosition;
		}
		
		public function set valuePosition(value:String):void 
		{
			_valuePosition = value;
			draw();
		}
		
		public function get currentStepIndex():int 
		{
			return _currentStepIndex;
		}
		
		public function set currentStepIndex(value:int):void 
		{
			//trace("set current step index :",value);
			_currentStepIndex = Math.max(Math.min(value, stepsObjects.length - 1),0);
			_currentStepObject = stepsObjects[currentStepIndex];
			normalizedValue = currentStepIndex / stepsObjects.length;
		}
		
		
		public function get noDecimals():Boolean 
		{
			return _noDecimals;
		}
		
		public function set noDecimals(value:Boolean):void 
		{
			_noDecimals = value;
			valueBox.text = this.value.toFixed(noDecimals?0:2).toString();
			draw();
		}
		
		public function get color():uint 
		{
			return _color;
		}
		
		public function set color(value:uint):void 
		{
			_color = value;
			draw();
		}
		
		public function get bgColor():uint 
		{
			return _bgColor;
		}
		
		public function set bgColor(value:uint):void 
		{
			_bgColor = value;
			draw();
		}
		
		public function get useRef():Boolean 
		{
			return _useRef;
		}
		
		public function set useRef(value:Boolean):void 
		{
			_useRef = value;
			draw();
		}
		
		public function get refValue():Number 
		{
			return _refValue;
		}
		
		public function set refValue(value:Number):void 
		{
			_refValue = value;
			draw();
		}
		
		public function get useTolerance():Boolean 
		{
			return _useTolerance;
		}
		
		public function set useTolerance(value:Boolean):void 
		{
			_useTolerance = value;
			if (value)
			{
				bar.addEventListener(MouseEvent.RIGHT_MOUSE_DOWN,rightMouseHandler);
			}else
			{
				bar.removeEventListener(MouseEvent.RIGHT_MOUSE_DOWN,rightMouseHandler);
			}
		}
		
		
		
	}

}