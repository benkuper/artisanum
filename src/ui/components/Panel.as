package ui.components 
{
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import ui.assets.Assets;
	import ui.fonts.Fonts;
	import ui.Style;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Panel extends Sprite 
	{
		
		//layout
		protected var baseWidth:Number;
		protected var baseHeight:Number;
		protected var _headerHeight:Number;
		
		//layout and scrolling
		private var _autoLayout:Boolean;
		public var scrollMask:Shape;
		private var _scrollable:Boolean;
		
		//mini mode
		private var _miniMode:Boolean;
		public var bgWidth:Number;
		public var bgHeight:Number;
		
		//data
		private var _title:String;
		private var defaultTitle:String;
		
		private var _state:String;
		static public const STATE_OK:String = "stateOk";
		static public const STATE_WARNING:String = "stateWarning";
		static public const STATE_ERROR:String = "stateError";
		static public const STATE_WAIT:String = "stateWait";
		
		//ui
		protected var bg:Sprite;
		protected var _bgColor:uint;
		protected var _borderColor:uint;
		protected var mainContainer:Sprite;
		
		protected var titleTF:TextField;
		protected var titleBox:TextBox;
		private var switcherBT:Button;
		
		private var scroller:Shape;
		
		protected var _transparent:Boolean;
		protected var _borders:Boolean;
		
		protected var selectable:Boolean;
		private var _selected:Boolean;
		
		public var stateIcon:Bitmap;
		
		//move
		private var _moveable:Boolean;
		private var offsetX:Number;
		private var offsetY:Number;
		private var initX:Number;
		private var initY:Number;
		
		//context
		private var renameMenuItem:NativeMenuItem;
		private var editionInContext:Boolean;
		
		private var miniModeMenuItem:NativeMenuItem;
		private var _miniModeEnabled:Boolean;
		
		public function Panel(title:String = "Panel", panelWidth:Number = 150, panelHeight:Number = 200,titleIsEditable:Boolean = false,editionInContext:Boolean = false) 
		{
			this.editionInContext = editionInContext;
			this.defaultTitle = title;
			this._title = title;
			
			bg = new Sprite();
			addChild(bg);
			
			scrollMask = new Shape();
			scroller = new Shape();
			scroller.alpha = 0;
			
			_bgColor = Style.UI_SUBPANEL;
			_borderColor = Style.UI_LIGHT;
			
			titleTF = Fonts.createTF(title, Fonts.normalTF);
			addChild(titleTF);
			titleTF.x = 5;
			titleTF.y = 5;
			titleTF.selectable = false;
			
			titleBox = new TextBox("", title);
			titleBox.showLabel = false;
			
			stateIcon = new Bitmap();
			
			contextMenu = new NativeMenu();
			miniModeMenuItem = new NativeMenuItem("Mini Mode");
			
			doubleClickEnabled = true;
			addEventListener(MouseEvent.CLICK, clickHandler);
			addEventListener(MouseEvent.RIGHT_MOUSE_DOWN, rightMouseHandler);
			
			contextMenu.addEventListener(Event.SELECT, contextMenuSelect);
			
			if (titleIsEditable)
			{
				titleTF.visible = false;
				
				if (!editionInContext) 
				{
					titleBox.editable = true;
				}
				
				titleBox.transparentBG = true;
				
				if (!editionInContext) titleBox.addEventListener(MouseEvent.CLICK, titleBoxClick);
				else titleBox.addEventListener(UIEvent.EDITING_START, titleBoxEditingStart);
				
				if (editionInContext) 
				{
					titleBox.mouseEnabled = true;
					titleBox.mouseChildren = true;
				}
				
				titleBox.addEventListener(UIEvent.EDITING_FINISH, titleBoxEditingFinish);
				
				
				renameMenuItem = new NativeMenuItem("Renommer le titre");
				contextMenu.addItem(renameMenuItem);
				addChild(titleBox);
				
			}else
			{
				titleBox.mouseEnabled = false;
				titleBox.mouseChildren = false;
			}
			
			mainContainer = new Sprite();
			addChild(mainContainer);
			
			baseWidth = panelWidth;
			baseHeight = panelHeight;
			headerHeight = 50;
			
			
			bg.filters = [new DropShadowFilter(1, 45, 0, 1, 6,6, .4, 1)];
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			
		}
		
		
		//data
		public function addElement(element:DisplayObject):void
		{
			mainContainer.addChild(element);
			if(autoLayout) draw();
		}
		
		public function removeElement(element:DisplayObject):void
		{
			if (mainContainer.contains(element)) mainContainer.removeChild(element);
		}
		
		public function removeAllEmenents():void 
		{
			mainContainer.removeChildren();
		}
		
		public function bringElementToFront(element:DisplayObject):void 
		{
			if (mainContainer.contains(element)) mainContainer.setChildIndex(element, mainContainer.numChildren - 1);
			else trace("element not child");
		}
		
		//ui	
		
		protected function draw():void
		{
			if (autoLayout)
			{
				var curY:Number = 0;
				for (var i:int = 0; i < mainContainer.numChildren; i++)
				{
					var d:DisplayObject  = mainContainer.getChildAt(i);
					d.y = curY;
					curY += d.height + 5;
				}
				
				baseHeight = mainContainer.y + mainContainer.height + 10;
			}
			
			stateIcon.x = baseWidth / 2;
			stateIcon.y = 5;
			
			if (!miniMode)
			{
				bgWidth = baseWidth;
				bgHeight = baseHeight;
			}else
			{
				bgWidth = baseWidth;
				bgHeight = headerHeight;
			}
			
			drawBG();
			
		}
		
		protected function drawBG():void 
		{
			bg.graphics.clear();
			if (!transparent) bg.graphics.beginFill(bgColor);
			
			if (borders) bg.graphics.lineStyle(2, borderColor, .3);
			if (selected) bg.graphics.lineStyle(2, Style.UI_BLUE_FILL);
			bg.graphics.drawRoundRect(0, 0, bgWidth, bgHeight,5,5);
			bg.graphics.endFill();
			
			scrollMask.y = headerHeight;
			scrollMask.graphics.clear();
			scrollMask.graphics.beginFill(0x550033);
			scrollMask.graphics.drawRect(0,0, baseWidth, baseHeight - headerHeight);
			scrollMask.graphics.drawRect(0,0, baseWidth, baseHeight - headerHeight);
			scrollMask.graphics.endFill();
			
			
			scroller.graphics.clear();
			if (scrollable)
			{
				if (mainContainer.height > baseHeight - headerHeight)
				{
					scroller.graphics.beginFill(Style.BLUE);
					scroller.graphics.drawRoundRect(0, 0, 5, ((baseHeight-headerHeight) / mainContainer.height)*(baseHeight-headerHeight-25),3,3);
					scroller.graphics.endFill();
					scroller.x = baseWidth - scroller.width;
					
				}
			}
		}
		
		
		
		//handlers
		protected function addedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			draw();
		}
		
		
		protected function contextMenuSelect(e:Event):void 
		{
			var item:NativeMenuItem = e.target as NativeMenuItem;
			switch(item)
			{
				case renameMenuItem:
					titleBox.transparentBG = false;
					titleBox.editable = true;
					titleBox.setFocus();
					titleBox.selectText();
					
					break;
					
				case miniModeMenuItem:
					miniMode = !miniModeMenuItem.checked;
					break;
			}
		}
		
		private function titleBoxEditingStart(e:UIEvent):void 
		{
			titleBox.transparentBG = false;
		}
		
		private function titleBoxClick(e:MouseEvent):void 
		{
			titleBox.selectText();
			titleBox.transparentBG = false;
			
		}
		
		private function titleBoxEditingFinish(e:UIEvent):void 
		{
			titleBox.transparentBG = true;
			
			if (titleBox.text.length > 0)
			{
				title = titleBox.text;
			}else
			{
				title = defaultTitle;
			}
		}
		
		private function mouseHandler(e:MouseEvent):void 
		{
			switch(e.type)
			{
				case MouseEvent.MOUSE_DOWN:
					TweenLite.killTweensOf(this, { x:true, y:true } );
					offsetX = stage.mouseX;
					offsetY = stage.mouseY;
					initX = this.x;
					initY = this.y;
					parent.setChildIndex(this, parent.numChildren - 1);
					stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
					stage.addEventListener(MouseEvent.MOUSE_UP, mouseHandler);
					break;
					
				case MouseEvent.MOUSE_UP:
					stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
					stage.removeEventListener(MouseEvent.MOUSE_UP, mouseHandler);
					if (selectable)
					{
						if (Math.abs(this.x - initX) < 5 && Math.abs(this.y - initY) < 5) selected = true;
					}
					break;
			}
		}
		
		
		
		private function mouseMoveHandler(e:MouseEvent):void 
		{
			this.x  = initX + (stage.mouseX - offsetX);
			this.y = initY + (stage.mouseY - offsetY);
			dispatchEvent(new UIEvent(UIEvent.DRAGGING));
		}
		
		
		private function mouseWheel(e:MouseEvent):void // TODO : Create scrollPosition getter / setter with global handling (no wheel / scrollTo separation)
		{
			drawBG();
			var minPos:Number = headerHeight;
			var maxPos:Number = baseHeight - mainContainer.height + headerHeight - 60;
			
			var tPos:Number = mainContainer.y + e.delta * 30;
			var ty:Number = Math.min(Math.max(tPos, maxPos), minPos);
			
			var percent:Number = (ty - minPos) / (maxPos - minPos);
			var sty:Number = percent * (baseHeight-headerHeight-scroller.height);
			TweenLite.killTweensOf(mainContainer);
			TweenLite.to(mainContainer, .5, { y:ty, ease:Strong.easeOut, onComplete:scrollComplete } );
			TweenLite.to(scroller, .5, { y:headerHeight+sty,ease:Strong.easeOut } );
			TweenLite.to(scroller, .2, { alpha:1 } );
		}
		
		public function scrollComplete():void 
		{
			TweenLite.to(scroller, .2, { alpha:0 } );
		}
		
		public function scrollTo(pos:Number):void //0-1
		{
			drawBG();
			var minPos:Number = headerHeight;
			var maxPos:Number = baseHeight - mainContainer.height + headerHeight - 60;
			
			var tPos:Number = minPos + (pos * (maxPos - minPos));
			var ty:Number = Math.min(Math.max(tPos, maxPos), minPos);
			
			var percent:Number = (ty - minPos) / (maxPos - minPos);
			var sty:Number = percent * (baseHeight-headerHeight-scroller.height);
			TweenLite.killTweensOf(mainContainer);
			TweenLite.to(mainContainer, .5, { y:ty, ease:Strong.easeOut, onComplete:scrollComplete } );
			TweenLite.to(scroller, .5, { y:headerHeight+sty,ease:Strong.easeOut } );
			TweenLite.to(scroller, .2, { alpha:1 } );
		}
		
		public function scrollToElement(element:DisplayObject):void
		{
			if (!mainContainer.contains(element)) return;
			if (mainContainer.height == 0) return;
			scrollTo(element.y / mainContainer.height);
		}
		
		private function clickHandler(e:MouseEvent):void 
		{
			if (miniModeEnabled && e.shiftKey) miniMode = !miniMode;
		}
		
		public function closeComplete():void 
		{
			if (contains(mainContainer)) removeChild(mainContainer);
			dispatchEvent(new UIEvent(UIEvent.PANEL_CLOSED));
		}
		
		private function rightMouseHandler(e:MouseEvent):void 
		{
			parent.setChildIndex(this, parent.numChildren - 1);
		}
		
		
		//clean
		public function clean():void
		{
			moveable = false;
			
			titleBox.removeEventListener(MouseEvent.CLICK, titleBoxClick);
			titleBox.removeEventListener(UIEvent.EDITING_START, titleBoxEditingStart);
			titleBox.removeEventListener(UIEvent.EDITING_FINISH, titleBoxEditingFinish);
			contextMenu.removeEventListener(Event.SELECT, contextMenuSelect);
			
			removeEventListener(MouseEvent.MOUSE_WHEEL, mouseWheel);
			removeEventListener(MouseEvent.RIGHT_MOUSE_DOWN, rightMouseHandler);
			
			removeEventListener(MouseEvent.CLICK, clickHandler);
			contextMenu.removeEventListener(Event.SELECT, contextMenuSelect);
		}
		
		
		//getter /setter
		override public function get width():Number
		{
			return baseWidth;
		}
		
		override public function set width(value:Number):void
		{
			if (width == value) return;
			//if (baseWidth == value) return;
			baseWidth = value;
			bgWidth = value;
			dispatchEvent(new UIEvent(UIEvent.PANEL_RESIZED));
			draw();
		}
		
		override public function set height(value:Number):void
		{
			if (height == value) return;
			//if (baseHeight == value) return;
			baseHeight = value;
			bgHeight = miniMode?headerHeight:value;
			dispatchEvent(new UIEvent(UIEvent.PANEL_RESIZED));
			draw();
		}
		
		public function get transparent():Boolean 
		{
			return _transparent;
		}
		
		public function set transparent(value:Boolean):void 
		{
			_transparent = value;
			draw();
		}
		
		public function get moveable():Boolean 
		{
			return _moveable;
		}
		
		public function set moveable(value:Boolean):void 
		{
			_moveable = value;
			
			//TODO : replace with a move handle
			
			if (moveable) 
			{
				bg.addEventListener(MouseEvent.MOUSE_DOWN, mouseHandler);
				titleTF.addEventListener(MouseEvent.MOUSE_DOWN, mouseHandler);
				titleBox.addEventListener(MouseEvent.MOUSE_DOWN, mouseHandler);
			}else 
			{
				bg.removeEventListener(MouseEvent.MOUSE_DOWN, mouseHandler);
				titleTF.removeEventListener(MouseEvent.MOUSE_DOWN, mouseHandler);
				titleBox.removeEventListener(MouseEvent.MOUSE_DOWN, mouseHandler);
				
			}
		}
		
		//public function get baseHeight():Number 
		//{
			//return _baseHeight;
		//}
		//
		//public function set baseHeight(value:Number):void 
		//{
			//_baseHeight = value;
			//dispatchEvent(new UIEvent(UIEvent.PANEL_RESIZED));
		//}
		
		public function get headerHeight():Number 
		{
			return _headerHeight;
		}
		
		public function set headerHeight(value:Number):void 
		{
			_headerHeight = value;
			mainContainer.y = headerHeight;
		}
		
		public function get title():String 
		{
			return _title;
		}
		
		public function set title(value:String):void 
		{
			_title = value;
			titleTF.text = value;
			titleBox.text = value;
		}
		
		public function get bgColor():uint 
		{
			return _bgColor;
		}
		
		public function set bgColor(value:uint):void 
		{
			_bgColor = value;
			draw();
		}
		
		public function get borders():Boolean 
		{
			return _borders;
			draw();
		}
		
		public function set borders(value:Boolean):void 
		{
			_borders = value;
		}
		
		public function get borderColor():uint 
		{
			return _borderColor;
			draw();
		}
		
		public function set borderColor(value:uint):void 
		{
			_borderColor = value;
		}
		
		
		public function get selected():Boolean 
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void 
		{
			if (selected == value) return;
			_selected = value;
			draw();
		}
		
		public function get miniMode():Boolean 
		{
			return _miniMode;
		}
		
		public function set miniMode(value:Boolean):void 
		{
			_miniMode = value;
			
			miniModeMenuItem.checked = value;
			
			if (!value)
			{
				//trace("dispatch !");
				TweenLite.to(this, .2, { bgHeight:baseHeight, onUpdate:drawBG,onComplete:dispatchEvent,onCompleteParams:[new UIEvent(UIEvent.PANEL_OPENED)] } );
				addChild(mainContainer);
				//mainContainer.alpha = 0;
				TweenLite.to(mainContainer, .2, { delay:.2, alpha:1} );
			}else
			{
				TweenLite.to(mainContainer, .2, { alpha:0} );
				TweenLite.to(this, .2, { delay:.2, bgHeight:headerHeight, onUpdate:drawBG, onComplete:closeComplete } );
				
				
			}
			
			
		}
		
		public function get miniModeEnabled():Boolean 
		{
			return _miniModeEnabled;
		}
		
		public function set miniModeEnabled(value:Boolean):void 
		{
			if (miniModeEnabled == value) return;
			
			_miniModeEnabled = value; 
			
			if (value) contextMenu.addItem(miniModeMenuItem);
			else contextMenu.removeItem(miniModeMenuItem);
		}
		
		public function get autoLayout():Boolean 
		{
			return _autoLayout;
		}
		
		public function set autoLayout(value:Boolean):void 
		{
			_autoLayout = value;
			
			draw();
		}
		
		public function get scrollable():Boolean 
		{
			return _scrollable;
		}
		
		public function set scrollable(value:Boolean):void 
		{
			_scrollable = value;
			if (value)
			{
				mainContainer.mask = scrollMask;
				addChild(scrollMask);
				addChild(scroller);
				addEventListener(MouseEvent.MOUSE_WHEEL, mouseWheel);
			
			}else
			{
				mainContainer.mask = null;
				if(contains(scrollMask)) removeChild(scrollMask);
				if (contains(scroller)) removeChild(scroller);
				removeEventListener(MouseEvent.MOUSE_WHEEL, mouseWheel);
			
			}
			
			draw();
		}
		
		public function get state():String 
		{
			return _state;
		}
		
		public function set state(value:String):void 
		{
			if (state == value) return;
			_state = value;
			switch(state)
			{
				case STATE_OK:
					if (contains(stateIcon)) removeChild(stateIcon);
					break;
					
				case STATE_WARNING:
					addChild(stateIcon);
					stateIcon.bitmapData = Assets.getBitmapData(Assets.WARNING);
					break;
					
				//case STATE_ERROR:
					//addChild(stateIcon);
					//stateIcon.bitmapData = Assets.getBitmapData(Assets.WARNING);
					//break;
				//
				//case STATE_WAIT:
					//addChild(stateIcon);
					//stateIcon.bitmapData = Assets.getBitmapData(Assets.WARNING);
					//break;
				
			}
		}
	}

}