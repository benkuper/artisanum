package ui.components 
{
	import com.greensock.TweenMax;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import ui.fonts.Fonts;
	import ui.Style;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Button extends UIComponent 
	{
		//layout
		private var buttonWidth:Number;
		private var buttonHeight:Number;
		
		//masking
		private var btMask:Shape;
		
		//style
		private var _roundCorners:String;
		public static const ROUND_ALL:String = "all";
		public static const ROUND_LEFT:String = "left";
		public static const ROUND_RIGHT:String = "right";
		static public const ROUND_NONE:String = "none";
		
		//data
		private var _label:String;
		private var _toggle:Boolean;
		private var _selected:Boolean;
		
		//ui	
		private var clickable:Boolean;
		private var bg:Shape;
		private var tf:TextField;
		private var _transparentBG:Boolean;
		
		public var selectedColor:uint = Style.UI_HIGHLIGHT;
		
		public function Button(label:String,toggle:Boolean = false,roundCorners:String = ROUND_ALL) 
		{
			this._label = label;
			this._toggle = toggle;
			this._roundCorners = roundCorners;
			
			bg = new Shape();
			addChild(bg);
			
			
			
			tf = Fonts.createTF(label, Fonts.buttonTF);
			tf.selectable = false;
			addChild(tf);
			tf.x = 10;
			tf.y = 10;
			tf.filters = [new DropShadowFilter(1, 45, 0, 1, 4,4, .7, 1)];
			
			buttonWidth = tf.textWidth + 30;
			buttonHeight = tf.textHeight + 10;
			
			
			btMask = new Shape();
			addChild(btMask);
			tf.mask = btMask;
			drawMask();
			
			filters = [new DropShadowFilter(2, 45, 0, 1, 6, 6, .5, 1)];
			
			mouseChildren = false;
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
		}
		
		//ui
		private function draw():void
		{
			bg.graphics.clear();
			bg.graphics.beginFill(selected?selectedColor:Style.UI_NORMAL);
			
			switch(roundCorners)
			{
				case ROUND_ALL:
					bg.graphics.drawRoundRect(0, 0, buttonWidth, buttonHeight, 5, 5);
					break;
					
				case ROUND_LEFT:
					bg.graphics.drawRoundRectComplex(0, 0, buttonWidth, buttonHeight,5, 0, 5, 0);
					break;
					
				case ROUND_RIGHT:
					bg.graphics.drawRoundRectComplex(0, 0, buttonWidth, buttonHeight, 0, 5, 0, 5);
					break;
					
				case ROUND_NONE:
					bg.graphics.drawRect(0, 0, buttonWidth, buttonHeight);
					break;
			}
			
			bg.graphics.endFill();
			
			
			
			tf.x = (buttonWidth - tf.textWidth) / 2;
			tf.y = (buttonHeight - tf.textHeight) / 2 - 3;
		}
		
		private function drawMask():void
		{
			btMask.graphics.clear();
			btMask.graphics.beginFill(0xff00ff);
			btMask.graphics.drawRect(0, 0, buttonWidth, buttonHeight);
			btMask.graphics.endFill();
		}
		
		
		//Handlers
		private function addedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, removedFromStage);
			
			addEventListener(MouseEvent.MOUSE_OVER, mouseOverOut);
			addEventListener(MouseEvent.MOUSE_OUT, mouseOverOut);
			addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			addEventListener(MouseEvent.MOUSE_UP, mouseUp);
			addEventListener(MouseEvent.RELEASE_OUTSIDE, releaseOutside);
			draw();
			
		}
		
		private function removedFromStage(e:Event):void 
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, removedFromStage);
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			removeEventListener(MouseEvent.MOUSE_OVER, mouseOverOut);
			removeEventListener(MouseEvent.MOUSE_OUT, mouseOverOut);
			removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
			removeEventListener(MouseEvent.RELEASE_OUTSIDE, releaseOutside);
			
		}
		
		
		
		private function releaseOutside(e:MouseEvent):void 
		{
			if (!toggle) selected = false;
		}
		
		private function mouseDown(e:MouseEvent):void 
		{
			selected = toggle? !selected : true;
		}
		
		private function mouseUp(e:MouseEvent):void 
		{
			dispatchEvent(new UIEvent(selected?UIEvent.BUTTON_SELECTED:UIEvent.BUTTON_DESELECTED));
			
			if (!toggle) selected = false;
			
		}
		
		private function mouseOverOut(e:MouseEvent):void 
		{
			var over:Boolean = (e.type == MouseEvent.MOUSE_OVER);
			TweenMax.to(this, .3, { colorMatrixFilter: { brightness:over?1.2:1, remove:over?false:true }} );
		}
		
		override public function clean():void
		{
			super.clean();
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
		}
		
		
		//GETTER SETTER
		public function get label():String 
		{
			return _label;
		}
		
		public function set label(value:String):void 
		{
			_label = value;
			tf.text = label;
			draw();
		}
		
		public function get toggle():Boolean 
		{
			return _toggle;
		}
		
		public function set toggle(value:Boolean):void 
		{
			_toggle = value;
		}
		
		
		
		public function get selected():Boolean 
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void 
		{
			_selected = value;
			draw();
		}
		
		//overrides
		override public function set width(value:Number):void
		{
			buttonWidth = value;
			draw();
			drawMask();
		}
		
		override public function set height(value:Number):void
		{
			buttonHeight = value;
			draw();
			drawMask();
		}
		
		public function get roundCorners():String 
		{
			return _roundCorners;
		}
		
		public function set roundCorners(value:String):void 
		{
			_roundCorners = value;
			draw();
		}
		
		public function get transparentBG():Boolean 
		{
			return _transparentBG;
		}
		
		public function set transparentBG(value:Boolean):void 
		{
			_transparentBG = value;
			bg.visible = !value;
		}
		
	}

}