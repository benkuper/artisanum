package ui
{
	import com.greensock.TweenLite;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import ui.fonts.Fonts;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Tooltip extends Sprite 
	{
		static public const INFO:String = "info";
		static public const WARNING:String = "warning";
		static public const ERROR:String = "error";
		
		static public var instance:Tooltip;
		
		public static var targets:Array;
		
		private var tf:TextField;
		
		public var parentWindow:DisplayObjectContainer;
		
		public static var enabled:Boolean;
		
		public function Tooltip(parentWindow:DisplayObjectContainer) 
		{
			
			
			this.parentWindow = parentWindow;
			
			tf = Fonts.createTF("tooltip",Fonts.normalTF);
			tf.x = 3;
			tf.y = 3;
			tf.selectable = false;
			addChild(tf);
			
			filters = [new DropShadowFilter(2, 45, 0, 1, 6, 6, .5)];
		}
		
		public static function init(parent:DisplayObjectContainer):void
		{
			
			if (instance != null)  return;
			enabled = true;
			
			instance = new Tooltip(parent);
			targets = new Array();
		}
		
		public static function show(text:String, type:String = INFO,offset:Point=null):void
		{
			
			if (instance == null)
			{
				trace("Must Tooltip.init() first !");
				return;
			}
			
			if (!enabled) {
				trace("Tooltip is not enabled !");
				return;
			}
			
			instance.showText(text, type,offset);
		}
		
		
		private function showText(text:String, type:String, offset:Point):void 
		{
			if(!parentWindow.contains(this)) parentWindow.addChild(this);
			
			tf.text = text;
			render(type);
			
			if (offset == null) offset = new Point(10,-tf.textHeight);
			
			//this.x = parentWindow.mouseX + offset.x;
			//this.y = parentWindow.mouseY + offset.y;
			this.x = parentWindow.mouseX - this.width / 2;
			this.y = parentWindow.mouseY - this.height - 10;
			
			stage.addEventListener(Event.ENTER_FRAME, mouseEnterFrame);
			
		}
		
		private function render(type:String):void 
		{
			graphics.clear();
			
			var c:uint;
			switch(type)
			{
				case INFO:
					c = 0x005279;
					break;
					
				case WARNING:
					c = 0xff7733;
					break;
					
				case ERROR:
					c = 0xff3322;
					break;
			}
			
			graphics.beginFill(c);
			graphics.drawRoundRect(0, 0, tf.textWidth + 10, tf.textHeight + 10, 3, 3);
			graphics.endFill();
			
		}
		
		public static function hide():void
		{
			instance.hideWindow();
		}
		
		private function hideWindow():void 
		{
			if (this.parent == parentWindow)
			{
				parentWindow.removeChild(this);
			}
			
		}
		
		//handlers
		
		public function mouseHandler(e:MouseEvent):void
		{
			TweenLite.killDelayedCallsTo(show);
			
			switch(e.type)
			{
				case MouseEvent.MOUSE_OVER:
					var props:Object = getTargetProps(e.currentTarget as DisplayObject);
					if (props.delay == 0)
					{
						show(props.text, props.type);
					}else
					{
						TweenLite.delayedCall(props.delay, show, [props.text, props.type]);
					}
					
					e.currentTarget.addEventListener(MouseEvent.MOUSE_OUT,mouseHandler);
					
					break;
				
				case MouseEvent.MOUSE_OUT:
					e.currentTarget.removeEventListener(MouseEvent.MOUSE_OUT,mouseHandler);
					if(stage != null) stage.removeEventListener(Event.ENTER_FRAME, mouseEnterFrame);
					hide();
					break;
			}
		}
		
		private function mouseEnterFrame(e:Event):void
		{
			this.x = parentWindow.mouseX - this.width / 2;
			this.y = parentWindow.mouseY - this.height - 10;
		}
		
		public static function addTarget(target:DisplayObject,text:String,type:String = INFO,delay:Number = .5):void
		{
			for each(var t:Object in targets)
			{
				if (t.target == target) {
					trace("target already added !, removing");
					removeTarget(target);
				}
			}
			
			targets.push( { target:target, text:text,type:type,delay:delay } );
			target.addEventListener(MouseEvent.MOUSE_OVER,instance.mouseHandler);
		}
		
		public static function removeTarget(target:DisplayObject):void
		{
			targets.splice(targets.indexOf(target), 1);
		}
		
		public static function getTargetProps(target:DisplayObject):Object
		{
			for each(var t:Object in targets)
			{
				if (t.target == target) {
					return t;
				}
			}
			
			return "[Target not found]";
		}
	}

}