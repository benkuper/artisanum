package  ui
{
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.ui.Keyboard;
	import ui.components.Button;
	import ui.components.UIEvent;
	import ui.fonts.Fonts;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Alert extends Sprite 
	{
		
		private var bg:Shape;
		private var textTF:TextField;
		private var window:Sprite;
		private var okButton:Button;
		private var cancelButton:Button;
		
		private static var instance:Alert;
		static private var okCallback:Function;
		static private var cancelCallback:Function;
		
		public function Alert() 
		{
			bg = new Shape();
			addChild(bg);
			
			window = new Sprite();
			addChild(window);
			okButton = new Button("OK");
			window.addChild(okButton);
			okButton.width = 80;
			okButton.height = 30;
			okButton.label = "OK";
			
			cancelButton = new Button("Annuler");
			window.addChild(cancelButton);
			cancelButton.width = 80;
			cancelButton.height = 30;
			cancelButton.label = "Annuler";
			
			
			okButton.addEventListener(UIEvent.BUTTON_SELECTED, okClick);
			cancelButton.addEventListener(UIEvent.BUTTON_SELECTED, cancelClick);
			
			
			textTF = Fonts.createTF("", Fonts.bigTF);
			window.addChild(textTF);
		}
		
		
		
		private function okClick(e:UIEvent = null):void 
		{
			
			close();
			if(okCallback != null) okCallback();
			
		}
		
		private function cancelClick(e:UIEvent = null):void 
		{
			close();
			if(cancelCallback != null) cancelCallback();
		}
		
		private function close():void
		{
			TweenLite.to(this, .3, { alpha:0, onComplete:clean } );
		}
		
		public function clean():void
		{
			if (parent == null) return; // TODO : clean this hack
			parent.removeEventListener(KeyboardEvent.KEY_DOWN, keyDowHandler);
			parent.removeChild(this);
		}
		
		public static function show(parent:DisplayObjectContainer,text:String = "",showCancel:Boolean = true,okCallback:Function = null,cancelCallback:Function = null):void
		{
			
			if (instance == null) instance = new Alert();
			
			Alert.cancelCallback = cancelCallback;
			Alert.okCallback = okCallback;
			
			parent.addChild(instance);
			parent.addEventListener(KeyboardEvent.KEY_DOWN, keyDowHandler);

			instance.bg.graphics.clear();
			instance.bg.graphics.beginFill(0, .4);
			instance.bg.graphics.drawRect(0, 0, parent.width, parent.height);
			instance.bg.graphics.endFill();
			
			
			instance.textTF.text = text;
			var windowWidth:Number = Math.max(instance.textTF.textWidth + 40, 300);
			
			instance.textTF.x = windowWidth / 2 - instance.textTF.textWidth / 2;
			
			
			instance.textTF.y = 20;
			
			instance.window.graphics.clear();
			instance.window.graphics.beginFill(0x222222);
			instance.window.graphics.drawRect(0, 0,windowWidth , instance.textTF.textHeight + 80);
			instance.window.graphics.endFill();
			
			instance.window.x = (Math.min(instance.parent.width,instance.stage.stageWidth) - instance.window.width) / 2;
			instance.window.y = (Math.min(instance.parent.height,instance.stage.stageHeight) - instance.window.height) / 2;
			
			
			if (showCancel)
			{
				instance.cancelButton.visible = true;
				instance.okButton.x = instance.window.width / 2 - instance.okButton.width - 20;
				instance.cancelButton.x = instance.window.width / 2 + 20;
				instance.cancelButton.y = instance.window.height - instance.cancelButton.height - 10;
			}else
			{
				instance.cancelButton.visible = false;
				instance.okButton.x = (instance.window.width - instance.okButton.width);
			}
			
			instance.okButton.y = instance.window.height - instance.okButton.height - 10;
			
			instance.alpha = 1;
			TweenLite.fromTo(instance.bg, .4, { alpha:0 }, { alpha:1, ease:Strong.easeOut } );
			TweenLite.fromTo(instance.window, .8, { alpha:0 }, { alpha:1, ease:Strong.easeOut } );
			
		}
		
		static private function keyDowHandler(e:KeyboardEvent):void 
		{
			switch(e.keyCode)
			{
				case Keyboard.ENTER:
				case Keyboard.NUMPAD_ENTER:
					instance.okClick();
					break;
					
				case Keyboard.ESCAPE:
					instance.cancelClick();
					break;
			}
		}

		
	}

}