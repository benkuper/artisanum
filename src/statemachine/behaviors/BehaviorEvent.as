package statemachine.behaviors 
{
	import statemachine.behaviors.actions.Action;
	import flash.events.Event;
	import statemachine.behaviors.mappings.Mapping;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class BehaviorEvent extends Event 
	{
		static public const ELEMENT_ADDED:String = "elementAdded";
		static public const ELEMENT_REMOVED:String = "elementRemoved";
		
		static public const BEHAVIOR_ADDED:String = "behaviorAdded";
		static public const BEHAVIOR_REMOVED:String = "behaviorRemoved";
		
		static public const LABEL_CHANGED:String = "labelChanged";
		
		static public const BEHAVIOR_ACTIVATED:String = "behaviorActivated";
		static public const BEHAVIOR_DEACTIVATED:String = "behaviorDeactivated";
		
		static public const BEHAVIOR_SELECTED:String = "behaviorSelected";
		
		//for state machine
		static public const ACTION_SELECTED:String = "actionSelected";
		static public const ACTION_DESELECTED:String = "actionDeselected";
		
		static public const MAPPING_SELECTED:String = "mappingSelected";
		static public const MAPPING_DESELECTED:String = "mappingDeselected";
		
		static public const CREATE_TRANSITION:String = "createTransition";
		static public const TRANSITION_CREATED:String = "transitionCreated";
		static public const TRANSITION_CREATION_CANCELED:String = "transitionCreationCanceled";
		
		
		public var action:Action;
		public var mapping:Mapping;
		
		
		public function BehaviorEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new BehaviorEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("BehaviorEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}