package statemachine.behaviors.modules {
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.getTimer;
	import flash.utils.Timer;
	import modules.input.InputModule;
	import modules.input.measure.ContinuousMeasure;
	import modules.ModuleType;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class BehaviorInputModule extends InputModule
	{
		
		public var timeAtStart:Number;
		public var timeSinceStart:Number;
		public var relativeTimeMeasure:ContinuousMeasure;
		public var secondsMeasure:ContinuousMeasure;
		
		//public var secondsTimer:Timer;
		
		public function BehaviorInputModule()
		{
			super(false);
			
			
			//relativeTimeMeasure = addMeasure(new ContinuousMeasure("[X] Temps relatif", 0, 40, 0)) as ContinuousMeasure; //must create a specific measure to trigger getTimer() each time the value is asked
			secondsMeasure = addMeasure(new ContinuousMeasure("secondes","Secondes", 0, 3600, 0)) as ContinuousMeasure;
			
			//secondsTimer = new Timer(1000, 0);
			//secondsTimer.addEventListener(TimerEvent.TIMER, secondsTimerTick);
			
			
			this.id = "thisBehavior"; //maybe move to a better place ?
		}
		
		private function this_enterFrame(e:Event):void 
		{
			var time:Number = (getTimer() / 1000 - timeAtStart);
			secondsMeasure.currentValue = Number(time.toFixed(3));
		}
		
		
		public function startTime():void
		{
			this.addEventListener(Event.ENTER_FRAME, this_enterFrame);
			timeAtStart = getTimer() / 1000;
			//secondsTimer.reset();
			//secondsTimer.start();
			secondsMeasure.currentValue = 0;
		}
		
		public function stop():void 
		{
			secondsMeasure.currentValue = 0;
			this.removeEventListener(Event.ENTER_FRAME, this_enterFrame);
			//secondsTimer.stop();
		}
		
		//handlers
		
		private function secondsTimerTick(e:TimerEvent):void 
		{
			//secondsMeasure.currentValue = secondsTimer.currentCount;
		}
		
		
		override public function clean():void
		{
			super.clean();
			stop();
			//secondsTimer.removeEventListener(TimerEvent.TIMER, secondsTimerTick);
		}
		
		
	}

}