package statemachine.behaviors.mappings 
{
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import ui.components.Button;
	import ui.components.Panel;
	import ui.components.UIEvent;
	import ui.Editor;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class MappingEditor extends Editor 
	{
		
		private var _opened:Boolean;
		
		private var _currentMapping:Mapping;
		private var closeBT:Button;
		
		
		public function MappingEditor()
		{
			super("Editeur de mapping");
			
			closeBT = new Button("-");
			addChild(closeBT);
			closeBT.addEventListener(UIEvent.BUTTON_SELECTED, closeBTSelected);
			closeBT.width = 30;
			closeBT.height = 15;
			
			baseWidth = 500;
			baseHeight = 200;
			
			opened = false;
			draw();
			
			this.y = -this.height;
			
			addEventListener(MappingEvent.MAPPING_COMMAND_LOADED, mappingCommandLoadedHandler);
		}
		
		
		
		override protected function draw():void
		{			
			
			closeBT.x = baseWidth - closeBT.width - 10;
			closeBT.y = 5;
			
			if (currentMapping != null)
			{
				currentMapping.x = 10;
				currentMapping.y = 10;
				currentMapping.width = baseWidth -20;
				
			}
			
			baseHeight = mainContainer.y + mainContainer.height + 20;
			super.draw();
		}
		
		
		private function loadCurrentMapping():void
		{
			mainContainer.removeChildren();
			
			if (currentMapping != null) 
			{
				mainContainer.addChild(currentMapping);
				
				title = "Editeur de mapping : " + currentMapping.mappingName;
			}
			
			
			draw();
		}
		
		public function finishClose():void 
		{
			visible = false;
		}
		
		//handlers
		
		
		private function closeBTSelected(e:UIEvent):void 
		{
			currentMapping = null;
		}
		
		private function mappingCommandLoadedHandler(e:MappingEvent):void 
		{
			draw();
		}
		
		
		//getter / setter
		
		public function get currentMapping():Mapping 
		{
			return _currentMapping;
		}
		
		public function set currentMapping(value:Mapping):void 
		{
			if (value == currentMapping) return;
			
			if (currentMapping != null)
			{
				if (mainContainer.contains(currentMapping)) mainContainer.removeChild(currentMapping);
				currentMapping.preview.editing = false;
			}
			
			_currentMapping = value;
			
			if (currentMapping != null)
			{
				currentMapping.preview.editing = true;
				opened = true;
				loadCurrentMapping();
			}else
			{
				opened = false;
			}
		}
		
		public function get opened():Boolean 
		{
			return _opened;
		}
		
		public function set opened(value:Boolean):void 
		{
			if (opened == value) return;
			
			_opened = value;
			
			if (value)
			{
				visible = true;
				TweenLite.to(this, .5, {y:0, ease:Strong.easeOut, onUpdate:draw } );
				//if (dispatchOpen) dispatchEvent(new MappingEvent(MappingEvent.MAPPING_EDITOR_OPEN));
			}else
			{
				TweenLite.to(this, .5, {y:-height,ease:Strong.easeOut, onComplete:finishClose, onUpdate:draw } );
				//if(dispatchOpen) dispatchEvent(new MappingEvent(MappingEvent.MAPPING_EDITOR_CLOSE));
			}
			
			
			
		}
		
		
		
	}

}