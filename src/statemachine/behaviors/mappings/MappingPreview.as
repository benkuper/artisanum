package statemachine.behaviors.mappings 
{
	import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import ui.Alert;
	import ui.components.Button;
	import ui.components.StatusBox;
	import ui.components.TextBox;
	import ui.components.UIEvent;
	import ui.menu.Menu;
	import ui.Style;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class MappingPreview extends Sprite 
	{
		//preview (behavior panel)
		//layout
		public var previewWidth:Number;
		public var previewHeight:Number;
		
		//data
		private var mapping:Mapping;
		private var _active:Boolean;
		private var _editing:Boolean;
		
		//ui
		private var previewTextBox:TextBox;
		//private var editBT:Button;
		private var removeBT:Button;
		private var statusBox:StatusBox;
		private var renameMenuItem:NativeMenuItem;
		private var enableBT:Button;
		
		
		public function MappingPreview(mapping:Mapping) 
		{
			this.mapping = mapping;
			
			previewTextBox = new TextBox("", mapping.mappingName);
			previewTextBox.y = -4;
			//previewTextBox.editable = true;
			previewTextBox.transparentBG = true;
			previewTextBox.addEventListener(UIEvent.EDITING_START, previewEditingStart);
			previewTextBox.addEventListener(UIEvent.EDITING_FINISH, previewEditingFinish);
			addChild(previewTextBox);
			
			
			removeBT = new Button("x");
			addChild(removeBT);
			removeBT.width = 30;
			removeBT.height = 20;
			removeBT.addEventListener(UIEvent.BUTTON_SELECTED, removeBTSelected);
			
			
			enableBT = new Button(">", true);
			enableBT.selectedColor = Style.BLUE;
			enableBT.selected = true;
			addChild(enableBT);
			enableBT.width = 25;
			enableBT.height = 20;
			enableBT.addEventListener(UIEvent.BUTTON_SELECTED, enableBTSelected);
			enableBT.addEventListener(UIEvent.BUTTON_DESELECTED, enableBTSelected);
			
			
			statusBox = new StatusBox();
			addChild(statusBox);
			
			
			previewWidth  = 50;
			previewHeight = 20;
			
			addEventListener(MouseEvent.CLICK, clickHandler);
			
			contextMenu = new NativeMenu();
			renameMenuItem = new NativeMenuItem("Renommer le mapping");
			contextMenu.addItem(renameMenuItem);
			contextMenu.addEventListener(Event.SELECT, contextMenuSelect);
			
		}
		
		private function draw():void
		{
			graphics.clear();
			graphics.beginFill(Style.UI_PANEL, .1);
			graphics.drawRect(0, 0, previewWidth, previewHeight);
			graphics.endFill();
			
			removeBT.x = previewWidth - removeBT.width - 10;
			removeBT.y = 2;
			removeBT.height = previewHeight -5;
			
			enableBT.x = removeBT.x - enableBT.width - 10;
			enableBT.y = 2;
			enableBT.height = previewHeight -5;
			
			
			statusBox.x = enableBT.x - statusBox.width - 5;
			statusBox.y = (previewHeight - statusBox.height) / 2+2;
			
			if (editing)
			{
				graphics.beginFill(Style.BLUE, .3);
				graphics.drawRoundRect(-5, 0, previewWidth+10, previewHeight, 5, 5);
				graphics.endFill();
			}
		}
		
		
		//handlers
		private function clickHandler(e:MouseEvent):void 
		{
			//trace("click handler");
			if(!editing) dispatchEvent(new MappingEvent(MappingEvent.MAPPING_CLICK,mapping));
		}
		
		
		private function removeBTSelected(e:UIEvent):void 
		{
			Alert.show(stage, "Tu veux vraiment supprimer ce mapping génial ?", true, confirmRemoveMapping);
		}
		
		private function enableBTSelected(e:UIEvent):void 
		{
			mapping.enabled = enableBT.selected;
		}
		
		public function confirmRemoveMapping():void 
		{
			dispatchEvent(new MappingEvent(MappingEvent.REMOVE_MAPPING,mapping));
			editing = false;
		}
		
		private function contextMenuSelect(e:Event):void 
		{
			var item:NativeMenuItem = e.target as NativeMenuItem;
			switch(item)
			{
				case renameMenuItem:
					previewTextBox.transparentBG = false;
					previewTextBox.editable = true;
					previewTextBox.setFocus();
					previewTextBox.selectText();
					break;
			}
		}
		
		private function previewEditingFinish(e:UIEvent):void 
		{
			//trace("editing finish");
			previewTextBox.transparentBG = true;
			mapping.mappingName = previewTextBox.text;
			previewTextBox.editable = false;
		}
		
		private function previewEditingStart(e:UIEvent):void 
		{
			previewTextBox.transparentBG = false;
		}
		
		//Mapping-data related
		public function updateMappingName():void 
		{
			previewTextBox.text = mapping.mappingName;
		}
		
		//getter / setter
		
		override public function set width(value:Number):void
		{
			previewWidth = value;
			draw();
		}
		
		override public function set height(value:Number):void
		{
			previewHeight = value;
			draw();
		}
		
		public function get editing():Boolean 
		{
			return _editing;
		}
		
		public function set editing(value:Boolean):void 
		{
			if (editing == value) return;
			_editing = value;
			draw();
			
			Menu.setEnabled("removeMapping", value);
			Menu.setEnabled("duplicateMapping", value);
			
			if(!editing) dispatchEvent(new MappingEvent(MappingEvent.MAPPING_CLICK));
		}
		
		public function get active():Boolean 
		{
			return _active;
		}
		
		public function set active(value:Boolean):void 
		{
			_active = value;
			statusBox.status = value?StatusBox.OK:StatusBox.NORMAL;
			//trace(statusBox.status);
			draw();
		}
	}

}