package statemachine.behaviors.mappings 
{
	import benkuper.util.KeyboardUtil;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import statemachine.behaviors.BehaviorEvent;
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import flash.display.Sprite;
	import ui.components.Button;
	import ui.components.Panel;
	import ui.components.UIEvent;
	import ui.Style;
	import ui.Toaster;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class MappingPanel extends Panel
	{		
		
		//ui
		private var addBT:Button;
		
		//data
		private var mappingsList:Vector.<Mapping>;
		
		private var _running:Boolean;
		
		public function MappingPanel(isSpecial:Boolean = false) 
		{
			super("Mappings");
			titleTF.textColor = Style.BLUE
			
			headerHeight = 30;
			baseHeight = 30;
			
			//_transparent = true;
			_borders = true;
			
			mappingsList = new Vector.<Mapping>;
			
			addBT = new Button("Ajouter");
			addBT.width = 50;
			addBT.height = 20;
			mainContainer.addChild(addBT);
			addBT.addEventListener(UIEvent.BUTTON_SELECTED, addBTHandler);
			
			addEventListener(MappingEvent.REMOVE_MAPPING,mappingRemoveHandler);
			
			filters = [];
		}
		
		
		//ui
		
		override protected function draw():void
		{
			placeElements();
			
			super.draw();
			
			//graphics.clear();
			//graphics.lineStyle(2, Style.UI_NORMAL);
			//graphics.moveTo(5, headerHeight-10);
			//graphics.lineTo(baseWidth - 5, headerHeight-10);
			//graphics.drawRect(5, headerHeight - 10, baseWidth - 10, baseHeight);
			
		}
		
		private function placeElements():void 
		{
			var curY:Number = 0;
			for (var i:int = 0; i < mappingsList.length; i++)
			{
				mappingsList[i].preview.y = curY;
				curY += mappingsList[i].preview.height + 5;
				
			}
			
			curY += 5;
			
			addBT.x = (baseWidth - addBT.width) / 2;
			addBT.y = curY;
			
			curY += addBT.height;
			
			var prevBH:Number = baseHeight;
			baseHeight = headerHeight + curY + 5;
			if(prevBH != baseHeight) dispatchEvent(new UIEvent(UIEvent.PANEL_RESIZED));
		}
		
		
		public function addMapping(sourceMapping:Mapping = null):Mapping 
		{
			var m:Mapping = new Mapping();
			mainContainer.addChild(m.preview);
			mappingsList.push(m);
			m.mappingName = "Mapping " + mappingsList.length;
			
			m.preview.x = 10;
			
			m.preview.width = baseWidth - 20;
			m.preview.height = 20;
			
			
			m.running = running;
			
			TweenLite.fromTo(m.preview, .5, { scaleY:0 , alpha:0}, { alpha:1, scaleY:1, ease:Strong.easeOut, onUpdate:draw } );
			
			dispatchEvent(new BehaviorEvent(BehaviorEvent.ELEMENT_ADDED));
			dispatchEvent(new MappingEvent(MappingEvent.MAPPING_ADDED,m));
			
			KeyboardUtil.addTarget(m.preview,mappingKeyDown);
			
			return m;
		}
		
		private function mappingKeyDown(e:KeyboardEvent):void 
		{
			var m:Mapping;
			
			if (e.type != KeyboardEvent.KEY_DOWN) return;
			switch(e.keyCode)
			{
				case Keyboard.D:
					if (e.ctrlKey)
					{
						m = getEditingMapping();
						if (m != null) addMapping(m);
					}
					
					break;
					
				case Keyboard.UP:
				case Keyboard.DOWN:
					if (e.ctrlKey)
					{
						m = getEditingMapping();
						moveMapping(m, (e.keyCode == Keyboard.UP)?"up":"down");
					}
					break;
			}
		}
		
		public function removeMapping(m:Mapping = null,animate:Boolean = true):void
		{
			if (m == null) m = getEditingMapping();
			if (m == null) return;
			
			if (animate) 
			{
				TweenLite.to(m.preview, .5,  { scaleY:0, alpha:0, ease:Strong.easeOut, onUpdate:draw, onComplete:finishRemoveMapping,onCompleteParams:[m] } );
			}else
			{
				finishRemoveMapping(m);
			}
			
			KeyboardUtil.removeTarget(m.preview, mappingKeyDown);
			
		}
		
		public function duplicateEditingMapping():void 
		{
			
			addMapping();
		}
		
		public function finishRemoveMapping(m:Mapping ):void 
		{
			
			m.clean();
			mainContainer.removeChild(m.preview);
			mappingsList.splice(mappingsList.indexOf(m), 1);
			
			Toaster.info("Mapping supprimé :" + m.mappingName);
			
			dispatchEvent(new BehaviorEvent(BehaviorEvent.ELEMENT_REMOVED));
			dispatchEvent(new MappingEvent(MappingEvent.MAPPING_REMOVED,m));
			
			draw();
		}
		
		public function moveMapping(m:Mapping = null, direction:String = "up"):void 
		{
			if (m == null) m = getEditingMapping();
			if (m == null) return;
			
			var mIndex:int = mappingsList.indexOf(m);
			var tIndex:int = (direction == "up")?mIndex - 1:mIndex + 1;
			mappingsList.splice(mIndex, 1);
			mappingsList.splice(tIndex, 0, m);
			
			placeElements();
		}
		
		private function clearMappings():void 
		{
			while (mappingsList.length > 0)
			{
				removeMapping(mappingsList[0], false);
			}
		}
		
		private function mappingRemoveHandler(e:MappingEvent):void 
		{
			removeMapping(e.mapping);
		}
		
		//util
		private function getEditingMapping():Mapping
		{
			for each(var m:Mapping in mappingsList)
			{
				if (m.preview.editing) return m;
			}
			
			return null;
		}
		
		
		//cleaning
		override public function clean():void
		{
			super.clean();
			
			clearMappings();
			
			addBT.removeEventListener(UIEvent.BUTTON_SELECTED, addBTHandler);
			
			removeEventListener(MappingEvent.REMOVE_MAPPING,mappingRemoveHandler);
		}
		
		//handlers
		
		private function addBTHandler(e:UIEvent):void 
		{
			addMapping();
		}		
		

		//Project XML saving
		public function getXML(baseXML:XML):XML 
		{
			var xml:XML = baseXML;
			
			for each(var m:Mapping in mappingsList)
			{
				xml.appendChild(m.getXML());
			}
			
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			clearMappings();
			for each(var mXML:XML in data.mapping)
			{
				var m:Mapping = addMapping();
				m.loadXML(mXML);
			}
		}
		
		
		
		public function get running():Boolean 
		{
			return _running;
		}
		
		public function set running(value:Boolean):void 
		{
			_running = value;
			
			for each(var m:Mapping in mappingsList)
			{
				m.running = value;
			}
		}
		
		
	}

}