package statemachine.behaviors.mappings  
{
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import modules.events.MeasureEvent;
	import modules.input.InputModule;
	import modules.input.measure.ContinuousMeasure;
	import modules.input.measure.Measure;
	import modules.Module;
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.CommandType;
	import modules.output.OutputModule;
	import ui.components.DropDown;
	import ui.components.Slider;
	import ui.components.UIEvent;
	import ui.Style;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Mapping extends Sprite
	{		
		public static var instances:Vector.<Mapping>;
		public static var inputOptions:Array;
		public static var outputOptions:Array;
		
		//data-logic
		private var _mappingName:String;
		private var _enabled:Boolean;
		public var running:Boolean;
		
		
		//ui
		private var _preview:MappingPreview;
		
		private var inputModuleBox:DropDown;
		private var inputMeasureBox:DropDown;
		
		private var outputModuleBox:DropDown;
		private var commandBox:DropDown;
		
		private var inputModule:InputModule;
		private var _inputMeasure:Measure;
		
		private var outputModule:OutputModule;
		private var _outputCommand:Command;
		
		private var normalizedValueFeedback:Slider;
		
		private var link:Sprite;
		
		private var baseWidth:Number;
		
		public function Mapping() 
		{
			if (instances == null) instances = new Vector.<Mapping>;
			instances.push(this);
			
			if (inputOptions == null)
				updateAllInputOptions(false);
				
			if (outputOptions == null)
				updateAllOutputOptions(false);
			
			_mappingName = "Nouveau mapping";
			
			_enabled = true;
			
			_preview = new MappingPreview(this);
			
			normalizedValueFeedback = new Slider("Valeur",0,0,1);
			normalizedValueFeedback.readOnly = true;
			addChild(normalizedValueFeedback);
			//normalizedValueFeedback.showRange = true;
			
			inputModuleBox = new DropDown(inputOptions, "[Entrée]");
			inputModuleBox.height = 20;
			inputModuleBox.setOptionsLayout(2, 200, 20);
			addChild(inputModuleBox);
			inputModuleBox.addEventListener(UIEvent.DROPDOWN_CHANGE, inputModuleBoxChanged);
			
			inputMeasureBox = new DropDown(null, "[Mesure]");
			inputMeasureBox.enabled = false;
			inputMeasureBox.height = 20;
			inputMeasureBox.setOptionsLayout(2, 200, 20);
			addChild(inputMeasureBox);
			inputMeasureBox.addEventListener(UIEvent.DROPDOWN_CHANGE, inputMeasureBoxChanged);
			
			outputModuleBox = new DropDown(outputOptions, "[Sortie]");
			outputModuleBox.height = 20;
			outputModuleBox.setOptionsLayout(2, 200, 20);
			addChild(outputModuleBox);
			outputModuleBox.addEventListener(UIEvent.DROPDOWN_CHANGE, outputModuleBoxChanged);
			
			commandBox = new DropDown(null, "[Commande]");
			commandBox.height = 20;
			commandBox.enabled = false;
			commandBox.setOptionsLayout(2, 200, 20);
			addChild(commandBox);
			commandBox.addEventListener(UIEvent.DROPDOWN_CHANGE, commandBoxChanged);
			
			
			link =  new Sprite();
			link.graphics.beginFill(Style.UI_NORMAL);
			link.graphics.drawRect(0, 5, 30, 10);
			link.graphics.moveTo(30, 0);
			link.graphics.lineTo(50, 10);
			link.graphics.lineTo(30, 20);
			link.graphics.lineTo(30, 0);
			addChild(link);
			
			normalizedValueFeedback.x = 10;
			normalizedValueFeedback.y = 35;
			normalizedValueFeedback.width = 200;
			
			
			addEventListener(UIEvent.DROPDOWN_OPEN, dropdownOpenHandler);
			
			draw();
		}	
		
		private function dropdownOpenHandler(e:UIEvent):void 
		{
			for each(var d:DropDown in [inputModuleBox, inputMeasureBox, outputModuleBox, commandBox])
			{
				if (d != e.target)
				{
					d.opened = false;
				}
			}
		}
		
		
		//ui
		private function draw():void
		{
			inputModuleBox.x = 0;
			inputMeasureBox.x = inputModuleBox.x + inputModuleBox.width + 5;
			
			link.x = (baseWidth - link.width) / 2;
			
			
			commandBox.x = baseWidth - commandBox.width;
			outputModuleBox.x = commandBox.x - outputModuleBox.width - 5;
			
			if (outputCommand != null)
			{
				outputCommand.gui.x = baseWidth/2 + 40;
				outputCommand.gui.y = commandBox.y + commandBox.height + 10;
				outputCommand.guiWidth = baseWidth / 2 - 20;
			}
			
			
		}
		
		//handlers
		private function inputModuleBoxChanged(e:UIEvent):void 
		{
			//trace("InputModule Box changed, selected Index :" + inputModuleBox.selectedIndex);
			
			if (inputModuleBox.selectedOption != null)
			{
				inputModule = inputModuleBox.selectedOption.value as InputModule;
				inputMeasureBox.setOptions(inputModule.getMeasureList());
				inputMeasureBox.enabled = true;
			}
			else
			{
				
				inputModule = null;
				inputMeasureBox.setOptions(null);
				inputMeasureBox.enabled = false;
			}
		}
		
		private function inputMeasureBoxChanged(e:UIEvent):void 
		{
			//trace("input measure box changed");
			if (inputMeasureBox.selectedOption != null)
			{
				inputMeasure = inputMeasureBox.selectedOption.value as Measure;
				
				if (inputMeasure is ContinuousMeasure)
				{
					normalizedValueFeedback.min = (inputMeasure as ContinuousMeasure).minimumValue;
					normalizedValueFeedback.max = (inputMeasure as ContinuousMeasure).maximumValue;
				}
				
			}
			else
			{
				inputMeasure = null;
			}
		}
		
		private function outputModuleBoxChanged(e:UIEvent):void 
		{
			if (outputModuleBox.selectedOption != null) 
			{
				outputModule = outputModuleBox.selectedOption.value as OutputModule;
				commandBox.setOptions(outputModule.getCommandsList(CommandType.MAPPING));
				commandBox.enabled = true;
			}else
			{
				
				outputModule = null;
				commandBox.setOptions(null);
				commandBox.enabled = false;
			}
		}
		
		
		//measure
		private function inputMeasureValueChangeHandler(e:MeasureEvent):void 
		{
			if (!running) return;
			if (!enabled) return;
			
			if (inputMeasure is ContinuousMeasure)
			{
				var cMeasure:ContinuousMeasure = inputMeasure as ContinuousMeasure;
				var normalizedValue:Number = (cMeasure.currentValue - cMeasure.minimumValue) / (cMeasure.maximumValue-cMeasure.minimumValue); //to put in ContinuousMeasure
				
				normalizedValueFeedback.value = cMeasure.currentValue;
				
				if (outputCommand != null)
				{
					outputCommand.setValue(normalizedValue);
				}
			}
		}
		
		private function commandBoxChanged(e:UIEvent):void 
		{			
			if (commandBox.selectedOption != null) 
			{
				var commandType:CommandType = commandBox.selectedOption.value as CommandType;
				outputCommand = commandType.createCommand(CommandGUIModes.EDITOR_MAPPING);
			}else
			{
				
				outputCommand = null;
			}
		}
		
		//external update
		public function updateInputOptions():void
		{
			inputModuleBox.setOptions(inputOptions);
		}
		public function updateOutputOptions():void
		{
			outputModuleBox.setOptions(outputOptions);
		}
		
		
		//static
		public static function updateAllInputOptions(updateActions:Boolean = true):void
		{
			inputOptions = InputModule.getModuleList();
			if (updateActions)
			{
				for each (var m:Mapping in instances)
				{
					m.updateInputOptions();
				}
			}
		}
		
		public static function updateAllOutputOptions(updateActions:Boolean = true):void
		{
			outputOptions = OutputModule.getModuleList();
			if (updateActions)
			{
				for each(var m:Mapping in instances)
				{
					m.updateOutputOptions();
				}
			}
		}
		
		
		//clean
		public function clean():void
		{
			instances.splice(instances.indexOf(this), 1);
			inputModule = null;
			inputMeasure = null;
			outputCommand = null;
		}
		
		//Project XML saving
		public function getXML():XML 
		{
			var xml:XML = <mapping></mapping>;
			
			xml.@name = mappingName;
			if (inputModule != null) 
			{
				xml.@moduleId = inputModule.id;
				//xml.@moduleIndex = inputModuleBox.selectedIndex;
			}
			
			if (inputMeasure != null) 
			{
				xml.@measureId = inputMeasure.id
				//xml.@measureIndex = inputMeasureBox.selectedIndex;
			}
			
			
			if (outputModule != null) 
			{
				xml.@outputModuleId = outputModule.id;
				//xml.@outputModuleIndex = outputModuleBox.selectedIndex;
			}
			
			if (outputCommand != null) 
			{
				xml.@outputCommandId = commandBox.selectedOption.id;
				var commandXML:XML = outputCommand.getXML();
				xml.appendChild(commandXML);
			}
			
			return xml;
			
		}
		
		public function loadXML(data:XML):void
		{
			this.mappingName = data.@name;
			
			if ("@moduleId" in data) inputModuleBox.setSelectedOption(data.@moduleId);
			if (inputModule != null && "@measureId" in data) inputMeasureBox.setSelectedOption(data.@measureId);
			if ("@outputModuleId" in data) outputModuleBox.setSelectedOption(data.@outputModuleId);
			if (outputModule != null && "@outputCommandId" in data) commandBox.setSelectedOption(data.@outputCommandId);
			if (outputCommand != null) outputCommand.loadXML(data.command[0]);
			
		}
		
		public function get mappingName():String 
		{
			return _mappingName;
		}
		
		public function set mappingName(value:String):void 
		{
			if (mappingName == value) return;
			
			_mappingName = value;
			preview.updateMappingName();
		}		
		
		public function get preview():MappingPreview 
		{
			return _preview;
		}
		
		public function get enabled():Boolean 
		{
			return _enabled;
		}
		
		public function set enabled(value:Boolean):void 
		{
			_enabled = value;
		}
		
		public function get outputCommand():Command 
		{
			return _outputCommand;
		}
		
		public function set outputCommand(value:Command):void 
		{
			if (value == outputCommand) return;
			
			if (outputCommand != null)
			{
				outputCommand.clean();
				removeChild(outputCommand.gui);
			}
			
			_outputCommand = value;
			
			if (outputCommand != null)
			{
				addChildAt(outputCommand.gui,1);
				
			}
			
			draw();
			
			dispatchEvent(new MappingEvent(MappingEvent.MAPPING_COMMAND_LOADED));
		}
		
		override public function set width(value:Number):void
		{
			baseWidth = value;
			draw();
		}
		
		public function get inputMeasure():Measure 
		{
			return _inputMeasure;
		}
		
		public function set inputMeasure(value:Measure):void 
		{
			if (inputMeasure != null)
			{
				inputMeasure.removeEventListener(MeasureEvent.VALUE_CHANGE, inputMeasureValueChangeHandler);
			}
			_inputMeasure = value;
			
			if (inputMeasure != null)
			{
				inputMeasure.addEventListener(MeasureEvent.VALUE_CHANGE, inputMeasureValueChangeHandler);
			}
		}
		

		
	}
	
	

}