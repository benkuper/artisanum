package statemachine.behaviors.mappings 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class MappingEvent extends Event 
	{
		static public const MAPPING_ADDED:String = "mappingAdded";
		static public const MAPPING_REMOVED:String = "mappingRemoved";
		
		static public const MAPPING_UPDATE:String = "mappingUpdate";
		
		
		static public const MAPPING_EDITOR_OPEN:String = "mappingEditorOpen";
		static public const MAPPING_EDITOR_CLOSE:String = "mappingEditorClose";
		static public const REMOVE_MAPPING:String = "removeMapping";
		static public const MAPPING_CLICK:String = "mappingClick";
		static public const MAPPING_COMMAND_LOADED:String = "mappingCommandLoaded";
		
		
		public var mapping:Mapping;
		
		public function MappingEvent(type:String, mapping:Mapping = null, bubbles:Boolean=true, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			this.mapping = mapping;
			
		} 
		
		public override function clone():Event 
		{ 
			return new MappingEvent(type, mapping, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("MappingEvent", "type", "mapping", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}