package statemachine.behaviors.common 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ConditionEvent extends Event 
	{
		
		static public const CONDITION_ACTIVE:String = "conditionActive";
		static public const CONDITION_INACTIVE:String = "conditionInactive";
		
		public function ConditionEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ConditionEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ConditionEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}