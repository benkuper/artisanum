package statemachine.behaviors.common 
{
	import com.laiyonghao.Uuid;
	import flash.display.Sprite;
	import io.ProjectEvent;
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.CommandType;
	import modules.output.OutputModule;
	import statemachine.behaviors.actions.ActionEvent;
	import ui.components.Button;
	import ui.components.DropDown;
	import ui.components.UIEvent;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Consequence extends Sprite
	{

		//static
		public static var instances:Vector.<Consequence>;
		public static var outputOptions:Array;

		//data
		public var id:String;
		
		//layout
		public var baseWidth:Number;
		
		//ui
		private var outputModuleBox:DropDown;
		private var commandBox:DropDown;
		
		//async data load
		private var dataToLoad:XML;
		
		//data-selection
		private var outputModule:OutputModule;
		private var _outputCommand:Command;
		
		private var sendOnChangeOnly:Button;
		private var removeBT:Button;
		
		
		
		public function Consequence() 
		{
			id = new Uuid().toString();
			
			if (instances == null) instances = new Vector.<Consequence>;
			instances.push(this);
			
			if (outputOptions == null) updateAllOutputOptions(false);
			
			//data init
			sendOnChangeOnly = new Button("=?", true);
			sendOnChangeOnly.selected = false; //force gate filtering so value action needs to pass through an inactive state in order to dispatch an active event again
			
			
			outputModuleBox = new DropDown(outputOptions, "[Sortie]");
			outputModuleBox.height = 20;
			outputModuleBox.setOptionsLayout(2, 200, 20);
			addChild(outputModuleBox);
			outputModuleBox.addEventListener(UIEvent.DROPDOWN_CHANGE, outputModuleBoxChanged);
			
			commandBox = new DropDown(null, "[Action]");
			commandBox.height = 20;
			commandBox.enabled = false;
			commandBox.setOptionsLayout(2, 200, 20);
			addChild(commandBox);
			commandBox.addEventListener(UIEvent.DROPDOWN_CHANGE, commandBoxChanged);
			
			
			removeBT = new Button("x");
			removeBT.width = 15;
			removeBT.height = 15;
			addChild(removeBT);
			removeBT.addEventListener(UIEvent.BUTTON_SELECTED, removeBTSelected);
			
			baseWidth = 300;
			
			draw();
		}
		
		//ui
		private function draw():void 
		{
			outputModuleBox.x = baseWidth/2-outputModuleBox.width - 5;
			outputModuleBox.y = 0;
			
			commandBox.x = baseWidth/2 + 5;
			commandBox.y = outputModuleBox.y;
			
			removeBT.x = baseWidth - removeBT.width;
			removeBT.y = 0;
			
			if (outputCommand != null)
			{
				outputCommand.gui.x = 0;
				outputCommand.gui.y = commandBox.y + commandBox.height + 10;
				outputCommand.guiWidth = baseWidth;
				
			}
		}
		
		
		//Data flow
		public function trigger():void 
		{
			if (outputCommand != null)
			{
				outputCommand.trigger();
			}
		}
		
		//handlers
		private function outputModuleBoxChanged(e:UIEvent):void 
		{
			if (outputModuleBox.selectedOption != null) 
			{
				outputModule = outputModuleBox.selectedOption.value as OutputModule;
				commandBox.setOptions(outputModule.getCommandsList(CommandType.TRIGGER));
				commandBox.enabled = true;
			}else
			{
				
				outputModule = null;
				commandBox.setOptions(null);
				commandBox.enabled = false;
			}
		}
		
		private function commandBoxChanged(e:UIEvent):void 
		{			
			if (commandBox.selectedOption != null) 
			{
				var commandType:CommandType = commandBox.selectedOption.value as CommandType;
				outputCommand = commandType.createCommand(CommandGUIModes.EDITOR_TRIGGER);
			}else
			{
				
				outputCommand = null;
			}
		}
		
		private function removeBTSelected(e:UIEvent):void 
		{
			dispatchEvent(new ActionEvent(ActionEvent.REMOVE_CONSEQUENCE));
		}
		
		
		//finish
		public function clean():void
		{
			instances.splice(instances.indexOf(this), 1);
			outputModuleBox.removeEventListener(UIEvent.DROPDOWN_CHANGE, outputModuleBoxChanged);
			commandBox.removeEventListener(UIEvent.DROPDOWN_CHANGE, commandBoxChanged);
		}
		
		//external update
		public function updateOutputOptions():void
		{
			outputModuleBox.setOptions(outputOptions);
		}
		
		//static
		public static function updateAllOutputOptions(updateActions:Boolean = true):void
		{
			outputOptions = OutputModule.getModuleList();
			if (updateActions)
			{
				for each(var c:Consequence in instances)
				{
					c.updateOutputOptions();
				}
			}
		}
		
		
		
		//Project XML Saving
		public function getXML():XML 
		{
			var xml:XML = <consequence></consequence>;
			xml.@id = id;
			
			if (outputModule != null) 
			{
				xml.@moduleId = outputModule.id;
				//xml.@moduleIndex = outputModuleBox.selectedIndex;
			}
			
			if (outputCommand != null)
			{
				xml.@commandId = commandBox.selectedOption.id;			
				var commandXML:XML = outputCommand.getXML();
				xml.appendChild(commandXML);
				
			}
			
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			this.id = data.@id;
			dataToLoad = data;
			if (MainFrame.instance.ready)
			{
				loadData();
			}else
			{
				MainFrame.instance.addEventListener(ProjectEvent.PROJECT_READY, loadData);
			}
		}
		
		private function loadData(e:ProjectEvent = null):void 
		{
			removeEventListener(ProjectEvent.PROJECT_READY, loadData);
			
			if (dataToLoad == null || !MainFrame.instance.ready) return;
			
			if ("@moduleId" in dataToLoad) outputModuleBox.setSelectedOption(dataToLoad.@moduleId);
			if ("@commandId" in dataToLoad) commandBox.setSelectedOption(dataToLoad.@commandId);
			
			if (outputCommand != null)
			{
				outputCommand.loadXML(dataToLoad.command[0]);
			}
			
			dataToLoad = null;
		}
		
		public function get outputCommand():Command 
		{
			return _outputCommand;
		}
		
		public function set outputCommand(value:Command):void 
		{
			if (value == outputCommand) return;
			
			if (outputCommand != null)
			{
				outputCommand.clean();
				removeChild(outputCommand.gui);
			}
			
			_outputCommand = value;
			
			if (outputCommand != null)
			{
				addChildAt(outputCommand.gui,0);
			}
			
			draw();
			
			dispatchEvent(new ActionEvent(ActionEvent.CONSEQUENCE_COMMAND_CHANGED));
			
			
		}
		
		
		override public function set width(value:Number):void
		{
			this.baseWidth = value;
			draw();
		}
		
	}

}