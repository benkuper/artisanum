package statemachine.behaviors.common
{
	import com.laiyonghao.Uuid;
	import io.ProjectEvent;
	import modules.events.ModuleEvent;
	import statemachine.behaviors.actions.ActionEvent;
	import statemachine.behaviors.Behavior;
	import flash.display.Sprite;
	import modules.events.MeasureEvent;
	import modules.input.InputModule;
	import modules.input.measure.comparators.Comparator;
	import modules.input.measure.comparators.ComparatorEvent;
	import modules.input.measure.comparators.ComparatorType;
	import modules.input.measure.ContinuousMeasure;
	import modules.input.measure.DiscreteMeasure;
	import modules.input.measure.Measure;
	import modules.input.measure.ReferenceValue;
	import ui.components.Button;
	import ui.components.DropDown;
	import ui.components.Slider;
	import ui.components.StatusBox;
	import ui.components.UIEvent;
	import ui.Tooltip;
	import statemachine.StateMachine;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Condition extends Sprite
	{
		//static
		public static var instances:Vector.<Condition>;
		public static var inputOptions:Array;
		
		//data
		public var id:String;
		
		//layout
		public var baseWidth:Number;
		public var buttonsOffset:Number;
		
		//ui
		private var inputModuleBox:DropDown;
		private var inputMeasureBox:DropDown;
		private var comparatorBox:DropDown;
		
		private var statusBox:StatusBox;
		private var removeBT:Button;
		
		//data-selection
		private var _inputModule:InputModule;
		private var _inputMeasure:Measure;
		private var _comparator:Comparator;
		
		//data-recover (modules / measures /comparator)
		private var ghostModuleID:String;
		private var ghostModuleName:String;
		
		private var ghostMeasureID:String;
		private var ghostMeasureName:String;
		
		private var ghostComparatorID:String;
		private var ghostComparatorData:XML;
		
		//data-flow
		private var _active:Boolean;
		private var sendOnChangeOnly:Boolean;
		
		private var _running:Boolean;
		
		//async load
		public var dataToLoad:XML;
		
		//special
		private var _behavior:Behavior;
		
		
		
		public function Condition()
		{
			id = new Uuid().toString();
			
			if (instances == null)
				instances = new Vector.<Condition>;
				
			instances.push(this);
			
			if (inputOptions == null)
				updateAllInputOptions(false);
			
			sendOnChangeOnly = true;
			
			buttonsOffset = -20;
			
			inputModuleBox = new DropDown(inputOptions, "[Entrée]");
			inputModuleBox.height = 20;
			inputModuleBox.width = 90;
			inputModuleBox.setOptionsLayout(2, 200, 20);
			addChild(inputModuleBox);
			inputModuleBox.addEventListener(UIEvent.DROPDOWN_CHANGE, inputModuleBoxChanged);
			
			inputMeasureBox = new DropDown(null, "[Mesure]");
			inputMeasureBox.enabled = false;
			inputMeasureBox.height = 20;
			inputMeasureBox.width = 90;
			inputMeasureBox.setOptionsLayout(2, 200, 20);
			addChild(inputMeasureBox);
			inputMeasureBox.addEventListener(UIEvent.DROPDOWN_CHANGE, inputMeasureBoxChanged);
			
			comparatorBox = new DropDown(null, "[Comparaison]");
			comparatorBox.enabled = false;
			comparatorBox.height = 20;
			comparatorBox.width = 90;
			comparatorBox.setOptionsLayout(2, 200, 20);
			addChild(comparatorBox);
			comparatorBox.addEventListener(UIEvent.DROPDOWN_CHANGE, comparatorBoxChanged);
			
			
			removeBT = new Button("x");
			removeBT.width = 15;
			removeBT.height = 15;
			addChild(removeBT);
			removeBT.addEventListener(UIEvent.BUTTON_SELECTED, removeBTSelected);
			
			statusBox = new StatusBox();
			statusBox.shape = StatusBox.RECTANGLE;
			addChild(statusBox);
			Tooltip.addTarget(statusBox, "Voyant d'état : Jaune = Allumé, Gris = éteint");
			
			draw();
			
		}
		
		
		
		private function draw():void
		{
			inputMeasureBox.x = baseWidth / 2 - inputModuleBox.width / 2 + buttonsOffset;
			inputModuleBox.x = inputMeasureBox.x - inputModuleBox.width - 10;
			comparatorBox.x = inputMeasureBox.x + inputMeasureBox.width + 10;
			
			if (comparator != null) 
			{
				comparator.gui.y = inputModuleBox.height + 5;
				comparator.guiWidth = baseWidth - 10;
				
				comparator.gui.x = baseWidth / 2 - (comparator.gui.width / 2);
			}
			
			removeBT.x = baseWidth - removeBT.width -5;
			removeBT.y = 0;
			
			statusBox.width = 10;
			statusBox.height = 20;
			statusBox.x = removeBT.x - statusBox.width - 5;
			statusBox.y = 0;
			
		}
		
		
		//static
		public static function updateAllInputOptions(updateActions:Boolean = true):void
		{
			inputOptions = InputModule.getModuleList();
			
			if (updateActions)
			{
				for each (var c:Condition in instances)
				{
					c.updateInputOptions();
				}
			}
		}
		
		
		//update and ghosting
		
		public function updateInputOptions():void
		{
			var options:Array = new Array();
			
			if (behavior != null) 
			{
				options.push( {id:behavior.module.id, label:behavior.module.moduleName, value:behavior.module } );
			}
			
			inputModuleBox.setOptions(inputOptions.concat(options));
			
			if (ghostModuleID != null)
			{
				if (inputModuleBox.selectedOption == null || (inputModuleBox.selectedOption.id != ghostModuleID))
				{
					trace("Set module from Ghost ("+ghostModuleID+")")
					inputModuleBox.setSelectedOption(ghostModuleID);
					if (inputModuleBox.selectedOption == null) //not found with ID
					{
						inputModuleBox.setSelectedOptionWithLabel(ghostModuleName);
					}
					
					//if (inputModuleBox.selectedOption != null)
					//{
						////trace("Ghost module assign successful, removing ghost module");
						////ghostModuleID = null;
					//}
				}
				
				
			}
		}
		
		private function updateMeasureOptions():void 
		{
			if (inputModule == null) return;
			
			inputMeasureBox.setOptions(inputModule.getMeasureList());
			
			if (ghostMeasureID != null)
			{
				if (inputMeasureBox.selectedOption == null || (inputMeasureBox.selectedOption.id != ghostMeasureID))
				{
					trace("Set measure from Ghost ("+ghostMeasureID+")")
					inputMeasureBox.setSelectedOption(ghostMeasureID);
					if (inputMeasureBox.selectedOption == null) //not found with ID
					{
						inputMeasureBox.setSelectedOptionWithLabel(ghostMeasureName);
					}
					
					//if (inputMeasureBox.selectedOption != null)
					//{
						//trace("Ghost assign on measure successful, removing ghost measure");
						////ghostMeasureID = null;
					//}
				}
			}
		}
		
		private function updateComparatorOptions():void 
		{
			if (inputMeasure == null) return;
			
			comparatorBox.setOptions(inputMeasure.getComparatorsList());
			
			if (ghostComparatorID != null)
			{
				if (comparatorBox.selectedOption == null || (comparatorBox.selectedOption.id != ghostComparatorID))
				{
					trace("Set comparator from Ghost ("+ghostComparatorID+")")
					comparatorBox.setSelectedOption(ghostComparatorID);
					//if (comparatorBox.selectedOption != null)
					//{
						//trace("ghost assign on comparator successful, removing ghost");
						////ghostComparatorID = null;
					//}
				}
			}
		}
		
		
		
		
		//handlers
		
		private function inputModuleBoxChanged(e:UIEvent):void 
		{
			trace("InputModule Box changed, selected Index :" + inputModuleBox.selectedIndex);
			if (inputModuleBox.selectedOption != null)
			{
				inputModule = inputModuleBox.selectedOption.value as InputModule;
				
				trace("> ", ghostModuleID, inputModule.id);
				ghostModuleID = inputModule.id;
				ghostModuleName = inputModule.moduleName;
				
				updateMeasureOptions();
				inputMeasureBox.enabled = true;
			}
			else
			{
				
				inputModule = null;
				
				if (ghostModuleID != null) inputModuleBox.noItemText  = "#" + ghostModuleName;
				else inputModuleBox.noItemText  = "[Entrée]";
				
				inputMeasureBox.setOptions(null);
				inputMeasureBox.enabled = false;
			}
		}
		
		private function inputMeasureBoxChanged(e:UIEvent):void 
		{
			trace("input measure box changed");
			if (inputMeasureBox.selectedOption != null)
			{
				inputMeasure = inputMeasureBox.selectedOption.value as Measure;
				
				ghostMeasureID = inputMeasure.id;
				ghostMeasureName = inputMeasure.label;
				comparatorBox.enabled = true;
				
				updateComparatorOptions();
			}else
			{
				inputMeasure = null;
				
				if (ghostMeasureID != null) inputMeasureBox.noItemText  = "#" + ghostMeasureID;
				else inputMeasureBox.noItemText  = "[Mesure]";
				
				comparatorBox.setOptions(null);
				comparatorBox.enabled = false;
			}
		}
		
		private function comparatorBoxChanged(e:UIEvent):void 
		{
			trace("comparator changed, selected index :", comparatorBox.selectedIndex);
			if (comparatorBox.selectedOption != null)
			{
				var prevComparator:Comparator = comparator;
				
				var comparatorType:ComparatorType = comparatorBox.selectedOption.value as ComparatorType;
				comparator =  comparatorType.createComparator(inputMeasure, prevComparator);
				
				
				if (ghostComparatorID != comparatorBox.selectedOption.id)
				{
					ghostComparatorID = comparatorBox.selectedOption.id;
				}else if (ghostComparatorData != null)
				{
					comparator.loadXML(ghostComparatorData);
				}
			}
			else
			{
				ghostComparatorData = comparator.getXML();
				
				if (ghostComparatorID != null) comparatorBox.noItemText  = "#" + ghostComparatorID;
				else comparatorBox.noItemText  = "[Comparateur]";
				
				comparator = null;
			}
		}
		
		
		private function comparatorResultChanged(e:ComparatorEvent):void 
		{
			active = comparator.result;
		}
		
		
		//Module measure for ghost recover
		private function moduleMeasureUpdated(e:ModuleEvent):void 
		{
			//trace("module measure updated !", e.type);
			updateMeasureOptions();
		}
		
		//for label update
		private function measureLabelChanged(e:MeasureEvent):void 
		{
			updateMeasureOptions();
		}
		
		//delete
		private function removeBTSelected(e:UIEvent):void 
		{
			dispatchEvent(new ActionEvent(ActionEvent.REMOVE_CONDITION));
		}
		
		
		//finish
		public function clean():void
		{
			active = false;
			
			instances.splice(instances.indexOf(this), 1);
			inputMeasure = null;
			
			inputModuleBox.removeEventListener(UIEvent.DROPDOWN_CHANGE, inputModuleBoxChanged);
			inputMeasureBox.removeEventListener(UIEvent.DROPDOWN_CHANGE, inputMeasureBoxChanged);
			comparatorBox.removeEventListener(UIEvent.DROPDOWN_CHANGE, comparatorBoxChanged);
			comparator = null;			
		}
		
		
		
		
		
		//Project XML saving
		public function getXML():XML
		{
			var xml:XML = <condition></condition>;
			xml.@id = id;
			
			if (inputModule != null) 
			{
				xml.@moduleId = inputModule.id;
				xml.@moduleName = inputModule.moduleName;
				trace("save inputModule", inputModule.id);
				//xml.@moduleIndex = inputModuleBox.selectedIndex;
			}else
			{
				xml.@ghostModuleId = ghostModuleID;
				xml.@ghostModuleName = ghostModuleName;
			}
			
			if (inputMeasure != null) 
			{
				xml.@measureId = inputMeasure.id;
				xml.@measureName = inputMeasure.label;
				//xml.@measureIndex = inputMeasureBox.selectedIndex;
			}else
			{
				xml.@ghostMeasureId = ghostMeasureID;
				xml.@ghostMeasureName = ghostMeasureName;
			}
			
			if (comparator != null) 
			{
				xml.@comparatorId = comparatorBox.selectedOption.id;
				//xml.@comparatorIndex = comparatorBox.selectedIndex;
				xml.appendChild(comparator.getXML());
			}else
			{
				xml.@ghostComparatorId = ghostComparatorID;
				var gxml:XML = <ghostComparatorData></ghostComparatorData>;
				gxml.appendChild(ghostComparatorData);
				xml.appendChild(gxml);
			}
			
			
			return xml;
			
		}
		
		public function loadXML(data:XML):void
		{
			this.id = data.@id;
			
			dataToLoad = data;
			
			if (MainFrame.instance.ready)
			{
				loadData();
			}else
			{
				MainFrame.instance.addEventListener(ProjectEvent.PROJECT_READY, loadData);
			}
		}
		
		private function loadData(e:ProjectEvent = null):void 
		{
			removeEventListener(ProjectEvent.PROJECT_READY, loadData);
			
			if (dataToLoad == null || !MainFrame.instance.ready) return;
			
			if ("@moduleId" in dataToLoad) 
			{
				inputModuleBox.setSelectedOption(dataToLoad.@moduleId);
				ghostModuleID = dataToLoad.@moduleId;
				ghostModuleName = dataToLoad.@moduleName;
			}else
			{
				ghostModuleID = dataToLoad.@ghostModuleId;
				ghostModuleName = dataToLoad.@ghostModuleName;
				inputModuleBox.noItemText  = "#" + ghostModuleName;
			}
			
			if (inputModule != null && "@measureId" in dataToLoad) 
			{
				inputMeasureBox.setSelectedOption(dataToLoad.@measureId);
				ghostMeasureID = dataToLoad.@measureId;
				ghostMeasureName = dataToLoad.@measureName;
			}else
			{
				ghostMeasureID = dataToLoad.@ghostMeasureId;
				ghostMeasureName = dataToLoad.@ghostMeasureName;
				inputMeasureBox.noItemText  = "#" + ghostMeasureID;
			}
			
			if (inputMeasure != null && "@comparatorId" in dataToLoad) 
			{
				comparatorBox.setSelectedOption(dataToLoad.@comparatorId);
				ghostComparatorID = dataToLoad.@comparatorId;
			}else
			{
				ghostComparatorID = dataToLoad.@ghostComparatorId;
				comparatorBox.noItemText  = "#" + ghostComparatorID;
			}
			
			if (comparator != null)
			{
				comparator.loadXML(dataToLoad.comparator[0]);
			}else
			{
				if (dataToLoad.ghostComparatorData.length() > 0)
				{
					ghostComparatorData = dataToLoad.ghostComparatorData[0].comparator[0];
				}
			}
			
			dataToLoad = null;
			
		}
		
		// getter / setter
		
		public function get active():Boolean 
		{
			return _active;
		}
		
		public function set active(value:Boolean):void 
		{
			if (active == value && sendOnChangeOnly) return;
			
			_active = value;
			
			var status:String = sendOnChangeOnly?StatusBox.OK:StatusBox.WARNING;
			
			statusBox.status = value?status:StatusBox.NORMAL;
			
			if(running) dispatchEvent(new ConditionEvent(value?ConditionEvent.CONDITION_ACTIVE:ConditionEvent.CONDITION_INACTIVE));
		}
		
		public function get behavior():Behavior 
		{
			return _behavior;
		}
		
		public function set behavior(value:Behavior):void 
		{
			_behavior = value;
			//trace("set this behavior", value);
			updateInputOptions();
		}
		
		public function get comparator():Comparator 
		{
			return _comparator;
		}
		
		public function set comparator(value:Comparator):void 
		{
			if (comparator == value) return;
			if (comparator != null)
			{
				comparator.clean();
				comparator.removeEventListener(ComparatorEvent.RESULT_CHANGED, comparatorResultChanged);
				removeChild(comparator.gui);
				
			}
			
			_comparator = value;
			
			if (comparator != null)
			{
				comparator.addEventListener(ComparatorEvent.RESULT_CHANGED, comparatorResultChanged);
				//force first changed if done before adding listener
				comparatorResultChanged(null);
				addChild(comparator.gui);
			}
			
			dispatchEvent(new ActionEvent(ActionEvent.CONDITON_COMPARATOR_CHANGED));
			draw();
		}
		
		override public function set width(value:Number):void
		{
			this.baseWidth = value;
			draw();
		}
		
		public function get inputModule():InputModule 
		{
			return _inputModule;
		}
		
		public function set inputModule(value:InputModule):void 
		{
			if (inputModule != null)
			{
				inputModule.removeEventListener(ModuleEvent.MEASURE_ADDED, moduleMeasureUpdated);
				inputModule.removeEventListener(ModuleEvent.MEASURE_REMOVED, moduleMeasureUpdated);
				inputModule.removeEventListener(MeasureEvent.LABEL_CHANGED, measureLabelChanged);
			}
			_inputModule = value;
			
			if (inputModule != null)
			{
				inputModule.addEventListener(ModuleEvent.MEASURE_ADDED, moduleMeasureUpdated);
				inputModule.addEventListener(ModuleEvent.MEASURE_REMOVED, moduleMeasureUpdated);
				inputModule.addEventListener(MeasureEvent.LABEL_CHANGED, measureLabelChanged);
			}
		}
		
		public function get inputMeasure():Measure 
		{
			return _inputMeasure;
		}
		
		public function set inputMeasure(value:Measure):void 
		{
			//if (inputMeasure != null)
			//{
				//inputMeasure.removeEventListener(MeasureEvent.LABEL_CHANGED, measureLabelChanged);
			//}
			
			_inputMeasure = value;
			
			//if (inputMeasure != null)
			//{
				//inputMeasure.addEventListener(MeasureEvent.LABEL_CHANGED, measureLabelChanged);
			//}
		}
		
		public function get running():Boolean 
		{
			return _running;
		}
		
		public function set running(value:Boolean):void 
		{
			_running = value;
			if (!value)
			{
				active = false;
			}
		}
		
		
		
	}

}