package statemachine.behaviors.transitions 
{
	import statemachine.behaviors.actions.ActionEvent;
	import statemachine.behaviors.common.Condition;
	import statemachine.behaviors.common.Consequence;
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import ui.components.Button;
	import ui.components.Panel;
	import ui.components.UIEvent;
	import ui.fonts.Fonts;
	import ui.Style;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TransitionEditor extends Panel
	{
		
		//data
		private var _currentTransition:Transition;
		
		//ui
		private var conditionsContainer:Sprite;
		private var consequencesContainer:Sprite;
		private var conditionTF:TextField;
		private var consequencesTF:TextField;
		
		private var addConditionBT:Button;
		private var addConsequenceBT:Button;
		private var _opened:Boolean;
		
		private var closeBT:Button;
		
		private var dispatchOpen:Boolean;
		
		public function TransitionEditor() 
		{
			super("Editeur de transition");
			
			closeBT = new Button("-");
			addChild(closeBT);
			closeBT.addEventListener(UIEvent.BUTTON_SELECTED, closeBTSelected);
			closeBT.width = 30;
			closeBT.height = 15;
			
			conditionsContainer = new Sprite();
			
			
			addConditionBT = new Button("Ajouter");
			
			addConditionBT.width = 50;
			addConditionBT.height = 20;
			addConditionBT.addEventListener(UIEvent.BUTTON_SELECTED, addConditionBTSelected);
			
			consequencesContainer = new Sprite();
			
			
			addConsequenceBT = new Button("Ajouter");
			
			addConsequenceBT.width = 50;
			addConsequenceBT.height = 20;
			addConsequenceBT.addEventListener(UIEvent.BUTTON_SELECTED, addConsequenceBTSelected);
			
			
			conditionTF = Fonts.createTF("Conditions", Fonts.normalTF, TextFieldAutoSize.CENTER);
			consequencesTF = Fonts.createTF("Conséquences", Fonts.normalTF, TextFieldAutoSize.CENTER);
			
			
			mainContainer.addChild(conditionTF);
			mainContainer.addChild(consequencesTF);
			mainContainer.addChild(addConsequenceBT);
			mainContainer.addChild(consequencesContainer);
			mainContainer.addChild(addConditionBT);
			mainContainer.addChild(conditionsContainer);
			
			baseWidth = 420;
			
			dispatchOpen = true;
			
			
			opened = false;
			draw();
			
			this.y = -this.height;
			
			addEventListener(ActionEvent.REMOVE_CONDITION, removeConditionHandler);
			addEventListener(ActionEvent.REMOVE_CONSEQUENCE, removeConsequenceHandler);
			
			addEventListener(ActionEvent.CONSEQUENCE_COMMAND_CHANGED, consequenceCommandChanged);
			
		}
		
		
		
		
		
		private function addConditionBTSelected(e:UIEvent):void 
		{
			if (currentTransition == null) return;
			var c:Condition = currentTransition.addCondition();
			conditionsContainer.addChild(c);
			TweenLite.fromTo(c, .5, { scaleY:0 }, { scaleY:1, ease:Strong.easeOut, onUpdate:draw } );
			draw();
		}
		
		private function addConsequenceBTSelected(e:UIEvent):void 
		{
			if (currentTransition == null) return;
			var c:Consequence = currentTransition.addConsequence();
			consequencesContainer.addChild(c);
			TweenLite.fromTo(c, .5, { scaleY:0 }, { scaleY:1, ease:Strong.easeOut, onUpdate:draw } );
			draw();
		}
		
		override protected function draw():void
		{
			var i:int;
			
			closeBT.x = baseWidth - closeBT.width - 10;
			closeBT.y = 5;
			
			var innerWidth:Number = baseWidth -20;
			
			//conditions 
			conditionsContainer.x = 10;
			
			conditionTF.x = conditionsContainer.x;
			conditionTF.y = conditionsContainer.y;
			conditionTF.width = innerWidth;
			
			var curConditionY:Number = 24;
			for (i = 0; i < conditionsContainer.numChildren; i++)
			{
				var cd:Condition = conditionsContainer.getChildAt(i) as Condition;
				cd.y = curConditionY;
				cd.x = 5;
				
				curConditionY += cd.height;
			}
			
			curConditionY += 5;
			addConditionBT.y = conditionsContainer.y + curConditionY;
			addConditionBT.x = (innerWidth - addConditionBT.width) / 2;
			
			conditionsContainer.graphics.clear();
			conditionsContainer.graphics.lineStyle(2,Style.UI_LIGHT, .5);
			conditionsContainer.graphics.drawRect(0, 0, innerWidth, curConditionY + 30);
			
			//consequences
			
			consequencesContainer.x = 10;
			consequencesContainer.y = conditionsContainer.y + conditionsContainer.height + 10;
			
			consequencesTF.x = consequencesContainer.x + 5;
			consequencesTF.y = consequencesContainer.y + 5;
			consequencesTF.width = innerWidth;
			
			var curConsequenceY:Number = 30;
			for (i = 0; i < consequencesContainer.numChildren; i++)
			{
				var cq:Consequence = consequencesContainer.getChildAt(i) as Consequence;
				cq.y = curConsequenceY;
				cq.x = (innerWidth -cq.width)/2 - 20;
				curConsequenceY += cq.height + 5;
			}
			
			curConsequenceY += 5,
			
			addConsequenceBT.y = consequencesContainer.y + curConsequenceY;
			addConsequenceBT.x = (innerWidth - addConsequenceBT.width) / 2;
			
			consequencesContainer.graphics.clear();
			consequencesContainer.graphics.lineStyle(2,Style.UI_LIGHT, .5);
			consequencesContainer.graphics.drawRect(0, 0, innerWidth, curConsequenceY + 30);
			
			baseHeight = headerHeight + consequencesContainer.y + consequencesContainer.height + 10;
			
			super.draw();
		}
		
		public function finishClose():void 
		{
			visible = false;
		}
		
		private function loadCurrentTransition():void 
		{
			
			conditionsContainer.removeChildren();
			consequencesContainer.removeChildren();
			
			if (currentTransition == null) 
			{
				draw();
				return;
			}
			
			for each(var cd:Condition in currentTransition.conditions)
			{
				conditionsContainer.addChild(cd);
			}
			
			for each(var cq:Consequence in currentTransition.consequences)
			{
				consequencesContainer.addChild(cq);
			}
			
			opened = true;
			dispatchOpen = false;
			
			title = "Editeur de transition : " + currentTransition.fromBehavior.name+" > "+currentTransition.toBehavior.name;
			
			draw();
		}
		
		
		//handlers
		
		
		private function closeBTSelected(e:UIEvent):void 
		{
			dispatchOpen = true;
			currentTransition = null;
		}
		
		private function removeConditionHandler(e:TransitionEvent):void 
		{
			TweenLite.to(e.target as Condition, .3, { scaleY:0, ease:Strong.easeOut, onUpdate:draw, onComplete:finishRemoveCondition, onCompleteParams:[e.target as Condition] } );
		}
		
		public function finishRemoveCondition(c:Condition):void 
		{
			currentTransition.removeCondition(c);
			draw();
		}
		
		private function removeConsequenceHandler(e:TransitionEvent):void 
		{
			TweenLite.to(e.target as Consequence, .3, { scaleY:0, ease:Strong.easeOut, onUpdate:draw, onComplete:finishRemoveConsequence, onCompleteParams:[e.target as Consequence] } );
		}
		
		public function finishRemoveConsequence(c:Consequence):void 
		{
			currentTransition.removeConsequence(c);
			draw();
		}
		
		private function consequenceCommandChanged(e:ActionEvent):void 
		{
			TweenLite.delayedCall(.05, draw);
		}	
		
		
		//getter / setter
		
		public function get currentTransition():Transition 
		{
			return _currentTransition;
		}
		
		public function set currentTransition(value:Transition):void 
		{
			if (currentTransition == value) return;
			
			var hasAlreadyTransition:Boolean;
			
			if (currentTransition != null)
			{
				hasAlreadyTransition = true;	
			}
			
			opened = false;
			
			_currentTransition = value;
			
			TweenLite.killDelayedCallsTo(loadCurrentTransition);
			
			if (currentTransition != null)
			{				
				if (hasAlreadyTransition) 
				{
					TweenLite.delayedCall(.5, loadCurrentTransition);
				}
				else loadCurrentTransition();
			}else
			{
				dispatchOpen = true;
			}
			
		}
		
		public function get opened():Boolean 
		{
			return _opened;
		}
		
		public function set opened(value:Boolean):void 
		{
			if (opened == value) return;
			
			_opened = value;
			
			if (value)
			{
				visible = true;
				TweenLite.to(this, .5, {y:0, ease:Strong.easeOut, onUpdate:draw } );
				if (dispatchOpen) dispatchEvent(new TransitionEvent(TransitionEvent.TRANSITION_EDITOR_OPEN));
			}else
			{
				TweenLite.to(this, .5, {y:-height,ease:Strong.easeOut, onComplete:finishClose, onUpdate:draw } );
				if(dispatchOpen) dispatchEvent(new TransitionEvent(TransitionEvent.TRANSITION_EDITOR_CLOSE));
			}
			
			
			
		}
		
		
		
		
		
	}

}