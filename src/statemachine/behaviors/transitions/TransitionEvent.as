package statemachine.behaviors.transitions
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TransitionEvent extends Event 
	{
		
		static public const EDIT_TRANSITION:String = "editTransition";
		static public const REMOVE_TRANSITION:String = "removeTransition";
		
		static public const TRANSITION_EDITOR_OPEN:String = "transitionEditorOpen";
		static public const TRANSITION_EDITOR_CLOSE:String = "transitionEditorClose";
		
		static public const TRANSITION_TRIGGER:String = "transitionTrigger";
		
		public function TransitionEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new TransitionEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("TransitionEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}