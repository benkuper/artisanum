package statemachine.behaviors.transitions 
{
	import com.laiyonghao.Uuid;
	import statemachine.behaviors.actions.Action;
	import statemachine.behaviors.actions.ActionEvent;
	import statemachine.behaviors.actions.ActionEvent;
	import statemachine.behaviors.Behavior;
	import statemachine.behaviors.BehaviorEvent;
	import statemachine.behaviors.common.Condition;
	import statemachine.behaviors.common.ConditionEvent;
	import statemachine.behaviors.common.Consequence;
	import com.greensock.TweenMax;
	import flash.display.NativeMenuItem;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.ui.ContextMenu;
	import ui.components.Button;
	import ui.components.UIEvent;
	import ui.Style;
	import statemachine.StateMachine;
	import util.findIntersection;
	import utils.geom.angle;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Transition extends Sprite 
	{
		
		//data
		public var id:String;
		
		private var _fromBehavior:Behavior;
		private var _toBehavior:Behavior;
		
		//public var _conditions:Vector.<Condition>;
		//private var _consequences:Vector.<Consequence>;
		private var _action:Action;
		
		private var _creationMode:Boolean;
		
		private var _highlight:Boolean;
		
		private var _role:String;
		static public const ROLE_NONE:String = "roleNone";
		static public const ROLE_FROM:String = "roleFrom";
		static public const ROLE_TO:String = "roleTo";
		
		
		
		//ui
		private var _color:uint;
		private var removeMI:NativeMenuItem;
		private var editMI:NativeMenuItem;
		
		private var _active:Boolean;
		
		
		public function Transition(fromBehavior:Behavior,toBehavior:Behavior) 
		{
			super();
			
			this.id = new Uuid().toString();
			//trace("new transition !");
			
			if(toBehavior != null) this.toBehavior = toBehavior;
			if(fromBehavior != null) this.fromBehavior = fromBehavior;
			
			//_conditions = new Vector.<Condition>;
			//_consequences = new Vector.<Consequence>();
			_action = new Action();
			action.addEventListener(ActionEvent.ACTIVE, actionActiveHandler);
			action.behavior = fromBehavior;
			
			_role = ROLE_NONE;
			_color = Style.UI_LINE;
			
			
			editMI = new NativeMenuItem("Editer");
			removeMI = new NativeMenuItem("Supprimer");
			
			contextMenu = new ContextMenu();
			contextMenu.addItem(editMI);
			contextMenu.addItem(removeMI);
			contextMenu.addEventListener(Event.SELECT, contextMenuSelect);
			
			addEventListener(MouseEvent.MOUSE_OVER, mouseHandler);
			addEventListener(MouseEvent.MOUSE_OUT, mouseHandler);
			addEventListener(MouseEvent.CLICK, mouseClick);
		}
		
		
		
		
		//data
		//public function addCondition():Condition
		//{
			//var c:Condition = new Condition();
			//conditions.push(c);
			//c.addEventListener(ConditionEvent.CONDITION_ACTIVE, conditionActive);
			//c.addEventListener(ConditionEvent.CONDITION_INACTIVE, conditionInactive);
			//
			//c.running = active;
			//
			//return c;
		//}
		//
		//public function removeCondition(c:Condition):void
		//{
			//conditions.splice(conditions.indexOf(c), 1);
			//c.clean();
			//c.removeEventListener(ConditionEvent.CONDITION_ACTIVE, conditionActive);
			//c.removeEventListener(ConditionEvent.CONDITION_INACTIVE, conditionInactive);
		//}
		//
		//public function addConsequence():Consequence
		//{
			//var c:Consequence = new Consequence();
			//consequences.push(c);
			//return c;
		//}
		//
		//public function removeConsequence(c:Consequence):void
		//{
			//c.clean();
			//consequences.splice(consequences.indexOf(c),1);
		//}
		//
		//private function clearConditions():void 
		//{
			//while (conditions.length > 0)
			//{
				//removeCondition(conditions[0]);
			//}
		//}
		//
		//private function clearConsequences():void 
		//{
			//while (consequences.length > 0)
			//{
				//removeConsequence(consequences[0]);
			//}
		//}
		//
		
		//Data flow
		//private function triggerAllActions():void
		//{
			//for each(var c:Consequence in consequences)
			//{
				//c.trigger();
			//}
		//}
		
		
		
		//util
		public function isConnectedToBehavior(b:Behavior):Boolean
		{
			return b == toBehavior || b == fromBehavior;
		}
		
		//ui
		
		private function draw():void
		{
			graphics.clear();
			
			if (fromBehavior == null || toBehavior == null) return;
			
		
			
			//this.x = fromBehavior.x + fromBehavior.width/2;
			//this.y = fromBehavior.y + fromBehavior.height /2;
			

			var fromP1:Point = new Point(fromBehavior.x+fromBehavior.width/2,fromBehavior.y+fromBehavior.height/2);
			var fromP2:Point = new Point(toBehavior.x + toBehavior.width / 2, toBehavior.y + toBehavior.height / 2);
			
			var margin:Number = 10;
			var segPoints:Vector.<Point> = new Vector.<Point>();
			segPoints.push(	new Point(toBehavior.x - margin, toBehavior.y - margin),
							new Point(toBehavior.x + toBehavior.width + margin, toBehavior.y - margin),
							new Point(toBehavior.x + toBehavior.width + margin, toBehavior.y + toBehavior.height + margin),
							new Point(toBehavior.x - margin, toBehavior.y + toBehavior.height + margin)
							);
							
			var toPoint:Point;
			for (var i:int = 0; i < segPoints.length;i++)
			{
				//graphics.clear();
				//graphics.lineStyle(3, Style.YELLOW);
				//graphics.moveTo(fromP1.x, fromP1.y);
				//graphics.lineTo(fromP2.x, fromP2.y);
				//graphics.lineStyle(3, Style.GREEN);
				//graphics.moveTo( segPoints[i].x, segPoints[i].y);
				//graphics.lineTo( segPoints[(i + 1) % segPoints.length].x, segPoints[(i + 1) % segPoints.length].y);
				
				toPoint = findIntersection(fromP1, fromP2, segPoints[i], segPoints[(i + 1) % segPoints.length]);
				if (toPoint != null) 
				{
					//graphics.lineStyle(5, Style.RED);
					//graphics.drawCircle(toPoint.x, toPoint.y, 10);
					break;
				}
			}
			
			if (toPoint == null) toPoint = fromP2;
			
			var a:Number = angle(fromP1, toPoint);
			var rad1:Number = a * Math.PI / 180 - Math.PI/2;
			var rad2:Number = a * Math.PI / 180 + Math.PI/2;
			var rad3:Number = a * Math.PI / 180;
			var flecheBase:Number = 5;
			var flecheLength:Number = 10;
			
			var flecheP1:Point = new Point(toPoint.x + Math.cos(rad1) * flecheBase, toPoint.y + Math.sin(rad1) * flecheBase);
			var flecheP2:Point = new Point(toPoint.x + Math.cos(rad2) * flecheBase, toPoint.y + Math.sin(rad2) * flecheBase);
			var flechePointe:Point = new Point(toPoint.x + Math.cos(rad3) * flecheLength, toPoint.y + Math.sin(rad3) * flecheLength);
			
			
			var midPoint:Point = Point.interpolate(fromP1, fromP2, .5);
			
			//var curveP1:Point = new Point(midPoint.x, fromP1.y);
			//var curveP2:Point = new Point(midPoint.x, fromP2.y);
			
				//trace("Draw !", _fromBehavior.id, toBehavior.id, fromP1.x, fromP2.x,stage);
				
			graphics.lineStyle(10, color,.01);
			graphics.moveTo(fromP1.x, fromP1.y);
			graphics.lineTo(toPoint.x, toPoint.y);
			//graphics.cubicCurveTo(curveP1.x,curveP1.y,curveP2.x,curveP2.y,toPoint.x, toPoint.y);
			
			graphics.lineStyle(2, color);
			graphics.moveTo(fromP1.x, fromP1.y);
			graphics.lineTo(toPoint.x, toPoint.y);
			//graphics.cubicCurveTo(curveP1.x, curveP1.y, curveP2.x, curveP2.y, toPoint.x, toPoint.y);
			
			graphics.lineStyle();
			graphics.beginFill(color);
			
			graphics.moveTo(flecheP1.x, flecheP1.y);
			graphics.lineTo(flechePointe.x, flechePointe.y);
			graphics.lineTo(flecheP2.x, flecheP2.y);
			graphics.lineTo(flecheP1.x, flecheP1.y);
			
			graphics.lineStyle(2, Style.UI_LIGHT);
			graphics.drawCircle(midPoint.x, midPoint.y, 6);
				
			//if (role != ROLE_NONE)
			//{
				//
				//
			//}
			
		}
		
		public function clean():void 
		{
			fromBehavior = null;
			toBehavior = null;
			
			//clearConditions();
			//clearConsequences();
			
			contextMenu.removeEventListener(Event.SELECT, contextMenuSelect);
		}
		
		
		
		//handlers
		
		private function mouseClick(e:MouseEvent):void 
		{
			dispatchEvent(new TransitionEvent(TransitionEvent.EDIT_TRANSITION));
		}
		
		private function mouseHandler(e:MouseEvent):void 
		{
			highlight = e.type == MouseEvent.MOUSE_OVER;
		}
		
		private function behaviorMoving(e:UIEvent):void 
		{
			draw();
		}
		
		
		private function creationEnterFrame(e:Event):void 
		{
			var targetToB:Behavior = null;
			
			for each(var b:Behavior in StateMachine.instance.behaviorsList)
			{
				if (b == fromBehavior) continue;
				
				var rect:Rectangle = b.getRect(this);
				if(mouseX > rect.x && mouseX <rect.x+rect.width && mouseY > rect.y && mouseY < rect.y + rect.height)
				{
					targetToB = b;
					break;
				}
			}
			
			toBehavior = targetToB;
			
			if (toBehavior != null) 
			{
				draw();
				return;
			}
			
			var p1:Point = new Point(fromBehavior.x + fromBehavior.width / 2, fromBehavior.y + fromBehavior.height / 2);
			var p2:Point = new Point(mouseX, mouseY);
			
			graphics.clear();
			graphics.lineStyle(3, color);
			graphics.moveTo(p1.x, p1.y);
			graphics.lineTo(p2.x, p2.y);
			
		}
		
		
		
		private function creationStageClick(e:MouseEvent):void 
		{
			creationMode = false;
		}
		
		
		private function contextMenuSelect(e:Event):void 
		{
			switch(e.target)
			{
				case editMI:
					dispatchEvent(new TransitionEvent(TransitionEvent.EDIT_TRANSITION));
					break;
					
				case removeMI:
					dispatchEvent(new TransitionEvent(TransitionEvent.REMOVE_TRANSITION));
					break;
			}
		}
		
		
		private function actionActiveHandler(e:ActionEvent):void 
		{
			dispatchEvent(new TransitionEvent(TransitionEvent.TRANSITION_TRIGGER));
			action.active = false;
		}
		
		
		//util
		private function getColorForCurrentRole():uint 
		{
			var c:uint = Style.UI_LINE;
			if (role == ROLE_FROM) c = Style.ORANGE;
			else if (role == ROLE_TO) c = Style.GREEN;
			return c;
		}
		
		
		//Project XML saving
		public function getXML():XML 
		{
			var xml:XML = <transition>
			</transition>;
			
			xml.@fromBehaviorID = fromBehavior.id;
			xml.@toBehaviorID = toBehavior.id;
			
			xml.appendChild(action.getXML());
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			fromBehavior = StateMachine.instance.getBehaviorByID(data.@fromBehaviorID);
			toBehavior = StateMachine.instance.getBehaviorByID(data.@toBehaviorID);
			
			action.loadXML(data.action[0]);
		}
		
		
		private function updateActionName():void 
		{
			if (action == null) return;
			action.actionName = (fromBehavior == null?"*":fromBehavior.title)+ " > " +(toBehavior == null?"*":toBehavior.title);
		}
		
		
		//getter / setter
		
		
		public function get action():Action 
		{
			return _action;
		}
		
		public function get fromBehavior():Behavior 
		{
			return _fromBehavior;
		}
		
		public function set fromBehavior(value:Behavior):void 
		{
			if (fromBehavior != null)
			{
				fromBehavior.removeEventListener(UIEvent.DRAGGING, behaviorMoving);
				fromBehavior.removeEventListener(UIEvent.PANEL_OPENED, behaviorMoving);
				fromBehavior.removeEventListener(UIEvent.PANEL_CLOSED, behaviorMoving);
				fromBehavior.removeEventListener(UIEvent.PANEL_RESIZED, behaviorMoving);
				action.behavior = null;
			}
			_fromBehavior = value;
			
			if (fromBehavior != null)
			{
				fromBehavior.addEventListener(UIEvent.DRAGGING, behaviorMoving);
				fromBehavior.addEventListener(UIEvent.PANEL_OPENED, behaviorMoving);
				fromBehavior.addEventListener(UIEvent.PANEL_CLOSED, behaviorMoving);
				fromBehavior.addEventListener(UIEvent.PANEL_RESIZED, behaviorMoving);
				if(action != null) action.behavior = fromBehavior;
			}
			
			updateActionName();
			
			draw();
		}
		
		public function get toBehavior():Behavior 
		{
			return _toBehavior;
		}
		
		public function set toBehavior(value:Behavior):void 
		{
			if (toBehavior != null)
			{
				toBehavior.removeEventListener(UIEvent.DRAGGING, behaviorMoving);
				toBehavior.removeEventListener(UIEvent.PANEL_OPENED, behaviorMoving);
				toBehavior.removeEventListener(UIEvent.PANEL_CLOSED, behaviorMoving);
				toBehavior.removeEventListener(UIEvent.PANEL_RESIZED, behaviorMoving);
			}
			_toBehavior = value;
			
			if (toBehavior != null)
			{
				toBehavior.addEventListener(UIEvent.DRAGGING, behaviorMoving);
				toBehavior.addEventListener(UIEvent.PANEL_OPENED, behaviorMoving);
				toBehavior.addEventListener(UIEvent.PANEL_CLOSED, behaviorMoving);
				toBehavior.addEventListener(UIEvent.PANEL_RESIZED, behaviorMoving);
				
			}
			
			updateActionName();
			
			draw();
		}
		
		public function get role():String 
		{
			return _role;
		}
		
		public function set role(value:String):void 
		{
			_role = value;
			draw();
			
			//mouseEnabled = (role != ROLE_NONE);
			
			TweenMax.to(this, .3, { alpha:value == ROLE_NONE?.5:1,hexColors:{color:getColorForCurrentRole()}} );
		}
		
		
		public function get color():uint 
		{
			return _color;
		}
		
		public function set color(value:uint):void 
		{
			_color = value;
			draw();
		}
		
		public function get creationMode():Boolean 
		{
			return _creationMode;
		}
		
		public function set creationMode(value:Boolean):void 
		{
			if (_creationMode == value) return;
			
			_creationMode = value;
			if (value) 
			{
				addEventListener(Event.ENTER_FRAME, creationEnterFrame);
				stage.addEventListener(MouseEvent.CLICK, creationStageClick);
				stage.addEventListener(MouseEvent.RIGHT_CLICK, creationStageClick);
			}else 
			{
				removeEventListener(Event.ENTER_FRAME, creationEnterFrame);
				if (stage != null)
				{
					stage.removeEventListener(MouseEvent.CLICK, creationStageClick);
					stage.removeEventListener(MouseEvent.RIGHT_CLICK, creationStageClick);
				}
				
				
				if (toBehavior != null) 
				{
					dispatchEvent(new BehaviorEvent(BehaviorEvent.TRANSITION_CREATED));
				}else
				{
					dispatchEvent(new BehaviorEvent(BehaviorEvent.TRANSITION_CREATION_CANCELED));
				}
			}
		}
		
		public function get highlight():Boolean 
		{
			return _highlight;
		}
		
		public function set highlight(value:Boolean):void 
		{
			_highlight = value;
			if (!creationMode) TweenMax.to(this, .3, { hexColors: { color:value?Style.RED:getColorForCurrentRole() }} );
		}
		
		public function get active():Boolean 
		{
			return _active;
		}
		
		public function set active(value:Boolean):void 
		{
			_active = value;
			_action.running = value;
			
			updateActionName();
			//for each(var c:Condition in conditions)
			//{
				//c.running = value;
			//}
		}
		
		
	}

}