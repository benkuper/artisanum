package statemachine.behaviors
{
	import benkuper.util.KeyboardUtil;
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.laiyonghao.Uuid;
	import flash.display.NativeMenuItem;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.registerClassAlias;
	import flash.ui.ContextMenu;
	import flash.utils.ByteArray;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import modules.events.MeasureEvent;
	import modules.input.measure.ContinuousMeasure;
	import modules.input.measure.Measure;
	import statemachine.behaviors.modules.BehaviorInputModule;
	import statemachine.behaviors.actions.Action;
	import statemachine.behaviors.actions.ActionEvent;
	import statemachine.behaviors.actions.ActionPanel;
	import statemachine.behaviors.mappings.Mapping;
	import statemachine.behaviors.mappings.MappingEvent;
	import statemachine.behaviors.mappings.MappingPanel;
	import ui.Alert;
	import ui.components.Button;
	import ui.components.Panel;
	import ui.components.TextBox;
	import ui.components.UIEvent;
	import ui.menu.Menu;
	import ui.Style;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Behavior extends Panel
	{
		
		//layout
		
		//data
		public var id:String;
		private var _behaviorName:String;
		
		private var _active:Boolean;
		
		//ui
		private var actionPanel:ActionPanel;
		private var mappingPanel:MappingPanel;
		private var removeBT:Button;
		private var miniModeBT:Button;

		//action related
		private var _selectedAction:Action;
		
		//mapping related
		private var _selectedMapping:Mapping;
		
		//context
		private var activateMI:NativeMenuItem;
		private var createTransitionMI:NativeMenuItem;
		private var resetTimeMI:NativeMenuItem;
		private var permanentMI:NativeMenuItem;

		//special
		private var _module:BehaviorInputModule;
		private var timeFeedback:TextBox;
		
		private var _permanent:Boolean;
		
		public function Behavior()
		{
			
			_behaviorName = "Comportement 1";
			this.id = new Uuid().toString();
			
			super(behaviorName, 230, 400, true, true);
			
			_module = new BehaviorInputModule();
			_module.secondsMeasure.addEventListener(MeasureEvent.VALUE_CHANGE, secondsMeasureChange);
			
			
			selectable = true;
			
			headerHeight = 50;
			
			actionPanel = new ActionPanel(this); //pass the behavior, to be able to target behavior's internal module in conditions (via ActionPanel > Action > Condition). May change in the future
			
			actionPanel.x = 5;
			actionPanel.width = baseWidth - 10;
			mainContainer.addChild(actionPanel);
			
			mappingPanel = new MappingPanel();
			mappingPanel.x = 5;
			mappingPanel.width = baseWidth - 10;
			mainContainer.addChild(mappingPanel);
			
			timeFeedback = new TextBox("Temps :", "0s");
			timeFeedback.editable = false;
			timeFeedback.transparentBG = true;
			mainContainer.addChild(timeFeedback);
			timeFeedback.visible = false;
			
			addEventListener(BehaviorEvent.ELEMENT_ADDED, elementAdded);
			addEventListener(BehaviorEvent.ELEMENT_REMOVED, elementRemoved);
			
			addEventListener(ActionEvent.ACTION_ADDED, actionAdded);
			addEventListener(ActionEvent.ACTION_REMOVED, actionRemoved);
			
			addEventListener(MappingEvent.MAPPING_ADDED, mappingAdded);
			addEventListener(MappingEvent.MAPPING_REMOVED, mappingRemoved);
			
			actionPanel.addEventListener(UIEvent.PANEL_RESIZED, contentResized);
			mappingPanel.addEventListener(UIEvent.PANEL_RESIZED, contentResized);
			
			
			removeBT = new Button("x");
			addChild(removeBT);
			removeBT.addEventListener(UIEvent.BUTTON_SELECTED, removeBTClick);
			removeBT.width = 30;
			removeBT.height = 20;
			
			miniModeBT = new Button("-");
			addChild(miniModeBT);
			miniModeBT.addEventListener(UIEvent.BUTTON_SELECTED, miniModeBTClick);
			miniModeBT.width = 30;
			miniModeBT.height = 20;
			
			miniModeEnabled = true;
			
			addEventListener(ActionEvent.ACTION_CLICK, actionClick);
			addEventListener(MappingEvent.MAPPING_CLICK, mappingClick);
			
			addEventListener(MouseEvent.CLICK, mouseClick);
			
			activateMI = new NativeMenuItem("Activer");
			contextMenu.addItem(activateMI);
			
			permanentMI = new NativeMenuItem("Permanent");
			contextMenu.addItem(permanentMI);
			
			createTransitionMI = new NativeMenuItem("Créer une transition");
			contextMenu.addItem(createTransitionMI);
			
			resetTimeMI = new NativeMenuItem("Reset Temps");
			contextMenu.addItem(resetTimeMI);
			
			draw();
			
		}
		
		//ui
		override protected function draw():void
		{
			
			mappingPanel.y = actionPanel.y + actionPanel.height + 5;
			
			removeBT.x = baseWidth - removeBT.width - 10;
			removeBT.y = 5;
			
			miniModeBT.x = removeBT.x - miniModeBT.width - 5;
			miniModeBT.y = 5;
			
			timeFeedback.x = baseWidth /2- timeFeedback.width/2;
			timeFeedback.y = mappingPanel.y + mappingPanel.height;
			
			
			updateHeightAuto();
			
			super.draw();
		}
		
		protected function updateHeightAuto():void
		{
			baseHeight = mainContainer.y + mainContainer.height + 10;
		}
		
		private function askForRemove():void
		{
			Alert.show(stage, "Tu veux vraiment supprimer\nce comportement qui déchire ?", true, removeBehavior);
		}
		
		private function removeBehavior():void
		{
			TweenLite.to(this, .5, {scaleX: 0, scaleY: 0, ease: Strong.easeOut, onComplete: removeFinished});
		
		}
		
		public function removeFinished():void
		{
			dispatchEvent(new BehaviorEvent(BehaviorEvent.BEHAVIOR_REMOVED));
		}
		
		
		//handlers
		private function elementAdded(e:BehaviorEvent):void
		{
			draw();
		}
		
		private function elementRemoved(e:BehaviorEvent):void
		{
			draw();
		}
		
		private function actionAdded(e:ActionEvent):void
		{
			selectedAction = e.action;
		}
		
		private function actionRemoved(e:ActionEvent):void
		{
			if (e.action == selectedAction)
				selectedAction = null;
			draw();
		}
		
		private function mappingAdded(e:MappingEvent):void
		{
			selectedMapping = e.mapping;
		}
		
		private function mappingRemoved(e:MappingEvent):void
		{
			if (e.mapping == selectedMapping)
				selectedMapping = null;
			draw();
		}
		
		private function removeBTClick(e:UIEvent):void
		{
			askForRemove();
		}
		
		private function miniModeBTClick(e:UIEvent):void 
		{
			miniMode = !miniMode;
			if (!miniMode) parent.setChildIndex(this, parent.numChildren - 1);
		}
		
		private function contentResized(e:UIEvent):void
		{
			draw();
		}
		
		private function actionClick(e:ActionEvent):void
		{
			selectedAction = (e.action != selectedAction) ? e.action : null;
		}
		
		private function mappingClick(e:MappingEvent):void
		{
			selectedMapping = (e.mapping != selectedMapping) ? e.mapping : null;
		}
		
		override protected function contextMenuSelect(e:Event):void
		{
			super.contextMenuSelect(e);
			switch (e.target)
			{
				case activateMI: 
					active = true;
					break;
				
				case createTransitionMI:
					dispatchEvent(new BehaviorEvent(BehaviorEvent.CREATE_TRANSITION));
					break;
					
				case resetTimeMI:
					module.startTime();
					break;
					
				case permanentMI:
					permanentMI.checked = !permanentMI.checked;
					permanent = permanentMI.checked;
					break;
			}
		}
		
		private function mouseClick(e:MouseEvent):void
		{
			if (e.ctrlKey)
				active = !active;
		}
		
		//from menu
		private function permanentChange():void 
		{
			permanent = Menu.getChecked("permanentBehavior");
		}
		
		//special : time
		private function secondsMeasureChange(e:MeasureEvent):void
		{
			
			timeFeedback.text = (e.target as ContinuousMeasure).currentValue.toFixed(0) + "s";
		}
		
		
		//cleaning
		override public function clean():void
		{
			
			module.secondsMeasure.removeEventListener(MeasureEvent.VALUE_CHANGE, secondsMeasureChange);
			
			removeEventListener(BehaviorEvent.ELEMENT_ADDED, elementAdded);
			removeEventListener(BehaviorEvent.ELEMENT_REMOVED, elementRemoved);
			
			actionPanel.removeEventListener(UIEvent.PANEL_RESIZED, contentResized);
			mappingPanel.removeEventListener(UIEvent.PANEL_RESIZED, contentResized);
			
			removeBT.removeEventListener(UIEvent.BUTTON_SELECTED, removeBTClick);
			miniModeBT.removeEventListener(UIEvent.BUTTON_SELECTED, removeBTClick);
			
			super.clean();
			module.clean();
			actionPanel.clean();
			mappingPanel.clean();
		}
		
		//Project XML saving
		public function getXML():XML
		{
			var xml:XML =  <behavior></behavior>
			xml.@id = id;
			xml.@name = behaviorName;
			xml.@x = this.x;
			xml.@y = this.y;
			xml.@active = this.active;
			xml.@miniMode = this.miniMode;
			
			xml.appendChild(actionPanel.getXML(<actions></actions>));
			xml.appendChild(mappingPanel.getXML(<mappings></mappings>));
			
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			this._behaviorName = data.@name;
			title = behaviorName;
			this.x = data.@x;
			this.y = data.@y;
			this.id = data.@id;
			this.active = data.@active == "true";
			this.miniMode = data.@miniMode == "true";
			
			actionPanel.loadXML(data.actions[0]);
			mappingPanel.loadXML(data.mappings[0]);
			
			selectedAction = null;
		}
		
		
		//getter / setter
		
		override public function set title(value:String):void
		{
			super.title = value;
			behaviorName = title;
		}
		
		public function get selectedAction():Action
		{
			return _selectedAction;
		}
		
		public function set selectedAction(value:Action):void
		{
			
			if (selectedAction == value && selectedAction != null) selectedAction = null
				//return;
			
			var evt:BehaviorEvent;
			//trace("action selected ",selectedAction,value);
			if (selectedAction != null)
			{
				
				evt = new BehaviorEvent(BehaviorEvent.ACTION_DESELECTED);
				evt.action = selectedAction;
				dispatchEvent(evt);
			}
			
			_selectedAction = value;
			
			if (selectedAction != null)
			{
				selectedMapping = null;
				evt = new BehaviorEvent(BehaviorEvent.ACTION_SELECTED);
				evt.action = selectedAction;
				dispatchEvent(evt);
			}
		}
		
		public function get selectedMapping():Mapping
		{
			return _selectedMapping;
		}
		
		public function set selectedMapping(value:Mapping):void
		{
			if (selectedMapping == value)
				return;
			
			var evt:BehaviorEvent;
			
			//trace("mapping selected ", selectedMapping, value);
			
			if (selectedMapping != null)
			{
				
				evt = new BehaviorEvent(BehaviorEvent.MAPPING_DESELECTED);
				evt.mapping = selectedMapping;
				dispatchEvent(evt);
			}
			
			_selectedMapping = value;
			
			if (selectedMapping != null)
			{
				selectedAction = null;
				evt = new BehaviorEvent(BehaviorEvent.MAPPING_SELECTED);
				evt.mapping = selectedMapping;
				dispatchEvent(evt);
			}
		}
		
		public function get active():Boolean
		{
			return _active;
		}
		
		public function set active(value:Boolean):void
		{
			if (active == value)
				return;
			
			_active = value;
			TweenMax.to(this, .3, {hexColors: {bgColor: active ? (permanent?Style.PANEL_PERMANENT:Style.PANEL_ACTIVE) : Style.UI_SUBPANEL}});
			
			actionPanel.running = value;
			mappingPanel.running = value;
			
			timeFeedback.visible = active;
			
			
			if (value && actionPanel.isSequential) actionPanel.curSequentialAction = 0;
			
			if (value)
				_module.startTime();
			else
				_module.stop();
				
			
			dispatchEvent(new BehaviorEvent(value ? BehaviorEvent.BEHAVIOR_ACTIVATED : BehaviorEvent.BEHAVIOR_DEACTIVATED));
		}
		
		override public function set selected(value:Boolean):void
		{
			if (selected == value)
				return;
			super.selected = value;
			
			
			Menu.setEnabled("addAction", selected);
			Menu.setEnabled("addMapping", selected);
			
			Menu.setEnabled("removeBehavior", selected);
			Menu.setEnabled("duplicateBehavior", selected);
			
			Menu.setEnabled("permanentBehavior", selected);
			Menu.setChecked("permanentBehavior", permanent);
			
			if (value)
			{
				dispatchEvent(new BehaviorEvent(BehaviorEvent.BEHAVIOR_SELECTED));
				Menu.setMenuCallback("addAction", actionPanel.addAction);
				Menu.setMenuCallback("addMapping", mappingPanel.addMapping );
				
				Menu.setMenuCallback("removeAction",actionPanel.removeAction);
				Menu.setMenuCallback("duplicateAction",actionPanel.duplicateEditingAction);
				
				Menu.setMenuCallback("removeMapping", mappingPanel.removeMapping );
				Menu.setMenuCallback("duplicateMapping", mappingPanel.duplicateEditingMapping);
				Menu.setMenuCallback("permanentBehavior", permanentChange);
			}else
			{
				
			}
		}
		
		public function get module():BehaviorInputModule
		{
			return _module;
		}
		
		public function get permanent():Boolean 
		{
			return _permanent;
		}
		
		public function set permanent(value:Boolean):void 
		{
			_permanent = value;
			
			if (active)
			{
				bgColor = value?Style.PANEL_PERMANENT:Style.PANEL_ACTIVE;
			}
		}
		
		public function get behaviorName():String 
		{
			return _behaviorName;
		}
		
		public function set behaviorName(value:String):void 
		{
			if (_behaviorName == value) return;
			_behaviorName = value;
			dispatchEvent(new BehaviorEvent(BehaviorEvent.LABEL_CHANGED));
		}
		
	}

}