package statemachine.behaviors.actions 
{
	import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import ui.Alert;
	import ui.components.Button;
	import ui.components.StatusBox;
	import ui.components.TextBox;
	import ui.components.UIEvent;
	import ui.menu.Menu;
	import ui.Style;
	import ui.Toaster;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ActionPreview extends Sprite 
	{
		//preview (behavior panel)
		//layout
		public var previewWidth:Number;
		public var previewHeight:Number;
		
		//data
		private var action:Action;
		private var _active:Boolean;
		private var _editing:Boolean;
		
		//ui
		private var previewTextBox:TextBox;
		//private var editBT:Button;
		private var removeBT:Button;
		private var statusBox:StatusBox;
		private var renameMenuItem:NativeMenuItem;
		private var enableBT:Button;
		
		public function ActionPreview(action:Action) 
		{
			this.action = action;
			action.addEventListener(ActionEvent.ACTION_ENABLED, actionEnableChanged);
			action.addEventListener(ActionEvent.ACTION_DISABLED, actionEnableChanged);
			
			previewTextBox = new TextBox("", action.actionName);
			previewTextBox.y = -4;
			//previewTextBox.editable = true;
			previewTextBox.transparentBG = true;
			previewTextBox.addEventListener(UIEvent.EDITING_START, previewEditingStart);
			previewTextBox.addEventListener(UIEvent.EDITING_FINISH, previewEditingFinish);
			addChild(previewTextBox);
			
			//editBT = new Button("[+]");
			//addChild(editBT);
			//editBT.addEventListener(UIEvent.BUTTON_SELECTED, editActionBTSelected);
			//editBT.width = 35;
			//editBT.height = 20;
			
			removeBT = new Button("x");
			addChild(removeBT);
			removeBT.width = 30;
			removeBT.height = 20;
			removeBT.addEventListener(UIEvent.BUTTON_SELECTED, removeBTSelected);
			
			enableBT = new Button(">", true);
			enableBT.selectedColor = Style.BLUE;
			enableBT.selected = true;
			addChild(enableBT);
			enableBT.width = 25;
			enableBT.height = 20;
			enableBT.addEventListener(UIEvent.BUTTON_SELECTED, enableBTSelected);
			enableBT.addEventListener(UIEvent.BUTTON_DESELECTED, enableBTSelected);
			
			statusBox = new StatusBox();
			addChild(statusBox);
			
			
			previewWidth  = 50;
			previewHeight = 20;
			
			addEventListener(MouseEvent.CLICK, clickHandler);
			
			contextMenu = new NativeMenu();
			renameMenuItem = new NativeMenuItem("Renommer l'action");
			contextMenu.addItem(renameMenuItem);
			contextMenu.addEventListener(Event.SELECT, contextMenuSelect);
			
		}		
		
		private function draw():void
		{
			graphics.clear();
			graphics.beginFill(editing?Style.BLUE:Style.UI_PANEL, editing?.3:.1);
			graphics.drawRoundRect(-5, 0, previewWidth+10, previewHeight, 5, 5);
			graphics.endFill();
			
			removeBT.x = previewWidth - removeBT.width - 10;
			removeBT.y = 2;
			removeBT.height = previewHeight -5;
			
			enableBT.x = removeBT.x - enableBT.width - 10;
			enableBT.y = 2;
			enableBT.height = previewHeight -5;
			
			
			statusBox.x = enableBT.x - statusBox.width - 5;
			statusBox.y = (previewHeight - statusBox.height) / 2+4;
		}
		
		private function refreshStatusBox():void 
		{
			statusBox.status = active?(action.enabled?StatusBox.OK:StatusBox.WARNING):StatusBox.NORMAL;
		}
		
		//handlers
		private function clickHandler(e:MouseEvent):void 
		{
			//if (editing) return; //no effect when editing
			if (e.target == enableBT || e.target == removeBT) return; //no effect when clicking on enableBT
			if (previewTextBox.editable) return; //editing the action name
			dispatchEvent(new ActionEvent(ActionEvent.ACTION_CLICK, action));
		}
		
		
		
		private function removeBTSelected(e:UIEvent):void 
		{
			Alert.show(stage, "Supprimer l'action ?", true, confirmRemoveAction);
		}
		
		public function confirmRemoveAction():void 
		{
			dispatchEvent(new ActionEvent(ActionEvent.REMOVE_ACTION, action));
		}
		
		private function enableBTSelected(e:UIEvent):void 
		{
			action.enabled = enableBT.selected;
			refreshStatusBox();
		}
		
		private function actionEnableChanged(e:ActionEvent):void 
		{
			Toaster.info("Action " + (enableBT.selected?"activée":"désactivée"));
			enableBT.selected = action.enabled;
			refreshStatusBox();
		}
		
		
		private function contextMenuSelect(e:Event):void 
		{
			var item:NativeMenuItem = e.target as NativeMenuItem;
			switch(item)
			{
				case renameMenuItem:
					previewTextBox.transparentBG = false;
					previewTextBox.editable = true;
					previewTextBox.setFocus();
					previewTextBox.selectText();
					break;
			}
		}
		
		private function previewEditingFinish(e:UIEvent):void 
		{
			//trace("editing finish");
			previewTextBox.transparentBG = true;
			action.actionName = previewTextBox.text;
			previewTextBox.editable = false;
		}
		
		private function previewEditingStart(e:UIEvent):void 
		{
			previewTextBox.transparentBG = false;
		}
		
		//Action-data related
		public function updateActionName():void 
		{
			previewTextBox.text = action.actionName;
		}
		
		
		//cleaning
		public function clean():void
		{
			
			action.removeEventListener(ActionEvent.ACTION_ENABLED, actionEnableChanged);
			action.removeEventListener(ActionEvent.ACTION_DISABLED, actionEnableChanged);
			action = null;
			
			previewTextBox.removeEventListener(UIEvent.EDITING_START, previewEditingStart);
			previewTextBox.removeEventListener(UIEvent.EDITING_FINISH, previewEditingFinish);
			
			enableBT.removeEventListener(UIEvent.BUTTON_SELECTED, enableBTSelected);
			enableBT.removeEventListener(UIEvent.BUTTON_DESELECTED, enableBTSelected);
			
			contextMenu.removeEventListener(Event.SELECT, contextMenuSelect);
			
		}
		
		
		
		//getter / setter
		
		override public function set width(value:Number):void
		{
			previewWidth = value;
			draw();
		}
		
		override public function set height(value:Number):void
		{
			previewHeight = value;
			draw();
		}
		
		public function get editing():Boolean 
		{
			return _editing;
		}
		
		public function set editing(value:Boolean):void 
		{
			if (editing == value) return;
			_editing = value;
			draw();
			
			Menu.setEnabled("removeAction", value);
			Menu.setEnabled("duplicateAction", value);
			
			
			if(!editing) dispatchEvent(new ActionEvent(ActionEvent.ACTION_CLICK));
		}
		
		public function get active():Boolean 
		{
			return _active;
		}
		
		public function set active(value:Boolean):void 
		{
			_active = value;
			refreshStatusBox();
			draw();
		}
		
		
	}

}