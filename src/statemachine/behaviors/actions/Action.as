package statemachine.behaviors.actions 
{
	import statemachine.behaviors.Behavior;
	import statemachine.behaviors.common.Condition;
	import statemachine.behaviors.common.ConditionEvent;
	import statemachine.behaviors.common.Consequence;
	import flash.events.EventDispatcher;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Action extends EventDispatcher
	{		
		
		
		//data-logic
		private var _actionName:String;
		
		private var _running:Boolean;
		private var _active:Boolean;
		
		private var _enabled:Boolean;
		
		
		//ui
		private var _preview:ActionPreview;
		
		private var _conditions:Vector.<Condition>;
		private var _consequences:Vector.<Consequence>;
		
		
		//special
		private var _behavior:Behavior;		
		
		public function Action() 
		{
			_actionName = "Nouveau déclencheur";
			_conditions = new Vector.<Condition>();
			_consequences = new Vector.<Consequence>();
			_enabled = true;
			
			_preview = new ActionPreview(this);
		}	
		
		
		public function addCondition():Condition
		{
			var c:Condition = new Condition();
			c.behavior = behavior;
			
			conditions.push(c);
			c.addEventListener(ConditionEvent.CONDITION_ACTIVE, conditionActive);
			c.addEventListener(ConditionEvent.CONDITION_INACTIVE, conditionInactive);
			
			c.running = running;
			return c;
		}
		
		public function removeCondition(c:Condition):void
		{
			conditions.splice(conditions.indexOf(c), 1);
			c.clean();
			c.removeEventListener(ConditionEvent.CONDITION_ACTIVE, conditionActive);
			c.removeEventListener(ConditionEvent.CONDITION_INACTIVE, conditionInactive);
			
			
			
		}
		
		public function addConsequence():Consequence
		{
			var c:Consequence = new Consequence();
			consequences.push(c);
			return c;
		}
		
		public function removeConsequence(c:Consequence):void
		{
			c.clean();
			consequences.splice(consequences.indexOf(c),1);
		}
		
		private function clearConditions():void 
		{
			while (conditions.length > 0)
			{
				removeCondition(conditions[0]);
			}
		}
		
		private function clearConsequences():void 
		{
			while (consequences.length > 0)
			{
				removeConsequence(consequences[0]);
			}
		}
		
		
		//Data flow
		private function checkActions():void 
		{
			//trace("checkActions",active);
			var checkAllActive:Boolean = true;
			for each(var c:Condition in conditions)
			{
				if (!c.active) 
				{
					checkAllActive = false;
					break;
				}
			}
			
			active = checkAllActive;
		}
		
		
		private function triggerAllActions():void
		{
			if (!enabled) return;
			
			for each(var c:Consequence in consequences)
			{
				c.trigger();
			}
		}
		
		
		//handlers
		private function conditionActive(e:ConditionEvent):void 
		{
			//trace("Condition active !");
			//[Ben] for now, assume that all conditions work together with "AND" connector, no "OR" support
			checkActions();
			
		}
		
		
		private function conditionInactive(e:ConditionEvent):void 
		{
			//trace("Condition not active !");
			active = false;
		}
		
		
		public function clean():void
		{
			clearConditions();
			clearConsequences();
			preview.clean();
		}
		
		//Project XML saving
		public function getXML():XML 
		{
			var xml:XML = <action>
				<conditions></conditions>
				<consequences></consequences>
			</action>;
			
			xml.@name = actionName;
			xml.@enabled = this.enabled;
			
			for each(var cd:Condition in conditions)
			{
				xml.conditions[0].appendChild(cd.getXML());
			}
			
			for each(var cq:Consequence in consequences)
			{
				xml.consequences[0].appendChild(cq.getXML());
			}
			
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			actionName = data.@name;
			this.enabled = data.@enabled == "true";

			clearConditions();
			clearConsequences();
			
			for each(var cdXML:XML in data.conditions[0].condition)
			{
				var cd:Condition = addCondition();
				cd.loadXML(cdXML);
			}
			
			for each(var cqXML:XML in data.consequences[0].consequence)
			{
				var cq:Consequence = addConsequence();
				cq.loadXML(cqXML);
			}
		}
		
		public function get actionName():String 
		{
			return _actionName;
		}
		
		public function set actionName(value:String):void 
		{
			if (actionName == value) return;
			
			_actionName = value;
			preview.updateActionName();
		}		
		
		public function get preview():ActionPreview 
		{
			return _preview;
		}
		
		public function get conditions():Vector.<Condition> 
		{
			return _conditions;
		}
		
		public function get consequences():Vector.<Consequence> 
		{
			return _consequences;
		}
		
		public function get active():Boolean 
		{
			return _active;
		}
		
		public function set active(value:Boolean):void 
		{
			//trace("[Action] set active :", value);
			if (active == value) return;
			
			_active = value;
			preview.active = value;
			if (value) triggerAllActions();
			dispatchEvent(new ActionEvent(value?ActionEvent.ACTIVE:ActionEvent.INACTIVE,this));
			
		}
		
		public function get running():Boolean 
		{
			return _running;
		}
		
		public function set running(value:Boolean):void 
		{
			_running = value;
			
			for each(var c:Condition in conditions)
			{
				c.running = value;
			}
			
			if (!value)
			{
				active = false;
			}
		}
		
		public function get enabled():Boolean 
		{
			return _enabled;
		}
		
		public function set enabled(value:Boolean):void 
		{
			if (enabled == value) return;
			
			_enabled = value;
			trace("dispatch");
			dispatchEvent(new ActionEvent(value?ActionEvent.ACTION_ENABLED:ActionEvent.ACTION_DISABLED, this));
		}
		
		public function get behavior():Behavior 
		{
			return _behavior;
		}
		
		public function set behavior(value:Behavior):void 
		{
			if (value == behavior) return;
			_behavior = value;
			for each(var c:Condition in conditions) c.behavior = behavior;
		}
	}

}