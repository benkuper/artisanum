package statemachine.behaviors.actions 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ActionEvent extends Event 
	{
		
		static public const ACTIVE:String = "actionActive";
		static public const INACTIVE:String = "actionInactive";
		
		static public const ACTION_EDITOR_CLOSE:String = "actionEditorClose";
		static public const ACTION_EDITOR_OPEN:String = "actionEditorOpen";
		
		static public const ACTION_RENAMED:String = "actionRenamed";
		
		
		static public const ACTION_ENABLED:String = "actionEnabled";
		static public const ACTION_DISABLED:String = "actionDisabled";
		
		static public const ACTION_ADDED:String = "actionAdded";
		static public const ACTION_REMOVED:String = "actionRemoved";
		
		
		static public const SPECIAL_ACTION_ADDED:String = "specialActionAdded";
		static public const SPECIAL_ACTION_REMOVED:String = "specialActionRemoved";
		
		
		
		
		//instruction
		static public const REMOVE_CONDITION:String = "removeCondition";
		static public const REMOVE_ACTION:String = "removeAction";
		static public const REMOVE_CONSEQUENCE:String = "removeConsequence";
		static public const ACTION_CLICK:String = "actionClick";
		
		//condition
		static public const CONDITON_COMPARATOR_CHANGED:String = "conditonComparatorChanged";
		//consequence
		static public const CONSEQUENCE_COMMAND_CHANGED:String = "consequenceCommandChanged";
		

		
		public var action:Action;
		
		public function ActionEvent(type:String, action:Action = null, bubbles:Boolean=true, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			this.action = action;
		} 
		
		public override function clone():Event 
		{ 
			return new ActionEvent(type, action, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ActionEvent", "action", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}