package statemachine.behaviors.actions 
{
	import statemachine.behaviors.Behavior;
	import statemachine.behaviors.BehaviorEvent;
	import benkuper.util.KeyboardUtil;
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import ui.components.Button;
	import ui.components.CheckBox;
	import ui.components.Panel;
	import ui.components.UIEvent;
	import ui.Style;
	import ui.Toaster;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ActionPanel extends Panel
	{		
		
		//ui
		private var addBT:Button;
		private var sequentialBT:CheckBox;
		
		
		//data
		private var actionsList:Vector.<Action>;
		private var _curSequentialAction:int;
		
		private var _running:Boolean;
		public var isSequential:Boolean;
		
		//special
		private var behavior:Behavior;
		
		
		public function ActionPanel(behavior:Behavior) 
		{
			super("Déclencheurs");
			this.behavior = behavior;
			titleTF.textColor = Style.ORANGE;
			
			sequentialBT = new CheckBox("Séquentiel",true);
			addChild(sequentialBT);
			sequentialBT.x = titleTF.x + titleTF.width + 10;
			sequentialBT.y = 5;
			sequentialBT.addEventListener(UIEvent.CHECKBOX_SELECTED, sequentialBTChanged);
			sequentialBT.addEventListener(UIEvent.CHECKBOX_DESELECTED, sequentialBTChanged);
			
			headerHeight = sequentialBT.y + sequentialBT.height + 5;
			baseHeight = 30;
			
			//_transparent = true;
			_borders = true;
			
			actionsList = new Vector.<Action>;
			
			addBT = new Button("Ajouter");
			addBT.width = 50;
			addBT.height = 20;
			mainContainer.addChild(addBT);
			addBT.addEventListener(UIEvent.BUTTON_SELECTED, addBTHandler);
			
			addEventListener(ActionEvent.REMOVE_ACTION, actionRemoveHandler);
			
			
			filters = [];
		}
		
		private function actionEnabled(e:ActionEvent):void 
		{
			trace("action enabled");
			if (isSequential) curSequentialAction = actionsList.indexOf(e.action);
		}
		
		//ui
		override protected function draw():void
		{
			placeElements();
			
			super.draw();
			
			//graphics.clear();
			//graphics.lineStyle(2, Style.UI_NORMAL);
			//graphics.moveTo(5, headerHeight-10);
			//graphics.lineTo(baseWidth - 5, headerHeight-10);
			//graphics.drawRect(5, headerHeight - 10, baseWidth - 10, baseHeight);
			
		}
		
		private function placeElements():void 
		{
			var curY:Number = 0;
			for (var i:int = 0; i < actionsList.length; i++)
			{
				actionsList[i].preview.y = curY;
				curY += actionsList[i].preview.height + 5;
				
			}
			
			curY += 5;
			
			addBT.x = (baseWidth - addBT.width) / 2;
			addBT.y = curY;
			
			curY += addBT.height;
			
			
			sequentialBT.x = titleTF.x + titleTF.width + 10;
			
			var prevBH:Number = baseHeight;
			baseHeight = headerHeight + curY + 5;
			if(prevBH != baseHeight) dispatchEvent(new UIEvent(UIEvent.PANEL_RESIZED));
		}
		
		
		public function addAction(sourceAction:Action = null):Action 
		{
			var a:Action = new Action();
			a.behavior = behavior;
			
			mainContainer.addChild(a.preview);
			actionsList.push(a);
			a.actionName = "Déclencheur " + actionsList.length;
			a.addEventListener(ActionEvent.ACTIVE, actionActiveHandler);
			a.addEventListener(ActionEvent.ACTION_ENABLED, actionEnabled);
			
			a.preview.x = 10;
			
			a.preview.width = baseWidth - 20;
			a.preview.height = 20;
			
			if (sourceAction == null)
			{
				a.addCondition();
				a.addConsequence();
			}else
			{
				a.loadXML(sourceAction.getXML());
			}
			
			a.running = running;
			
			TweenLite.fromTo(a.preview, .5, { scaleY:0 , alpha:0}, { alpha:1, scaleY:1, ease:Strong.easeOut, onUpdate:draw } );
			
			dispatchEvent(new BehaviorEvent(BehaviorEvent.ELEMENT_ADDED));
			dispatchEvent(new ActionEvent(ActionEvent.ACTION_ADDED, a));
			
			//see if it's not too much cost on performance
			KeyboardUtil.addTarget(a.preview,actionKeyDown);
			
			return a;
		}
		
		public function duplicateEditingAction():void
		{
			addAction(getEditingAction());
		}
		
		
		public function removeAction(a:Action = null,animate:Boolean = true):void
		{
			if (a == null) a = getEditingAction();
			if (a == null) return;
			
			a.removeEventListener(ActionEvent.ACTIVE, actionActiveHandler);
			a.addEventListener(ActionEvent.ACTION_ENABLED, actionEnabled);
			
			if (animate) 
			{
				TweenLite.to(a.preview, .5,  { scaleY:0, alpha:0, ease:Strong.easeOut, onUpdate:draw, onComplete:finishRemoveAction,onCompleteParams:[a] } );
			}else
			{
				finishRemoveAction(a);
			}
			
			KeyboardUtil.removeTarget(a.preview,actionKeyDown);
		}
		
		
		
		public function finishRemoveAction(a:Action ):void 
		{
			a.clean();
			mainContainer.removeChild(a.preview);
			actionsList.splice(actionsList.indexOf(a), 1);
			
			Toaster.info("Action supprimée :" + a.actionName);
			
			dispatchEvent(new BehaviorEvent(BehaviorEvent.ELEMENT_REMOVED));
			dispatchEvent(new ActionEvent(ActionEvent.ACTION_REMOVED, a));
			
			draw();
		}
		
		
		public function moveAction(a:Action = null, direction:String = "up"):void 
		{
			if (a == null) a = getEditingAction();
			if (a == null) return;
			
			var aIndex:int = actionsList.indexOf(a);
			var tIndex:int = (direction == "up")?aIndex - 1:aIndex + 1;
			actionsList.splice(aIndex, 1);
			actionsList.splice(tIndex, 0, a);
			
			placeElements();
		}
		
		
		private function clearActions():void 
		{
			while (actionsList.length > 0)
			{
				removeAction(actionsList[0], false);
			}
		}
		
		private function actionRemoveHandler(e:ActionEvent):void 
		{
			removeAction(e.action);
		}
		
		
		//handlers
		
		private function addBTHandler(e:UIEvent):void 
		{
			addAction();
		}		
		
		
		private function actionActiveHandler(e:ActionEvent):void 
		{
			//trace("Action active !",actionsList.indexOf(e.action),e.action.enabled);
			if (!e.action.enabled) return;
			
			if (isSequential && actionsList.indexOf(e.action) == curSequentialAction)
			{
				curSequentialAction++;
			}
		}
		
		private function sequentialBTChanged(e:UIEvent):void 
		{
			
			isSequential = sequentialBT.selected;
			
			if (isSequential)
			{
				curSequentialAction = 0;
			}
		}
		
		
		private function actionKeyDown(e:KeyboardEvent):void 
		{
			var a:Action;
			
			//trace("key down action: " + this);
			if (e.type != KeyboardEvent.KEY_DOWN) return;
			switch(e.keyCode)
			{
				case Keyboard.D:
					if (e.ctrlKey)
					{
						a = getEditingAction();
						if (a != null) addAction(a);
					}
					
					break;
					
				case Keyboard.UP:
				case Keyboard.DOWN:
					if (e.ctrlKey)
					{
						a = getEditingAction();
						moveAction(a, (e.keyCode == Keyboard.UP)?"up":"down");
					}
					break;
			}
		}
		
		//util
		private function getEditingAction():Action
		{
			for each(var a:Action in actionsList)
			{
				if (a.preview.editing) return a;
			}
			
			return null;
		}
		
		//cleaning
		override public function clean():void
		{
			super.clean();
			clearActions();
			sequentialBT.removeEventListener(UIEvent.BUTTON_SELECTED, sequentialBTChanged);
			sequentialBT.removeEventListener(UIEvent.BUTTON_DESELECTED, sequentialBTChanged);
			
			addBT.removeEventListener(UIEvent.BUTTON_SELECTED, addBTHandler);
			
			removeEventListener(ActionEvent.REMOVE_ACTION, actionRemoveHandler);
			removeEventListener(ActionEvent.ACTION_ENABLED, actionEnabled);
			
		}
		
		//Project XML saving
		public function getXML(baseXML:XML):XML 
		{
			var xml:XML = baseXML;
			xml.@isSequential = isSequential;
			
			for each(var a:Action in actionsList)
			{
				xml.appendChild(a.getXML());
			}
			
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			//trace(data.@isSequential);
			sequentialBT.selected = data.@isSequential == "true";
			
			
			clearActions();
			for each(var aXML:XML in data.action)
			{
				var a:Action  = addAction();
				a.loadXML(aXML);
			}
		}
		
		public function get running():Boolean 
		{
			return _running;
		}
		
		public function set running(value:Boolean):void 
		{
			_running = value;
			
			for each(var a:Action in actionsList)
			{
				a.running = value;
			}
		}
		
		public function get curSequentialAction():int 
		{
			return _curSequentialAction;
		}
		
		public function set curSequentialAction(value:int):void 
		{
			if (value == actionsList.length) value = 0;
			
			_curSequentialAction = value;
			
			for (var i:int = 0; i < actionsList.length; i++)
			{
				if (i != value) actionsList[i].enabled = false;
			}
			
			if(value < actionsList.length) actionsList[value].enabled = true;
		}
		
		
	}

}