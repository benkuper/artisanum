package statemachine.modules.commands 
{
	import modules.output.commands.Command;
	import modules.parameters.ParameterEnum;
	import statemachine.behaviors.Behavior;
	import statemachine.behaviors.BehaviorEvent;
	import statemachine.StateMachine;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class StateMachineBehaviorCommand extends Command 
	{
		protected var op:String;
		protected var stateMachine:StateMachine;
		
		protected var behaviorParam:ParameterEnum;
		
		public function StateMachineBehaviorCommand(label:String, guiMode:String="") 
		{
			super(label, guiMode);
			behaviorParam = addParameter(new ParameterEnum("targetBehavior", "[Comportement]", null), "Comportement cible", true) as ParameterEnum;
			behaviorParam.guiWidth = 200;
		}
		
		override protected function setProp(propName:String, value:*):void 
		{
			super.setProp(propName, value);
			switch(propName)
			{
				case "op":
					this.op = value;
					break;
				
				case "stateMachine":
					this.stateMachine = value as StateMachine;
					stateMachine.addEventListener(BehaviorEvent.BEHAVIOR_ADDED, behaviorAdded);
					stateMachine.addEventListener(BehaviorEvent.BEHAVIOR_REMOVED, behaviorRemoved);
					stateMachine.addEventListener(BehaviorEvent.LABEL_CHANGED, behaviorLabelChanged);
					updateBehaviorOptions();
					break;
			}
		}
		
		
		
		override public function trigger():void 
		{
			super.trigger();
			switch(op)
			{
				case "activate":
					if (behaviorParam.selectedItem != null) (behaviorParam.selectedItem.value as Behavior).active = true;
					break;
					
				case "deactivate":
					if (behaviorParam.selectedItem != null) (behaviorParam.selectedItem.value as Behavior).active = false;
					break;
			}
		}
		
		private function updateBehaviorOptions():void
		{
			var options:Array = new Array();
			for each(var b:Behavior in stateMachine.behaviorsList) options.push( { id:b.id, label:b.behaviorName, value:b } );
			
			behaviorParam.setOptions(options);
		}
		
		private function behaviorAdded(e:BehaviorEvent):void 
		{
			updateBehaviorOptions();
		}
		
		private function behaviorRemoved(e:BehaviorEvent):void 
		{
			updateBehaviorOptions();
		}
		
		private function behaviorLabelChanged(e:BehaviorEvent):void 
		{
			updateBehaviorOptions();
		}
		
		override public function clean():void 
		{
			super.clean();
			if (stateMachine != null) 
			{
				stateMachine.removeEventListener(BehaviorEvent.BEHAVIOR_ADDED, behaviorAdded);
				stateMachine.removeEventListener(BehaviorEvent.BEHAVIOR_REMOVED, behaviorRemoved);
				stateMachine.removeEventListener(BehaviorEvent.LABEL_CHANGED, behaviorLabelChanged);
			}
			
		}
		
		
		
		
	}

}