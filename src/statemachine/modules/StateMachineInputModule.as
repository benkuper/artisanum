package statemachine.modules 
{
	import modules.input.InputModule;
	import statemachine.StateMachine;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class StateMachineInputModule extends InputModule 
	{
		
		private var stateMachine:StateMachine;
		
		public function StateMachineInputModule(stateMachine:StateMachine) 
		{
			super(true);
			this.stateMachine = stateMachine;
			this.id = "stateMachineInput";
		}
		
	}

}