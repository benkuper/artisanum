package statemachine.modules 
{
	import modules.output.commands.CommandType;
	import modules.output.OutputModule;
	import statemachine.modules.commands.StateMachineBehaviorCommand;
	import statemachine.StateMachine;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class StateMachineOutputModule extends OutputModule 
	{
		private var stateMachine:StateMachine;
		
		public function StateMachineOutputModule(stateMachine:StateMachine) 
		{
			super();
			this.stateMachine = stateMachine;
			this.id = "stateMachineOutput";

			addCommandTypes(
				new CommandType("activeBehavior","Activer", StateMachineBehaviorCommand, CommandType.TRIGGER, { op:"activate",stateMachine:stateMachine }),
				new CommandType("deactivateBehavior","Désactiver", StateMachineBehaviorCommand, CommandType.TRIGGER, { op:"deactivate", stateMachine:stateMachine } )
			);
		}
		
		
	}

}