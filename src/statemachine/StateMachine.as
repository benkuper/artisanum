package statemachine
{
	import benkuper.util.KeyboardUtil;
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import com.laiyonghao.Uuid;
	import flash.display.NativeMenuItem;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.registerClassAlias;
	import flash.ui.ContextMenu;
	import flash.ui.Keyboard;
	import flash.utils.ByteArray;
	import statemachine.behaviors.actions.ActionEditor;
	import statemachine.behaviors.Behavior;
	import statemachine.behaviors.BehaviorEvent;
	import statemachine.behaviors.mappings.MappingEditor;
	import statemachine.behaviors.transitions.Transition;
	import statemachine.modules.StateMachineInputModule;
	import statemachine.modules.StateMachineOutputModule;
	import ui.components.DropDown;
	import ui.menu.Menu;
	//import statemachine.behaviors.transitions.TransitionEditor;
	import statemachine.behaviors.transitions.TransitionEvent;
	import ui.components.Button;
	import ui.components.UIEvent;
	import ui.guider.Guider;
	import ui.Style;
	import utils.geom.getRectangleCenter;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class StateMachine extends Sprite
	{
		//layout
		private var baseWidth:Number;
		private var baseHeight:Number;
		
		//dragging
		private var containerOffset:Point;
		private var mouseOffset:Point;
		private var dragging:Boolean;

		//ui
		private var bg:Sprite;
		private var container:Sprite;
		private var transitionContainer:Sprite;
		
		//context
		private var miniModeAllMI:NativeMenuItem;
		private var addBehaviorMI:NativeMenuItem;
		
		//data
		private var transitionsList:Vector.<Transition>;
		public var behaviorsList:Vector.<Behavior>;
		private var addBehaviorBT:Button;
		
		private var _activeBehavior:Behavior;
		private var _selectedBehavior:Behavior;
		
		//action editor
		private var actionEditor:ActionEditor;
		
		//transition editor
		//private var transitionEditor:TransitionEditor;
		
		//mapping editor
		private var mappingEditor:MappingEditor;
		
		
		//stateMachine input/outputmodule
		private var smInput:StateMachineInputModule;
		private var smOutput:StateMachineOutputModule;
		
		//static
		public static var instance:StateMachine;
		
		public function StateMachine()
		{
			instance = this;
			
			baseWidth = 100;
			baseHeight = 100;
			
			behaviorsList = new Vector.<Behavior>;
			transitionsList = new Vector.<Transition>;
			
			bg = new Sprite();
			addChild(bg);
			
			KeyboardUtil.addTarget(bg, bgKeyDown);
			KeyboardUtil.addTarget(this, stateMachineKeyDown);
			
			//dragging
			bg.addEventListener(MouseEvent.MOUSE_DOWN, bgMouseHandler);
			containerOffset = new Point();
			
			container = new Sprite();
			addChild(container);
			
			transitionContainer = new Sprite();
			container.addChild(transitionContainer);
			
			actionEditor = new ActionEditor();
			addChild(actionEditor);
			
			//transitionEditor = new TransitionEditor();
			//addChild(transitionEditor);
			
			mappingEditor = new MappingEditor();
			addChild(mappingEditor);
			
			addBehaviorBT = new Button("Ajouter un comportement");
			addChild(addBehaviorBT);
			addBehaviorBT.addEventListener(UIEvent.BUTTON_SELECTED, addBehaviorBTHandler);
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			addEventListener(BehaviorEvent.BEHAVIOR_REMOVED, behaviorRemoved);
			addEventListener(BehaviorEvent.BEHAVIOR_SELECTED, behaviorSelected);
			
			addEventListener(BehaviorEvent.ACTION_SELECTED, actionSelected);
			addEventListener(BehaviorEvent.ACTION_DESELECTED, actionDeselected);
			
			addEventListener(BehaviorEvent.MAPPING_SELECTED, mappingSelected);
			addEventListener(BehaviorEvent.MAPPING_DESELECTED, mappingDeselected)
			
			addEventListener(BehaviorEvent.BEHAVIOR_ACTIVATED, behaviorActivated);
			addEventListener(BehaviorEvent.BEHAVIOR_DEACTIVATED, behaviorDeactivated)
			
			addEventListener(BehaviorEvent.CREATE_TRANSITION, behaviorCreateTransition);
			addEventListener(BehaviorEvent.TRANSITION_CREATED, transitionCreatedHandler);
			addEventListener(BehaviorEvent.TRANSITION_CREATION_CANCELED, transitionCreationCanceledHandler);
			
			addEventListener(TransitionEvent.EDIT_TRANSITION, editTransitionHandler);
			addEventListener(TransitionEvent.REMOVE_TRANSITION, removeTransitionHandler);
			addEventListener(TransitionEvent.TRANSITION_TRIGGER, transitionTrigger);
			
			bg.contextMenu = new ContextMenu();
			addBehaviorMI = new NativeMenuItem("Ajouter un comportement");
			miniModeAllMI = new NativeMenuItem("Réduire tous les comportements");
			bg.contextMenu.addItem(addBehaviorMI);
			bg.contextMenu.addItem(miniModeAllMI);
			bg.contextMenu.addEventListener(Event.SELECT, bgContextMenuSelect);
		
			
			
			smInput = new StateMachineInputModule(this);
			smOutput = new StateMachineOutputModule(this);
			
			//Menu
			Menu.addMenu("addBehavior", "State Machine/Ajouter un comportement", addBehavior);
			Menu.addSeparator("State Machine");
			
			Menu.addMenu("addAction","State Machine/Ajouter un déclencheur",null,"",null,false);
			Menu.addMenu("addMapping", "State Machine/Ajouter un mapping", null, "", null, false);
			Menu.addMenu("duplicateBehavior","State Machine/Dupliquer le comportement", duplicateSelectedBehavior,"",null,false);
			Menu.addMenu("removeBehavior","State Machine/Supprimer le comportement",removeBehavior,"",null,false);
			
			Menu.addSeparator("State Machine");
			Menu.addMenu("duplicateAction","State Machine/Dupliquer le déclencheur", null,"",null,false);
			Menu.addMenu("removeAction","State Machine/Supprimer le déclencheur", null,"",null,false);
			
			Menu.addSeparator("State Machine");
			Menu.addMenu("duplicateMapping","State Machine/Dupliquer le mapping", null,"",null,false);
			Menu.addMenu("removeMapping", "State Machine/Supprimer le mapping", null, "", null, false);
			
			Menu.addSeparator("State Machine");
			Menu.addMenu("permanentBehavior","State Machine/Comporement Permanent", null,"",null,false,true);
			
			Menu.addSeparator("State Machine");
			Menu.addMenu("onlyOneActiveBehavior", "State Machine/Un seul comportement actif à la fois", null, "", null, true, true);
			//to implement in GlobalSettings
			Menu.setChecked("onlyOneActiveBehavior", true);
			
		}
		
		//Data
		private function addBehavior(sourceBehavior:Behavior = null,initX:Number = NaN, initY:Number = NaN):Behavior
		{
			var b:Behavior = new Behavior();
			
			var sourceName:String = b.behaviorName;
			
			if (sourceBehavior != null)
			{
				b.loadXML(sourceBehavior.getXML());
				b.id = new Uuid().toString();
				sourceName = sourceBehavior.behaviorName;
				b.x = sourceBehavior.x + sourceBehavior.width + 20;
				b.y = sourceBehavior.y;
				
			}else
			{
				if (!isNaN(initX) && !isNaN(initY))
				{
					b.x = initX - containerOffset.x;
					b.y = initY - containerOffset.y;
				}else
				{
					b.x = (baseWidth - b.width) / 2 - containerOffset.x;
					b.y = (baseHeight - b.height) / 2 - containerOffset.y;
					
					for each(var be:Behavior in behaviorsList)
					{
						if (b.x == be.x) b.x += be.width;
					}
				}
			}
			
			b.title = getUniqueNameFor(b, sourceName);
			
			behaviorsList.push(b);
			container.addChild(b);
			b.moveable = true;
			
			TweenLite.fromTo(b, .3, {scaleX: 0, scaleY: 0}, {scaleX: 1, scaleY: 1});
			
			
			
			
			
			//temp
			if(activeBehavior == null) activeBehavior = b;
			
			dispatchEvent(new BehaviorEvent(BehaviorEvent.BEHAVIOR_ADDED));
			
			return b;
		}
		
		private function getUniqueNameFor(b:Behavior, sourceName:String):String 
		{
			for each(var tb:Behavior in behaviorsList)
			{
				if (tb == b) continue;
				
				if (tb.behaviorName == sourceName)
				{
					var nextName:String;
					var lastN:Number = Number(sourceName.slice(sourceName.length - 1));
					if (!isNaN(lastN))
					{
						nextName = sourceName.slice(0, sourceName.length - 1) + (lastN + 1);
					}else
					{
						nextName = sourceName+" 2";
					}
					
					return getUniqueNameFor(b, nextName);
				}
			}
			
			
			return sourceName;
		}
		
		
		private function removeBehavior(b:Behavior = null):void
		{
			if (b == null) b = selectedBehavior;
			if (b == null) return;
			
			behaviorsList.splice(behaviorsList.indexOf(b), 1);
			container.removeChild(b);
			b.clean();
			
			var ti:int = 0;
			while (ti < transitionsList.length)
			{
				if (transitionsList[ti].isConnectedToBehavior(b))
					removeTransition(transitionsList[ti]);
				else
					ti++;
			}
		}
		
		private function duplicateSelectedBehavior():void
		{
			if (selectedBehavior == null) return;
			addBehavior(selectedBehavior);
		}
		
		
		private function clearBehaviors():void
		{
			while (behaviorsList.length > 0)
			{
				removeBehavior(behaviorsList[0]);
			}
		}
		
		public function getBehaviorByID(id:String):Behavior
		{
			for each (var b:Behavior in behaviorsList)
			{
				if (b.id == id)
					return b;
			}
			
			return null;
		}
		
		private function addTransition(fromBehavior:Behavior, toBehavior:Behavior, creationMode:Boolean = false):Transition
		{
			var t:Transition = new Transition(fromBehavior, toBehavior);
			if (creationMode)
			{
				addChild(t);
				t.creationMode = creationMode;
				
			}else
			{
				transitionContainer.addChild(t);
				
				if (fromBehavior != null && toBehavior != null)
				{
					t.action.actionName = fromBehavior.name + " > " + toBehavior.name;
				}
			}
			
			transitionsList.push(t);
			
			return t;
		}
		
		private function removeTransition(t:Transition):void
		{
			if (transitionContainer.contains(t))
				transitionContainer.removeChild(t);
			else
				removeChild(t);
			transitionsList.splice(transitionsList.indexOf(t), 1);
			t.clean();
			
			//if (transitionEditor.currentTransition == t)
				//transitionEditor.currentTransition = null;
		}
		
		private function clearTransitions():void
		{
			while (transitionsList.length > 0)
			{
				removeTransition(transitionsList[0]);
			}
		}
		
		private function updateTransitions():void
		{
			for each (var t:Transition in transitionsList)
			{
				if (selectedBehavior == t.fromBehavior)
					t.role = Transition.ROLE_TO;
				else if (selectedBehavior == t.toBehavior)
					t.role = Transition.ROLE_FROM;
				else
					t.role = Transition.ROLE_NONE;
				
				t.active = (activeBehavior == t.fromBehavior);
			}
		}
		
		public function switchMiniModeAllBehaviors():void
		{
			var b:Behavior;
			//find if collapse or expand, when at least one behavior is not in miniMode, set miniMode to all
			var targetMiniMode:Boolean = false;
			for each (b in behaviorsList)
			{
				if (!b.miniMode)
				{
					targetMiniMode = true;
					break;
				}
			}
			
			for each (b in behaviorsList)
			{
				b.miniMode = targetMiniMode;
			}
		
		}
		
		//ui
		protected function draw():void
		{
			drawBG();
			addBehaviorBT.x = (baseWidth - addBehaviorBT.width) / 2;
			addBehaviorBT.y = baseHeight - addBehaviorBT.height - 10;
			
			actionEditor.x = (baseWidth - actionEditor.width) / 2;
			//transitionEditor.x = (baseWidth - transitionEditor.width) / 2;
			mappingEditor.x = (baseWidth - mappingEditor.width) / 2;
		}
		
		
		//bg
		private function drawBG():void
		{
			
			bg.graphics.clear();
			bg.graphics.beginFill(Style.BG_COLOR);
			bg.graphics.drawRect(0, 0, baseWidth, baseHeight);
			bg.graphics.endFill();
			
			var gridSize:int = 20;
			var spacing1:int = 6;
			var spacing2:int = 3;
			var i:Number;
			
			var gridOffsetX:Number = container.x % (gridSize*spacing1);
			var gridOffsetY:Number = container.y % (gridSize*spacing1);
			
			//trace(gridOffsetX, gridOffsetY,containerOffset.y);
			
			var curLine:int = 0;
			for (i = -gridSize+gridOffsetX; i < baseWidth+gridSize; i += gridSize)
			{
				if (curLine % spacing1 == 0)
					bg.graphics.lineStyle(1, Style.GRID1);
				else if (curLine % spacing2 == 0)
					bg.graphics.lineStyle(1, Style.GRID2);
				else
					bg.graphics.lineStyle(1, Style.GRID3);
				
				bg.graphics.moveTo(i, 0);
				bg.graphics.lineTo(i, baseHeight);
				curLine++;
			}
			
			curLine = 0;
			for (i = -gridSize+gridOffsetY; i < baseHeight+gridSize; i += gridSize)
			{
				if (curLine % spacing1 == 0)
					bg.graphics.lineStyle(1, Style.GRID1);
				else if (curLine % spacing2 == 0)
					bg.graphics.lineStyle(1, Style.GRID2);
				else
					bg.graphics.lineStyle(1, Style.GRID3);
				
				bg.graphics.moveTo(0, i);
				bg.graphics.lineTo(baseWidth, i);
				curLine++;
			}
		
		}
		
		//view
		public function frameView():void 
		{
			var containerBounds:Rectangle = container.getBounds(container);
			var centerPoint:Point = getRectangleCenter(containerBounds);
			setContainerOffset(-centerPoint.x+stage.stageWidth/2, -centerPoint.y+stage.stageHeight/2,.6);
		}
		
		public function homeView():void
		{
			setContainerOffset(0, 0);
		}
		
		public function setContainerOffset(tx:Number, ty:Number, smoothing:Number = 0):void
		{
			containerOffset.x = tx;
			containerOffset.y = ty;
			if (smoothing == 0)
			{
				container.x = containerOffset.x;
				container.y = containerOffset.y;
				drawBG();
			}else
			{
				TweenLite.to(container, smoothing, { x:containerOffset.x, y:containerOffset.y, ease:Strong.easeOut,onUpdate:drawBG } );
			}
			
			
		}
		
		//handlers
		private function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		private function addBehaviorBTHandler(e:UIEvent):void
		{
			addBehavior();
		}
		
		private function behaviorRemoved(e:BehaviorEvent):void
		{
			removeBehavior(e.target as Behavior);
		}
		
		private function behaviorSelected(e:BehaviorEvent):void
		{
			selectedBehavior = e.target as Behavior;
		}
		
		private function actionSelected(e:BehaviorEvent):void
		{
			if (e.action != null) selectedBehavior = null;
			
			if (!KeyboardUtil.altPressed) actionEditor.currentAction = e.action;
			else e.action.preview.editing = true;
			
			//transitionEditor.currentTransition = null;
			mappingEditor.currentMapping = null;
		}
		
		
		private function actionDeselected(e:BehaviorEvent):void
		{
			//trace("action deselected", e.action, actionEditor.currentAction);
			if (e.action == actionEditor.currentAction)
				actionEditor.currentAction = null;
			else e.action.preview.editing = false;
							
			DropDown.currentOpenedDropDown = null;
		}
		
		private function mappingSelected(e:BehaviorEvent):void
		{
			if (e.mapping != null) selectedBehavior = null;
			
			if (!KeyboardUtil.altPressed) mappingEditor.currentMapping = e.mapping;
			else e.mapping.preview.editing = true;
			
			actionEditor.currentAction = null;
		}
		
		private function mappingDeselected(e:BehaviorEvent):void
		{
			//trace("mapping deselected", e.mapping,  mappingEditor.currentMapping);
			if (e.mapping == mappingEditor.currentMapping)
				mappingEditor.currentMapping = null;
			else e.mapping.preview.editing = false;
				
			DropDown.currentOpenedDropDown = null;
		}
		
		private function bgContextMenuSelect(e:Event):void
		{
			switch (e.target)
			{
				case addBehaviorMI:
					addBehavior(null, bg.mouseX, bg.mouseY);
					break;
					
				case miniModeAllMI: 
					switchMiniModeAllBehaviors();
					break;
			}
		}
		
		private function bgMouseHandler(e:MouseEvent):void 
		{
			switch(e.type)
			{
				case MouseEvent.MOUSE_DOWN:
					
					mouseOffset = new Point(stage.mouseX, stage.mouseY);
					stage.addEventListener(MouseEvent.MOUSE_UP, bgMouseHandler);
					
					dragging = e.shiftKey;
					if(dragging) stage.addEventListener(Event.ENTER_FRAME, bgMouseEnterFrame);
					
					break;
					
				case MouseEvent.MOUSE_UP:
					
					stage.removeEventListener(MouseEvent.MOUSE_UP, bgMouseHandler);
					
					if (dragging)
					{
						stage.removeEventListener(Event.ENTER_FRAME, bgMouseEnterFrame);
						
						//var offset:Point = new Point(stage.mouseX - mouseOffset.x, stage.mouseY - mouseOffset.y);
						//setContainerOffset(offset.x, offset.y);
					
					}else
					{
						selectedBehavior = null;
						//transitionEditor.currentTransition = null;
						mappingEditor.currentMapping = null;
					}
					
					
					
					break;
			}
		}
		
		private function bgMouseEnterFrame(e:Event):void 
		{
			if (!dragging) return;
			
			var offset:Point = new Point(stage.mouseX - mouseOffset.x, stage.mouseY - mouseOffset.y);
			setContainerOffset(containerOffset.x + offset.x, containerOffset.y + offset.y);
			mouseOffset.x = stage.mouseX;
			mouseOffset.y = stage.mouseY;
			//trace(stage.mouseY,mouseOffset.y,
			drawBG();
		}
		
		private function bgKeyDown(e:KeyboardEvent):void 
		{
			if (e.type != KeyboardEvent.KEY_DOWN) return;
			
			switch(e.keyCode)
			{
				case Keyboard.F:
					frameView();
					break;
			}
		}
		
		private function stateMachineKeyDown(e:KeyboardEvent):void 
		{
			if (e.type != KeyboardEvent.KEY_DOWN) return;
			
			//trace("stateMachine Key down !");
			switch(e.keyCode)
			{
				case Keyboard.D:
					if (e.ctrlKey && selectedBehavior != null)
					{
						addBehavior(selectedBehavior);
					}
					break;
			}
		}
		
		
		private function behaviorActivated(e:BehaviorEvent):void
		{
			if (activeBehavior != e.target as Behavior)
				activeBehavior = e.target as Behavior;
		}
		
		private function behaviorDeactivated(e:BehaviorEvent):void
		{
			if (activeBehavior == e.target as Behavior)
				activeBehavior = null;
		}
		
		private function behaviorCreateTransition(e:BehaviorEvent):void
		{
			var t:Transition = addTransition(e.target as Behavior, null, true);
		}
		
		private function transitionCreatedHandler(e:BehaviorEvent):void
		{
			transitionContainer.addChild(e.target as Transition);
		}
		
		private function transitionCreationCanceledHandler(e:BehaviorEvent):void
		{
			removeTransition(e.target as Transition);
		}
		
		private function editTransitionHandler(e:TransitionEvent):void
		{
			//transitionEditor.currentTransition = e.target as Transition;
			actionEditor.currentAction = (e.target as Transition).action;
			mappingEditor.currentMapping = null;
		}
		
		private function removeTransitionHandler(e:TransitionEvent):void
		{
			removeTransition(e.target as Transition);
		}
		
		private function transitionTrigger(e:TransitionEvent):void
		{
			var t:Transition = e.target as Transition;
			if (activeBehavior == t.fromBehavior)
			{
				activeBehavior = t.toBehavior;
			}
		}
		
		//Project XML saving
		public function getXML():XML
		{
			var xml:XML =  <stateMachine></stateMachine>;
			for each (var b:Behavior in behaviorsList)
			{
				xml.appendChild(b.getXML());
			}
			
			for each(var t:Transition in transitionsList)
			{
				xml.appendChild(t.getXML());
			}
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			if (data == null) return; 
			
			clearBehaviors();
			for each (var bXML:XML in data.behavior)
			{
				var b:Behavior = addBehavior();
				b.loadXML(bXML);
			}
			
			clearTransitions();
			for each(var tXML:XML in data.transition)
			{
				var t:Transition = addTransition(null, null);
				t.loadXML(tXML);
			}
		}
		
		public function reset():void
		{
			clearBehaviors();
			clearTransitions();
		}
		
		public function showFirstGuider():void
		{
			Guider.show(addBehaviorBT, "Maintenant nous pouvons ajouter un comportement !");
		}
		
		override public function set width(value:Number):void
		{
			baseWidth = value;
			draw();
		}
		
		override public function set height(value:Number):void
		{
			baseHeight = value;
			draw();
		}
		
		public function get activeBehavior():Behavior
		{
			return _activeBehavior;
		}
		
		public function set activeBehavior(value:Behavior):void
		{
			if (Menu.getChecked("onlyOneActiveBehavior")) 
			{
				for each(var b:Behavior in behaviorsList)
				{
					if (!b.permanent && b != value) b.active = false;
				}
			}
			
			_activeBehavior = value;
			
			if (activeBehavior != null)
			{
				activeBehavior.active = true;
			}
			
			updateTransitions();
		}
		
		public function get selectedBehavior():Behavior
		{
			return _selectedBehavior;
		}
		
		public function set selectedBehavior(value:Behavior):void
		{
			if (selectedBehavior != null)
			{
				selectedBehavior.selected = false;
			}
			
			_selectedBehavior = value;
			
			actionEditor.currentAction = null;
			mappingEditor.currentMapping = null;
			
			updateTransitions();
			
			
		}
	
	}

}