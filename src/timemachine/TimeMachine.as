package timemachine 
{
	import com.greensock.TweenLite;
	import flash.events.Event;
	import timemachine.modules.TimeMachineInputModule;
	import timemachine.modules.TimeMachineOutputModule;
	import timemachine.sequences.Sequence;
	import timemachine.sequences.SequenceEvent;
	import timemachine.sequences.SequencesPanel;
	import timemachine.sequences.timeline.TimelineEditor;
	import timemachine.sequences.trigger.TriggerEditor;
	import timemachine.sequences.trigger.TriggerEvent;
	import timemachine.sequences.trigger.TriggerHandle;
	import ui.components.Button;
	import ui.components.Panel;
	import ui.components.UIEvent;
	import ui.menu.Menu;
	import ui.Style;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TimeMachine extends Panel
	{
		
		//layout
		private var _opened:Boolean;
		private var _selectedSequence:Sequence;
		
		//ui
		private var switcher:Button;
		private var sequencePanel:SequencesPanel;
		private var timelineEditor:TimelineEditor;
		
		private var triggerEditor:TriggerEditor;
		private var tmInput:TimeMachineInputModule;
		private var tmOutput:TimeMachineOutputModule;
		
		public function TimeMachine() 
		{
			super("Time Machine");
			this.titleTF.visible = false;
			_bgColor = Style.UI_PANEL;
			headerHeight = 10;
			this.y = 100;
			
			triggerEditor = new TriggerEditor();
			triggerEditor.addEventListener(TriggerEvent.TRIGGER_EDITOR_OPEN, triggerEditorOpenHandler);
			triggerEditor.addEventListener(TriggerEvent.TRIGGER_EDITOR_CLOSE, triggerEditorOpenHandler);
			addChildAt(triggerEditor,0);
			
			sequencePanel = new SequencesPanel();
			sequencePanel.width = 300;
			addElement(sequencePanel);
			
			timelineEditor = new TimelineEditor();
			addElement(timelineEditor);
			
			switcher = new Button("");
			switcher.roundCorners = Button.ROUND_NONE;
			addChildAt(switcher,this.getChildIndex(mainContainer)-1);
			switcher.addEventListener(UIEvent.BUTTON_SELECTED, switcherHandler);
			
			
			tmInput = new TimeMachineInputModule(this);
			tmOutput = new TimeMachineOutputModule(this);
			
			
			baseWidth = 200;
			baseHeight = 250;
			draw();
			
			
			addEventListener(SequenceEvent.SELECT_SEQUENCE, selectSequenceHandler);
			
			addEventListener(TriggerEvent.TRIGGER_SELECTED, triggerSelectHandler);
			addEventListener(TriggerEvent.TRIGGER_DESELECTED, triggerSelectHandler);
			
			addEventListener(SequenceEvent.SEQUENCE_ADDED, sequenceAdded);
			addEventListener(SequenceEvent.SEQUENCE_REMOVED, sequenceRemoved);
		}
		
		
		
		
		//ui
		override protected function draw():void 
		{
			super.draw();
			switcher.width = baseWidth;
			switcher.height = 10;
			switcher.y = -switcher.height;
			
			sequencePanel.x = 10;
			sequencePanel.height = baseHeight - headerHeight - 50;
			
			timelineEditor.x = sequencePanel.x + sequencePanel.width + 5;
			timelineEditor.width = baseWidth - timelineEditor.x -10;
			timelineEditor.height = baseHeight - headerHeight -10;
			
			triggerEditor.x = (baseWidth - triggerEditor.width) / 2;
			
		}
		
		//data / control
		
		public function stopAllSequences():void 
		{
			//trace("stop all sequences", sequencesList.length);
			for each(var s:Sequence in sequencesList)
			{
				s.stop();
			}
		}
		
		
		//handlers
		
		override protected function addedToStage(e:Event):void 
		{
			super.addedToStage(e);
			this.y = stage.stageHeight - 40;
			draw();
		}
		
		private function switcherHandler(e:UIEvent):void 
		{
			opened = !opened;
		}
		
		private function selectSequenceHandler(e:SequenceEvent):void 
		{
			selectedSequence = e.sequence;
		}
		
		private function triggerSelectHandler(e:TriggerEvent):void 
		{
			if (e.type == TriggerEvent.TRIGGER_SELECTED)
			{
				triggerEditor.currentTrigger = (e.target as TriggerHandle).sequenceTrigger;
			}else
			{
				triggerEditor.currentTrigger = null;
			}
		}
		
		private function triggerEditorOpenHandler(e:TriggerEvent):void 
		{
			if (e.type == TriggerEvent.TRIGGER_EDITOR_CLOSE) TriggerHandle.selectedHandle = null;
		}
		
		private function sequenceAdded(e:SequenceEvent):void 
		{
			//trace("TimeMachine :: sequence added !");
		}
		
		private function sequenceRemoved(e:SequenceEvent):void 
		{
			if (timelineEditor.currentSequence == e.sequence) timelineEditor.currentSequence = null;
		}
		
		//load / save
		public function getXML():XML
		{
			var xml:XML =  <timeMachine></timeMachine>;
			xml.appendChild(sequencePanel.getXML());
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			if (data == null) return; 
			sequencePanel.loadXML(data.sequencePanel[0]);
			
			timelineEditor.currentSequence = null;
		}
		
		public function reset():void 
		{
			sequencePanel.reset();
			timelineEditor.currentSequence = null;
		}
		
		//getter / setter
		public function get opened():Boolean 
		{
			return _opened;
		}
		
		public function set opened(value:Boolean):void 
		{
			_opened = value;
			
			var props:Object = { y:opened?stage.stageHeight - baseHeight:stage.stageHeight };
			
			if (value)
			{
				props.onComplete = dispatchEvent;
				props.onCompleteParams = [new UIEvent(UIEvent.PANEL_OPENED)];
			}else
			{
				dispatchEvent(new UIEvent(UIEvent.PANEL_CLOSED));
			}
			
			TweenLite.to(this, .3, props);
		}
		
		
		
		public function get selectedSequence():Sequence 
		{
			return _selectedSequence;
		}
		
		public function set selectedSequence(value:Sequence):void 
		{
			if (selectedSequence == value) return;
			
			if (selectedSequence != null)
			{
				selectedSequence.selected = false;
			}
			
			_selectedSequence = value;
			
			if (selectedSequence != null)
			{
				selectedSequence.selected = true;
				sequencePanel.scrollToElement(selectedSequence.preview);
			}
			
			timelineEditor.currentSequence = selectedSequence;
		}
		
		override public function get height():Number
		{
			return baseHeight;
		}
		
		public function get sequencesList():Vector.<Sequence>
		{
			return sequencePanel.sequencesList;
		}
	}

}