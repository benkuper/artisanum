package timemachine.sequences 
{
	import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import ui.Alert;
	import ui.components.Button;
	import ui.components.TextBox;
	import ui.components.UIEvent;
	import ui.menu.Menu;
	import ui.Style;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class SequencePreview extends Sprite 
	{
		//data
		private var sequence:Sequence;
		
		private var renameMenuItem:NativeMenuItem;
		
		//ui
		private var bg:Sprite;
		private var playBT:Button;
		private var removeBT:Button;
		private var titleBox:TextBox;
		private var _editing:Boolean;
		
		//layout
		private var _previewWidth:Number;
		private var _previewHeight:Number;
		
		
		
		
		
		public function SequencePreview(sequence:Sequence) 
		{
			super();
			this.sequence = sequence;
			sequence.addEventListener(SequenceEvent.SEQUENCE_SELECTED, sequenceSelected);
			sequence.addEventListener(SequenceEvent.SEQUENCE_DESELECTED, sequenceDeselected);
			
			sequence.addEventListener(SequenceEvent.PLAY, sequencePlayHandler);
			sequence.addEventListener(SequenceEvent.PAUSE, sequencePlayHandler);
			sequence.addEventListener(SequenceEvent.STOP, sequencePlayHandler);
			sequence.addEventListener(SequenceEvent.FINISH, sequencePlayHandler);
			
			bg = new Sprite();
			addChild(bg);
			
			titleBox = new TextBox("", sequence.label);
			titleBox.transparentBG = true;
			titleBox.addEventListener(UIEvent.EDITING_START, previewEditingStart);
			titleBox.addEventListener(UIEvent.EDITING_FINISH, previewEditingFinish);
			addChild(titleBox);
			
			
			playBT = new Button(">",true);
			addChild(playBT);
			playBT.width = 30;
			playBT.height = 20;
			playBT.addEventListener(UIEvent.BUTTON_SELECTED, playBTSelected);
			playBT.addEventListener(UIEvent.BUTTON_DESELECTED, playBTSelected);
			removeBT = new Button("x");
			addChild(removeBT);
			removeBT.width = 30;
			removeBT.height = 20;
			removeBT.addEventListener(UIEvent.BUTTON_SELECTED, removeBTSelected);
			
			
			contextMenu = new NativeMenu();
			renameMenuItem = new NativeMenuItem("Renommer la séquence");
			contextMenu.addItem(renameMenuItem);
			contextMenu.addEventListener(Event.SELECT, contextMenuSelect);
			
			
			addEventListener(MouseEvent.CLICK, clickHandler);
			
			_previewWidth = 50;
			_previewHeight = 25;
			
		}
		
		
		//ui
		
		protected function draw():void
		{
			bg.graphics.clear();
			bg.graphics.beginFill(editing?Style.BLUE:Style.UI_SUBPANEL,editing?.5:.2);
			bg.graphics.drawRoundRect(0, 0, previewWidth, previewHeight,5,5);
			bg.graphics.endFill();
			
			removeBT.x = previewWidth - removeBT.width - 10;
			removeBT.y = 2;
			removeBT.height = previewHeight -5;
			
			playBT.x = removeBT.x - playBT.width - 5;
			playBT.y = 2;
			playBT.height = previewHeight - 5;
			
			titleBox.width = previewWidth / 2;
		}
		
		
		// handlers
		
		private function clickHandler(e:MouseEvent):void 
		{
			if (e.target == removeBT) return; 
			dispatchEvent(new SequenceEvent(SequenceEvent.SELECT_SEQUENCE,sequence));
		}
		
		private function previewEditingFinish(e:UIEvent):void 
		{
			//trace("editing finish");
			titleBox.transparentBG = true;
			sequence.label = titleBox.text;
			titleBox.editable = false;
		}
		
		private function previewEditingStart(e:UIEvent):void 
		{
			titleBox.transparentBG = false;
		}
		
		private function contextMenuSelect(e:Event):void 
		{
			var item:NativeMenuItem = e.target as NativeMenuItem;
			switch(item)
			{
				case renameMenuItem:
					titleBox.transparentBG = false;
					titleBox.editable = true;
					titleBox.setFocus();
					titleBox.selectText();
					break;
			}
		}
		
		private function removeBTSelected(e:UIEvent):void 
		{
			Alert.show(stage, "Supprimer la sequence ?", true, confirmRemoveSequence);
		}
		
		public function confirmRemoveSequence():void 
		{
			dispatchEvent(new SequenceEvent(SequenceEvent.REMOVE_SEQUENCE,sequence));
		}
		
		private function sequenceSelected(e:SequenceEvent):void 
		{
			//trace("sequence selected");
			editing = true;
		}
		
		private function sequenceDeselected(e:SequenceEvent):void 
		{
			//trace("sequence deselected");
			editing = false;
		}
		
		
		private function playBTSelected(e:UIEvent):void 
		{
			if (e.type == UIEvent.BUTTON_SELECTED)
			{
				sequence.play();
			}else
			{
				sequence.pause();
			}
		}
		
		private function sequencePlayHandler(e:SequenceEvent):void 
		{
			switch(e.type)
			{
				case SequenceEvent.PLAY:
					playBT.selected = true;
					if(Menu.getChecked("selectSequenceOnPlay")) dispatchEvent(new SequenceEvent(SequenceEvent.SELECT_SEQUENCE,sequence));
					break;
					
				case SequenceEvent.PAUSE:
				case SequenceEvent.STOP:
				case SequenceEvent.FINISH:
					playBT.selected = false;
					break;
			}
			
		}
		
		
		//cleaning
		
		public function clean():void 
		{
			removeBT.clean();
			titleBox.addEventListener(UIEvent.EDITING_START, previewEditingStart);
			titleBox.addEventListener(UIEvent.EDITING_FINISH, previewEditingFinish);
			removeBT.addEventListener(UIEvent.BUTTON_SELECTED, removeBTSelected);
			contextMenu.addEventListener(Event.SELECT, contextMenuSelect);
			
			trace(sequence);
			sequence.removeEventListener(SequenceEvent.SEQUENCE_SELECTED, sequenceSelected);
			sequence.removeEventListener(SequenceEvent.SEQUENCE_DESELECTED, sequenceDeselected);
			
			sequence = null;
			
			removeEventListener(MouseEvent.CLICK, clickHandler);
		}
		
		public function updateLabel():void 
		{
			titleBox.text = sequence.label;
			dispatchEvent(new SequenceEvent(SequenceEvent.LABEL_CHANGED));
		}
		
		//Getter / setter
		
		private function get previewHeight():Number 
		{
			return _previewHeight;
		}
		
		private function set previewHeight(value:Number):void 
		{
			_previewHeight = value;
			draw();
		}
		
		private function get previewWidth():Number 
		{
			return _previewWidth;
		}
		
		private function set previewWidth(value:Number):void 
		{
			_previewWidth = value;
			draw();
		}
		
		
		override public function set width(value:Number):void
		{
			previewWidth = value;
		}
		
		public function get editing():Boolean 
		{
			return _editing;
		}
		
		public function set editing(value:Boolean):void 
		{
			_editing = value;
			draw();
		}
	}

}