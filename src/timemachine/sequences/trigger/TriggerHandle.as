package timemachine.sequences.trigger {
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import timemachine.sequences.trigger.SequenceTrigger;
	import ui.components.TextBox;
	import ui.components.UIEvent;
	import ui.menu.Menu;
	import ui.Style;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TriggerHandle extends Sprite 
	{
		
		private var _sequenceTrigger:SequenceTrigger;
		private var _baseHeight:Number;
		private var _previewHeight:Number;
		
		private var _selected:Boolean;
		
		private var _color:uint;
		private var timeBox:TextBox;
		
		private var baseColor:uint;
		
		public var preview:Shape;
		
		private static var _selectedHandle:TriggerHandle;
		
		public function TriggerHandle(sequenceTrigger:SequenceTrigger) 
		{
			super();
			_baseHeight = 50;
			_color = Style.ORANGE;
			
			_sequenceTrigger = sequenceTrigger;
			_sequenceTrigger.addEventListener(TriggerEvent.TIME_CHANGED, timeChanged);
			_sequenceTrigger.addEventListener(TriggerEvent.TRIGGER, triggerTrigger);
			_sequenceTrigger.addEventListener(TriggerEvent.UNTRIGGER, triggerTrigger);
			
			timeBox = new TextBox("", sequenceTrigger.time.toFixed(2));
			addChild(timeBox);
			timeBox.alpha = 0;
			timeBox.transparentBG = true;
			timeBox.editable = true;
			timeBox.addEventListener(UIEvent.EDITING_FINISH, timeBoxEditingFinish);
			
			preview = new Shape();
			_previewHeight = 20;
			
			updateBaseColor();
			
			draw();
			
			addEventListener(MouseEvent.MOUSE_OVER, mouseHandler);
			addEventListener(MouseEvent.MOUSE_OUT, mouseHandler);
			//addEventListener(MouseEvent.CLICK, mouseClick);
			
			
		}
		
		
		
		//ui
		
		public function draw():void
		{
			graphics.clear();
			
			//for mouse
			graphics.beginFill(0, 0);
			graphics.drawRect( -10, 0, 20, baseHeight);
			graphics.endFill();
			
			graphics.lineStyle(2,color);
			graphics.moveTo(0, 0);
			graphics.lineTo(0, baseHeight);
			
			graphics.beginFill(color);
			graphics.moveTo( -2, 0);
			graphics.lineTo(2, 0);
			graphics.lineTo(0, 2);
			graphics.lineTo( -2, 0);
			graphics.endFill();
			
			graphics.beginFill(color);
			graphics.moveTo(-2, baseHeight);
			graphics.lineTo(2, baseHeight);
			graphics.lineTo(0, baseHeight-2);
			graphics.lineTo( -2, baseHeight);
			graphics.endFill();
			
			drawPreview();
		}
		
		public function drawPreview():void
		{
			preview.graphics.lineStyle(2,color);
			preview.graphics.moveTo(0, 0);
			preview.graphics.lineTo(0, previewHeight);
		}
		
		public function trigger():void
		{
			updateBaseColor();
			TweenMax.fromTo(this, .5, { glowFilter: { blurX:10,blurY:10,color:baseColor,alpha:1,strength:1 }}, { glowFilter: { blurX:0,blurY:0,color:baseColor,alpha:1,strength:0,remove:true }} );
		}
		
		public function clean():void
		{
			sequenceTrigger.removeEventListener(TriggerEvent.TIME_CHANGED, timeChanged);
			sequenceTrigger.removeEventListener(TriggerEvent.TRIGGER, triggerTrigger);
			sequenceTrigger.removeEventListener(TriggerEvent.UNTRIGGER, triggerTrigger);
			
			removeEventListener(MouseEvent.MOUSE_OVER, mouseHandler);
			removeEventListener(MouseEvent.MOUSE_OUT, mouseHandler);
			//removeEventListener(MouseEvent.CLICK, mouseClick);
		}
		
		
		
		private function updateBaseColor(updateColor:Boolean = true):void 
		{
			TweenMax.killTweensOf(this,{color:true});
			
			baseColor = Style.ORANGE;
			
			if (sequenceTrigger.triggered)
			{
				baseColor = Style.UI_LINE;
			}
			
			if (selected)
			{
				baseColor = Style.RED;
			}
			
			if (updateColor) color = baseColor;
		}
		
		
		//handlers
		private function timeChanged(e:TriggerEvent):void 
		{
			trace("time changed");
			timeBox.text = sequenceTrigger.time.toFixed(2);
		}
		
		private function mouseHandler(e:MouseEvent):void 
		{
			var over:Boolean = e.type == MouseEvent.MOUSE_OVER;
			TweenMax.killTweensOf(this,{color:true});
			TweenMax.to(this, .3, { hexColors: { color:over?Style.YELLOW:baseColor }} );
			TweenLite.to(timeBox, .3, { alpha:over?1:0 } );
		}
		
		private function triggerTrigger(e:TriggerEvent):void 
		{
			updateBaseColor();
			
			if (e.type == TriggerEvent.TRIGGER) 
			{
				trigger();
			}
		}
		
		private function timeBoxEditingFinish(e:UIEvent):void 
		{
			if (!isNaN(Number(timeBox.text)))
			{
				sequenceTrigger.time = Number(timeBox.text);
			}else
			{
				timeBox.text = sequenceTrigger.time.toFixed(2);
			}
			
		}
		
		//getter / setter
		
		public function get baseHeight():Number 
		{
			return _baseHeight;
		}
		
		public function set baseHeight(value:Number):void 
		{
			_baseHeight = value;
			draw();
		}
		
		public function get color():uint 
		{
			return _color;
		}
		
		public function set color(value:uint):void 
		{
			_color = value;
			draw();
		}
		
		public function get sequenceTrigger():SequenceTrigger 
		{
			return _sequenceTrigger;
		}
		
		public function get previewHeight():Number 
		{
			return _previewHeight;
		}
		
		public function set previewHeight(value:Number):void 
		{
			_previewHeight = value;
			drawPreview();
		}
		
		public function get selected():Boolean 
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void 
		{
			if (value == selected) return;
			_selected = value;
			updateBaseColor();
			dispatchEvent(new TriggerEvent(value?TriggerEvent.TRIGGER_SELECTED:TriggerEvent.TRIGGER_DESELECTED));
		}
		
		static public function get selectedHandle():TriggerHandle 
		{
			return _selectedHandle;
		}
		
		static public function set selectedHandle(value:TriggerHandle):void 
		{
			if (selectedHandle == value) return;
			if (selectedHandle != null) selectedHandle.selected = false;
			
			_selectedHandle = value;
			Menu.setEnabled("removeTrigger", value != null);
			
			if (selectedHandle != null) selectedHandle.selected = true;
			
		}
		
	}

}