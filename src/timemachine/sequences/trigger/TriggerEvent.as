package timemachine.sequences.trigger {
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TriggerEvent extends Event 
	{
		
		static public const TIME_CHANGED:String = "timeChanged";
		
		static public const TRIGGER:String = "trigger";
		static public const UNTRIGGER:String = "untrigger";
		
		//editor
		static public const TRIGGER_EDITOR_OPEN:String = "triggerEditorOpen";
		static public const TRIGGER_EDITOR_CLOSE:String = "triggerEditorClose";
		
		static public const TRIGGER_SELECTED:String = "triggerSelected";
		static public const TRIGGER_DESELECTED:String = "triggerDeselected";
		
		public function TriggerEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new TriggerEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("TriggerEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}