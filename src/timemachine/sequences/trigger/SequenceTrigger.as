package timemachine.sequences.trigger {
	import com.laiyonghao.Uuid;
	import flash.events.EventDispatcher;
	import statemachine.behaviors.common.Consequence;
	import ui.Toaster;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class SequenceTrigger extends EventDispatcher
	{
		public var id:String;
		public var name:String;
		
		private var _time:Number;
		private var _triggered:Boolean;
		
		public var consequences:Vector.<Consequence>;
		
		public function SequenceTrigger(id:String = "", time:Number = 0) 
		{
			if (id == "") id = new Uuid().toString();
			
			this.id = id;
			this.time = time;
			
			consequences = new Vector.<Consequence>();
		}
		
		
		//data
		public function addConsequence():Consequence
		{
			var c:Consequence = new Consequence();
			consequences.push(c);
			return c;
		}
		
		public function setTrigger(value:Boolean, silent:Boolean = false):void
		{
			if (silent)
			{
				_triggered = value;
			}else
			{
				triggered = value;
			}
		}
		
		
		public function removeConsequence(c:Consequence):void
		{
			c.clean();
			consequences.splice(consequences.indexOf(c),1);
		}
		
		
		private function clearConsequences():void 
		{
			while (consequences.length > 0)
			{
				removeConsequence(consequences[0]);
			}
		}
		
		public function clean():void 
		{
			clearConsequences();
		}
		
		
		
		private function triggerAllActions():void
		{
			for each(var c:Consequence in consequences)
			{
				c.trigger();
			}
		}
		
		//save / load
		public function getXML():XML 
		{
			trace("Sequence Trigger : getXML", consequences.length);
			
			var xml:XML = <trigger></trigger>;
			xml.@name = this.name;
			xml.@id = this.id;
			xml.@time = this.time;
			
			for each(var c:Consequence in consequences)
			{
				xml.appendChild(c.getXML());
			}
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			this.name = data.@name;
			this.id = data.@id;
			this.time = data.@time;
			
			for each(var cXML:XML in data.consequence)
			{
				var c:Consequence = addConsequence();
				c.loadXML(cXML);
			}
		}
		
		
		
		//getter / setter
		
		
		public function get time():Number 
		{
			return _time;
		}
		
		public function set time(value:Number):void 
		{
			if (time == value) return;
			_time = value;
			
			dispatchEvent(new TriggerEvent(TriggerEvent.TIME_CHANGED));
		}
		
		public function get triggered():Boolean 
		{
			return _triggered;
		}
		
		public function set triggered(value:Boolean):void 
		{
			if (triggered == value) return;
			_triggered = value;
			
			if (value) 
			{
				Toaster.info("Trigger : " + name);
				triggerAllActions();
			}
			
			dispatchEvent(new TriggerEvent(value?TriggerEvent.TRIGGER:TriggerEvent.UNTRIGGER));
			
			
			
			
		}
		
	}

}