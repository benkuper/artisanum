package timemachine.sequences.trigger 
{
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import flash.display.Sprite;
	import statemachine.behaviors.actions.ActionEvent;
	import statemachine.behaviors.common.Consequence;
	import ui.components.Button;
	import ui.components.UIEvent;
	import ui.Editor;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TriggerEditor extends Editor 
	{
		
		//data
		private var _currentTrigger:SequenceTrigger;
		private var dispatchOpen:Boolean;
		
		//layout
		private var lockY:Boolean;
		private var _opened:Boolean;
		
		
		//ui
		private var consequencesContainer:Sprite;
		private var addConsequenceBT:Button;
		private var closeBT:Button;
		
		
		
		public function TriggerEditor() 
		{
			super("Editeur de déclenchement :");
			closeBT = new Button("-");
			addChild(closeBT);
			closeBT.addEventListener(UIEvent.BUTTON_SELECTED, closeBTSelected);
			closeBT.width = 30;
			closeBT.height = 15;
			
			
			consequencesContainer = new Sprite();
			
			
			addConsequenceBT = new Button("Ajouter");
			
			addConsequenceBT.width = 50;
			addConsequenceBT.height = 20;
			addConsequenceBT.addEventListener(UIEvent.BUTTON_SELECTED, addConsequenceBTSelected);
			
			
			mainContainer.addChild(addConsequenceBT);
			mainContainer.addChild(consequencesContainer);
			
			
			autoLayout = false;
			scrollable = true;
			baseWidth = 400;
			baseHeight = 100;
			
			addEventListener(ActionEvent.REMOVE_CONSEQUENCE, removeConsequenceHandler);
			
			addEventListener(ActionEvent.CONSEQUENCE_COMMAND_CHANGED, consequenceCommandChanged);
			
		}
		
		//ui
		
		override protected function draw():void 
		{
			super.draw();
			
			closeBT.x = baseWidth - closeBT.width - 10;
			closeBT.y = 5;
			
			//consequences
			
			consequencesContainer.x = 10;
			consequencesContainer.y = 0;
			
			
			var curConsequenceY:Number = 0;
			for (var i:int = 0; i < consequencesContainer.numChildren; i++)
			{
				var cq:Consequence = consequencesContainer.getChildAt(i) as Consequence;
				cq.y = curConsequenceY;
				cq.x = 10;
				cq.width = baseWidth - 40;
				curConsequenceY += cq.height + 5;
			}
			
			curConsequenceY += 5,
			
			addConsequenceBT.y = consequencesContainer.y + curConsequenceY;
			addConsequenceBT.x = (baseWidth - addConsequenceBT.width) / 2;
			
			baseHeight = Math.min(headerHeight + addConsequenceBT.y + addConsequenceBT.height + 20,300);	
			if(opened && !lockY) this.y = -baseHeight-10;
		}
		
		private function updatePosition():void 
		{
			//lockY = true;
			draw();
			if (opened)
			{
				visible = true;
				TweenLite.killTweensOf(this);
				TweenLite.to(this, .5, { y: -baseHeight-10, ease:Strong.easeOut, onComplete:finishOpen, onUpdate:draw } );
			}else
			{
				TweenLite.to(this, .5, {delay:.1,y:0,ease:Strong.easeOut, onComplete:finishOpen, onUpdate:draw } );
			}
			
		}
		
		public function finishOpen():void 
		{
			lockY = false;
			if (!opened) visible = false;
			else draw();
		}
		
		
		//data
		
		public function loadCurrentTrigger():void
		{
			consequencesContainer.removeChildren();
			
			if (currentTrigger == null) 
			{
				draw();
				return;
			}
			
			for each(var cq:Consequence in currentTrigger.consequences)
			{
				consequencesContainer.addChild(cq);
				cq.width = baseWidth - 40;
				cq.x = 10;
			}
			
			
			updatePosition();
			
			title = "Editeur de déclenchement : " + currentTrigger.name;
		}
		
		
		
		//handlers 
		
		private function closeBTSelected(e:UIEvent):void 
		{
			dispatchOpen = true;
			currentTrigger = null;
		}
		
		
		
		
		
		private function addConsequenceBTSelected(e:UIEvent):void 
		{
			if (currentTrigger == null) return;
			var c:Consequence = currentTrigger.addConsequence();
			consequencesContainer.addChild(c);
			
			TweenLite.fromTo(c, .5, { scaleY:0 }, { scaleY:1, ease:Strong.easeOut, onUpdate:draw,onComplete:finishAddConsequence } );
			draw();
			
		}
		
		public function finishAddConsequence():void 
		{
			draw();
		}
		
		
		private function removeConsequenceHandler(e:ActionEvent):void 
		{
			TweenLite.to(e.target as Consequence, .3, { scaleY:0, ease:Strong.easeOut, onUpdate:draw, onComplete:finishRemoveConsequence, onCompleteParams:[e.target as Consequence] } );
		}
		
		public function finishRemoveConsequence(c:Consequence):void 
		{
			if(currentTrigger != null) currentTrigger.removeConsequence(c);
			draw();
			updatePosition();
		}
		
		private function consequenceCommandChanged(e:ActionEvent):void 
		{
			updatePosition();
		}		
		
		
		//getter / setter
		
		public function get currentTrigger():SequenceTrigger 
		{
			return _currentTrigger;
		}
		
		public function set currentTrigger(value:SequenceTrigger):void 
		{
			if (currentTrigger != null)
			{
				//currentTrigger.preview.editing = false;
			}
			
			_currentTrigger = value;
			
			opened = (value != null);
			
			if (currentTrigger != null)
			{
				//currentTrigger.preview.editing = true
				loadCurrentTrigger();
				
			}else
			{
				dispatchOpen = true;
				
			}
		}
		
		public function get opened():Boolean 
		{
			return _opened;
		}
		
		public function set opened(value:Boolean):void 
		{
			if (opened == value) return;
			
			_opened = value;
			
			trace("set trigger opened", value);
			
			lockY = true;
			updatePosition();
			
			if (dispatchOpen) 
			{
				dispatchEvent(new TriggerEvent(value?TriggerEvent.TRIGGER_EDITOR_OPEN:TriggerEvent.TRIGGER_EDITOR_CLOSE));
			}
			
		}
	}

}