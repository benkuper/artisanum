package timemachine.sequences 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class SequenceEvent extends Event 
	{
		
		//actions
		static public const REMOVE_SEQUENCE:String = "removeSequence";
		static public const SELECT_SEQUENCE:String = "selectSequence";
		
		//notifications
		static public const SEQUENCE_ADDED:String = "sequenceAdded";
		static public const SEQUENCE_REMOVED:String = "sequenceRemoved";
		static public const LABEL_CHANGED:String = "labelChanged";
		
		static public const SEQUENCE_SELECTED:String = "sequenceSelected";
		static public const SEQUENCE_DESELECTED:String = "sequenceDeselected";
		
		//sequence related
		static public const TOTALTIME_UPDATE:String = "totaltimeUpdate";
		
		static public const TIME_UPDATE:String = "timeUpdate";
		static public const FINISH:String = "finish";
		
		static public const PLAY:String = "play";
		static public const PAUSE:String = "pause";
		static public const STOP:String = "stop";
		
		
		
		//Variables
		public var sequence:Sequence;
		
		public function SequenceEvent(type:String, sequence:Sequence=null, bubbles:Boolean=true, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			this.sequence = sequence;
			
		} 
		
		public override function clone():Event 
		{ 
			return new SequenceEvent(type, sequence, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("SequenceEvent", "type", "sequence", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}