package timemachine.sequences 
{
	import benkuper.util.KeyboardUtil;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.getTimer;
	import timemachine.sequences.automation.SequenceAutomation;
	import timemachine.sequences.trigger.SequenceTrigger;
	import timemachine.sequences.trigger.TriggerEvent;
	import ui.menu.Menu;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Sequence extends EventDispatcher
	{
		//static
		public static var sequences:Vector.<Sequence>;
		
		
		//data
		public var id:String;
		private var _label:String;
		private var _selected:Boolean;
		
		//for playing
		private var playStartTime:Number;
		
		//for trigger-check
		public var seeking:Boolean;
		
		//sequence-data
		private var _playing:Boolean;
		private var _curTime:Number;
		private var _totalTime:Number;
		
		public var triggers:Vector.<SequenceTrigger>;
		public var automations:Vector.<SequenceAutomation>;
		
		//ui
		public var preview:SequencePreview;
		
		
		public function Sequence(id:String,label:String) 
		{
			if (sequences == null) sequences = new Vector.<Sequence>();
			sequences.push(this);
			
			this.id = id;
			this._label = label;
			
			totalTime = 100;
			curTime = 0;
			
			
			triggers = new Vector.<SequenceTrigger>;
			automations = new Vector.<SequenceAutomation>;
			
			preview = new SequencePreview(this);
		}
		
		//commands
		
		public function play():void
		{
			var dispatch:Boolean = !playing;
			playing = true;
			
			if (dispatch)
			{
				dispatchEvent(new SequenceEvent(SequenceEvent.PLAY));	
			}
		}
		
		public function pause():void
		{
			var dispatch:Boolean = playing;
			playing = false;
			if(dispatch) dispatchEvent(new SequenceEvent(SequenceEvent.PAUSE));
		}
		
		public function stop():void
		{
			var dispatch:Boolean = playing;
			playing = false;
			curTime = 0;
			if(dispatch) dispatchEvent(new SequenceEvent(SequenceEvent.STOP));
		}
		
		public function seek(time:Number):void 
		{
			
			time = Math.min(Math.max(time,0),totalTime);
			if (playing) 
			{
				playStartTime = getTimer() - time * 1000;
			}
			else curTime = time;
		}	
		
	
		public function addTrigger(id:String = "",time:Number = 0):SequenceTrigger 
		{
			var t:SequenceTrigger = new SequenceTrigger(id, time);
			t.name = "Trigger #" + (triggers.length + 1);
			t.addEventListener(TriggerEvent.TRIGGER, triggerTrigger);
			triggers.push(t);
			return t;
		}
		
		public function removeTrigger(t:SequenceTrigger):void
		{
			t.removeEventListener(TriggerEvent.TRIGGER,triggerTrigger);
			triggers.splice(triggers.indexOf(t),1);
			t.clean();
		}
		
		
		public function addAutomation(id:String = ""):SequenceAutomation
		{
			var a:SequenceAutomation = new SequenceAutomation(id);
			a.name = "Automation #" + (automations.length + 1);
			automations.push(a);
			
			return a;
		}
		
		public function removeAutomation(a:SequenceAutomation):void
		{
			a.clean();
			automations.splice(automations.indexOf(a), 1);
			
		}
		
		//handlers
		
		private function triggerTrigger(e:TriggerEvent):void 
		{
			var t:SequenceTrigger = e.target as SequenceTrigger;
			//trace("Sequence : " + label + " > trigger !" + t.time);
		}
		
		private function playingEnterFrame(e:Event):void 
		{
			curTime = (getTimer() - playStartTime)/1000; //in seconds
		}
		
		
		public function clean():void 
		{
			preview.clean();
		}
		
		
		//save / load
		public function getXML():XML 
		{
			trace("Sequence get : getXML", triggers.length);
			
			var xml:XML = <sequence></sequence>;
			xml.@label = this.label;
			xml.@id = this.id;
			xml.@totalTime = this.totalTime;
			
			for each(var t:SequenceTrigger in triggers)
			{
				xml.appendChild(t.getXML());
			}
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			this.label = data.@label;
			this.id = data.@id;
			this.totalTime = data.@totalTime;
			
			for each(var tXML:XML in data.trigger)
			{
				
				var t:SequenceTrigger = addTrigger();
				t.loadXML(tXML);
			}
		}
		
		
		
		//getter / setter
		
		public function get selected():Boolean 
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void 
		{
			if (selected == value) return;
			_selected = value;
			
			if(!playing) curTime = 0;
			
			Menu.setEnabled("duplicateSequence",value);
			Menu.setEnabled("playSequence",value);
			
			dispatchEvent(new SequenceEvent(value?SequenceEvent.SEQUENCE_SELECTED:SequenceEvent.SEQUENCE_DESELECTED));
		}
		
		public function get curTime():Number 
		{
			return _curTime;
		}
		
		public function set curTime(value:Number):void 
		{
			if (curTime == value && curTime > 0) return; //allow 0 to be dispatch for events that are at 0s to be triggered on play
			_curTime = value; // Math.min(Math.max(value, 0), totalTime);
			
			for each(var t:SequenceTrigger in triggers)
			{
				var triggerVal:Boolean = curTime >= t.time;
				if (t.time == 0) triggerVal = curTime > t.time;
				t.setTrigger(triggerVal,(seeking && triggerVal && !KeyboardUtil.ctrlPressed));
			}
			
			dispatchEvent(new SequenceEvent(SequenceEvent.TIME_UPDATE));
			
			
			if (curTime >= totalTime)
			{
				dispatchEvent(new SequenceEvent(SequenceEvent.FINISH));
				//to implement : loop / mirror, etc. for now, just stop the playing
				if(!seeking) playing = false;
			}
		}
		
		public function get playing():Boolean 
		{
			return _playing;
		}
		
		public function set playing(value:Boolean):void 
		{
			if (playing == value) return;
			
			_playing = value;
			
			if (value)
			{
				playStartTime = getTimer()-curTime*1000;
				MainFrame.instance.stage.addEventListener(Event.ENTER_FRAME, playingEnterFrame);
			}else
			{
				MainFrame.instance.stage.removeEventListener(Event.ENTER_FRAME, playingEnterFrame);
			}
		}
		
		public function get totalTime():Number 
		{
			return _totalTime;
		}
		
		public function set totalTime(value:Number):void 
		{
			_totalTime = value;
			dispatchEvent(new SequenceEvent(SequenceEvent.TOTALTIME_UPDATE));
		}
		
		public function get label():String 
		{
			return _label;
		}
		
		public function set label(value:String):void 
		{
			if (value == _label) return;
			_label = value;
			preview.updateLabel();
		}
		
		
		override public function toString():String 
		{
			return "[Sequence id=" + id + ", label=" + label +", totalTime="+totalTime+"]";
		}
		
	}

}