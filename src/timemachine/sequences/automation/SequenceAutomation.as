package timemachine.sequences.automation 
{
	import com.laiyonghao.Uuid;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class SequenceAutomation extends EventDispatcher 
	{
		public var id:String;
		public var name:String;
		
		public var controlPoints:Vector.<AutomationControlPoint>;
		
		public function SequenceAutomation(id:String = "") 
		{
			if (id == "") id = new Uuid().toString();
			this.id = id;
			
			controlPoints = new Vector.<AutomationControlPoint>;
			
			
			//debug
			for (var i:int = 0; i < 10; i++)
			{
				addControlPoint(int(Math.random() * 100), 0);
			}
			
			orderControlPoints();
		}
		
		//data
		public function addControlPoint(time:Number = 0, value:Number = 0):AutomationControlPoint
		{
			var cp:AutomationControlPoint = new AutomationControlPoint(time, value);
			controlPoints.push(cp);
			
			orderControlPoints();
			
			dispatchEvent(new AutomationEvent(AutomationEvent.CONTROLPOINT_ADDED));
			return cp;
		}
		
		
		public function removeControlPoint(cp:AutomationControlPoint):void
		{
			cp.clean();
			controlPoints.splice(controlPoints.indexOf(cp), 1);
			
			dispatchEvent(new AutomationEvent(AutomationEvent.CONTROLPOINT_REMOVED));
			
		}
		
		
		//util
		public function orderControlPoints():void
		{
			controlPoints.sort(orderByTime);
		}
		
		
		private function orderByTime(cp1:AutomationControlPoint, cp2:AutomationControlPoint):int 
		{
			if (cp1.time > cp2.time) return 1;
			else if (cp1.time < cp2.time) return -1;
			return 0;
		}
		
		
		//clean
		public function clean():void 
		{
			
		}
		
	}

}