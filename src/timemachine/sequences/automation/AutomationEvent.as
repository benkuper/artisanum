package timemachine.sequences.automation 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class AutomationEvent extends Event 
	{
		
		static public const AUTOMATION_ADDED:String = "automationAdded";
		static public const AUTOMATION_REMOVED:String = "automationRemoved";
		
		static public const CONTROLPOINT_ADDED:String = "controlpointAdded";
		static public const CONTROLPOINT_REMOVED:String = "controlpointRemoved";
		
		public function AutomationEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new AutomationEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("AutomationEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}