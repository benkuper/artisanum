package timemachine.sequences.automation 
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class AutomationControlPoint extends EventDispatcher 
	{
		public var time:Number;
		public var value:Number;
		
		public function AutomationControlPoint(time:Number,value:Number) 
		{
			super();
			this.time = time;
			this.value = value;
		}
		
		override public function toString():String
		{
			return "[Control Point, time=" + time+", value=" + value+"]";
		}
		
		public function clean():void 
		{
			
		}
		
	}

}