package timemachine.sequences.timeline 
{
	import benkuper.nativeExtensions.ExtendedMouse;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Point;
	import flash.system.Capabilities;
	import flash.text.TextField;
	import flash.ui.Mouse;
	import timemachine.sequences.automation.SequenceAutomation;
	import timemachine.sequences.Sequence;
	import timemachine.sequences.SequenceEvent;
	import timemachine.sequences.trigger.SequenceTrigger;
	import timemachine.sequences.trigger.TriggerEvent;
	import timemachine.sequences.trigger.TriggerHandle;
	import ui.components.Panel;
	import ui.fonts.Fonts;
	import ui.menu.Menu;
	import ui.Style;
	import util.drawStringToBitmap;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TimelineEditor extends Panel 
	{
		
		//data
		private var _viewStart:Number;
		private var _viewEnd:Number;
		private var viewTime:Number;
		
		//sequence related
		private var _currentSequence:Sequence;
		private var triggerHandles:Vector.<TriggerHandle>;
		
		private var draggingTrigger:TriggerHandle;
		
		public var followTheCursor:Boolean;
		public var viewTimeOnPlay:Number; //for stable cursor follow on begin and end
		
		//for mouse
		private var initMousePos:Point;
		private var initViewStart:Number;
		private var initViewEnd:Number;
		private var mouseViewMode:String;
		private var mouseViewModeFixed:Boolean;
		private var viewModePersistance:int; //for fixing view mode after persistance
		private var seekerMotion:Boolean; //using seekerBar or right-click on timeline
		
		
		//layout
		private var timelineWidth:Number;
		private var timelineHeight:Number;
		
		private var timelineSeekerHeight:Number;
		
		private var gridSpacingThreshold:Number = 40;
		
		
		//ui
		private var timelineMask:Shape;
		private var timeline:Sprite;
		private var timelineBG:Sprite;
		
		private var timelineSeeker:Sprite;
		private var timelineSeekerHandle:Sprite;
		private var timelineSeekerPreview:Sprite;
		
		private var triggerContainer:Sprite;
		private var ghostTrigger:Shape;
		
		private var timeCursor:Sprite;
		private var timeCursorPreview:Sprite;
		
		private var timeFields:Bitmap;
		private var timeFieldTF:TextField; //for bitmapDrawing purpose
		
		//prop editor
		private var propEditor:TimeLinePropEditor;
		private var _propEditorWidth:Number;
		
		public function TimelineEditor() 
		{
			super("Editeur de séquence : ");
			
			
			//init vars
			_viewStart = 0;
			_viewEnd = 10;
			
			
			headerHeight = 30;
			
			mainContainer.x = 10;
			
			
			
			
			//timeline
			timeline = new Sprite();
			mainContainer.addChild(timeline);
			timeline.filters = [new DropShadowFilter(1, 45, 0, 1, 8,8, .5, 1, true)];
			
			timelineBG = new Sprite();
			timeline.addChild(timelineBG);
			
			timeFields = new Bitmap();
			timeline.addChild(timeFields);
			timeFieldTF = Fonts.createTF("time", Fonts.smallGreyTF);
			
			timelineMask = new Shape();
			timeline.mask = timelineMask;
			mainContainer.addChild(timelineMask);
			
			timeCursor = new Sprite();
			timeline.addChild(timeCursor);
			timeCursor.mouseEnabled = false;
			
			triggerHandles = new Vector.<TriggerHandle>;
			triggerContainer = new Sprite();
			timeline.addChild(triggerContainer);
			
			ghostTrigger = new Shape();
			
			//Seeker
			timelineSeeker = new Sprite();
			mainContainer.addChild(timelineSeeker);
			timelineSeekerHandle = new Sprite();
			timelineSeeker.addChild(timelineSeekerHandle);
			timelineSeeker.filters = [new DropShadowFilter(1, 45, 0, 1, 8,8, .5, 1, true)];
			
			timelineSeekerPreview = new Sprite();
			timelineSeeker.addChild(timelineSeekerPreview);
			
			timeCursorPreview = new Sprite();
			timelineSeeker.addChild(timeCursorPreview);
			
			
			
			
			
			//layout
			timeline.y = 0;
			timelineMask.x = 0;
			timelineMask.y = timeline.y;
			
			timelineWidth = 100;
			timelineHeight = 100;
			timelineSeekerHeight = 20;
			
			//Events
			timeline.addEventListener(MouseEvent.RIGHT_MOUSE_DOWN, rightMouseHandler);
			timelineSeekerHandle.addEventListener(MouseEvent.MOUSE_DOWN, seekerMouseHandler);
			timelineSeekerHandle.addEventListener(MouseEvent.RIGHT_CLICK, seekerMouseHandler);
			timelineSeekerHandle.addEventListener(MouseEvent.RIGHT_CLICK, seekerMouseHandler);
			
			timelineBG.addEventListener(MouseEvent.MOUSE_DOWN, timelineMouseHandler);
			triggerContainer.addEventListener(MouseEvent.MOUSE_DOWN, triggerMouseHandler);
			
			_propEditorWidth = 200;
			propEditor = new TimeLinePropEditor(this);
			addElement(propEditor)
			
			
			Menu.addSeparator("Time Machine");
			Menu.addMenu("removeTrigger", "Time Machine/Supprimer le trigger", removeTrigger, "", null, false);
			
			//final
			draw();
			
			
		}
		
		
		//data
		private function loadCurrentSequence():void 
		{
			clear();
			
			if (currentSequence == null) return;
			_viewStart = 0;
			_viewEnd = currentSequence.totalTime;
			for each(var t:SequenceTrigger in currentSequence.triggers)
			{
				addTrigger(t);
			}
			
			draw();
			
			title = "Editeur de séquence : " + currentSequence.label;
		}
		
		
		private function clear():void
		{
			for each(var h:TriggerHandle in triggerHandles)
			{
				h.clean();
			}
			triggerContainer.removeChildren();
			timelineSeekerPreview.removeChildren();
			triggerHandles = new Vector.<TriggerHandle>;
		}
		
		
		public function addTrigger(t:SequenceTrigger = null,addAtCurTime:Boolean = false):void 
		{
			if (currentSequence == null) return;
			
			var selectTrigger:Boolean;
			if (t == null)
			{
				var time:Number = addAtCurTime?currentSequence.curTime:viewStart + getSecondsForPixels(timeline.mouseX);
				trace("Add trigger at time " + time);
				t = currentSequence.addTrigger("", time);
				selectTrigger = true;
			}
			
			var h:TriggerHandle = new TriggerHandle(t);
			h.baseHeight = timelineHeight;
			h.previewHeight = timelineSeekerHeight;
			
			triggerContainer.addChild(h);
			timelineSeekerPreview.addChild(h.preview);
			
			triggerHandles.push(h);
			
			t.addEventListener(TriggerEvent.TIME_CHANGED, triggerTimeChanged);
			
			if (selectTrigger) TriggerHandle.selectedHandle = h;
			
			updateTriggerPos(h);
			
		}
		
		
		
		public function removeTrigger():void
		{
			if (TriggerHandle.selectedHandle == null) return;
			if (currentSequence == null) return;
			
			currentSequence.removeTrigger(TriggerHandle.selectedHandle.sequenceTrigger);
			triggerHandles.splice(triggerHandles.indexOf(TriggerHandle.selectedHandle), 1);
			triggerContainer.removeChild(TriggerHandle.selectedHandle);
			timelineSeekerPreview.removeChild(TriggerHandle.selectedHandle.preview);
			
			TriggerHandle.selectedHandle.removeEventListener(TriggerEvent.TIME_CHANGED, triggerTimeChanged);
			
			TriggerHandle.selectedHandle = null;
			
			
			draw();
		}
		
		
		private function addAutomation():void 
		{
			if (currentSequence == null) return;
			var a:SequenceAutomation = currentSequence.addAutomation();

			//to implement with ui
		}
		
		private function removeAutomation():void 
		{
			//to implement
		}
		
		
		//ui
		override protected function draw():void
		{
			
			if (isNaN(timelineWidth) || isNaN(timelineHeight)) return;
			
			timelineSeeker.y = 0;// baseHeight - headerHeight - timelineSeekerHeight - 5;
			
			propEditor.width = propEditorWidth;
			propEditor.height = baseHeight - 15;
			
			propEditor.y = -headerHeight + 10;
			propEditor.x = baseWidth - propEditor.width - 20;
			
			
			var targetW:Number = propEditor.x - 5;
			if (timelineWidth != targetW)
			{
				timelineWidth = propEditor.x - 5;
				if(timelineWidth > 0) timeFields.bitmapData = new BitmapData(timelineWidth-20, 30,true,0)
			}
			
			timelineHeight = baseHeight - headerHeight - timelineSeekerHeight - 10;
			
			timeline.y = timelineSeekerHeight + 5;
			timelineMask.y = timeline.y;
			
			timelineMask.graphics.clear();
			timelineMask.graphics.beginFill(0x550033);
			timelineMask.graphics.drawRect(0, 0, timelineWidth, timelineHeight);
			timelineMask.graphics.endFill();
			
			
			drawTimeline();
			super.draw();
		}
		
		private function drawTimeline():void
		{
			
			timelineBG.graphics.clear();
			timelineSeeker.graphics.clear();
			timeCursor.graphics.clear();
			
			if (currentSequence == null) return;
			
			viewTime = viewEnd - viewStart;
			
			if (viewTime <= 0 || timelineWidth <= 0 || timelineHeight <=0) return;
			
			timelineBG.graphics.beginFill(Style.BG_COLOR);
			timelineBG.graphics.drawRoundRect(0, 0, timelineWidth, timelineHeight,10,10);
			timelineBG.graphics.endFill();
			
			var secondWidth:Number = getPixelsForSeconds(1);
			var detailsSteps:Number = 1;
			
			while (detailsSteps * secondWidth < gridSpacingThreshold) detailsSteps++;
			
			
			//draw lines
			var secondAfterViewStart:int = Math.ceil(viewStart/detailsSteps)*detailsSteps;
			var secondBeforeViewEnd:int = Math.floor(viewEnd / detailsSteps) * detailsSteps;
			
			
			//clearing the time fields bitmapData
			timeFields.bitmapData.fillRect(timeFields.bitmapData.rect, 0);
			
			
			for (var i:Number = secondAfterViewStart; i <= secondBeforeViewEnd; i+=detailsSteps)
			{
				var mainLine:Boolean = i % 4 == 0;
				var secondLine:Boolean = i % 2 == 0;
				var color:uint = mainLine?Style.GRID1:(secondLine?Style.GRID2:Style.GRID3);
				timelineBG.graphics.lineStyle(1,color , 1);
				
				var pos:Number = (i - viewStart) * secondWidth;
				timelineBG.graphics.moveTo(pos, 0);
				timelineBG.graphics.lineTo(pos, timelineHeight);
				
				timeFieldTF.text = i + "'";
				timeFieldTF.textColor = color;
				drawStringToBitmap(timeFields.bitmapData, timeFieldTF, pos+2, 2);
				
			}
			
			timelineSeeker.graphics.clear();
			timelineSeeker.graphics.beginFill(Style.BG_COLOR);
			timelineSeeker.graphics.drawRoundRect(0, 0, timelineWidth, timelineSeekerHeight,5,5);
			timelineSeeker.graphics.endFill();
			
			
			timelineSeekerHandle.x = viewStart*timelineWidth/currentSequence.totalTime;// getPixelsForSeconds(viewStart);
			timelineSeekerHandle.graphics.clear();
			timelineSeekerHandle.graphics.lineStyle(2, Style.BLUE);
			timelineSeekerHandle.graphics.beginFill(Style.UI_BLUE_FILL, .2);
			timelineSeekerHandle.graphics.drawRoundRect(0, 0, viewTime*timelineWidth/currentSequence.totalTime, timelineSeekerHeight,5,5);
			timelineSeekerHandle.graphics.endFill();
			
			
			timeCursor.graphics.clear();
			timeCursor.graphics.beginFill(0, 0);
			timeCursor.graphics.drawRect( -10, 0, 20, timelineHeight);
			timeCursor.graphics.endFill();
			timeCursor.graphics.lineStyle(2,Style.BLUE);
			timeCursor.graphics.moveTo(0, 0);
			timeCursor.graphics.lineTo(0, timelineHeight);
			
			timeCursorPreview.graphics.clear();
			timeCursorPreview.graphics.lineStyle(2,Style.BLUE);
			timeCursorPreview.graphics.moveTo(0, 0);
			timeCursorPreview.graphics.lineTo(0, timelineSeekerHeight);
			
			updateCursorPos();
			
			drawTriggers();
			
		}
		
		private function drawTriggers():void
		{
			timelineSeekerPreview.graphics.clear();
			
			for each(var h:TriggerHandle in triggerHandles)
			{
				var triggerIsInViewRange:Boolean = h.sequenceTrigger.time >= viewStart && h.sequenceTrigger.time <= viewEnd;
				
				if (triggerIsInViewRange)
				{
					if (!triggerContainer.contains(h)) triggerContainer.addChild(h);
				}else
				{
					if (triggerContainer.contains(h)) triggerContainer.removeChild(h);
				}
				
				updateTriggerPos(h);
			}
			
		}
		
		private function drawAutomations():void 
		{
			
		}
		
		
		private function updateCursorPos():void
		{
			var pos:Number = getPosForTime(currentSequence.curTime);
			timeCursor.x = pos;
			
			var previewPos:Number = currentSequence.curTime * timelineWidth / currentSequence.totalTime;
			timeCursorPreview.x = previewPos;
			
		}
		
		private function updateTriggerPos(triggerHandle:TriggerHandle):void 
		{
			triggerHandle.x = getPosForTime(triggerHandle.sequenceTrigger.time);
			triggerHandle.preview.x =  triggerHandle.sequenceTrigger.time * timelineWidth / currentSequence.totalTime;
		}
		
		
		
		
		
		//handlers
		
		private function timelineMouseHandler(e:MouseEvent):void 
		{
			if (currentSequence == null) return;
			
			switch(e.type)
			{
				case MouseEvent.MOUSE_DOWN:
					if (e.shiftKey) 
					{
						addTrigger();
						drawTriggers();
					}else if(e.altKey)
					{
						addAutomation();
						drawAutomations();
					}else 
					{
						TriggerHandle.selectedHandle = null;
						
						stage.addEventListener(Event.ENTER_FRAME, timelineMouseEnterFrame);
						stage.addEventListener(MouseEvent.MOUSE_UP, timelineMouseHandler);
						currentSequence.seeking = true;
					}
					break;
					
				case MouseEvent.MOUSE_UP:
					stage.removeEventListener(Event.ENTER_FRAME, timelineMouseEnterFrame);
					stage.removeEventListener(MouseEvent.MOUSE_UP, timelineMouseHandler);
					currentSequence.seeking = false;
					break;
					
			}
			
		}
		
		
		
		private function timelineMouseEnterFrame(e:Event):void
		{
			currentSequence.seek(getTimeForPos(timeline.mouseX));
		}
		
		private function seekerMouseHandler(e:MouseEvent):void 
		{
			switch(e.type)
			{
				case MouseEvent.MOUSE_DOWN:
					stage.addEventListener(Event.ENTER_FRAME, mouseEnterFrame);
					stage.addEventListener(MouseEvent.MOUSE_UP, seekerMouseHandler);
					seekerMotion = true;
					initMousePos = new Point(mouseX, mouseY);
					initViewStart = viewStart;
					initViewEnd = viewEnd;
					mouseViewMode = "none";
					mouseViewModeFixed = false;
					Mouse.hide();
					break;
					
				case MouseEvent.MOUSE_UP:
					stage.removeEventListener(Event.ENTER_FRAME, mouseEnterFrame);
					stage.removeEventListener(MouseEvent.MOUSE_UP, seekerMouseHandler);
					Mouse.show();
					if(Capabilities.os.indexOf("Windows") >= 0) ExtendedMouse.setCursorRelativePos(timelineSeekerHandle.width/2,timelineSeekerHandle.height/2,timelineSeekerHandle);
					break;
					
				case MouseEvent.RIGHT_CLICK:
					trace("double click");
					viewStart = 0;
					viewEnd = currentSequence.totalTime;
					draw();
					break;
			}
		}
		
		private function rightMouseHandler(e:MouseEvent):void 
		{
			switch(e.type)
			{
				case MouseEvent.RIGHT_MOUSE_DOWN:
					stage.addEventListener(Event.ENTER_FRAME, mouseEnterFrame);
					stage.addEventListener(MouseEvent.RIGHT_MOUSE_UP, rightMouseHandler);
					seekerMotion = false;
					initMousePos = new Point(mouseX, mouseY);
					initViewStart = viewStart;
					initViewEnd = viewEnd;
					mouseViewMode = "none";
					mouseViewModeFixed = false;
					Mouse.hide();
					break;
					
				case MouseEvent.RIGHT_MOUSE_UP:
					stage.removeEventListener(Event.ENTER_FRAME, mouseEnterFrame);
					stage.removeEventListener(MouseEvent.RIGHT_MOUSE_UP, rightMouseHandler);
					Mouse.show();
					break;
			}
		}
		
		private function mouseEnterFrame(e:Event):void 
		{
			var offsetX:Number = mouseX - initMousePos.x;
			var offsetY:Number = mouseY - initMousePos.y;
			
			switch(mouseViewMode)
			{
				case "horizontal":
					var startOffset:Number;
					if (seekerMotion)
					{
						startOffset = offsetX * currentSequence.totalTime / timelineWidth;
					}else
					{
						startOffset = getSecondsForPixels(-offsetX); //inverse motion
					}
					var targetStart:Number = Math.min(Math.max(initViewStart + startOffset, 0), currentSequence.totalTime-viewTime);
					_viewStart = targetStart; // force no minMax on value assign (see "set viewStart());
					viewEnd = viewStart + viewTime;
					break;
					
				case "vertical":
					var vertOffset:Number = getPixelsForSeconds( -offsetY) * viewTime / 5000;
					var startRelative:Number = (getTimeForPos(initMousePos.x) - viewStart) / viewTime;
					var endRelative:Number = 1 - startRelative;
					viewStart = Math.max(initViewStart-vertOffset*startRelative,0);
					viewEnd = Math.min(initViewEnd+vertOffset*endRelative,currentSequence.totalTime);
					break;
					
				
					
				default:
					break;
			}
			
			
			//INFINITE DRAG : only for rightClick vertical and seeker 
			if (Capabilities.os.indexOf("Windows") >= 0) 
			{
				ExtendedMouse.setCursorRelativePos(initMousePos.x, initMousePos.y, this);
				initViewStart = viewStart;
				initViewEnd = viewEnd;
			}
			
			
			
			
			if (!mouseViewModeFixed)
			{
				var newView:String = (Math.abs(offsetX) > Math.abs(offsetY))?"horizontal":"vertical";
				if (mouseViewMode != newView)
				{
					viewModePersistance = 0;
					mouseViewMode = newView;
				}else
				{
					viewModePersistance++;
					if (viewModePersistance > 5) 
					{
						mouseViewModeFixed = true;
					}
					
				}
			}
			
			drawTimeline();
		}
		
		
		private function triggerMouseHandler(e:MouseEvent):void 
		{
			switch(e.type)
			{
				case MouseEvent.MOUSE_DOWN:
					if (e.target is TriggerHandle)
					{
						
						if (e.ctrlKey) 
						{
							draggingTrigger = e.target as TriggerHandle;
							ExtendedMouse.setCursorRelativePos(draggingTrigger.x, timeline.mouseY, timeline);
							initMousePos = new Point(draggingTrigger.x, timeline.mouseY);
							stage.addEventListener(Event.ENTER_FRAME, triggerMouseEnterFrame);
							stage.addEventListener(MouseEvent.MOUSE_UP, triggerMouseHandler);
						}else
						{
							TriggerHandle.selectedHandle = e.target as TriggerHandle;
						}
					}
					
					break;
					
				case MouseEvent.MOUSE_UP:
					if (draggingTrigger != null)
					{
						stage.removeEventListener(Event.ENTER_FRAME, triggerMouseEnterFrame);
						stage.removeEventListener(MouseEvent.MOUSE_UP, triggerMouseHandler);
						
						if (Math.abs(timeline.mouseX - initMousePos.x) < 5) //click
						{
							TriggerHandle.selectedHandle = draggingTrigger;
						}
						
						draggingTrigger = null;
					}
					
					break;
			}
		}
		
		private function triggerMouseEnterFrame(e:Event):void 
		{
			var motionThreshold:Boolean = Math.abs(timeline.mouseX - initMousePos.x) >= 5;
			var targetX:Number = motionThreshold?timeline.mouseX:initMousePos.x;
			
			draggingTrigger.sequenceTrigger.time = Math.min(Math.max(getTimeForPos(targetX),0),currentSequence.totalTime);
			//updateTriggerPos(draggingTrigger);
			
		}
		
		private function triggerTimeChanged(e:TriggerEvent):void 
		{
			var handle:TriggerHandle = getHandleForTrigger(e.target as SequenceTrigger);
			updateTriggerPos(handle);
		}
		
		
		private function sequencePlay(e:SequenceEvent):void 
		{
			viewTimeOnPlay = viewTime;
		}
		
		private function sequenceTimeUpdate(e:SequenceEvent):void 
		{
			if (followTheCursor && currentSequence.playing)
			{
				viewStart = currentSequence.curTime-viewTimeOnPlay / 2;
				viewEnd = currentSequence.curTime+viewTimeOnPlay / 2;
				drawTimeline();
			}else
			{
				updateCursorPos();
			}
		}
		
		private function sequenceTotalTimeUpdate(e:SequenceEvent):void 
		{
			trace("TimelineEditor :: totalTimeUpdate");
			//force re-evalute viewStart/viewEnd
			viewStart = viewStart;
			viewEnd = viewEnd;
			
			draw();
		}
		
		
		
		//util
		private function getHandleForTrigger(sequenceTrigger:SequenceTrigger):TriggerHandle 
		{
			for each(var t:TriggerHandle in triggerHandles)
			{
				if (t.sequenceTrigger == sequenceTrigger) return t;
			}
			
			return null;
		}
		
		public function getPixelsForSeconds(seconds:Number):Number //not relative to viewStart
		{
			return seconds * timelineWidth / viewTime;
		}
		
		public function getSecondsForPixels(pixels:Number):Number //not relative to viewStart
		{
			return pixels * viewTime / timelineWidth;
		}
		
		public function getTimeForPos(pos:Number):Number
		{
			return getSecondsForPixels(pos) + viewStart;
		}
		
		public function getPosForTime(time:Number):Number
		{
			return getPixelsForSeconds(time-viewStart);
		}
		
		
		// getter setter;
		
		public function get viewStart():Number 
		{
			return _viewStart;
		}
		
		public function set viewStart(value:Number):void 
		{
			_viewStart = Math.min(value, viewEnd - 2);
			_viewStart = Math.min(Math.max(_viewStart, 0), currentSequence.totalTime);
		}
		
		public function get viewEnd():Number 
		{
			return _viewEnd;
		}
		
		public function set viewEnd(value:Number):void 
		{
			_viewEnd = Math.max(value, viewStart + 2);
			_viewEnd = Math.min(Math.max(_viewEnd, 0), currentSequence.totalTime);
		
		}
		
		
		public function get currentSequence():Sequence 
		{
			return _currentSequence;
		}
		
		public function set currentSequence(value:Sequence):void 
		{
			if (currentSequence == value) 
			{
				if (value == null) clear();
				return;
			}
			
			if (currentSequence != null)
			{
				currentSequence.removeEventListener(SequenceEvent.PLAY, sequencePlay);
				currentSequence.removeEventListener(SequenceEvent.TIME_UPDATE, sequenceTimeUpdate);
				currentSequence.removeEventListener(SequenceEvent.TOTALTIME_UPDATE, sequenceTotalTimeUpdate);
			}
			
			_currentSequence = value;
			propEditor.currentSequence = value;
			
			if (currentSequence != null)
			{
				currentSequence.addEventListener(SequenceEvent.PLAY, sequencePlay);
				currentSequence.addEventListener(SequenceEvent.TIME_UPDATE, sequenceTimeUpdate);
				currentSequence.addEventListener(SequenceEvent.TOTALTIME_UPDATE, sequenceTotalTimeUpdate);
				
			}
			
			loadCurrentSequence();
		}
		
		
		
		
		public function get propEditorWidth():Number 
		{
			return _propEditorWidth;
		}
		
		public function set propEditorWidth(value:Number):void 
		{
			_propEditorWidth = value;
			draw();
		}
		
		
		
	}

}