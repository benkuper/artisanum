package timemachine.sequences.timeline 
{
	import timemachine.sequences.Sequence;
	import timemachine.sequences.SequenceEvent;
	import timemachine.sequences.trigger.TriggerEvent;
	import timemachine.sequences.trigger.TriggerHandle;
	import ui.components.Button;
	import ui.components.CheckBox;
	import ui.components.Panel;
	import ui.components.Slider;
	import ui.components.TextBox;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TimeLinePropEditor extends Panel 
	{
		
		//data
		private var editor:TimelineEditor;
		
		private var _currentSequence:Sequence;
		
		//layout
		
		//ui
		private var totalTimeBox:TextBox;
		private var currentTimeBox:TextBox;
		private var followCursorCheckbox:CheckBox;
		
		private var addTriggerBT:Button;
		private var removeTriggerBT:Button;
		
		public function TimeLinePropEditor(editor:TimelineEditor) 
		{
			super("Propriétés");
			this.editor = editor;
			editor.addEventListener(TriggerEvent.TRIGGER_SELECTED, triggerSelectHandler);
			editor.addEventListener(TriggerEvent.TRIGGER_DESELECTED, triggerSelectHandler);
			
			//miniMode = true;
			
			headerHeight = 35;
			
			currentTimeBox = new TextBox("Temps actuel :", "0");
			currentTimeBox.visible = false;
			addElement(currentTimeBox);
			currentTimeBox.editable = true;
			currentTimeBox.addEventListener(UIEvent.EDITING_FINISH, currentTextBoxEditingFinish);
			
			
			totalTimeBox = new TextBox("Durée totale :", "0");
			totalTimeBox.visible = false;
			addElement(totalTimeBox);
			totalTimeBox.editable = true;
			totalTimeBox.addEventListener(UIEvent.EDITING_FINISH, totalTextBoxEditingFinish);
			
			followCursorCheckbox = new CheckBox("Suivre le curseur", true);
			addElement(followCursorCheckbox);
			followCursorCheckbox.addEventListener(UIEvent.CHECKBOX_SELECTED, followChanged);
			followCursorCheckbox.addEventListener(UIEvent.CHECKBOX_DESELECTED, followChanged);
			followCursorCheckbox.visible = false;
			
			
			//ui
			addTriggerBT = new Button("Ajouter un trigger");
			addElement(addTriggerBT);
			addTriggerBT.visible = false;
			
			removeTriggerBT = new Button("Supprimer le trigger");
			addElement(removeTriggerBT);
			removeTriggerBT.visible = false;
			
			addEventListener(UIEvent.BUTTON_SELECTED, buttonSelected);
			
			autoLayout = true;
		}
		
		
		//ui
		override protected function draw():void
		{
			super.draw();
			
			currentTimeBox.x = 10;
			currentTimeBox.width = baseWidth - 20;
			totalTimeBox.x = 10;
			totalTimeBox.width = baseWidth - 20;
			followCursorCheckbox.x = 10;
			
			addTriggerBT.x = 10;
			addTriggerBT.width = baseWidth - 20;
			
			removeTriggerBT.x = 10;
			removeTriggerBT.width = baseWidth - 20;
			
		}
		
		
		// handlers
		private function currentTextBoxEditingFinish(e:UIEvent):void 
		{
			
			if (currentSequence != null) 
			{
				currentSequence.seeking = true;
				currentSequence.seek(Number(currentTimeBox.text));
				currentSequence.seeking = false;
			}
		}
		
		private function totalTextBoxEditingFinish(e:UIEvent):void 
		{
			if (currentSequence != null) currentSequence.totalTime = Number(totalTimeBox.text);
		
		}
		
		private function followChanged(e:UIEvent):void 
		{
			editor.followTheCursor = followCursorCheckbox.selected;
		}
		
		private function timeUpdate(e:SequenceEvent = null):void 
		{
			currentTimeBox.text = currentSequence.curTime.toFixed(2);
		}
		
		
		private function triggerSelectHandler(e:TriggerEvent):void 
		{
			removeTriggerBT.visible = (e.target as TriggerHandle).selected;// TriggerHandle.selectedHandle != null;
		}
		
		private function buttonSelected(e:UIEvent):void 
		{
			switch(e.target)
			{
				case addTriggerBT:
					editor.addTrigger(null, true);
					break;
					
				case removeTriggerBT:
					editor.removeTrigger();
					break;
			}
		}
		
		//utils
		
		
		//cleaning
		override public function clean():void
		{
			super.clean();
		}
		
		// getter / setter
		
		public function get currentSequence():Sequence 
		{
			return _currentSequence;
		}
		
		public function set currentSequence(value:Sequence):void 
		{
			
			if (currentSequence != null)
			{
				currentSequence.removeEventListener(SequenceEvent.TIME_UPDATE, timeUpdate);
			}
			_currentSequence = value;
			
			totalTimeBox.visible = currentSequence != null;
			currentTimeBox.visible = currentSequence != null;
			followCursorCheckbox.visible = currentSequence != null;
			addTriggerBT.visible = currentSequence != null;
			removeTriggerBT.visible = false;
			
			if(currentSequence != null){
				title = "Propriétés de " + currentSequence.label;
				totalTimeBox.text = currentSequence.totalTime.toString();
				timeUpdate();
				currentSequence.addEventListener(SequenceEvent.TIME_UPDATE, timeUpdate);
				
			}else
			{
				title = "Propriétés";
			}
		}
		
		
		
		
	}

}