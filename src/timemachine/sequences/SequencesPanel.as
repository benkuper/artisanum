package timemachine.sequences 
{
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import com.laiyonghao.Uuid;
	import flash.display.Sprite;
	import flash.events.Event;
	import ui.components.Button;
	import ui.components.Panel;
	import ui.components.UIEvent;
	import ui.menu.Menu;
	import ui.Style;
	import ui.Toaster;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class SequencesPanel extends Panel 
	{
		//Data
		public var sequencesList:Vector.<Sequence>;
		private var previews:Vector.<SequencePreview>;
		private var _selectedPreview:SequencePreview;
		
		//ui
		private var previewContainer:Sprite;
		private var addSequenceBT:Button;
		
		public function SequencesPanel() 
		{
			super("Séquences");
			_bgColor = Style.UI_PANEL;
			headerHeight = 30;
			
			
			previews = new Vector.<SequencePreview>;
			sequencesList = new Vector.<Sequence>;
			
			previewContainer = new Sprite();
			addElement(previewContainer);
			
			addSequenceBT = new Button("Ajouter");
			addChild(addSequenceBT);
			addSequenceBT.addEventListener(UIEvent.BUTTON_SELECTED, addSequenceBTClick);
			
			this.addEventListener(SequenceEvent.REMOVE_SEQUENCE, removeSequenceHandler);
			
			
			
			Menu.addMenu("addSequence", "Time Machine/Ajouter une séquence", addSequence);
			Menu.addSeparator("Time Machine");
			
			baseWidth = 200;
			baseHeight = 100;
			autoLayout = false;
			scrollable = true;
			
			
			Menu.addMenu("duplicateSequence", "Time Machine/Dupliquer la séquence", duplicateEditingSequence, "", null, false);
			Menu.addMenu("playSequence", "Time Machine/Lire la séquence", playEditingSequence," ");
			
			Menu.addSeparator("Time Machine");
			Menu.addMenu("sequenceUpRank", "Time Machine/Remonter la séquence", upSequenceRank);
			Menu.addMenu("sequenceDownRank", "Time Machine/Descendre la séquence", downSequenceRank);
			
			Menu.addSeparator("Time Machine");
			Menu.addMenu("selectSequenceOnPlay", "Time Machine/Sélectionner la dernière séquence jouée", upSequenceRank,"",null,true,true);
			
			draw();
		}
		
		
		
		public function playEditingSequence():void 
		{
			var edSeq:Sequence = getEditingSequence();
			if (edSeq)
			{
				if (edSeq.playing) edSeq.pause();
				else edSeq.play();
			}
		}
		
		private function duplicateEditingSequence():void 
		{
			addSequence(getEditingSequence());
		}
		
		
		
		//ui
		
		override protected function draw():void
		{
			super.draw();
			
			var curY:int = 0;
			for (var i:int = 0; i < previews.length; i++)
			{
				previews[i].x = 5;
				previews[i].y = curY;
				previews[i].width = baseWidth - 10;
				curY += previews[i].height + 5;
			}
			
			addSequenceBT.x = (baseWidth - addSequenceBT.width) / 2;
			addSequenceBT.y = baseHeight + 5;
		}
		
		//data
		
		private function addSequence(sourceSequence:Sequence = null):Sequence 
		{
			var s:Sequence = new Sequence(new Uuid().toString(), "Séquence #"+previews.length);
			sequencesList.push(s);
			previews.push(s.preview);
			previewContainer.addChild(s.preview);
			draw();
			TweenLite.fromTo(s.preview, .5, { scaleY:0 , alpha:0 }, { alpha:1, scaleY:1, ease:Strong.easeOut, onUpdate:draw } );
			
			dispatchEvent(new SequenceEvent(SequenceEvent.SEQUENCE_ADDED, s));
			return s;
		}
		
		private function removeSequence(s:Sequence, animate:Boolean = true):void
		{
			if (animate) 
			{
				TweenLite.to(s.preview, .5,  { scaleY:0, alpha:0, ease:Strong.easeOut, onUpdate:draw, onComplete:finishRemoveSequence,onCompleteParams:[s] } );
			}else
			{
				finishRemoveSequence(s);
			}
		}
		
		public function finishRemoveSequence(s:Sequence ):void 
		{
			s.clean();
			if (previewContainer.contains(s.preview)) previewContainer.removeChild(s.preview);
			previews.splice(previews.indexOf(s.preview), 1);
			sequencesList.splice(sequencesList.indexOf(s), 1);
			
			
			Toaster.info("Séquence supprimée : " + s.label);
			
			dispatchEvent(new SequenceEvent(SequenceEvent.SEQUENCE_REMOVED,s));
			
			draw();
		}
		
		public function moveSequence(s:Sequence = null, direction:String = "up"):void 
		{
			if (s == null) s = getEditingSequence();
			if (s == null) return;
			
			trace("move sequence");
			
			var sIndex:int = sequencesList.indexOf(s);
			var tIndex:int = (direction == "up")?sIndex - 1:sIndex + 1;
			sequencesList.splice(sIndex, 1);
			sequencesList.splice(tIndex, 0, s);
			previews.splice(sIndex, 1);
			previews.splice(tIndex, 0, s.preview);
			draw();
		}
		
		private function clearSequences():void 
		{
			while (sequencesList.length > 0) removeSequence(sequencesList[0], false);
		}
		
		public function reset():void 
		{
			clearSequences();
		}
		
		
		
		//utils
		private function getEditingSequence():Sequence
		{
			for each(var s:Sequence in sequencesList)
			{
				if (s.preview.editing) return s;
			}
			
			return null;
		}
		
		//handlers
		
		private function addSequenceBTClick(e:Event):void 
		{
			var s:Sequence = addSequence();
			dispatchEvent(new SequenceEvent(SequenceEvent.SELECT_SEQUENCE,s));
		}
		
		private function removeSequenceHandler(e:SequenceEvent):void 
		{
			removeSequence(e.sequence);
		}
		
		private function sequenceClick(e:SequenceEvent):void 
		{
			trace("sequence click");
		}
		
		//callbacks
		private function upSequenceRank():void 
		{
			moveSequence(getEditingSequence(),"up");
		}
		
		private function downSequenceRank():void 
		{
			moveSequence(getEditingSequence(),"down");
		}
		
		
		//save / load
		public function getXML():XML 
		{
			var xml:XML = <sequencePanel></sequencePanel>;
			trace("Sequence Panel : getXML", sequencesList.length);
			
			for each(var s:Sequence in sequencesList)
			{
				xml.appendChild(s.getXML());
			}
			trace(xml.toXMLString())
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			
			clearSequences();
			if (data == null ||data.sequence == null) return;
			
			for each(var sXML:XML in data.sequence)
			{
				
				var s:Sequence = addSequence();
				s.loadXML(sXML);
			}
		}
		
		
		
	}

}