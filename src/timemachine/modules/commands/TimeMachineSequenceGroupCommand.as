package timemachine.modules.commands 
{
	import flash.text.TextField;
	import modules.events.ParameterEvent;
	import modules.output.commands.Command;
	import modules.parameters.ParameterBool;
	import modules.parameters.ParameterEnum;
	import timemachine.sequences.Sequence;
	import timemachine.sequences.SequenceEvent;
	import timemachine.TimeMachine;
	import ui.components.Button;
	import ui.components.UIEvent;
	import ui.fonts.Fonts;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TimeMachineSequenceGroupCommand extends Command 
	{
		protected var op:String;
		protected var timeMachine:TimeMachine;
		
		protected var sequenceStartParam:ParameterEnum;
		protected var sequenceEndParam:ParameterEnum;
		
		protected var sequenceStartIndex:int;
		protected var sequenceEndIndex:int;
		protected var _currentSequenceIndex:int;
		
		private var random:ParameterBool;
		private var loop:ParameterBool;
		private var solo:ParameterBool;
		
		private var feedbackTF:TextField;
		private var resetCounterBT:Button;
		
		
		public function TimeMachineSequenceGroupCommand(label:String, guiMode:String="") 
		{
			super(label, guiMode);
			sequenceStartParam = addParameter(new ParameterEnum("targetSequence", "[Séquence]", null), "Séquence cible", true) as ParameterEnum;
			sequenceEndParam = addParameter(new ParameterEnum("targetSequence", "[Séquence]", null), "Séquence cible", true) as ParameterEnum;
			
			
			random = addParameter(new ParameterBool("aleatoire","Aleatoire", false),"",true) as ParameterBool;
			random.addEventListener(ParameterEvent.VALUE_CHANGE, randomChange);
			
			loop = addParameter(new ParameterBool("boucle", "Boucle", false), "", true) as ParameterBool;
			
			solo = addParameter(new ParameterBool("solo", "Solo", false), "Arrêter les autres séquences", true) as ParameterBool;
			
			resetCounterBT = new Button("Remettre à zéro");
			gui.addChild(resetCounterBT);
			resetCounterBT.addEventListener(UIEvent.BUTTON_SELECTED, resetSelected);
			
			_currentSequenceIndex = 0;
			
			sequenceStartParam.guiWidth = 200;
			sequenceEndParam.guiWidth = 200;
			
			sequenceStartParam.gui.x = 10;
			sequenceEndParam.gui.x = 10;
			
			sequenceEndParam.gui.y = sequenceStartParam.gui.y + sequenceStartParam.gui.height + 5;
			
			
			random.gui.x = 10;
			random.gui.y = sequenceEndParam.gui.y + sequenceEndParam.gui.height+5;
			
			loop.gui.x = random.gui.x + random.gui.width +5;
			loop.gui.y = random.gui.y;
			
			solo.gui.x = loop.gui.x + loop.gui.width + 5;
			solo.gui.y = loop.gui.y;
			
			resetCounterBT.x = solo.gui.x + solo.guiWidth + 5;
			resetCounterBT.y = solo.gui.y;
			
			feedbackTF = Fonts.createTF("Courant",Fonts.normalTF);
			gui.addChild(feedbackTF);
			feedbackTF.x = 10;
			feedbackTF.y = random.gui.y + random.gui.height + 5;
			updateFeedbackTF();
			
			sequenceStartParam.addEventListener(ParameterEvent.VALUE_CHANGE, sequenceChanged);
			sequenceEndParam.addEventListener(ParameterEvent.VALUE_CHANGE, sequenceChanged);
			
		}
		
		private function resetSelected(e:UIEvent):void 
		{
			currentSequenceIndex = 0;
		}
		
		private function randomChange(e:ParameterEvent):void 
		{
			loop.enabled = !random.value;
		}
		
		private function sequenceChanged(e:ParameterEvent):void 
		{
			updateSequenceIndices();
			updateFeedbackTF();
		}
		
		private function updateSequenceIndices():void 
		{
			if (timeMachine != null) 
			{
				if(sequenceStartParam.selectedItem != null) sequenceStartIndex = timeMachine.sequencesList.indexOf(sequenceStartParam.selectedItem.value);
				if(sequenceEndParam.selectedItem != null) sequenceEndIndex = timeMachine.sequencesList.indexOf(sequenceEndParam.selectedItem.value);
			}
		}
		
		override protected function setProp(propName:String, value:*):void 
		{
			super.setProp(propName, value);
			switch(propName)
			{				
				case "timeMachine":
					this.timeMachine = value as TimeMachine;
					timeMachine.addEventListener(SequenceEvent.SEQUENCE_ADDED, sequenceAdded);
					timeMachine.addEventListener(SequenceEvent.SEQUENCE_REMOVED, sequenceRemoved);
					timeMachine.addEventListener(SequenceEvent.LABEL_CHANGED, sequenceLabelChanged);
					updateSequenceOptions();
					updateSequenceIndices();
					break;
			}
		}
		
		override public function trigger():void
		{
			if (random.value) 
			{
				var rIndex:int = currentSequenceIndex;
				while (rIndex == currentSequenceIndex) rIndex = int(Math.random() * (sequenceEndIndex-sequenceStartIndex) + sequenceStartIndex +.5);
				currentSequenceIndex = rIndex;
				
			}
			
			
			if (solo.value)
			{
				if (timeMachine != null) timeMachine.stopAllSequences();
			}
			
			if (currentSequenceIndex < timeMachine.sequencesList.length)
			{
				timeMachine.sequencesList[currentSequenceIndex].play();
			}
			
			super.trigger();
			
			
			var reverse:Boolean = sequenceEndIndex < sequenceStartIndex;
			
			if ((!reverse && (currentSequenceIndex >= sequenceEndIndex)) || (reverse && (currentSequenceIndex <= sequenceEndIndex)))
			{
				if (loop.value) currentSequenceIndex = (!reverse)?sequenceStartIndex-1:sequenceStartIndex+1;
			}
			
			if (!reverse) currentSequenceIndex = Math.min(Math.max(currentSequenceIndex + 1, sequenceStartIndex), sequenceEndIndex);
			else currentSequenceIndex = Math.min(Math.max(currentSequenceIndex - 1, sequenceEndIndex),sequenceStartIndex);
			
		}
		
		private function updateSequenceOptions():void
		{
			var options:Array = new Array();
			for each(var s:Sequence in timeMachine.sequencesList) options.push( { id:s.id, label:s.label, value:s } );
			
			sequenceStartParam.setOptions(options);
			sequenceEndParam.setOptions(options);
		}
		
		private function sequenceAdded(e:SequenceEvent):void 
		{
			updateSequenceOptions();
		}
		
		private function sequenceRemoved(e:SequenceEvent):void 
		{
			updateSequenceOptions();
		}
		
		private function sequenceLabelChanged(e:SequenceEvent):void 
		{
			updateSequenceOptions();
		}
		
		//util
		private function updateFeedbackTF():void 
		{
			if(feedbackTF.text != null) feedbackTF.text = "Prochain : Sequence "+ currentSequenceIndex+ " ["+(sequenceStartIndex+1)+">"+(sequenceEndIndex+1)+"]";
		}
		
		
		override public function clean():void 
		{
			super.clean();
			if (timeMachine != null) 
			{
				timeMachine.removeEventListener(SequenceEvent.SEQUENCE_ADDED, sequenceAdded);
				timeMachine.removeEventListener(SequenceEvent.SEQUENCE_REMOVED, sequenceRemoved);
				timeMachine.removeEventListener(SequenceEvent.LABEL_CHANGED, sequenceLabelChanged);
			}
			
		}
		
		public function get currentSequenceIndex():int 
		{
			return _currentSequenceIndex;
		}
		
		public function set currentSequenceIndex(value:int):void 
		{
			_currentSequenceIndex = value;
			updateFeedbackTF();
		}
		
		
		
		
	}

}