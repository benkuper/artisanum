package timemachine.modules.commands 
{
	import flash.events.Event;
	import modules.output.commands.Command;
	import modules.parameters.ParameterBool;
	import modules.parameters.ParameterEnum;
	import timemachine.sequences.Sequence;
	import timemachine.sequences.SequenceEvent;
	import timemachine.TimeMachine;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TimeMachineSequenceCommand extends Command 
	{
		protected var op:String;
		protected var timeMachine:TimeMachine;
		
		protected var sequenceParam:ParameterEnum;
		
		protected var solo:ParameterBool;
		
		public function TimeMachineSequenceCommand(label:String, guiMode:String="") 
		{
			super(label, guiMode);
			sequenceParam = addParameter(new ParameterEnum("targetSequence", "[Séquence]", null), "Séquence cible", true) as ParameterEnum;
			sequenceParam.guiWidth = 200;
			
			solo = addParameter(new ParameterBool("solo", "Solo", false), "Arrêter les autres séquences", true) as ParameterBool;
			solo.gui.y = sequenceParam.gui.y + sequenceParam.gui.height + 5;
		}
		
		
		override protected function setProp(propName:String, value:*):void 
		{
			super.setProp(propName, value);
			switch(propName)
			{
				case "op":
					this.op = value;
					switch(op)
					{
						case "play":
							solo.gui.visible = true;
							sequenceParam.gui.visible = true;
							break;
							
						case "pause":
						case "toggle":
						case "stop":
							solo.gui.visible = false;
							sequenceParam.gui.visible = true;
							break;
							
						case "stopAll":
							solo.gui.visible = false;
							sequenceParam.gui.visible = false;
							break;
						
					}
					break;
				
				case "timeMachine":
					this.timeMachine = value as TimeMachine;
					timeMachine.addEventListener(SequenceEvent.SEQUENCE_ADDED, sequenceAdded);
					timeMachine.addEventListener(SequenceEvent.SEQUENCE_REMOVED, sequenceRemoved);
					timeMachine.addEventListener(SequenceEvent.LABEL_CHANGED, sequenceLabelChanged);
					updateSequenceOptions();
					break;
			}
		}
		
		
		override public function trigger():void 
		{
			super.trigger();
			
			if (op != "stopAll")
			{
				if (sequenceParam.selectedItem == null) return;
				var sequence:Sequence = sequenceParam.selectedItem.value as Sequence;
				if (sequence == null) return;
			}
			
			//trace("trigger, op =" + op);
			
			switch(op)
			{
				case "play":
					if (sequence != null) 
					{
						if (solo.value && timeMachine != null) timeMachine.stopAllSequences();
						sequence.play();
					}
					break;
					
				case "pause":
					if (sequence != null) sequence.pause();
					break;
					
				case "stop":
					if (sequence != null) sequence.stop();
					break;
					
				case "toggle":
					if (sequence != null) 
					{
						if (sequence.playing) sequence.pause();
						else sequence.play();
					}
					break;
				
				case "stopAll":
					if (timeMachine != null) timeMachine.stopAllSequences();
					break;
			}
		}
		
		private function updateSequenceOptions():void
		{
			trace("updateSequenceOptions :", timeMachine.sequencesList);
			var options:Array = new Array();
			for each(var s:Sequence in timeMachine.sequencesList) options.push( { id:s.id, label:s.label, value:s } );
			
			sequenceParam.setOptions(options);
		}
		
		private function sequenceAdded(e:SequenceEvent):void 
		{
			updateSequenceOptions();
		}
		
		private function sequenceRemoved(e:SequenceEvent):void 
		{
			updateSequenceOptions();
		}
		
		private function sequenceLabelChanged(e:SequenceEvent):void 
		{
			updateSequenceOptions();
		}
		
		override public function clean():void 
		{
			super.clean();
			if (timeMachine != null) 
			{
				timeMachine.removeEventListener(SequenceEvent.SEQUENCE_ADDED, sequenceAdded);
				timeMachine.removeEventListener(SequenceEvent.SEQUENCE_REMOVED, sequenceRemoved);
				timeMachine.removeEventListener(SequenceEvent.LABEL_CHANGED, sequenceLabelChanged);
			}
			
			
		}
		
		
		
		
	}

}