package timemachine.modules 
{
	import modules.input.InputModule;
	import timemachine.TimeMachine;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TimeMachineInputModule extends InputModule 
	{
		private var timeMachine:TimeMachine;
		
		public function TimeMachineInputModule(timeMachine:TimeMachine) 
		{
			super(true);
			this.timeMachine = timeMachine;
			this.id = "timeMachineInput";
			
		}
		
	}

}