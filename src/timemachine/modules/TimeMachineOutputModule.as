package timemachine.modules 
{
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.CommandType;
	import modules.output.OutputModule;
	import timemachine.modules.commands.TimeMachineSequenceCommand;
	import timemachine.modules.commands.TimeMachineSequenceGroupCommand;
	import timemachine.TimeMachine;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TimeMachineOutputModule extends OutputModule 
	{
		private var timeMachine:TimeMachine;
		
		public function TimeMachineOutputModule(timeMachine:TimeMachine) 
		{
			super();
			this.timeMachine = timeMachine;
			this.id = "timeMachineOutput";
			
			addCommandTypes(
				new CommandType("playSequence","Jouer", TimeMachineSequenceCommand, CommandType.TRIGGER, { op:"play",timeMachine:timeMachine }),
				new CommandType("playGroupSequence","Jouer un groupe", TimeMachineSequenceGroupCommand, CommandType.TRIGGER, { timeMachine:timeMachine }),
				new CommandType("pauseSequence", "Pause", TimeMachineSequenceCommand, CommandType.TRIGGER, { op:"pause", timeMachine:timeMachine } ),
				new CommandType("stopSequence", "Stop", TimeMachineSequenceCommand, CommandType.TRIGGER, { op:"stop", timeMachine:timeMachine } ),
				new CommandType("stopAllSequence", "Arrêter tout", TimeMachineSequenceCommand, CommandType.TRIGGER, { op:"stopAll", timeMachine:timeMachine } ),
				new CommandType("togglePlay","Jouer/Pause", TimeMachineSequenceCommand, CommandType.TRIGGER, { op:"toggle",timeMachine:timeMachine })
			);
		}
		
	}

}