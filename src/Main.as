﻿package 
{
	import benkuper.util.KeyboardUtil;
	import benkuper.util.osc.OSCMaster;
	import benkuper.util.StageUtil;
	import com.laiyonghao.Uuid;
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.events.InvokeEvent;
	import flash.filesystem.File;
	import flash.system.Capabilities;
	import io.Project;
	import io.ProjectEvent;
	import modules.ModuleTypes;
	import ui.fonts.Fonts;
	import ui.guider.Guider;
	import ui.menu.Menu;
	import ui.Toaster;
	import ui.Tooltip;
	import MainFrame;
	import update.UpdateManager;
	
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Main extends Sprite 
	{
		private var frame:MainFrame;
		private var updateManager:UpdateManager;
		
		public function Main():void 
		{
			//utils
			StageUtil.init(stage);
			StageUtil.setNoScale();
			KeyboardUtil.init(stage);
			
			//update
			updateManager = new UpdateManager();
			updateManager.checkUpdate();
			
			//KeyboardUtil.init(stage);
			
			//not used for now
			//KeyboardUtil.init(stage);
			
			//menu
			Menu.init(stage);
			
			
			//Project
			Project.init();
			Project.instance.addEventListener(ProjectEvent.PROJECT_LOADED, projectLoaded);
			
			
			//font
			Fonts.init();
			
			//osc
			OSCMaster.init();
			
			//ui
			Toaster.init(stage);
			Tooltip.init(stage);
			
			//modules
			ModuleTypes.init();
			
			//guider
			Guider.init(stage);
			
			//main frame
			frame = new MainFrame();
			addChild(frame);

			NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, invokeHandler);
		}
		
		private function invokeHandler(e:InvokeEvent):void 
		{
			if (e.arguments.length > 0)
			{
				Project.loadProjectFile(File.documentsDirectory.resolvePath(e.arguments[0]));
			}else if (Project.loadOnStartup)
			{
				//Project.load();
			}
			
			NativeApplication.nativeApplication.activeWindow.title = "Artisanum " + UpdateManager.instance.appVersion + " - Nouveau Projet";
			
		}
		
		private function projectLoaded(e:ProjectEvent):void 
		{
			//debug
			frame.loadXML(e.projectData);
			return;
			
			//prod
			try {
				frame.loadXML(e.projectData);
			}catch (e:Error)
			{
				Toaster.error("Erreur durant le chargement de la sauvegarde");
				trace(e.getStackTrace());
			}
			
		}
		
		
	}
	
}