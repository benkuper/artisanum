package benkuper.util 
{
	import flash.display.DisplayObject;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class KeyboardTarget 
	{
		
		public var target:DisplayObject;
		public var callback:Function;
		
		
		public function KeyboardTarget(target:DisplayObject, callback:Function):void
		{
			this.callback = callback;
			this.target = target;
			
		}
		
	}

}