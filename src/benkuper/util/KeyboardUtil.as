package benkuper.util 
{
	import flash.display.DisplayObject;
	import flash.display.Stage;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.ui.Keyboard;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class KeyboardUtil 
	{
		static private var stage:Stage;
		
		private static var _ctrlPressed:Boolean;
		private static var _shiftPressed:Boolean;
		private static var _altPressed:Boolean;
		
		private static var targets:Vector.<KeyboardTarget>;
		private static var lastMouseTarget:DisplayObject;
		private static var mouseBubbleTargets:Vector.<DisplayObject>;
		
		public function KeyboardUtil() 
		{
			
		}
		
		public static function init(stage:Stage):void
		{
			stage = stage;
			
			targets = new Vector.<KeyboardTarget>();
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyHandler);
			stage.addEventListener(KeyboardEvent.KEY_UP, keyHandler);
			stage.addEventListener(MouseEvent.CLICK, mouseClick);
		}
		
		static private function mouseClick(e:MouseEvent):void 
		{
			lastMouseTarget = e.target as DisplayObject;
			mouseBubbleTargets = new Vector.<DisplayObject>();
			
			var bubbleTarget:DisplayObject = lastMouseTarget;
			
			while (bubbleTarget != stage)
			{
				mouseBubbleTargets.push(bubbleTarget);
				bubbleTarget = bubbleTarget.parent;
			}
			
			//trace("new bubbles :", mouseBubbleTargets);
		}
		
		
		public static function addTarget(target:DisplayObject, callback:Function):void
		{
			if (getTargetIndex(target,callback) != -1) return;
			targets.push( new KeyboardTarget(target, callback) );
		}
		
		public static function removeTarget(target:DisplayObject, callback:Function):void
		{
			targets.splice(getTargetIndex(target, callback), 1);
		}
		
		public static function getTargetIndex(target:DisplayObject, callback:Function):int
		{
			for (var i:int = 0; i < targets.length; i++)
			{
				if (targets[i].target == target && targets[i].callback == callback) return i;
			}
			
			return -1;
		}
		
		
		static private function keyHandler(e:KeyboardEvent):void 
		{
			_shiftPressed = e.shiftKey;
			_altPressed = e.altKey;
			_ctrlPressed = e.ctrlKey;
			
			for each(var t:KeyboardTarget in targets)
			{
				for each(var d:DisplayObject in mouseBubbleTargets)
				{
					if (t.target == d) t.callback.apply(t.target,[e.clone()]);
				}
			}
			
		}
		
		static public function get ctrlPressed():Boolean 
		{
			return _ctrlPressed;
		}
		
		static public function get shiftPressed():Boolean 
		{
			return _shiftPressed;
		}
		
		static public function get altPressed():Boolean 
		{
			return _altPressed;
		}
	}

}