package io 
{
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileFilter;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	import ui.menu.Menu;
	import ui.Toaster;
	import update.UpdateManager;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Project extends EventDispatcher
	{
		//data
		
		private static var loadFile:File;
		
		private static var stream:FileStream;
		
		static private var dataToSave:XML;
		static public var instance:Project;
		
		public static var loadOnStartup:Boolean;
		public static var saveOnExit:Boolean;
		
		//autosave
		public static var maxAutoSave:int;
		public static var currentAutoSaveIndex:int;
		
		//public static var autosaveInterval:int; //minutes
		//static private var autosaveTimer:Timer;
		
		
		private static var _currentFile:File;
		
		public function Project() 
		{
			
		}
		
		public static function init():void
		{
			if (instance != null) return;
			
			loadOnStartup = false;
			saveOnExit = true;
			
			maxAutoSave = 10;
			currentAutoSaveIndex = 0;
			
			instance =  new Project();
			var artDir:File = File.documentsDirectory.resolvePath("Artisanum");
			if (!artDir.exists) artDir.createDirectory();

			
			loadFile = File.documentsDirectory.resolvePath("Artisanum");
			loadFile.addEventListener(Event.SELECT, fileLoadSelect);
			
			stream = new FileStream();
			
			trace("Project IO init");
			
			Menu.addMenu("newFile","Fichier/Nouveau", reset,"n");
			Menu.addMenu("openFile","Fichier/Ouvrir", load,"o");
			Menu.addMenu("saveFile","Fichier/Sauver", save,"s");
			Menu.addMenu("saveAs", "Fichier/Sauver sous...", saveAs, "S");
			Menu.addMenu("saveOnExit", "Fichier/Sauvegarder à la fermeture",null,"",null,true,true);
			
			//autosaveTimer = new Timer(autosaveInterval * 1000);
			
			
			NativeApplication.nativeApplication.addEventListener(Event.EXITING, appExit);
		}
		
		
		public static function reset():void
		{
			MainFrame.instance.reset();
		}
		
		
		
		public static function save():void
		{
			Project.dataToSave = getXMLData();
			
			if (currentFile == null)
			{
				saveAs();
				return;
			}
			
			saveProject();
		}
		
		public static function saveAs():void
		{
			Project.dataToSave = getXMLData();
			if (currentFile == null) currentFile = File.documentsDirectory.resolvePath("Artisanum/Nouveau Projet.art");
			currentFile.browseForSave("Sauver le projet sous...");
			
		}
		
		static private function fileSaveSelect(e:Event):void 
		{
			trace("File save As selected : "+currentFile.nativePath);
			saveProject();
		}
		
		static private function saveProject():void 
		{
			
			trace("save Project :", currentFile.nativePath);
			
			if (currentFile.extension != "art")
			{
				trace("extension is not .art");
				currentFile = currentFile.resolvePath("../" + currentFile.name + ".art");
			}
			
			stream.open(currentFile, FileMode.WRITE);		
			stream.writeUTFBytes(dataToSave.toXMLString());
			stream.close();
			
			//autoSave
			var spl:Array = currentFile.name.split(".");
			spl.pop();
			var nameNoExt:String = spl.join(".");
			var autosaveFolder:File = currentFile.resolvePath("../" + nameNoExt+"_autosave");
			if (!autosaveFolder.exists) autosaveFolder.createDirectory();
			var autosaveFile:File =  autosaveFolder.resolvePath(nameNoExt + "_autosave" + currentAutoSaveIndex + ".art");
			stream.open(autosaveFile, FileMode.WRITE);		
			stream.writeUTFBytes(dataToSave.toXMLString());
			stream.close();
			currentAutoSaveIndex = (currentAutoSaveIndex + 1) % maxAutoSave;
			
			dataToSave = null;
			
			
			updateWindowTitle();
			
			Toaster.success("Projet enregistré !");
			
			
		}
		
		public static function load():void
		{			
			loadFile.browseForOpen("Ouvrir un projet",[new FileFilter("Artisanum Project File","*.art")]);
			
		}
		
		static private function fileLoadSelect(e:Event):void 
		{
			loadProjectFile(loadFile);
		}
		
		public static function loadProjectFile(file:File):void
		{
			if (!file.exists)
			{
				Toaster.error("Fichier introuvable :\n" + file.nativePath);
				return;
			}
			
			stream.open(file, FileMode.READ);
			var data:XML = new XML(stream.readUTFBytes(stream.bytesAvailable));
			stream.close();
			
			currentFile = File.documentsDirectory.resolvePath(file.nativePath);
			
			Toaster.info("Chargement du projet : " + file.name);
			
			instance.dispatchEvent(new ProjectEvent(ProjectEvent.PROJECT_LOADED,data));
			
			updateWindowTitle();
			
		}
		
		
		public static function getXMLData():XML
		{
			var data:XML = new XML();
			return MainFrame.instance.getXML();
		}
		
		
		static private function appExit(e:Event):void 
		{
			saveOnExit = Menu.getChecked("saveOnExit");
			if (saveOnExit && currentFile != null) save();
		}
		
		
		static public function get currentFile():File 
		{
			return _currentFile;
		}
		
		static public function set currentFile(value:File):void 
		{
			if (currentFile == value) return;
			if (currentFile != null) 
			{
				currentFile.removeEventListener(Event.SELECT, fileSaveSelect);
			}
			
			_currentFile = value;
			
			if (currentFile != null) 
			{
				currentFile.addEventListener(Event.SELECT, fileSaveSelect);
				
				updateWindowTitle();
			}
			
			currentAutoSaveIndex = 0;
		}
		
		static private function updateWindowTitle():void 
		{
			if (!NativeApplication.nativeApplication.activeWindow.closed)
			{
				NativeApplication.nativeApplication.activeWindow.title = "Artisanum " + UpdateManager.instance.appVersion + " - " + currentFile.name;
			}
		}
	}
	

}