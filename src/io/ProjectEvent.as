package io 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ProjectEvent extends Event 
	{
		static public const PROJECT_LOADED:String = "projectLoaded";
		static public const PROJECT_READY:String = "projectReady";
		
		public var projectData:XML;
		
		public function ProjectEvent(type:String, projectData:XML = null, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			this.projectData = projectData;
			
		} 
		
		public override function clone():Event 
		{ 
			return new ProjectEvent(type, projectData, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ProjectEvent", "type", "projectData", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}