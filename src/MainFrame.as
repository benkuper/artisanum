package 
{
	import io.ProjectEvent;
	import statemachine.behaviors.actions.Action;
	import statemachine.behaviors.common.Condition;
	import statemachine.behaviors.common.Consequence;
	import com.greensock.TweenLite;
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import io.Project;
	import modules.events.ModuleEvent;
	import modules.Module;
	import modules.ModulesPanel;
	import modules.ModuleType;
	import modules.ModuleTypes;
	import modules.output.OutputModule;
	import statemachine.behaviors.mappings.Mapping;
	import statemachine.StateMachine;
	import timemachine.sequences.SequencesPanel;
	import timemachine.TimeMachine;
	import ui.components.UIEvent;
	import ui.Layout;
	import ui.menu.Menu;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class MainFrame extends Sprite 
	{
		//data
		private var inputsPanel:ModulesPanel;
		private var outputsPanel:ModulesPanel;
		private var stateMachine:StateMachine;
		private var timeMachine:TimeMachine;
		
		//async load
		private var _ready:Boolean;
		
		//guide
		private var _guideMode:Boolean;
		private var _guideStep:int;
		
		
		public static var instance:MainFrame;
		
		public function MainFrame() 
		{
			instance = this;
			
			ready = true;
			
			stateMachine = new StateMachine();
			addChild(stateMachine);
			
			inputsPanel = new ModulesPanel(ModuleType.INPUT);
			inputsPanel.side = Layout.SIDE_LEFT;
			addChild(inputsPanel);
			inputsPanel.addEventListener(ModuleEvent.MODULE_ADDED, inputChange);
			inputsPanel.addEventListener(ModuleEvent.MODULE_REMOVED, inputChange);
			
			outputsPanel = new ModulesPanel(ModuleType.OUTPUT);
			addChild(outputsPanel);
			outputsPanel.side = Layout.SIDE_RIGHT;
			outputsPanel.addEventListener(ModuleEvent.MODULE_ADDED, outputChange);
			outputsPanel.addEventListener(ModuleEvent.MODULE_REMOVED, outputChange);
			
			
			Menu.addSeparator("State Machine");
			Menu.addMenu("toggleInputPanel", "State Machine/Basculer les modules d'entrée", inputsPanel.toggleOpened,"I");
			Menu.addMenu("toggleOutputPanel", "State Machine/Basculer les modules de sortie", outputsPanel.toggleOpened,"O");
			Menu.addMenu("toggleMiniModes", "State Machine/Basculer les mini-modes", stateMachine.switchMiniModeAllBehaviors,"M");
			
			
			
			timeMachine = new TimeMachine();
			addChild(timeMachine);
			timeMachine.addEventListener(UIEvent.PANEL_OPENED, timeMachineOpenClose);
			timeMachine.addEventListener(UIEvent.PANEL_CLOSED, timeMachineOpenClose);
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			
			
			
			Menu.addMenu("showGuider", "Aide/Guide de la State Machine", showGuider, "H");
			
			Menu.addMenu("showGuider", "Fenêtre/Replacer la fenêtre au centre", recenterWindow,null );
		}
		
		
		
		
		//handlers
		private function addedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			stage.addEventListener(Event.RESIZE, stageResize);
			
			//TweenLite.delayedCall(1,inputsPanel.showFirstGuider);
		}
		
		private function inputChange(e:ModuleEvent):void 
		{
			if (guideMode && guideStep == 1) guideStep = 2;
			Condition.updateAllInputOptions();
			Mapping.updateAllInputOptions();
		}
		
		private function outputChange(e:ModuleEvent):void 
		{
			if (guideMode && guideStep == 2) guideStep = 3;
			//trace("output change, output Modules :", OutputModule.getModuleList());
			Consequence.updateAllOutputOptions();
			Mapping.updateAllOutputOptions();
		}
		
		
		private function timeMachineOpenClose(e:UIEvent):void 
		{
			draw();
		}
		
		
		private function stageResize(e:Event):void 
		{
			draw(true);
		}
		
		
		//ui
		
		private function draw(replacePanels:Boolean = false ):void 
		{
			
			timeMachine.width = stage.stageWidth;
			timeMachine.height = 300;
			
			stateMachine.x = 0; // inputsPanel.x + inputsPanel.width;
			stateMachine.y = 0;
			stateMachine.width = stage.stageWidth; // stage.stageWidth - inputsPanel.width - outputsPanel.width;
			stateMachine.height = timeMachine.opened?stage.stageHeight-timeMachine.height:stage.stageHeight-10;
			
			inputsPanel.height = stage.stageHeight-(timeMachine.opened?timeMachine.height:0);
			inputsPanel.width = 280;//stage.stageWidth / 3;
			
			outputsPanel.width = 250;
			outputsPanel.height = stage.stageHeight - (timeMachine.opened?timeMachine.height:0);
			
			if (replacePanels) replaceElements();
			
		}
		
		private function replaceElements():void
		{
			timeMachine.x = 0;
			timeMachine.y = timeMachine.opened?stage.stageHeight - timeMachine.height:stage.stageHeight;
			
			inputsPanel.x = inputsPanel.opened?0:-inputsPanel.width;
			inputsPanel.y = 0;
			
			outputsPanel.x = outputsPanel.opened?stage.stageWidth - outputsPanel.width:stage.stageWidth;
			outputsPanel.y = 0;
			
		}
		
		public function reset():void
		{
			inputsPanel.reset();
			outputsPanel.reset();
			stateMachine.reset();
			timeMachine.reset();
		}
		
		public function getXML():XML
		{
			var xml:XML = <project></project>;
			
			xml.appendChild(inputsPanel.getXML(<inputs></inputs>));
			xml.appendChild(outputsPanel.getXML(<outputs></outputs>));
			xml.appendChild(stateMachine.getXML());
			xml.appendChild(timeMachine.getXML());
			return xml;
		}
		
		public function loadXML(data:XML):void 
		{
			_ready = false;
			
			inputsPanel.loadXML(data.inputs[0]);
			outputsPanel.loadXML(data.outputs[0]);
			stateMachine.loadXML(data.stateMachine[0]);
			if (data.timeMachine.length() > 0) timeMachine.loadXML(data.timeMachine[0]);
			
			_ready = true;
			dispatchEvent(new ProjectEvent(ProjectEvent.PROJECT_READY));
		}
		
		
		//recenter window
		private function recenterWindow():void 
		{
			stage.nativeWindow.x = 20;
			stage.nativeWindow.y = 20;
			stage.nativeWindow.bounds.width = 800;
			stage.nativeWindow.bounds.height = 600;
		}
		
		
		//guide
		
		private function showGuider():void 
		{
			guideMode = true;
		}
		
		
		public function get guideMode():Boolean 
		{
			return _guideMode;
		}
		
		public function set guideMode(value:Boolean):void 
		{
			_guideMode = value;
			guideStep = 1;
		}
		
		public function get guideStep():int 
		{
			return _guideStep;
		}
		
		public function set guideStep(value:int):void 
		{
			
			_guideStep = value;
			
			if (value)
			{
				switch(guideStep)
				{
					case 1:
						inputsPanel.opened = true;
						TweenLite.delayedCall(.2, inputsPanel.showFirstGuider);
						break;
						
					case 2:
						outputsPanel.opened = true;
						TweenLite.delayedCall(1,outputsPanel.showFirstGuider);
						break;
						
					case 3:
						TweenLite.delayedCall(1,stateMachine.showFirstGuider);
						break;
				}
			}
		}
		
		public function get ready():Boolean 
		{
			return _ready;
		}
		
		public function set ready(value:Boolean):void 
		{
			_ready = value;
		}
		
		
	}

}