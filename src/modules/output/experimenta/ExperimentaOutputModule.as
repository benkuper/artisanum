package modules.output.experimenta 
{
	import benkuper.util.IPUtil;
	import benkuper.util.osc.OSCMaster;
	import modules.events.ParameterEvent;
	import modules.output.commands.Command;
	import modules.output.commands.CommandType;
	import modules.output.IOSCOutputModule;
	import modules.output.OutputModule;
	import modules.parameters.ParameterString;
	import org.tuio.osc.IOSCListener;
	import org.tuio.osc.OSCMessage;
	import ui.components.Button;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author ...
	 */
	public class ExperimentaOutputModule extends OutputModule implements IOSCOutputModule
	{
		private var ip:ParameterString;
		private var port:ParameterString;
		
		private var localIP:String;
		private var scanBT:Button;
		
		
		public function ExperimentaOutputModule() 
		{
			super();
			
			localIP = IPUtil.getLocalIP();
			
			ip = addParameter(new ParameterString("ip", "IP", "127.0.0.1") ) as ParameterString;
			port =  addParameter(new ParameterString("port", "Port", "8000")) as ParameterString;
			
			ip.addEventListener(ParameterEvent.VALUE_CHANGE, valueChange);
			port.addEventListener(ParameterEvent.VALUE_CHANGE, valueChange);
			
			addCommandTypes(
				new CommandType("color", "Couleur", ExperimentaColorCommand, CommandType.MAPPING, { address:"/artisanum/hue" } ),
				new CommandType("rotX", "Rotation X", ExperimentaCommand, CommandType.MAPPING, { address:"/artisanum/posX" } ),
				new CommandType("size", "Taille", ExperimentaCommand, CommandType.MAPPING, { address:"/artisanum/size" } )
			);
			
		}
		private function valueChange(e:ParameterEvent):void 
		{
			trace("valueChange");
		}
		
		
		public function sendMessage(message:OSCMessage):void
		{
			OSCMaster.sendTo(message, ip.value, int(port.value), true );
		}
		
		override protected function draw():void
		{
			ip.gui.x = 0;
			ip.gui.y = 0;
			ip.guiWidth = moduleWidth - 30;
			port.gui.x = 0;
			port.gui.y = 30;
			port.guiWidth = moduleWidth - 30;
			
			super.draw();
		}
		
		
		override public function clean():void
		{
			super.clean();
			ip.removeEventListener(ParameterEvent.VALUE_CHANGE, valueChange);
			port.removeEventListener(ParameterEvent.VALUE_CHANGE, valueChange);
		}
	}

}