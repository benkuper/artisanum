package modules.output.experimenta 
{
	import modules.events.ParameterEvent;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterInt;
	
	/**
	 * ...
	 * @author ...
	 */
	public class ExperimentaCommand extends SendOSCCommand 
	{
		
		public function ExperimentaCommand(label:String, guiMode:String) 
		{
			super(label, guiMode);
		}
		
		
		override public function setValue(value:Number):void
		{
			args[0] = { label:"f", value:value };
			super.setValue(value);
		}
	}

}