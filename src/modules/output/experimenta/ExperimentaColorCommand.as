package modules.output.experimenta 
{
	import modules.events.ParameterEvent;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterEnum;
	import modules.parameters.ParameterFloat;
	import modules.parameters.ParameterInt;
	
	/**
	 * ...
	 * @author ...
	 */
	public class ExperimentaColorCommand extends SendOSCCommand 
	{
		
		private var colorParam:ParameterEnum;
		
		public function ExperimentaColorCommand(label:String, guiMode:String) 
		{
			super(label, guiMode);
			
			colorParam = addParameter(ParameterEnum.create("couleur", "Couleur", ["Bleu", 250], [ "Rouge", 360 ], [ "Orange", 30 ] , [ "Mauve", 300 ], [ "Cyan", 175 ]),"",  false) as ParameterEnum;
			
			if (guiMode == CommandGUIModes.EDITOR_TRIGGER)
			{
				colorParam.gui.x = 180;
				//colorParam.gui.y = 0;
				colorParam.guiWidth = 50;
				gui.addChild(colorParam.gui);
			}

		}
		
		override public function trigger():void
		{
			args[0] = { label:"f", value:(colorParam.selectedItem ==null)?0:colorParam.selectedItem.value };
			super.trigger();
		}
		
		override public function setValue(value:Number):void
		{
			args[0] = { label:"f", value:360*value};
			super.setValue(value);
		}
	}

}