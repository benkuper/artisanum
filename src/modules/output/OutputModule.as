package modules.output 
{
	import modules.Module;
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.CommandType;
	import ui.components.DropDown;
	import ui.components.Panel;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class OutputModule extends Module 
	{
		
		protected var commandTypes:Vector.<CommandType>;
		
		public static var moduleList:Vector.<OutputModule>;
		
		
		//protected var parametersContainer:Panel;
		protected var commandTester:Panel;
		protected var commandTestList:DropDown;
		
		private var _currentTestCommand:Command;
		
		
		public function OutputModule() 
		{
			if (moduleList == null) moduleList = new Vector.<OutputModule>;
			moduleList.push(this);
			
			commandTypes = new Vector.<CommandType>;
			
			/*
			parametersContainer = new Panel("Paramètres du module", moduleWidth, 100);
			parametersContainer.headerHeight = 30;
			parametersContainer.transparent = true;
			parametersContainer.borders = true;
			contentContainer.addChild(parametersContainer);
			*/
			
			commandTester = new Panel("Tester une commande", moduleWidth, 100);
			commandTester.headerHeight = 30;
			//commandTester.transparent = true;
			//commandTester.borders = true;
			contentContainer.addChild(commandTester);
			
			commandTestList = new DropDown(getCommandsList(), "Commandes");
			commandTester.addChild(commandTestList);
			commandTestList.addEventListener(UIEvent.DROPDOWN_CHANGE, commandTestListChange);
			commandTestList.width = 70;
			commandTestList.height = 20;
			
		}		
		
		//data 
		protected function addCommandTypes(...arguments):void 
		{
			for each(var a:CommandType in arguments)
			{
				a.module = this;
				commandTypes.push(a);
			}
			
			commandTestList.setOptions(getCommandsList());
		}
		
		// TODO : à remonter
		//protected function addOutputParameter(newParam:Parameter, tooltip:String = ""):Parameter // TODO :  checker la classe de newParam  ?
		//{
			//super.addParameter(newParam, tooltip);
			//parametersContainer.addElement(newParam.gui);
			//
			//return newParam;
		//}
		//
		//protected function removeOutputParameter(param:Parameter):void // TODO :  checker la classe de newParam  ?
		//{
			//super.removeParameter(param);
			//parametersContainer.removeElement(param.gui);
		//}
		
		//ui
		override protected function draw():void
		{
			//parametersContainer.x = 10;
			//parametersContainer.y = 0;
			//parametersContainer.width = moduleWidth -20;
			
			commandTester.x = 10;
			commandTester.y = 0;// parameterContainer.y + parameterContainer.height + 10;
			commandTester.width = moduleWidth - 20;
			
			commandTestList.x = commandTester.width - commandTestList.width - 5;
			commandTestList.y = 5;
			
			if (currentTestCommand != null)
			{
				//currentTestCommand.gui.x = 10;
				currentTestCommand.guiWidth = commandTester.width - 10;
				commandTester.height = currentTestCommand.gui.height + commandTester.headerHeight + 40;
			}
			
			moduleHeight = contentContainer.y + commandTester.y + commandTester.height + 10; 
			
			dispatchEvent(new UIEvent(UIEvent.PANEL_RESIZED));
			super.draw();
		}
		
		
		//handlers
		private function commandTestListChange(e:UIEvent):void 
		{
			var commandType:CommandType = commandTestList.selectedOption.value as CommandType;
			var CommandClass:Class = commandType.typeClass;
			
			//trace("command test list change !",commandType);
			currentTestCommand = commandType.createCommand(CommandGUIModes.OUTPUT_MODULE);
		}	
		
		//clean
		override public function clean():void
		{
			commandTestList.removeEventListener(UIEvent.DROPDOWN_CHANGE, commandTestListChange);
			commandTester.clean();
			
			moduleList.splice(moduleList.indexOf(this), 1);
			
			currentTestCommand = null;
		}
		
		
		//util
		public function getCommandsList(filter:String = null):Array
		{
			var a:Array = new Array();
			for each(var ct:CommandType in commandTypes)
			{
				if (filter == null || filter == ct.commandType || ct.commandType == CommandType.BOTH)
				{
					a.push( {id:ct.id, label:ct.label, value:ct } );
				}
			}
			
			return a;
		}
		
		//static module list providing
		public static function getModuleList():Array
		{
			var a:Array = new Array();
			for each(var m:OutputModule in moduleList)
			{
				a.push( {id:m.id, label:m.moduleName, value:m } );
			}
			return a;
		}
		
		public function get currentTestCommand():Command 
		{
			return _currentTestCommand;
		}
		
		public function set currentTestCommand(value:Command):void 
		{
			if (currentTestCommand != null)
			{
				currentTestCommand.clean();
				commandTester.removeElement(currentTestCommand.gui);
			}
			
			_currentTestCommand = value;
			
			if (currentTestCommand != null)
			{
				commandTester.addElement(currentTestCommand.gui);
			}
			
			draw();
		}
	}

}