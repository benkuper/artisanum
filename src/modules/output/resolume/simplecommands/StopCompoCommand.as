package modules.output.resolume.simplecommands 
{
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterInt;
	
	/**
	 * ...
	 * @author ...
	 */
	public class StopCompoCommand extends SendOSCCommand
	{
		
		//private var layer:ParameterInt;
		//private var clip:ParameterInt;
		
		public function StopCompoCommand(label:String,guiMode:String) 
		{
			super(label, guiMode);
			
			//layer = addParameter(new ParameterInt("Calque", 1,1, 8),"Calque à arrêter",true) as ParameterInt;
			//clip = addParameter(new ParameterInt("Clip", 1, 1, 8),"Numéro du clip cible",true) as ParameterInt;
			//
			//layer.gui.x = 10;
			//layer.guiWidth = 160;
			//clip.gui.x = 10;
			//clip.gui.y = 25;
			//clip.guiWidth = 160;
			
			args = new Array();
			
		}
		
		override protected function updateGUI():void
		{
			super.updateGUI();
			//if (guiMode != CommandGUIModes.OUTPUT_MODULE) triggerGUI.x = 250;
		}
		
		override public function trigger():void
		{
			
			address = "/composition/disconnectall";
			args = [{label:"i",value:1}];
			
			super.trigger();
		}
	}

}