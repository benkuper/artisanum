package modules.output.resolume.simplecommands 
{
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterFloat;
	import modules.parameters.ParameterInt;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class LayerSpeedCommand extends SendOSCCommand 
	{
		private var layer:ParameterInt;
		private var maxSpeed:ParameterFloat;
		
		public function LayerSpeedCommand(label:String, guiMode:String) 
		{
			super(label, guiMode);
			
			layer = addParameter(new ParameterInt("calque","Calque", 1,1, 8),"Calque ciblé",true) as ParameterInt;
			maxSpeed = addParameter(new ParameterFloat("vitesseMax","Vitesse max", 1, 0, 1),"Vitesse Max",true) as ParameterFloat;
			layer.gui.y = 25;
			maxSpeed.gui.y = 65;
			
			
		}
		
		override public function setValue(value:Number):void
		{
			if (value == .5) return;
			
			var direction:int = value < .5?0:1;
			address = "/layer" + layer.value +"/video/position/direction";
			args = [{ label:"i", value:direction }];
			super.setValue(value);
			address = "/layer" + layer.value + "/audio/position/direction"; //need audio if video has audio
			//super.setValue(value);
			
			var speed:Number = Math.abs((value)-.5) * 2 * maxSpeed.value +.1;
			
			address = "/layer" + layer.value + "/video/position/speed";
			args = [{ label:"f", value:speed }];
			super.setValue(value);
			
			//need audio if video has audio, maxValue is 2
			speed = Math.abs((value)-.5) * 2 * maxSpeed.value + .5;
			address = "/layer" + layer.value + "/audio/pitch/values"; 
			args = [{ label:"f", value:speed }];
			//super.setValue(value);
		}		
	}

}