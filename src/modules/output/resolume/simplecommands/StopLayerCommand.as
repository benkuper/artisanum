package modules.output.resolume.simplecommands 
{
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterInt;
	import ui.components.ButtonBar;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author ...
	 */
	public class StopLayerCommand extends SendOSCCommand
	{
		
		private var layer:ParameterInt;
		private var layerBar:ButtonBar;
		//private var clip:ParameterInt;
		
		public function StopLayerCommand(label:String,guiMode:String) 
		{
			super(label, guiMode);
			
			layer = addParameter(new ParameterInt("calque","Calque", 1,1, 11),"Calque à arrêter") as ParameterInt;
			//clip = addParameter(new ParameterInt("Clip", 1, 1, 8),"Numéro du clip cible",true) as ParameterInt;
			
			//layer.gui.x = 10;
			//layer.guiWidth = 160;
			//clip.gui.x = 10;
			//clip.gui.y = 25;
			//clip.guiWidth = 160;
			
			var options:Array = new Array();
			for (var i:int = 1; i <= 11; i++) options.push( { label:i.toString(), value:i } );
			layerBar = new ButtonBar("Calque", options);
			layerBar.buttonWidth = 20;
			layerBar.height = 20;
			gui.addChild(layerBar);
			layerBar.x = 10;
			layerBar.addEventListener(UIEvent.SELECTION_CHANGED, layerChanged);
			
			
			args = new Array();
			
			
		}
		
		
		private function layerChanged(e:UIEvent):void 
		{
			layer.value = layerBar.value;
		}
		
		override public function loadXML(data:XML):void
		{
			super.loadXML(data);
			layerBar.selectedIndex = layer.value -1;
		}
		
		override protected function updateGUI():void
		{
			super.updateGUI();
			//if (guiMode != CommandGUIModes.OUTPUT_MODULE) triggerGUI.x = 250;
		}
		
		override public function trigger():void
		{
			
			address = "/layer" + layer.value + "/clear";
			args = [{label:"i",value:1}];
			
			super.trigger();
		}
	}

}