package modules.output.resolume.simplecommands 
{
	import modules.events.ParameterEvent;
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.SendOSCCommand;
import modules.parameters.ParameterBool;
import modules.parameters.ParameterEnum;
	import modules.parameters.ParameterFloat;
	import modules.parameters.ParameterInt;
	import ui.components.ButtonBar;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author ...
	 */
	public class CibouletteWaveCommand extends SendOSCCommand
	{

		private var layer:ParameterInt;
		private var clip:ParameterInt;
		private var effectID:ParameterInt;
		private var effectParamID:ParameterInt;

        private var inverseParam:ParameterBool;

        private var minValParam:ParameterFloat;
        private var maxValParam:ParameterFloat;
		private var param:ParameterEnum;
		
		private var layerBar:ButtonBar;
		private var clipBar:ButtonBar;
		
		private var effectBar:ButtonBar;
		private var effectParamBar:ButtonBar;
		
		public function CibouletteWaveCommand(label:String,guiMode:String)
		{
			super(label, guiMode);
			
			layer = addParameter(new ParameterInt("calque","Calque", 1,1, 90),"Calque contenant le clip",false) as ParameterInt;
			clip = addParameter(new ParameterInt("clip","Clip", 1, 1, 90),"Numéro du clip cible",false) as ParameterInt;

            inverseParam = addParameter(new ParameterBool("inverse", "Inverse", false), "", true) as ParameterBool;

            minValParam = addParameter(new ParameterFloat("minVal", "valeur min", 0, 0, 1)) as ParameterFloat;
            maxValParam = addParameter(new ParameterFloat("maxVal", "valeur max", 1, 0, 1)) as ParameterFloat;

			args = new Array();
			
			
			var options:Array = new Array();
			for (var i:int = 1; i <= 90; i++) options.push( { label:i.toString(), value:i } );
			

			layerBar = new ButtonBar("Calque", options,6);
			clipBar = new ButtonBar("Clip", options, 6);
			effectBar = new ButtonBar("ID Effet", options, 6);
			effectParamBar = new ButtonBar("ID Param", options,6);
			
			layerBar.buttonWidth = 20;
			layerBar.height = 20;
			clipBar.buttonWidth = 20;
			clipBar.height = 20;
			clipBar.width = layerBar.width;

			gui.addChild(layerBar);
			gui.addChild(clipBar);

            //inverseParam.gui.x = 50;
            //inverseParam.gui.y = 50;
            gui.addChild(inverseParam.gui);

            minValParam.gui.y = 25;
            minValParam.guiWidth = 200;
            gui.addChild(minValParam.gui);
            maxValParam.gui.y = 45;
            maxValParam.guiWidth = 200;
            gui.addChild(maxValParam.gui);

			
			layerBar.x = -5;
			layerBar.y = 70;
			clipBar.x = -5;
			clipBar.y = 100;

			layerBar.addEventListener(UIEvent.SELECTION_CHANGED, layerChanged);
			clipBar.addEventListener(UIEvent.SELECTION_CHANGED, clipChanged);

		}
		
		private function layerChanged(e:UIEvent):void 
		{
			layer.value = layerBar.value;
		}
		
		private function clipChanged(e:UIEvent):void 
		{
			clip.value = clipBar.value;
		}
		
		override public function loadXML(data:XML):void
		{
			super.loadXML(data);
			layerBar.selectedIndex = layer.value -1;
			clipBar.selectedIndex = clip.value -1;
		}

		override public function clean():void 
		{
			super.clean();
			layerBar.removeEventListener(UIEvent.SELECTION_CHANGED, layerChanged);
			clipBar.removeEventListener(UIEvent.SELECTION_CHANGED, clipChanged);

			layerBar.clean();
			clipBar.clean();
			effectBar.clean();
			effectParamBar.clean();
			
		}

		override public function setValue(value:Number):void
		{
			var baseAddress:String;
            baseAddress = "/layer" + layer.value + "/clip" + clip.value;
			baseAddress += "/video/effect1/param2/values";
			address = baseAddress;
			
			args = [];
            var normalizedValue:Number = minValParam.value + (maxValParam.value - minValParam.value)*value;
            if (inverseParam.value) normalizedValue = maxValParam.value + (minValParam.value - maxValParam.value)*value;
            trace(value, minValParam.value, maxValParam.value, normalizedValue);

            args.push( { label:"f", value:normalizedValue} );

            super.setValue(value);


        }
	}

}