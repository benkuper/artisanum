package modules.output.resolume.simplecommands 
{
	import flash.text.TextField;
	import modules.events.ParameterEvent;
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.SendOSCCommand;
	import modules.output.resolume.ResolumeSimpleOuputModule;
	import modules.parameters.ParameterBool;
	import modules.parameters.ParameterInt;
	import ui.components.ButtonBar;
	import ui.components.UIEvent;
	import ui.fonts.Fonts;
	
	/**
	 * ...
	 * @author ...
	 */
	public class ColumnGroupCommand extends SendOSCCommand
	{
		
		private var columnStart:ParameterInt;
		private var columnEnd:ParameterInt;
		
		private var random:ParameterBool;
		private var loop:ParameterBool;
		
		
		private var columnStartBar:ButtonBar;
		private var columnEndBar:ButtonBar;
		
		private var _currentColumnIndex:int;
		
		private var feedbackTF:TextField;
		
		public function ColumnGroupCommand(label:String,guiMode:String) 
		{
			
			super(label, guiMode);
			
			columnStart = addParameter(new ParameterInt("premierClip","Premier clip", 1, 1, 90),"Numéro du premier clip du groupe") as ParameterInt;
			columnEnd = addParameter(new ParameterInt("dernierClip","Dernier clip", 1, 1, 90), "Numéro du dernier clip du groupe") as ParameterInt;
			
			random = addParameter(new ParameterBool("aleatoire","Aleatoire", false),"",true) as ParameterBool;
			random.addEventListener(ParameterEvent.VALUE_CHANGE, randomChange);
			
			loop = addParameter(new ParameterBool("boucle", "Boucle", false), "", true) as ParameterBool;
			
			args = new Array();
			
			
			
			
			var i:int;
			var cOptions:Array = new Array();
			for (i = 1; i <= 90; i++) cOptions.push( { label:i.toString(), value:i } );
			
			columnStartBar = new ButtonBar("Premiere Clip", cOptions,6);
			columnStartBar.selectedIndex = 0;
			
			columnEndBar = new ButtonBar("Dernier Clip", cOptions,6);
			columnEndBar.selectedIndex = 0;
			
			columnStartBar.buttonWidth = 20;
			columnStartBar.height = 20;
			
			columnEndBar.buttonWidth = 20;
			columnEndBar.height = 20;
			
			gui.addChild(columnStartBar);
			gui.addChild(columnEndBar);
			
			columnStartBar.x = 10;
			columnStartBar.y = 10;
			columnEndBar.x = 10;
			columnEndBar.y = 40;
			
			random.gui.x = 10;
			random.gui.y = columnEndBar.y + 30;
			
			loop.gui.x = random.gui.x + random.gui.width +5;
			loop.gui.y = random.gui.y;
			
			_currentColumnIndex = 1; //default value
			
			
			feedbackTF = Fonts.createTF("Courant",Fonts.normalTF);
			gui.addChild(feedbackTF);
			feedbackTF.x = 10;
			feedbackTF.y = random.gui.y + random.gui.height + 5;
			updateFeedbackTF();
			
			columnStartBar.addEventListener(UIEvent.SELECTION_CHANGED, columnStartChanged);
			columnEndBar.addEventListener(UIEvent.SELECTION_CHANGED, columnEndChanged);
		}
		
		//temp
		override public function loadXML(data:XML):void
		{
			super.loadXML(data);
			columnStartBar.selectedIndex = columnStart.value -1;
			columnEndBar.selectedIndex = columnEnd.value -1;
		}
		
		private function columnStartChanged(e:UIEvent):void 
		{
			columnStart.value = columnStartBar.value;
			currentColumnIndex = columnStart.value;
		}
		
		private function columnEndChanged(e:UIEvent):void 
		{
			columnEnd.value = columnEndBar.value;
		}
		
		private function randomChange(e:ParameterEvent):void 
		{
			loop.enabled = !random.value;
		}
		
		//util
		private function updateFeedbackTF():void 
		{
			if(feedbackTF.text != null) feedbackTF.text = "Prochaine colonne : "+_currentColumnIndex;
		}
		
		
		//overrides 
		
		override protected function updateGUI():void
		{
			super.updateGUI();
			if (guiMode != CommandGUIModes.OUTPUT_MODULE) triggerGUI.x = 250;
		}
		
		
		override public function trigger():void
		{
			if (random.value) 
			{
				var rIndex:int = currentColumnIndex;
				while (rIndex == currentColumnIndex) rIndex = int(Math.random() * (columnEnd.value-columnStart.value) + columnStart.value +.5);
				currentColumnIndex = rIndex;
				
			}
			
			address = "/track" + currentColumnIndex +  "/connect";
			args = [{label:"i",value:1}];
			
			super.trigger();
			
			var reverseColumn:Boolean = columnEnd.value < columnStart.value;
		
			var isLast:Boolean = (!reverseColumn)?(currentColumnIndex >= columnEnd.value):(currentColumnIndex <= columnEnd.value);
			
			if(isLast && loop.value)
			{
				currentColumnIndex = reverseColumn?(columnStart.value+1):(columnStart.value-1);
			}
			
			if (!reverseColumn) currentColumnIndex = Math.min(Math.max(currentColumnIndex + 1, columnStart.value), columnEnd.value);
			else currentColumnIndex = Math.min(Math.max(currentColumnIndex - 1, columnEnd.value), columnStart.value);			
		}
		
		
		public function get currentColumnIndex():int 
		{
			return _currentColumnIndex;
		}
		
		public function set currentColumnIndex(value:int):void 
		{
			_currentColumnIndex = value;
			updateFeedbackTF();
		}
		
		
	}

}