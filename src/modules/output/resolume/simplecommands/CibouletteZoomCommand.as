package modules.output.resolume.simplecommands 
{
	import com.greensock.TweenLite;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterFloat;
	import modules.parameters.ParameterInt;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class CibouletteZoomCommand extends SendOSCCommand 
	{
		private var layer:ParameterInt;
		private var clip:ParameterInt;
		private var intensity:ParameterFloat;
		private var duration:ParameterFloat;
		
		public var zoomValue:Number;
		
		
		public function CibouletteZoomCommand(label:String, guiMode:String) 
		{
			super(label, guiMode);
			
			layer = addParameter(new ParameterInt("calque","Calque", 1,1, 8),"Calque contenant le clip",true) as ParameterInt;
			clip = addParameter(new ParameterInt("clip","Clip", 1, 1, 90),"Numéro du clip cible",true) as ParameterInt;
			intensity = addParameter(new ParameterFloat("intensity","intensité", 1, 0, 1),"Intensité du zoom",true) as ParameterFloat;
			duration = addParameter(new ParameterFloat("duration","Vitesse max", 1, 0, 5),"Durée de l'effet",true) as ParameterFloat;
			layer.gui.y = 25;
			clip.gui.y = 45;
			intensity.gui.y = 65;
			duration.gui.y = 95;
		}
		
		override public function trigger():void
		{
			resetZoom();
			TweenLite.killTweensOf(this);
			TweenLite.to(this, duration.value, { zoomValue:0.1 * (1 + intensity.value), onUpdate:updateZoom, onComplete:resetZoom } );
		}	
		
		private function resetZoom():void
		{
			zoomValue = 0.1;
			updateZoom();
		}	
		
		private function updateZoom():void
		{
			address = "/layer" + layer.value + "/clip" + clip.value+"/video/scale/values";
			args = [ { label:"f", value:zoomValue } ];
			
			sendOSC();
		}
	}

}