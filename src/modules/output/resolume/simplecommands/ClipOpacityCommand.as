package modules.output.resolume.simplecommands 
{
	import modules.events.ParameterEvent;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterInt;
	
	/**
	 * ...
	 * @author ...
	 */
	public class ClipOpacityCommand extends SendOSCCommand 
	{
		
		private var layer:ParameterInt;
		private var clip:ParameterInt;
		
		private  var currentIndex:int;
		
		public function ClipOpacityCommand(label:String, guiMode:String) 
		{
			super(label, guiMode);
			layer = addParameter(new ParameterInt("Layer", 1, 1, 8)) as ParameterInt;
			clip = addParameter(new ParameterInt("Clip", 1, 1, 8)) as ParameterInt;
			
			gui.addChild(layer.gui);
			gui.addChild(clip.gui);
			
			currentIndex = clip.value;
			
			updateGUI();
		}
		
		override protected function updateGUI():void
		{
			super.updateGUI();
			
			
			layer.gui.y = 50;
			clip.gui.y = 80;
		}
		
		
		override public function trigger():void
		{
			sendOSC();
		}
		
		override protected function sendOSC():void
		{
			if (currentIndex < clip.value) currentIndex = clip.value;
			
			address = "/layer" + layer.value + "/clip" + currentIndex + "/video/opacity/values";
			args = new Array( { label:"f", value:mappingValue } );
			super.sendOSC();
		}
			
		
	}

}