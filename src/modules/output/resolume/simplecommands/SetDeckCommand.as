package modules.output.resolume.simplecommands 
{
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterInt;
	import ui.components.ButtonBar;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author ...
	 */
	public class SetDeckCommand extends SendOSCCommand
	{
		
		private var deck:ParameterInt;
		
		private var deckBar:ButtonBar;
		
		public function SetDeckCommand(label:String,guiMode:String) 
		{
			super(label, guiMode);
			
			deck = addParameter(new ParameterInt("deck","Deck", 1,1, 90),"Deck à sélectionner") as ParameterInt;
			
			
			var options:Array = new Array();
			for (var i:int = 1; i <= 90; i++) options.push( { label:i.toString(), value:i } );
			
			deckBar = new ButtonBar("Deck", options,6);
			deckBar.selectedIndex = 0;
			
			deckBar.buttonWidth = 20;
			deckBar.height = 20;
			
			gui.addChild(deckBar);
			
			deckBar.x = 10;
			
			deckBar.addEventListener(UIEvent.SELECTION_CHANGED, deckChanged);
			
			args = new Array();
			
		}
		
		//temp
		override public function loadXML(data:XML):void
		{
			super.loadXML(data);
			deckBar.selectedIndex = deck.value -1 ;
		}
		
		private function deckChanged(e:UIEvent):void 
		{
			deck.value = deckBar.value;
		}
		
		
		override protected function updateGUI():void
		{
			super.updateGUI();
			//if (guiMode != CommandGUIModes.OUTPUT_MODULE) triggerGUI.x = 250;
		}
		
		
		override public function trigger():void
		{
			
			address = "/composition/deck" + deck.value + "/select";
			args = [{label:"i",value:1}];
			
			super.trigger();
		}
	}

}