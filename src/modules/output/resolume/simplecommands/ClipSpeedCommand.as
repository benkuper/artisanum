package modules.output.resolume.simplecommands 
{
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterFloat;
	import modules.parameters.ParameterInt;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ClipSpeedCommand extends SendOSCCommand 
	{
		private var layer:ParameterInt;
		private var clip:ParameterInt;
		private var maxSpeed:ParameterFloat;
		
		public function ClipSpeedCommand(label:String, guiMode:String) 
		{
			super(label, guiMode);
			
			layer = addParameter(new ParameterInt("calque","Calque", 1,1, 99),"Calque contenant le clip",true) as ParameterInt;
			clip = addParameter(new ParameterInt("clip","Clip", 1, 1, 99),"Numéro du clip cible",true) as ParameterInt;
			maxSpeed = addParameter(new ParameterFloat("vitesseMax","Vitesse max", 1, 0, 1),"Vitesse Max",true) as ParameterFloat;
			layer.gui.y = 25;
			clip.gui.y = 45;
			maxSpeed.gui.y = 65;
			
			
		}
		
		override public function setValue(value:Number):void
		{
			if (value == .5) return;
			
			var direction:int = value < .5?0:1;
			var abs:Number = Math.abs(value - .5)*2;
			var speed:Number = 0;
			

						
			address = "/layer" + layer.value + "/clip" + clip.value+"/video/position/direction";
			args = [{ label:"i", value:direction }];
			super.setValue(value);
			
			address = "/layer" + layer.value + "/clip" + clip.value+"/video/position/speed";
			if (abs < 0.5) speed = abs / 5;
			else speed = abs*1.8 - .8;
			args = [{ label:"f", value:speed }]; // for video maxValue is 10
			super.setValue(value);
			
			//need audio if video has audio
			address = "/layer" + layer.value + "/clip" + clip.value+"/audio/position/direction"; 
			args = [{ label:"i", value:direction }];
			super.setValue(value);
			
			address = "/layer" + layer.value + "/clip" + clip.value+"/audio/pitch/values"; 
			args = [{ label:"f", value:abs }]; // for audio maxValue is 2
			super.setValue(value);
			
			trace(direction, value, speed);
		}		
	}

}