package modules.output.resolume.simplecommands 
{
	import modules.events.ParameterEvent;
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterEnum;
	import modules.parameters.ParameterFloat;
	import modules.parameters.ParameterInt;
	import ui.components.ButtonBar;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author ...
	 */
	public class MultiTransformCommand extends SendOSCCommand
	{
		
		private var target:ParameterEnum;
		private var layerStart:ParameterInt;
		private var layerEnd:ParameterInt;
		private var clipStart:ParameterInt;
		private var clipEnd:ParameterInt;
		
		private var valueParam:ParameterFloat;
		
		private var param:ParameterEnum;
		
		private var layerStarBar:ButtonBar;
		private var layerEndBar:ButtonBar;
		private var clipStartBar:ButtonBar;
		private var clipEndBar:ButtonBar;
		
		public function MultiTransformCommand(label:String,guiMode:String) 
		{
			super(label, guiMode);
			
			layerStart = addParameter(new ParameterInt("layerStart","Premier Calque", 1,1, 90),"Calque contenant le clip",false) as ParameterInt;
			layerEnd = addParameter(new ParameterInt("layerEnd","Dernier Calque", 1,1, 90),"Calque contenant le clip",false) as ParameterInt;
			clipStart = addParameter(new ParameterInt("clipStart","Premiere Colonne", 1, 1, 90),"Numéro du clip cible",false) as ParameterInt;
			clipEnd = addParameter(new ParameterInt("clipEnd","Derniere Colonne", 1, 1, 90),"Numéro du clip cible",false) as ParameterInt;
			
			valueParam = addParameter(new ParameterFloat("value", "valeur", 1, 0, 1)) as ParameterFloat;
			
			param = addParameter(ParameterEnum.create("parametre","Paramètre",
									{id:"opacity", label:"Opacité", value:{name:"opacity",hasRange:false}},
									{id:"positionx", label:"Position X", value:{ name:"positionx", hasRange:true, min:0, max:640, realMin:-16384,realMax:16384 }},
									{id:"positiony", label:"Position Y",value: {name:"positiony",hasRange:true,min:0,max:480, realMin:-16384,realMax:16384 }},
									{id:"width", label:"Largeur",value: {name:"width",hasRange:true,min:0,max:640, realMin:0,realMax:16384 }},
									{id:"height", label:"Hauteur", value:{name:"height",hasRange:true,min:0,max:480, realMin:0,realMax:16384 }},
									{id:"rotatex", label:"Rotation X", value:{name:"rotatex",hasRange:false,min:-180,max:180}},
									{id:"rotatey", label:"Rotation Y",value:{name:"rotatey",hasRange:false,min:-180,max:180}},
									{id:"rotatez", label:"Rotation Z",value:{name:"rotatez",hasRange:false,min:0,max:20}},
									{id:"position", label:"Position (Temps)",value: { name:"position", hasRange:false, min:0, max:1 } }
									)
									,"Paramètre à régler", true) as ParameterEnum;
									
									
			param.selectedIndex = 0;
									
			target = addParameter(ParameterEnum.create("cible", "Cible", 
									{id:"clip", label:"Clip", value:"clip"}, 
									{id:"layer", label:"Calque", value:"layer"}, 
									{id:"composition", label:"Composition",value:"composition" } 
									)
									,"Type de cible",false) as ParameterEnum;
			target.selectedIndex = 0;
			target.addEventListener(ParameterEvent.VALUE_CHANGE, targetValueChange);
			
			target.guiHeight = 20;
							
			//target.gui.x = 10;
			target.guiWidth = 160;
			//layer.gui.x = 10;
			layer.gui.y = 30;
			layer.guiWidth = 160;
			clip.gui.y = 50;
			clip.guiWidth = 160;
			param.gui.y = 90;
			param.guiWidth = 160;
			
			triggerGUI.visible = false;
			
			args = new Array();
			
			
			var options:Array = new Array();
			for (var i:int = 1; i <= 90; i++) options.push( { label:i.toString(), value:i } );
			

			layerStartBar = new ButtonBar("Premier calque", options,6);
			layerEndBar = new ButtonBar("Dernier calque", options,6);
			
			var cOptions:Array = new Array();
			for (i = 1; i <= 90; i++) cOptions.push( { label:i.toString(), value:i } );
			
			clipStartBar = new ButtonBar("Premiere colonne", cOptions,6);
			clipEndBar = new ButtonBar("Derniere Colonne", cOptions,6);
			
			layerStartBar.buttonWidth = 20;
			layerStartBar.height = 20;
			layerEndBar.buttonWidth = 20;
			layerEndBar.height = 20;
			
			clipStartBar.buttonWidth = 20;
			clipStartBar.height = 20;
			clipStartBar.width = layerBar.width;
			clipEndBar.buttonWidth = 20;
			clipEndBar.height = 20;
			clipEndBar.width = layerBar.width;
			
			gui.addChild(layerStartBar);
			gui.addChild(layerEndBar);
			gui.addChild(clipStartBar);
			gui.addChild(clipEndBar);
			
			gui.addChild(target.gui);
			
			if (guiMode == CommandGUIModes.EDITOR_TRIGGER)
			{
				valueParam.gui.x = 180;
				valueParam.gui.y = 50;
				valueParam.guiWidth = 200;
				
				gui.addChild(valueParam.gui);
			}

			layerStartBar.x = -5;
			layerStartBar.y = 30;
			layerEndBar.x = -5;
			layerEndBar.y = 60;
			clipStartBar.x = -5;
			clipStartBar.y = 90;
			clipEndBar.x = -5;
			clipEndBar.y = 120;
			
			
			layerBar.addEventListener(UIEvent.SELECTION_CHANGED, layerChanged);
			clipBar.addEventListener(UIEvent.SELECTION_CHANGED, clipChanged);
			
		}
		
		private function layerChanged(e:UIEvent):void 
		{
			layer.value = layerBar.value;
		}
		
		private function clipChanged(e:UIEvent):void 
		{
			clip.value = clipBar.value;
		}
		
		override public function loadXML(data:XML):void
		{
			super.loadXML(data);
			layerBar.selectedIndex = layer.value -1;
			clipBar.selectedIndex = clip.value -1;
		}
		
		
		private function targetValueChange(e:ParameterEvent):void 
		{
			switch(target.selectedItem.value)
			{
				case "clip":
					clipStartBar.visible = true;
					layerStartBar.visible = true;
					clipEndBar.visible = true;
					layerEndBar.visible = true;
					
					break;
					
				case "layer":
					clipStartBar.visible = false;
					clipEndBar.visible = false;
					layerStartBar.visible = true;
					layerEndBar.visible = true;
					break;
					
				case "composition":
					clipStartBar.visible = false;
					layerStartBar.visible = false;
					clipEndBar.visible = false;
					layerEndBar.visible = false;
					
					break;
			}
		}
		
		override public function trigger():void
		{
			prepareMessage();
			super.trigger();
		}
		
		override public function setValue(value:Number):void
		{
			valueParam.value = value;
			super.setValue(value);
		}
		
		private function prepareMessage():void
		{
			var baseAddress:String;
			switch(target.selectedItem.value)
			{
				case "clip":
					baseAddress = "/layer" + layer.value + "/clip" + clip.value;
					break;
					
				case "layer":
					baseAddress = "/layer" + layer.value;
					break;
					
				case "composition":
					baseAddress = "/composition";
					break;
			}
			var pv:Object = param.selectedItem.value;
			baseAddress += (pv.isAudio?"/audio/":"/video/") + pv.name + "/values";
			address = baseAddress;
			
			args = [];
			if (pv.hasRange)
			{
				var relMinValue:Number = (pv.min - pv.realMin) / (pv.realMax - pv.realMin);
				//args.push( { label:"f", value:0/param.selectedItem.value.range } );
				//args.push( { label:"f", value:1/param.selectedItem.value.range } );
				args.push( { label:"f", value:relMinValue });// param.selectedItem.value.min } );
				
			}
			
			args.push( { label:"f", value:valueParam.value  } );
			
			if (pv.hasRange)
			{
				var relMaxValue:Number = (pv.max - pv.realMin) / (pv.realMax - pv.realMin);
				args.push( { label:"f", value:relMaxValue });// param.selectedItem.value.max } );
			}
		}
	}

}