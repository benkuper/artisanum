package modules.output.resolume.simplecommands 
{
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterInt;
	import ui.components.ButtonBar;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author ...
	 */
	public class LaunchColumnCommand extends SendOSCCommand
	{
		
		private var column:ParameterInt;
		
		private var columnBar:ButtonBar;
		
		public function LaunchColumnCommand(label:String,guiMode:String) 
		{
			super(label, guiMode);
			
			column = addParameter(new ParameterInt("colonne","Colonne", 1,1, 90),"Colonne à lancer") as ParameterInt;
			
			//layer.gui.x = 10;
			//layer.guiWidth = 160;
			//clip.gui.x = 10;
			//clip.gui.y = 25;
			//clip.guiWidth = 160;
			
			
			var options:Array = new Array();
			for (var i:int = 1; i <= 90; i++) options.push( { label:i.toString(), value:i } );
			
			columnBar = new ButtonBar("Colonne", options,6);
			columnBar.selectedIndex = 0;
			
			columnBar.buttonWidth = 20;
			columnBar.height = 20;
			
			gui.addChild(columnBar);
			columnBar.x = 10;
			
			
			columnBar.addEventListener(UIEvent.SELECTION_CHANGED, columnChanged);
			
			args = new Array();
			
		}
		
		//temp
		override public function loadXML(data:XML):void
		{
			super.loadXML(data);
			columnBar.selectedIndex = column.value -1 ;
		}
		
		private function columnChanged(e:UIEvent):void 
		{
			column.value = columnBar.value;
		}
		
		
		
		override protected function updateGUI():void
		{
			super.updateGUI();
			//if (guiMode != CommandGUIModes.OUTPUT_MODULE) triggerGUI.x = 250;
		}
		
		
		override public function trigger():void
		{
			
			address = "/track" + column.value + "/connect";
			args = [{label:"i",value:1}];
			
			super.trigger();
		}
	}

}