package modules.output.resolume.simplecommands 
{
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterFloat;
	import modules.parameters.ParameterInt;
	import ui.Toaster;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ResetCommand extends SendOSCCommand 
	{
		private var nbLayers:ParameterInt;
		private var nbClips:ParameterInt;
		
		public function ResetCommand(label:String, guiMode:String) 
		{
			super(label, guiMode);
			
			nbLayers = addParameter(new ParameterInt("nbLayers", "nombre total de calques", 15, 1, 50), "", true) as ParameterInt;
			nbClips = addParameter(new ParameterInt("nbClips", "nombre total de clips", 50, 1, 99), "", true) as ParameterInt;
			nbLayers.gui.y = 25;
			nbClips.gui.y = 45;
			nbLayers.guiWidth = 200;
			nbClips.guiWidth = 200;
		}
		
		override public function trigger():void
		{
			//Toaster.warning("Réinitialisation en cours, veuillez patienter...");
			
			for (var i:int = 1; i < nbLayers.value; i++) 
			{
				for (var j:int = 1; j < nbClips.value; j++) 
				{
					// reset opacity
					address = "/layer" + i + "/clip" + j + "/video/opacity/values";
					args = [ { label:"f", value:1 } ];
					super.trigger();
					
					// reset rotation
					address = "/layer" + i + "/clip" + j+"/video/rotatez/values";
					args = [{ label:"f", value:.5 }];
					super.trigger();
					
					// reset scale
					address = "/layer" + i + "/clip" + j+"/video/scale/values";
					args = [{ label:"f", value:.1 }];
					super.trigger();
					
					// reset speed (no audio)
					address = "/layer" + i + "/clip" + j+"/video/position/speed";
					args = [ { label:"f", value:.1 } ];
					super.trigger();
		
					// reset speed (with audio)
					address = "/layer" + i + "/clip" + j+"/audio/pitch/values"; 
					args = [{ label:"f", value:.475 }];
					super.trigger();
					
					// reset direction
					address = "/layer" + i + "/clip" + j+"/video/position/direction";
					args = [ { label:"i", value:1 } ];
					super.trigger();
					
					address = "/layer" + i + "/clip" + j+"/video/position/playmodeaway";
					args = [ { label:"i", value:0 } ];
					super.trigger();

                    address = "/layer" + i + "/clip" + j+"/audio/tempo/timelinemode";
                    args = [ { label:"i", value:0 } ];
                    super.trigger();

                    address = "/layer" + i + "/clip" + j+"/video/tempo/timelinemode";
                    args = [ { label:"i", value:0 } ];
                    super.trigger();
					
				}	
			}
			//Toaster.success("Réinitialisation terminée !");
		}
	}

}