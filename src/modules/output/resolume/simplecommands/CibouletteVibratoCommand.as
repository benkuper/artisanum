package modules.output.resolume.simplecommands 
{
	import flash.utils.getTimer;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterFloat;
	import modules.parameters.ParameterInt;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class CibouletteVibratoCommand extends SendOSCCommand 
	{
		private var layer:ParameterInt;
		private var clip:ParameterInt;
		private var threshold:ParameterFloat;
		private var intensity:ParameterFloat;
		private var frequency:ParameterFloat;
		
		public function CibouletteVibratoCommand(label:String, guiMode:String) 
		{
			super(label, guiMode);
			
			layer = addParameter(new ParameterInt("calque","Calque", 1,1, 8),"Calque contenant le clip",true) as ParameterInt;
			clip = addParameter(new ParameterInt("clip","Clip", 1, 1, 90),"Numéro du clip cible",true) as ParameterInt;
			threshold = addParameter(new ParameterFloat("threshold","Seuil", .1, 0, 1),"Seuil d'activation", true) as ParameterFloat;
			intensity = addParameter(new ParameterFloat("intensity","Intensité", 1, 0, 10),"Intensité de l'effet",true) as ParameterFloat;
			frequency = addParameter(new ParameterFloat("frequency","Vitesse", 1, 0, 2),"Rapidité de vibration",true) as ParameterFloat;
			
			layer.gui.y = 25;
			clip.gui.y = 45;
			threshold.gui.y = 65;
			intensity.gui.y = 85;
			frequency.gui.y = 105;
		}
		
		override public function setValue(value:Number):void
		{
			var rotate:Number = 0.5;

            if (value > 0) value = value + threshold.value;

			var vibration:Number = Math.sin(frequency.value * getTimer() / 35);
			rotate = .5 + value * intensity.value * vibration / 25;

			
			address = "/layer" + layer.value + "/clip" + clip.value+"/video/rotatez/values";
			args = [{ label:"f", value:rotate}];

			super.setValue(value);
		}		
		
	}

}