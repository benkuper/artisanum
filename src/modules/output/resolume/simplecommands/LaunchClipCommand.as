package modules.output.resolume.simplecommands 
{
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.SendOSCCommand;
	import modules.output.resolume.ResolumeSimpleOuputModule;
	import modules.parameters.ParameterBool;
	import modules.parameters.ParameterInt;
	import ui.components.ButtonBar;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author ...
	 */
	public class LaunchClipCommand extends SendOSCCommand
	{
		
		private var layer:ParameterInt;
		private var clip:ParameterInt;
		private var solo:ParameterBool;
		
		private var layerBar:ButtonBar;
		private var clipBar:ButtonBar;
		
		
		public function LaunchClipCommand(label:String,guiMode:String) 
		{
			super(label, guiMode);
			
			layer = addParameter(new ParameterInt("calque","Calque", 1,1, 90),"Calque contenant le clip") as ParameterInt;
			clip = addParameter(new ParameterInt("clip","Clip", 1, 1, 90),"Numéro du clip cible") as ParameterInt;
			
			solo = addParameter(new ParameterBool("solo", "Solo", false), "Enlève tous les clips pour ne jouer que celui-ci",true) as ParameterBool;
			
			//layer.gui.x = 10;
			//layer.guiWidth = 160;
			//clip.gui.x = 10;
			//clip.gui.y = 25;
			//clip.guiWidth = 160;
			
			
			var options:Array = new Array();
			for (var i:int = 1; i <= 90; i++) options.push( { label:i.toString(), value:i } );
			
			var cOptions:Array = new Array();
			for (i = 1; i <= 90; i++) cOptions.push( { label:i.toString(), value:i } );
			
			layerBar = new ButtonBar("Calque", options,6);
			layerBar.selectedIndex = 0;
			clipBar = new ButtonBar("Clip", cOptions, 6);
			clipBar.selectedIndex = 0;
			
			layerBar.buttonWidth = 20;
			layerBar.height = 20;
			clipBar.buttonWidth = 20;
			clipBar.height = 20;
			clipBar.width = layerBar.width;
			
			gui.addChild(layerBar);
			gui.addChild(clipBar);
			
			
			layerBar.x = 10;
			clipBar.x = 10;
			clipBar.y = 30;
			solo.gui.x =  10;
			solo.gui.y = clipBar.y + clipBar.height + 5;
			
			layerBar.addEventListener(UIEvent.SELECTION_CHANGED, layerChanged);
			clipBar.addEventListener(UIEvent.SELECTION_CHANGED, clipChanged);
			
			args = new Array();
			
		}
		
		//temp
		override public function loadXML(data:XML):void
		{
			super.loadXML(data);
			layerBar.selectedIndex = layer.value -1 ;
			clipBar.selectedIndex = clip.value -1;
		}
		
		private function layerChanged(e:UIEvent):void 
		{
			layer.value = layerBar.value;
		}
		
		private function clipChanged(e:UIEvent):void 
		{
			clip.value = clipBar.value;
		}
		
		
		override protected function updateGUI():void
		{
			super.updateGUI();
			//if (guiMode != CommandGUIModes.OUTPUT_MODULE) triggerGUI.x = 250;
		}
		
		
		override public function trigger():void
		{
			
			address = "/layer" + layer.value + "/clip" + clip.value + "/connect";
			args = [{label:"i",value:1}];
			
			super.trigger();
			
			if (solo.value)
			{
				
				var exception:int = (module as ResolumeSimpleOuputModule).permanentLayerParam.value;
				for (var i:int = 0; i < 11; i++)
				{
					if (i != exception && i != layer.value)
					{
						address = "/layer"+i+"/clear";
						args = [{label:"i",value:1}];
						sendOSC();
					}
					
				}
			}
			
		}
	}

}