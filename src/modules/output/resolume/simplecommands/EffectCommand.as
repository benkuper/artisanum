package modules.output.resolume.simplecommands 
{
	import modules.events.ParameterEvent;
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterEnum;
	import modules.parameters.ParameterFloat;
	import modules.parameters.ParameterInt;
	import ui.components.ButtonBar;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author ...
	 */
	public class EffectCommand extends SendOSCCommand
	{
		
		private var target:ParameterEnum;
		private var layer:ParameterInt;
		private var clip:ParameterInt;
		private var effectID:ParameterInt;
		private var effectParamID:ParameterInt;
		
		private var valueParam:ParameterFloat;
		private var param:ParameterEnum;
		
		private var layerBar:ButtonBar;
		private var clipBar:ButtonBar;
		
		private var effectBar:ButtonBar;
		private var effectParamBar:ButtonBar;
		
		public function EffectCommand(label:String,guiMode:String) 
		{
			super(label, guiMode);
			
			layer = addParameter(new ParameterInt("calque","Calque", 1,1, 90),"Calque contenant le clip",false) as ParameterInt;
			clip = addParameter(new ParameterInt("clip","Clip", 1, 1, 90),"Numéro du clip cible",false) as ParameterInt;
			effectID = addParameter(new ParameterInt("effectID", "Effect ID", 1, 1, 90), "ID de l'effet", false) as ParameterInt;
			effectParamID = addParameter(new ParameterInt("parameterID", "Parameter ID", 1, 1, 90), "ID du paramètre", false) as ParameterInt;
			
			valueParam = addParameter(new ParameterFloat("value", "valeur", 1, 0, 1)) as ParameterFloat;

			
			param = addParameter(ParameterEnum.create("parametre","Paramètre",
									{id:"bypass", label:"Bypass", value:{name:"bypassed",hasRange:false}},
									{id:"opacity", label:"Opacity", value:{ name:"opacity", hasRange:false}},
									{id:"parameter", label:"Parameter #", value: { name:"parameter", hasRange:false }} 
									)
									,"Paramètre à régler", true) as ParameterEnum;
									
									
			param.selectedIndex = 0;
			
			param.addEventListener(ParameterEvent.VALUE_CHANGE, targetParamChange);
			
			target = addParameter(ParameterEnum.create("cible", "Cible", 
									{id:"clip", label:"Clip", value:"clip"}, 
									{id:"layer", label:"Calque", value:"layer"}, 
									{id:"composition", label:"Composition",value:"composition" } 
									)
									,"Type de cible",false) as ParameterEnum;
			target.selectedIndex = 0;
			target.addEventListener(ParameterEvent.VALUE_CHANGE, targetValueChange);
			
			target.guiHeight = 20;
			
			param.gui.y = 0;
			param.guiWidth = 160;
			
			target.guiWidth = 160;
			target.gui.y = 30;
			
			
			args = new Array();
			
			
			var options:Array = new Array();
			for (var i:int = 1; i <= 90; i++) options.push( { label:i.toString(), value:i } );
			

			layerBar = new ButtonBar("Calque", options,6);
			clipBar = new ButtonBar("Clip", options, 6);
			effectBar = new ButtonBar("ID Effet", options, 6);
			effectParamBar = new ButtonBar("ID Param", options,6);
			
			layerBar.buttonWidth = 20;
			layerBar.height = 20;
			clipBar.buttonWidth = 20;
			clipBar.height = 20;
			clipBar.width = layerBar.width;
			
			effectBar.buttonWidth = 20;
			effectBar.height = 20;
			effectBar.width = layerBar.width;
			effectParamBar.buttonWidth = 20;
			effectParamBar.height = 20;
			effectParamBar.width = layerBar.width;
			
			gui.addChild(layerBar);
			gui.addChild(clipBar);
			gui.addChild(effectBar);
			gui.addChild(effectParamBar);
			
			gui.addChild(target.gui);
			
			if (guiMode != CommandGUIModes.EDITOR_MAPPING)
			{
				//valueParam.gui.x = 180;
				valueParam.gui.y = 180;
				valueParam.guiWidth = 200;
				
				gui.addChild(valueParam.gui);
			}
			
			layerBar.x = -5;
			layerBar.y = 60;
			clipBar.x = -5;
			clipBar.y = 90;
			
			effectBar.x = -5;
			effectBar.y = 120;
			effectParamBar.x = -5;
			effectParamBar.y = 150;
			
			effectParamBar.visible = false;
			
			
			layerBar.addEventListener(UIEvent.SELECTION_CHANGED, layerChanged);
			clipBar.addEventListener(UIEvent.SELECTION_CHANGED, clipChanged);
			effectBar.addEventListener(UIEvent.SELECTION_CHANGED, effectChanged);
			effectParamBar.addEventListener(UIEvent.SELECTION_CHANGED, effectParamChanged);
			
			
		}
		
		
		
		private function effectChanged(e:UIEvent):void 
		{
			effectID.value = effectBar.value;
		}
		
		private function effectParamChanged(e:UIEvent):void 
		{
			effectParamID.value = effectParamBar.value;
		}
		
		private function layerChanged(e:UIEvent):void 
		{
			layer.value = layerBar.value;
		}
		
		private function clipChanged(e:UIEvent):void 
		{
			clip.value = clipBar.value;
		}
		
		override public function loadXML(data:XML):void
		{
			super.loadXML(data);
			layerBar.selectedIndex = layer.value -1;
			clipBar.selectedIndex = clip.value -1;
		}
		
		
		private function targetValueChange(e:ParameterEvent):void 
		{
			trace("target value change");
			switch(target.selectedItem.value)
			{
				case "clip":
					clipBar.visible = true;
					layerBar.visible = true;
					break;
					
				case "layer":
					clipBar.visible = false;
					layerBar.visible = true;
					break;
					
				case "composition":
					clipBar.visible = false;
					layerBar.visible = false;
					break;
			}
		}
		
		private function targetParamChange(e:ParameterEvent):void 
		{
			trace("target param changed");
			trace(param.selectedItem.value.name);
			switch(param.selectedItem.value.name)
			{
				case "bypassed":
				case "opacity":
					effectParamBar.visible = false;
					break;
					
				case "parameter":
					effectParamBar.visible = true;
					break;
			}
		}
		
		override public function clean():void 
		{
			super.clean();
			layerBar.removeEventListener(UIEvent.SELECTION_CHANGED, layerChanged);
			clipBar.removeEventListener(UIEvent.SELECTION_CHANGED, clipChanged);
			effectBar.removeEventListener(UIEvent.SELECTION_CHANGED, effectChanged);
			effectParamBar.removeEventListener(UIEvent.SELECTION_CHANGED, effectParamChanged);
			
			param.removeEventListener(ParameterEvent.VALUE_CHANGE, targetParamChange);
			target.removeEventListener(ParameterEvent.VALUE_CHANGE, targetValueChange);
			
			
			layerBar.clean();
			clipBar.clean();
			effectBar.clean();
			effectParamBar.clean();
			
		}
		
		override public function trigger():void
		{
			prepareMessage();
			super.trigger();
		}
		
		override public function setValue(value:Number):void
		{
			valueParam.value = value;
			super.setValue(value);
		}
		
		private function prepareMessage():void
		{
			var baseAddress:String;
			switch(target.selectedItem.value)
			{
				case "clip":
					baseAddress = "/layer" + layer.value + "/clip" + clip.value;
					break;
					
				case "layer":
					baseAddress = "/layer" + layer.value;
					break;
					
				case "composition":
					baseAddress = "/composition";
					break;
			}
			var pv:Object = param.selectedItem.value;
			var pvName:String = pv.name;
			if (pvName == "parameter") pvName = "param" + effectParamID.value;
			baseAddress += (pv.isAudio?"/audio/":"/video/") + "effect"+effectID.value+"/"+pvName ;
			
			if (param.selectedItem.value.name != "bypassed") baseAddress += "/values";
			
			address = baseAddress;
			
			args = [];
			if (pv.hasRange)
			{
				var relMinValue:Number = (pv.min - pv.realMin) / (pv.realMax - pv.realMin);
				//args.push( { label:"f", value:0/param.selectedItem.value.range } );
				//args.push( { label:"f", value:1/param.selectedItem.value.range } );
				args.push( { label:"f", value:relMinValue });// param.selectedItem.value.min } );
				
			}
			
			if (param.selectedItem.value.name == "bypassed")
			{
				args.push( { label:"i", value:int(valueParam.value) } );
			}else
			{
				args.push( { label:"f", value:valueParam.value  } );
			}
			
			if (pv.hasRange)
			{
				var relMaxValue:Number = (pv.max - pv.realMin) / (pv.realMax - pv.realMin);
				args.push( { label:"f", value:relMaxValue });// param.selectedItem.value.max } );
			}
			
			trace("prepareMessage :", address, args[0].value,args[0].label);
		}
	}

}