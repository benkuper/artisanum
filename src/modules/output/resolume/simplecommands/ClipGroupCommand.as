package modules.output.resolume.simplecommands 
{
	import flash.text.TextField;
	import modules.events.ParameterEvent;
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.SendOSCCommand;
	import modules.output.resolume.ResolumeSimpleOuputModule;
	import modules.parameters.ParameterBool;
	import modules.parameters.ParameterInt;
	import ui.components.ButtonBar;
	import ui.components.UIEvent;
	import ui.fonts.Fonts;
	
	/**
	 * ...
	 * @author ...
	 */
	public class ClipGroupCommand extends SendOSCCommand
	{
		
		private var layerStart:ParameterInt;
		private var layerEnd:ParameterInt;
		private var clipStart:ParameterInt;
		private var clipEnd:ParameterInt;
		
		private var random:ParameterBool;
		private var loop:ParameterBool;
		private var solo:ParameterBool;
		
		
		private var layerStartBar:ButtonBar;
		private var layerEndBar:ButtonBar;
		private var clipStartBar:ButtonBar;
		private var clipEndBar:ButtonBar;
		
		private var _currentClipIndex:int;
		private var _currentLayerIndex:int;
		
		private var feedbackTF:TextField;
		
		public function ClipGroupCommand(label:String,guiMode:String) 
		{
			
			super(label, guiMode);
			
			layerStart = addParameter(new ParameterInt("premierCalque","Premier Calque", 1,1, 90),"Premier calque") as ParameterInt;
			layerEnd = addParameter(new ParameterInt("dernierCalque","Dernier Calque", 1,1, 90),"Dernier calque") as ParameterInt;
			clipStart = addParameter(new ParameterInt("premierClip","Premier clip", 1, 1, 90),"Numéro du premier clip du groupe") as ParameterInt;
			clipEnd = addParameter(new ParameterInt("dernierClip","Dernier clip", 1, 1, 90), "Numéro du dernier clip du groupe") as ParameterInt;
			
			random = addParameter(new ParameterBool("aleatoire","Aleatoire", false),"",true) as ParameterBool;
			random.addEventListener(ParameterEvent.VALUE_CHANGE, randomChange);
			
			loop = addParameter(new ParameterBool("boucle", "Boucle", false), "", true) as ParameterBool;
			
			solo = addParameter(new ParameterBool("solo", "Solo", false), "Enlève tous les clips pour ne jouer que celui-ci",true) as ParameterBool;
			
			
			
			args = new Array();
			
			
			
			
			var i:int;
			var options:Array = new Array();
			for (i = 1; i <= 90; i++) options.push( { label:i.toString(), value:i } );
			
			layerStartBar = new ButtonBar("Premier Calque", options,6);
			layerStartBar.selectedIndex = 0;
			
			layerEndBar = new ButtonBar("Dernier Calque", options,6);
			layerEndBar.selectedIndex = 0;
			
			
			var cOptions:Array = new Array();
			for (i = 1; i <= 90; i++) cOptions.push( { label:i.toString(), value:i } );
			
			clipStartBar = new ButtonBar("Premiere Clip", cOptions,6);
			clipStartBar.selectedIndex = 0;
			
			clipEndBar = new ButtonBar("Dernier Clip", cOptions,6);
			clipEndBar.selectedIndex = 0;
			
			layerStartBar.buttonWidth = 20;
			layerStartBar.height = 20;
			
			layerEndBar.buttonWidth = 20;
			layerEndBar.height = 20;
			
			clipStartBar.buttonWidth = 20;
			clipStartBar.height = 20;
			clipStartBar.width = layerStartBar.width;
			
			clipEndBar.buttonWidth = 20;
			clipEndBar.height = 20;
			clipEndBar.width = layerEndBar.width;
			
			gui.addChild(layerStartBar);
			gui.addChild(layerEndBar);
			gui.addChild(clipStartBar);
			gui.addChild(clipEndBar);
			
			layerStartBar.x = 10;
			layerEndBar.x = 10;
			layerEndBar.y = 20;
			clipStartBar.x = 10;
			clipStartBar.y = 50;
			clipEndBar.x = 10;
			clipEndBar.y = 70;
			
			random.gui.x = 10;
			random.gui.y = clipEndBar.y + 30;
			
			loop.gui.x = random.gui.x + random.gui.width +5;
			loop.gui.y = random.gui.y;
			
			solo.gui.x = loop.gui.x + loop.gui.width + 5;
			solo.gui.y = loop.gui.y;
			
			
			_currentClipIndex = 1; //default value
			_currentLayerIndex = 1;
			
			
			feedbackTF = Fonts.createTF("Courant",Fonts.normalTF);
			gui.addChild(feedbackTF);
			feedbackTF.x = 10;
			feedbackTF.y = random.gui.y + random.gui.height + 5;
			updateFeedbackTF();
			
			
			layerStartBar.addEventListener(UIEvent.SELECTION_CHANGED, layerStartChanged);
			layerEndBar.addEventListener(UIEvent.SELECTION_CHANGED, layerEndChanged);
			clipStartBar.addEventListener(UIEvent.SELECTION_CHANGED, clipStartChanged);
			clipEndBar.addEventListener(UIEvent.SELECTION_CHANGED, clipEndChanged);
		}
		
		//temp
		override public function loadXML(data:XML):void
		{
			super.loadXML(data);
			layerStartBar.selectedIndex = layerStart.value -1 ;
			layerEndBar.selectedIndex = layerEnd.value -1 ;
			clipStartBar.selectedIndex = clipStart.value -1;
			clipEndBar.selectedIndex = clipEnd.value -1;
		}
		
		private function layerStartChanged(e:UIEvent):void 
		{
			layerStart.value = layerStartBar.value;
		}
		
		private function layerEndChanged(e:UIEvent):void 
		{
			layerEnd.value = layerEndBar.value;
		}
		
		private function clipStartChanged(e:UIEvent):void 
		{
			clipStart.value = clipStartBar.value;
			currentClipIndex = clipStart.value;
		}
		
		private function clipEndChanged(e:UIEvent):void 
		{
			clipEnd.value = clipEndBar.value;
		}
		
		private function randomChange(e:ParameterEvent):void 
		{
			loop.enabled = !random.value;
		}
		
		//util
		private function updateFeedbackTF():void 
		{
			if(feedbackTF.text != null) feedbackTF.text = "Prochain : Calque " + currentLayerIndex + ", Clip " + currentClipIndex;
		}
		
		
		//overrides 
		
		override protected function updateGUI():void
		{
			super.updateGUI();
			if (guiMode != CommandGUIModes.OUTPUT_MODULE) triggerGUI.x = 250;
		}
		
		
		override public function trigger():void
		{
			if (random.value) 
			{
				var rIndex:int = currentClipIndex;
				while (rIndex == currentClipIndex) rIndex = int(Math.random() * (clipEnd.value-clipStart.value) + clipStart.value +.5);
				currentClipIndex = rIndex;
				
			}
			
			address = "/layer" + currentLayerIndex + "/clip" + currentClipIndex +  "/connect";
			args = [{label:"i",value:1}];
			
			super.trigger();
			
			
			if (solo.value)
			{
				var exception:int = (module as ResolumeSimpleOuputModule).permanentLayerParam.value;
				
				for (var i:int = 0; i < 11; i++)
				{
					if (i != exception && i != currentLayerIndex)
					{
						address = "/layer"+i+"/clear";
						args = [{label:"i",value:1}];
						sendOSC();
					}
					
				}
			}
			
			var reverseLayer:Boolean = layerEnd.value < layerStart.value;
			var reverseClip:Boolean = clipEnd.value < clipStart.value;
			
			if ((!reverseLayer && (currentLayerIndex >= layerEnd.value)) || (reverseLayer && (currentLayerIndex <= layerEnd.value)))
			{
				var isLast:Boolean = (!reverseClip)?(currentClipIndex >= clipEnd.value):(currentClipIndex <= clipEnd.value);
				
				if (!isLast || loop.value) //loop
				{
					currentLayerIndex = reverseLayer?(layerStart.value+1):(layerStart.value-1);
				}
				
				if(isLast && loop.value)
				{
					currentClipIndex = reverseClip?(clipStart.value+1):(clipStart.value-1);
				}
				
				if (!reverseClip) currentClipIndex = Math.min(Math.max(currentClipIndex + 1, clipStart.value), clipEnd.value);
				else currentClipIndex = Math.min(Math.max(currentClipIndex - 1, clipEnd.value), clipStart.value);
			}	
			
			if (!reverseLayer) currentLayerIndex = Math.min(Math.max(currentLayerIndex + 1, layerStart.value), layerEnd.value);
			else currentLayerIndex = Math.min(Math.max(currentLayerIndex - 1, layerEnd.value), layerStart.value);
			
		}
		
		
		public function get currentClipIndex():int 
		{
			return _currentClipIndex;
		}
		
		public function set currentClipIndex(value:int):void 
		{
			_currentClipIndex = value;
			updateFeedbackTF();
		}
		
		
		public function get currentLayerIndex():int 
		{
			return _currentLayerIndex;
		}
		
		public function set currentLayerIndex(value:int):void 
		{
			_currentLayerIndex = value;
			updateFeedbackTF();
		}
	}

}