package modules.output.resolume 
{
	import benkuper.util.osc.OSCMaster;
	import modules.events.ParameterEvent;
	import modules.output.commands.CommandType;
	import modules.output.commands.CommandType;
	import modules.output.IOSCOutputModule;
	import modules.output.OutputModule;
	import modules.output.resolume.simplecommands.*;
	import modules.parameters.Parameter;
	import modules.parameters.ParameterBool;
	import modules.parameters.ParameterInt;
	import modules.parameters.ParameterString;
	import org.tuio.osc.OSCMessage;
	
	/**
	 * ...
	 * @author ...
	 */
	public class ResolumeSimpleOuputModule extends OutputModule implements IOSCOutputModule
	{
		public var ip:String = "192.168.0.132";
		public var cacheIP:String = "192.168.0.132";
		public var port:int = 7000;
		
		protected var localParam:ParameterBool;
		protected var verboseParam:ParameterBool;
		protected var ipParam:ParameterString;
		protected var portParam:ParameterString;
		
		//CIBOULETTE ou à intégrer autrement
		public var permanentLayerParam:ParameterInt;
		
		public function ResolumeSimpleOuputModule() 
		{
			super();
			
			localParam = addParameter(new ParameterBool("isLocal", "local", false)) as ParameterBool;
			verboseParam = addParameter(new ParameterBool("verbose", "verbose", false), false) as ParameterBool;
			ipParam = addParameter(new ParameterString("ip", "IP cible", ip), true, "l'adresse IP de l'ordinateur envoyant le signal OSC") as ParameterString;
			portParam = addParameter(new ParameterString("port", "port", port.toString()), true, "port sur lequel le message OSC est envoyé") as ParameterString;
			
			permanentLayerParam = addParameter(new ParameterInt("permanentLayer", "Calque permanent", 0, 0, 99, ParameterInt.TEXTBOX_GUI)) as ParameterInt;
			
			localParam.addEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			ipParam.addEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			portParam.addEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			
			localParam.value = true;
			
			addCommandTypes
			(
				new CommandType("launchClip","Lancer clip", LaunchClipCommand, CommandType.TRIGGER, { } ),
				new CommandType("launchMultiClip","Lancer plusieurs clips", ClipGroupCommand, CommandType.TRIGGER, { } ),
				new CommandType("launchColonne","Lancer colonne", LaunchColumnCommand, CommandType.TRIGGER, { } ),
				new CommandType("launchColonne","Plusieurs colonnes", ColumnGroupCommand, CommandType.TRIGGER, { } ),
				new CommandType("stopLayer","Arrêter calque", StopLayerCommand, CommandType.TRIGGER, { } ),
				new CommandType("stopCompo","Arrêter compo", StopCompoCommand, CommandType.TRIGGER, { } ),
				new CommandType("reset","Réinitialiser", ResetCommand, CommandType.TRIGGER, { } ),
				new CommandType("deck", "Deck", SetDeckCommand, CommandType.TRIGGER, { } ),
				new CommandType("strobe","Strobe", StrobeEffect, CommandType.TRIGGER, { } ),
				new CommandType("zoom","Zoom", CibouletteZoomCommand, CommandType.TRIGGER, { } ),
				
				new CommandType("paramVideo","Param. vidéo", TransformCommand, CommandType.BOTH, { } ),
                new CommandType("effectVideo","Effet vidéo", EffectCommand, CommandType.BOTH, { } ),
				new CommandType("clipSpeed","Vitesse clip", ClipSpeedCommand, CommandType.MAPPING, { } ),
				new CommandType("layerSpeed","Vitesse calque", LayerSpeedCommand, CommandType.MAPPING, { } ),
				new CommandType("vibrato","Vibrato", CibouletteVibratoCommand, CommandType.MAPPING, { } ),
                new CommandType("vague","Vague", CibouletteWaveCommand, CommandType.MAPPING, { } ),
            new CommandType("manege","Manège", CibouletteSpeedCommand, CommandType.MAPPING, { } )
			);
			
			commandTester.headerHeight = 60;
		}
		
		protected function paramChanged(e:ParameterEvent):void 
		{
			var targetParam:Parameter = e.target as Parameter;
			
			switch (targetParam)
			{
				case ipParam:
				case portParam:
					ip = ipParam.value;
					port = int(portParam.value);
					break;
					
				case localParam:
					if (localParam.value)
					{
						cacheIP = ipParam.value;
						ipParam.value = "127.0.0.1";
					} else
					{
						ipParam.value = cacheIP;
					}
					ipParam.enabled = !localParam.value;
					break;
			}
		}
		
		public function sendMessage(message:OSCMessage):void
		{
			if (!enabled) return;
			OSCMaster.sendTo(message,localParam.value?"127.0.0.1":ip,port, verboseParam.value);
		}
		
		override protected function draw():void
		{
			super.draw();
			
			// parameters
			localParam.gui.y = -10;
			verboseParam.gui.x = moduleWidth - 100;
			verboseParam.gui.y = -10;
			
			ipParam.gui.y = 30;
			ipParam.guiWidth = moduleWidth - 30;
			
			portParam.gui.y = 60;
			portParam.guiWidth = moduleWidth - 30;
			
			permanentLayerParam.gui.y = 90;
			permanentLayerParam.guiWidth = moduleWidth - 30;
			
			parameterContainer.height = parameterContainer.headerHeight + permanentLayerParam.gui.y + permanentLayerParam.guiHeight + 15;
			
		}
		
		//clean
		override public function clean():void
		{
			super.clean();
			localParam.removeEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			ipParam.removeEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			portParam.removeEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
		}
	}
}