package modules.output.openosc 
{
	import modules.output.commands.CommandType;
	import modules.output.openosc.commands.OSCTriggerCommand;
	import modules.output.OutputModule;
	import modules.parameters.ParameterString;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class OpenOSC extends OutputModule 
	{
		public var ip:ParameterString;
		public var port:ParameterString;
		
		public function OpenOSC() 
		{
			super();
			
			addCommandTypes(
				new CommandType("trigger","Trigger", OSCTriggerCommand, CommandType.TRIGGER, {module:this } )
			);
			
			ip = addParameter(new ParameterString("ip","IP", "127.0.0.1") ) as ParameterString;
			port =  addParameter(new ParameterString("port","Port", "8000")) as ParameterString;
			
			//ip.addEventListener(ParameterEvent.VALUE_CHANGE, ipChange);
			//port.addEventListener(ParameterEvent.VALUE_CHANGE, portChange);
			
			commandTester.headerHeight = 60;
			
		}
		
		override protected function draw():void
		{
			
			ip.gui.x = 0;
			ip.gui.y = 0;
			ip.guiWidth = moduleWidth - 30;
			port.gui.x = 0;
			port.gui.y = 30;
			port.guiWidth = moduleWidth - 30;
			
			super.draw();
		}
		
	}

}