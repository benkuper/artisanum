package modules.output.openosc.commands 
{
	import modules.output.commands.SendOSCCommand;
	import modules.output.openosc.OpenOSC;
	import modules.parameters.ParameterString;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class OSCTriggerCommand extends SendOSCCommand 
	{
		private var oscAddress:ParameterString;
		
		public function OSCTriggerCommand(label:String, guiMode:String) 
		{
			super(label, guiMode);
			
			oscAddress = addParameter(new ParameterString("address","Addresse OSC","/example"),"Address du message OSC",true) as ParameterString;
			
			
		}
		
		override public function trigger():void
		{
			this.ip = (module as OpenOSC).ip.value;
			this.port = int(Number((module as OpenOSC).port.value));
			
			address = oscAddress.value;
			args = [];
			//args = [{label:"i",value:1}];
			
			super.trigger();
		}
		
	}

}