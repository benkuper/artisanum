package modules.output.commands 
{
	import benkuper.util.osc.OSCMaster;
	import modules.output.commands.Command;
	import modules.output.IOSCOutputModule;
	import org.tuio.osc.OSCMessage;
	import ui.Toaster;
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class SendOSCCommand extends Command
	{
		protected var address:String;
		protected var args:Array;
		
		public function SendOSCCommand(label:String, guiMode:String)
		{
			super(label, guiMode);
			args = new Array();
		}
		
		override protected function setProp(propName:String, value:*):void 
		{
			switch(propName)
			{
				case "address":
					this.address = value as String;
					break;
					
				case "args":
					this.args = value as Array;
					break;
			}
			
			super.setProp(propName, value);
		}
		
		override public function setValue(value:Number):void
		{
			super.setValue(value);
			// set value calls trigger
		}
		
		override public function trigger():void
		{
			super.trigger();
			sendOSC();
		}
		
		protected function sendOSC():void
		{
			if (address == null)
			{
				trace("[SendOSCCommand::SendOSC] Error : address null !");
				return;
			}
			
			var msg:OSCMessage = new OSCMessage();
			msg.address = address;
			
			for each( var loopObject:Object in args)
				msg.addArgument(loopObject.label, loopObject.value);
			
			(module as IOSCOutputModule).sendMessage(msg);
		}
	}
}
