package modules.output.commands 
{
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class CommandGUIModes 
	{
		static public const OUTPUT_MODULE:String = "outputModule";
		static public const EDITOR_TRIGGER:String = "editorTrigger";
		static public const EDITOR_MAPPING:String = "editorMapping";
		
	}

}