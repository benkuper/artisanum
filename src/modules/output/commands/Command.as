package modules.output.commands 
{
	import com.greensock.TweenMax;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.text.TextField;
	import modules.Module;
	import modules.output.OutputModule;
	import modules.parameters.Parameter;
	import modules.parameters.ParameterString;
	import ui.components.CheckBox;
	import ui.components.UIEvent;
	import ui.fonts.Fonts;
	import ui.Style;
	import ui.Tooltip;
	/**
	 * ...
	 * @author ...
	 */
	public class Command
	{
		public var label:String;
		
		public var gui:Sprite;
		protected var guiMode:String;
		public var triggerGUI:CheckBox;
		protected var _guiWidth:Number;
		protected var _guiHeight:Number;
		
		public var type:String;
		public var module:OutputModule;
		
		protected var mappingValue:Number;
		
		protected var parameters:Vector.<Parameter>;
		
		
		
		public function Command(label:String, guiMode:String = "")//CommandGUIModes.OUTPUT_MODULE) 
		{
			this.label = label;
			this.guiMode = guiMode;
			
			gui = new Sprite();
			
			_guiWidth = 100;
			_guiHeight = 30;
			
			triggerGUI = new CheckBox("Tester", false);
			if(guiMode == CommandGUIModes.OUTPUT_MODULE ||guiMode == CommandGUIModes.EDITOR_TRIGGER) gui.addChild(triggerGUI);
			triggerGUI.addEventListener(UIEvent.CHECKBOX_SELECTED, triggerGUISelected);
			
			//updateGUI();
			
			parameters = new Vector.<Parameter>();
		}
		
		protected function addParameter(newParam:Parameter, tooltip:String = "", addToChildren:Boolean = false):Parameter
		{
			parameters.push(newParam);
			
			if (addToChildren)
			{
				gui.addChild(newParam.gui);
				gui.setChildIndex(newParam.gui, 0);
			}
			if (tooltip != "") Tooltip.addTarget(newParam.gui, tooltip);
			
			return newParam;
		}
		
		protected function removeParameter(targetParam:Parameter):void
		{
			var index:int = parameters.indexOf(targetParam);
			
			if (index < 0) throw new Error("Can't delete Parameter " + targetParam.label + " because it doesn't belong to this command");
		
			if (gui.contains(targetParam.gui)) gui.removeChild(targetParam.gui);
			
			parameters.splice(index, 1);
		}
		
		protected function removeAllParameters():void
		{
			while (parameters.length > 0)
			{
				removeParameter(parameters[0]);
			}
		}
		
		
		public function setProps(props:Object):void
		{
			for (var propName:String in props)
			{
				setProp(propName, props[propName]);
			}
		}
		
		protected function setProp(propName:String, value:*):void 
		{
			//to be overriden
		}
		
		//Mapping related
		public function setValue(value:Number):void
		{
			mappingValue = value;
			trigger();
		}
		
		//trigger relaterd
		public function trigger():void
		{
			// to be overriden
		}
		
		// GUI
		protected function updateGUI():void 
		{
			var i:int;
			var curX:int = -5;
			var curY:int = 10;
			
			switch(guiMode)
			{
				case CommandGUIModes.OUTPUT_MODULE:
					triggerGUI.x = guiWidth -triggerGUI.width;
					triggerGUI.y = -30;
					break;
					
				case CommandGUIModes.EDITOR_MAPPING:
				case CommandGUIModes.EDITOR_TRIGGER:
					triggerGUI.x = guiWidth - triggerGUI.width - 5;
					break;
			}
		}
			
		
		//handlers
		private function triggerGUISelected(e:UIEvent):void 
		{
			trigger();
		}
		
		
		public function clean():void
		{
			
			if (triggerGUI != null) triggerGUI.removeEventListener(UIEvent.CHECKBOX_SELECTED, trigger);
			
			removeAllParameters();
		}
		
		//Saving and Loading
		
		public function getXML():XML 
		{
			var xml:XML = <command></command>;
			var paramsXML:XML = <parameters></parameters>;
			
			//var pIndex:int = 0;
			for each(var p:Parameter in parameters)
			{
				var pXML:XML = p.getXML();
				//pXML.@parameterIndex = pIndex;
				
				paramsXML.appendChild(pXML);
				
				//pIndex++;
			}
			xml.appendChild(paramsXML);
			
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			for each(var pXML:XML in data.parameters[0].parameter)
			{
				var p:Parameter = getParameterByID(pXML.@id);
				if(p != null) p.loadXML(pXML);
			}
		}
		
		private function getParameterByID(id:String):Parameter
		{
			for each(var p:Parameter in parameters)
			{
				if (p.id == id) return p;
			}
			
			return null;
		}
		
		// Getters & setters
		public function get guiWidth():Number 
		{
			return _guiWidth;
		}
		
		public function set guiWidth(value:Number):void 
		{
			_guiWidth = value;
			updateGUI();
		}
		
		public function get guiHeight():Number 
		{
			return _guiHeight;
		}
		
		public function set guiHeight(value:Number):void 
		{
			_guiHeight = value;
			updateGUI();
		}
		
		//public function get guiMode():String 
		//{
			//return _guiMode;
		//}
		//
		//public function set guiMode(value:String):void 
		//{
			//_guiMode = value;
			//updateGUI();
		//}
	}

}