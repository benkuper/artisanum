package modules.output.commands 
{
	import modules.Module;
	import modules.output.OutputModule;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class CommandType 
	{
		public var id:String;
		public var label:String;
		public var typeClass:Class;
		public var props:Object;
		public var commandType:String;
		public var module:Module;

		static public const BOTH:String = "both";
		static public const TRIGGER:String = "trigger";
		static public const MAPPING:String = "mapping";
		
		
		public function CommandType(id:String, label:String,typeClass:Class,commandType:String, props:Object) 
		{
			this.id = id;
			this.label = label;
			this.typeClass = typeClass;
			this.commandType = commandType;
			this.props = props;
		}
		
		public function toString():String
		{
			return "[CommandType, label = " + label + ", typeClass = " + typeClass + "]";
		}
		
		public function createCommand(guiMode:String):Command
		{
			var c:Command = new typeClass(label, guiMode) as Command;
			c.type = commandType;
			c.setProps(props);
			c.module = module as OutputModule;
			return c;
		}
		
	}

}