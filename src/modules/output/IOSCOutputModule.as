package modules.output 
{
	import org.tuio.osc.OSCMessage;
	
	/**
	 * ...
	 * @author To
	 */
	public interface IOSCOutputModule 
	{
		function sendMessage(message:OSCMessage):void;
		
	}
	
}