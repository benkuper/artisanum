package modules.output.bionic 
{
	import modules.output.commands.Command;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.CommandType;
	import modules.output.OutputModule;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class LooperOutputModule extends OutputModule 
	{
		
		public function LooperOutputModule() 
		{
			super();
			
			addCommandTypes(
				new CommandType("selectTrack", "Selectionner piste", null, CommandType.BOTH, { } )
			,	new CommandType("record", "Enregistrer", null, CommandType.TRIGGER, { } )
			,	new CommandType("mute", "Mute", null, CommandType.TRIGGER, { } )
			,	new CommandType("stop", "Stop", null, CommandType.TRIGGER, { } )
			,	new CommandType("clear", "Vider", null, CommandType.TRIGGER, { } )
			,	new CommandType("clearAll", "Tout vider", null, CommandType.TRIGGER, { } )
			
			);
		}
		
	}

}