package modules.output.bionic.commands 
{
	import modules.output.commands.SendOSCCommand;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class LooperCommand extends SendOSCCommand 
	{
		
		public function LooperCommand(label:String, guiMode:String) 
		{
			super(label, guiMode);
			
		}
		
	}

}