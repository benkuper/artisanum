package modules.output.demo 
{
	import modules.events.ParameterEvent;
	import modules.output.commands.CommandGUIModes;
	import modules.output.commands.SendOSCCommand;
	import modules.parameters.ParameterInt;
	
	/**
	 * ...
	 * @author ...
	 */
	public class CubeCommand extends SendOSCCommand 
	{
		
		private var cubeID:int;
		private var cubeParam:ParameterInt;
		
		public function CubeCommand(label:String, guiMode:String) 
		{
			super(label, guiMode);
			
			cubeParam = addParameter(new ParameterInt("cubeNumber", "Numéro cube", 1, 1, 4),"",true) as ParameterInt;
			cubeID = 1; //Default value
			
			cubeParam.addEventListener(ParameterEvent.VALUE_CHANGE, cubeChange);
		}
		
		private function cubeChange(e:ParameterEvent):void 
		{
			cubeID = cubeParam.value;
		}
		
		override protected function setProp(propName:String, value:*):void 
		{
			super.setProp(propName, value);
			
			switch(propName)
			{
				case "cubeID":
					cubeParam.value = value as int;
					break;
			}
		}
		
		override protected function updateGUI():void
		{
			
			super.updateGUI();
			
			switch(guiMode)
			{
				case CommandGUIModes.OUTPUT_MODULE:
					cubeParam.gui.y = 5;
					break;
					
				case CommandGUIModes.EDITOR_MAPPING:
				case CommandGUIModes.EDITOR_TRIGGER:
					cubeParam.gui.x = 30;
					cubeParam.gui.y = 30;
					
					break;
			}
		}
		
		override public function setValue(value:Number):void
		{
			args[0] = { label:"i", value:cubeID };
			args[1] = { label:"f", value:value };
			super.setValue(value);
		}
	}

}