package modules.output.demo 
{
	import benkuper.util.IPUtil;
	import benkuper.util.osc.OSCMaster;
	import modules.events.ParameterEvent;
	import modules.output.commands.Command;
	import modules.output.commands.CommandType;
	import modules.output.OutputModule;
	import modules.parameters.ParameterString;
	import org.tuio.osc.IOSCListener;
	import org.tuio.osc.OSCMessage;
	import ui.components.Button;
	import ui.components.UIEvent;
	
	/**
	 * ...
	 * @author ...
	 */
	public class DemoOutputModule extends OutputModule implements IOSCListener
	{
		private var ip:ParameterString;
		private var port:ParameterString;
		
		private var localIP:String;
		private var scanBT:Button;
		
		
		public function DemoOutputModule() 
		{
			super();
			
			localIP = IPUtil.getLocalIP();
			
			ip = addParameter(new ParameterString("IP", "127.0.0.1") ) as ParameterString;
			port =  addParameter(new ParameterString("Port", "8000")) as ParameterString;
			
			ip.addEventListener(ParameterEvent.VALUE_CHANGE, valueChange);
			port.addEventListener(ParameterEvent.VALUE_CHANGE, valueChange);
			
			addCommandTypes(
				new CommandType("Couleur", CubeCommand, CommandType.TRIGGER, { address:"/artisanum/color" },
				new CommandType("Rotation", CubeCommand, CommandType.MAPPING, { address:"/artisanum/rotation" } ),
				new CommandType("Taille", CubeCommand, CommandType.MAPPING, { address:"/artisanum/size" } )
			);
			
			scanBT = new Button("Detection IP auto.");
			addControl(scanBT);
			scanBT.addEventListener(UIEvent.BUTTON_SELECTED, scanBTSelected);
			controlsContainer.headerHeight = 40;
			controlsContainer.height = 80;
			
			refreshOSC();
			
		}
		
		private function scanBTSelected(e:UIEvent):void 
		{
			scanOSC();
		}
		
		private function valueChange(e:ParameterEvent):void 
		{
			trace("valueChange");
			refreshOSC();
		}
		
		private function scanOSC():void
		{
			
			var baseIP:String = localIP.slice(0, localIP.lastIndexOf(".")+1);
			trace("scan : " + baseIP);
			var msg:OSCMessage
			
			for (var i:int = 1; i < 255; i++)
			{
				var targetIP:String = baseIP + i;
				msg = new OSCMessage();
				msg.address = "/artisanum/ping";
				msg.addArgument("s", localIP);
				msg.addArgument("s", targetIP);
				msg.addArgument("i", int( Math.random() * 1000));
				OSCMaster.sendTo(msg, targetIP, 8000);
			}
		}
		
		
		/* INTERFACE org.tuio.osc.IOSCListener */
		
		public function acceptOSCMessage(msg:OSCMessage):void 
		{
			trace("Message !", msg.address, msg.argumentsToString());
			var msgSplit:Array = msg.address.split("/");
			if (msgSplit[msgSplit.length - 1] == "pong")
			{
				trace("found ip ! host is ", msg.arguments[0]);
				ip.value = msg.arguments[0] as String;
			}
		}
		
		override protected function draw():void
		{
			scanBT.x = 10;
			scanBT.y = 0;
			scanBT.width = moduleWidth - 40;
			
			ip.gui.x = 0;
			ip.gui.y = 0;
			ip.guiWidth = moduleWidth - 30;
			port.gui.x = 0;
			port.gui.y = 30;
			port.guiWidth = moduleWidth - 30;
			
			super.draw();
		}
		
		
		override public function clean():void
		{
			super.clean();
			ip.removeEventListener(ParameterEvent.VALUE_CHANGE, valueChange);
			port.removeEventListener(ParameterEvent.VALUE_CHANGE, valueChange);
		}
	}

}