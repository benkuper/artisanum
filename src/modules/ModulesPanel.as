package modules 
{
	import com.greensock.easing.Strong;
	import com.greensock.TweenLite;
	import com.laiyonghao.Uuid;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;
	import modules.events.ModuleEvent;
	import modules.input.InputModule;
	import modules.Module;
	import modules.ModuleType;
	import modules.ModuleTypes;
	import ui.components.Button;
	import ui.components.DropDown;
	import ui.components.Panel;
	import ui.components.UIEvent;
	import ui.fonts.Fonts;
	import ui.guider.Guider;
	import ui.Layout;
	import ui.Style;
	import ui.Toaster;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ModulesPanel extends Panel 
	{
		
		//layout
		private var _side:String;
		private var _opened:Boolean;
		
		//ui
		private var addModuleDropDown:DropDown;
		private var switcher:Button;
		
		//data
		private var moduleList:Vector.<Module>;
		private var moduleType:String;
		
		
		public function ModulesPanel(moduleType:String) 
		{
			
			super(moduleType == ModuleType.INPUT?"Modules d'entrée":"Modules de sortie");
			this.moduleType = moduleType;
			
			titleTF.autoSize = TextFieldAutoSize.CENTER;
			titleTF.defaultTextFormat = Fonts.bigTF;
			titleTF.text = title;
			
			_bgColor = Style.UI_PANEL;
			headerHeight = 30;
			
			
			switcher = new Button("");
			switcher.roundCorners = Button.ROUND_NONE;
			addChildAt(switcher,this.getChildIndex(mainContainer)-1);
			switcher.addEventListener(UIEvent.BUTTON_SELECTED, switcherHandler);
			
			addModuleDropDown = new DropDown(ModuleTypes.getList(moduleType),"Ajouter un module");
			mainContainer.addChild(addModuleDropDown);
			addModuleDropDown.addEventListener(UIEvent.DROPDOWN_CHANGE, dropDownChange);
			addModuleDropDown.instantClose = false;
			
			moduleList = new Vector.<Module>;
			baseWidth = 100;
			baseHeight = 200;
			
			draw();
			
			_side = Layout.SIDE_LEFT;
			_opened = true;
			
			scrollable = true;
			
			filters = [new DropShadowFilter(0, 90, 0, 1, 6, 6, .5)];
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			addEventListener(ModuleEvent.REMOVE_MODULE, removeModuleHandler);
			
			addEventListener(UIEvent.PANEL_OPENED, panelResizeHandler);
			addEventListener(UIEvent.PANEL_CLOSED, panelResizeHandler);
			
			addEventListener(UIEvent.PANEL_RESIZED, panelResizeHandler);
			
		}
		
		override protected function addedToStage(e:Event):void 
		{
			super.addedToStage(e);
			baseWidth = 200;
			baseHeight = stage.stageHeight;
			draw();
		}
		
		
		
		
		
		//data
		
		public function addModule(mt:ModuleType,fromData:XML = null):Module
		{
			if (mt == null) 
			{
				Toaster.warning("Module inexistant");
				return null;
			}
			
			var numModuleOfSameType:int = getNumberOfModulesForType(mt);
			
			if (mt.maxAllowed > 0 && numModuleOfSameType >= mt.maxAllowed)
			{
				Toaster.warning("Il ne peut y avoir " + (mt.maxAllowed == 1?"qu'un seul module":"que " + mt.maxAllowed + " modules") + " " + mt.name);
				return null;
			}
			
			var newModule:Module = new mt.typeClass();
			newModule.y = addModuleDropDown.y;
			
			if (fromData != null) newModule.loadXML(fromData);
			
			var im:Module = InputModule.getModuleById(newModule.id);
			var om:Module = InputModule.getModuleById(newModule.id);
			if (im != null || om != null)
			{
				if (im != newModule && om != newModule) 
				{
					trace("Set new id because one already exists");
					newModule.id = new Uuid().toString();
				}
			}
			
			
			moduleList.push(newModule);
			mainContainer.addChild(newModule);
			
			
			draw();
			
			TweenLite.fromTo(newModule, .8, {scaleY:0},{scaleY:1, ease:Strong.easeOut } );
			
			Toaster.info("Module ajouté : " + newModule.moduleName);
			
			dispatchEvent(new ModuleEvent(ModuleEvent.MODULE_ADDED));
			
			return newModule;
			
		}
		
		
		public function removeModuleAnimated(module:Module):void
		{
			
			TweenLite.to(module, .5, { x: (side == Layout.SIDE_LEFT)?-module.width:baseWidth, ease:Strong.easeOut, onComplete:removeModule, onCompleteParams:[module] } );
			Toaster.warning("Module supprimé : " + module.moduleName);
			
			
		}
		
		public function removeModule(module:Module):void 
		{
			//trace("remove Module", moduleList.indexOf(module));
			moduleList.splice(moduleList.indexOf(module), 1);
			//trace("remove after splice, new length :", moduleList.length);
			module.clean();
			mainContainer.removeChild(module);
			draw();
			
			dispatchEvent(new ModuleEvent(ModuleEvent.MODULE_REMOVED));
		}
		
		private function clearModules():void 
		{
			while (moduleList.length > 0)
			{
				removeModule(moduleList[0]);
			}
		}
		
		
		//ui
		override protected function draw():void
		{
			super.draw();
			
			
			
			switcher.width = 10;
			switcher.height = baseHeight;
			
			switch(side)
			{
				case Layout.SIDE_LEFT:
					switcher.x = baseWidth;
					break;
					
				case Layout.SIDE_RIGHT:
					switcher.x = -switcher.width;
					break;
			}
			
			
			placeModules();
			
			titleTF.x = 0;
			titleTF.width = baseWidth;
		}
		
		private function placeModules():void 
		{
			var curY:Number = 10;
			
			for (var i:int = 0; i < moduleList.length; i++) 
			{
				moduleList[i].x = 10;
				moduleList[i].width = baseWidth - 20;
				TweenLite.to(moduleList[i],.5,{y:curY,ease:Strong.easeOut});
				curY += moduleList[i].height + 10;
			}
			
			curY += 10;
			addModuleDropDown.x = 10;
			TweenLite.to(addModuleDropDown, .5, { y:curY,ease:Strong.easeOut } );
			addModuleDropDown.width = baseWidth - 20;
			addModuleDropDown.height = 20;
			curY += addModuleDropDown.height + 10;
			
		}
		
		
		
		
		//util
		private function getNumberOfModulesForType(mt:ModuleType):int
		{
			if (mt == null) return 0;
			
			var numModules:int = 0;
			
			for each(var m:Module in moduleList)
			{
				//trace("module type search", m.moduleType == mt, m.moduleType.type, mt.type);
				if (m.moduleType == mt) 
				{
					numModules++;
					
				}
			}
			
			return numModules;
		}
		
		
		//handlers
		
		private function removeModuleHandler(e:ModuleEvent):void 
		{
			var m:Module = e.target as Module;
			removeModuleAnimated(m);
		}
		
		private function switcherHandler(e:UIEvent):void 
		{
			toggleOpened();
		}
		
		
		private function panelResizeHandler(e:UIEvent):void 
		{
			placeModules();
			
		}
		
		
		private function dropDownChange(e:UIEvent):void 
		{
			if (addModuleDropDown.selectedOption != null) 
			{
				addModule(addModuleDropDown.selectedOption.value as ModuleType);
				addModuleDropDown.resetSelection();
			}
		}
		
		
		
		//util
		public function showFirstGuider():void
		{
			Guider.show(addModuleDropDown,moduleType == ModuleType.INPUT?"Commençons par ajouter un module d'entrée":"Ajoutons ensuite un module de sortie",true);
		}
		
		
		//XML Project saving
		public function getXML(baseXML:XML):XML 
		{
			var xml:XML = baseXML;
			for each(var module:Module in moduleList)
			{
				xml.appendChild(module.getXML());
			}
			
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			if (data == null) return;
			
			clearModules();
			var xmlInputs:XMLList = data.children();
			var getMTFunction:Function = (data.name() == "inputs")?ModuleTypes.getInputTypeForType:ModuleTypes.getOutputTypeForType;
			for each(var xi:XML in data.module)
			{
				
				var m:Module = addModule(getMTFunction(xi.@type),xi);
				if (m == null)
				{
					trace("le module "+xi.@type+" n'existe pas ou max limit");
				}
			}
		}
		
		public function reset():void 
		{
			clearModules();
		}
		
		
		
		public function toggleOpened():void 
		{
			opened = !opened;
		}
		
		
		//getter / setter
		
		public function get opened():Boolean 
		{
			return _opened;
		}
		
		public function set opened(value:Boolean):void 
		{
			_opened = value;
			
			var tx:Number;
			switch(side)
			{
				case Layout.SIDE_LEFT:
					tx = opened?0:-baseWidth;
					break;
					
				case Layout.SIDE_RIGHT:
					tx = opened?stage.stageWidth - baseWidth:stage.stageWidth;
					break;
			}
			
			TweenLite.to(this, .3, { x:tx } );
		}
		
		public function get side():String 
		{
			return _side;
		}
		
		public function set side(value:String):void 
		{
			_side = value;
			draw();
		}
		
		override public function get width():Number
		{
			return baseWidth;
		}
		
	}

}