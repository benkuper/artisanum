package modules
{
	import com.greensock.easing.Sine;
	import com.greensock.TweenLite;
	import com.laiyonghao.Uuid;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import modules.events.ModuleEvent;
	import modules.parameters.Parameter;
	import ui.Alert;
	import ui.components.Button;
	import ui.components.Panel;
	import ui.components.UIEvent;
	import ui.fonts.Fonts;
	import ui.Style;
	import ui.Tooltip;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	
	[RemoteClass]
	
	public class Module extends Sprite
	{
		//layout
		public var bgWidth:Number;
		public var bgHeight:Number;
		
		protected var moduleWidth:Number;
		protected var moduleHeight:Number;
		
		protected var baseHeight:Number;
		
		protected var headerHeight:Number;
		private var _opened:Boolean;
		
		//ui
		private var titleTF:TextField;
		protected var controlsContainer:Panel;
		protected var parameterContainer:Panel;
		protected var contentContainer:Sprite;
		protected var hasParameters:Boolean;
		protected var hasControls:Boolean;
		
		private var bg:Shape;
		private var removeBT:Button;
		private var switcherBT:Button;
		private var enableBT:Button;
		
		private var openItem:NativeMenuItem;
		
		//data
		public var id:String;
		private var _moduleName:String;
		private var _moduleType:ModuleType;
		protected var parameters:Vector.<Parameter>;
		
		private var _enabled:Boolean;
		
		public function Module()
		{
			
			id = new Uuid().toString();
			
			this._moduleType = ModuleTypes.getTypeForModule(this);
			this.moduleName = this.moduleType.name;
			
			_opened = true;
			
			//trace("New module : " + name + ", type : " + moduleType.name);
			
			parameters = new Vector.<Parameter>;
			
			bg = new Shape();
			addChild(bg);
			
			titleTF = Fonts.createTF(moduleName, Fonts.normalTF);
			addChild(titleTF);
			titleTF.x = 5;
			titleTF.y = 5;
			titleTF.selectable = false;
			
			removeBT = new Button("x");
			removeBT.width = 30;
			removeBT.height = 20;
			addChild(removeBT);
			removeBT.addEventListener(UIEvent.BUTTON_SELECTED, removeBTHandler);
			
			switcherBT = new Button("");
			switcherBT.width = 50;
			switcherBT.height = 15;
			addChild(switcherBT);
			switcherBT.addEventListener(UIEvent.BUTTON_SELECTED, switcherBTHandler);
			
			enableBT = new Button("<>",true);
			enableBT.width = 30;
			enableBT.height = 20;
			enableBT.selected = true;
			_enabled = true;
			addChild(enableBT);
			enableBT.addEventListener(UIEvent.BUTTON_DESELECTED, enableBTHandler);
			enableBT.addEventListener(UIEvent.BUTTON_SELECTED, enableBTHandler);
			
			
			baseHeight = 0;
			moduleWidth = 100;
			moduleHeight = baseHeight + headerHeight;
			headerHeight = 50;
			
			contentContainer = new Sprite();
			addChild(contentContainer);
			
			parameterContainer = new Panel("Paramètres");
			parameterContainer.height = 120; //resize by default
			//parameterContainer.state = Panel.STATE_WARNING; //Added icon if needed
			
			controlsContainer = new Panel("Contrôles", 100, 50);
			//controlsContainer.autoLayout = true;
			//addChild(controlsContainer);
			
			filters = [new DropShadowFilter(2, 45, 0, 1, 4, 4, .5)];
			
			contextMenu = new NativeMenu();
			//contextMenu.addItem(new NativeMenuItem("Renommer"));
			contextMenu.addItem(new NativeMenuItem("Supprimer"));
			openItem = new NativeMenuItem("Mini Mode");
			contextMenu.addItem(openItem);
			
			contextMenu.addEventListener(Event.SELECT, contextMenuSelect);
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		protected function addControl(control:DisplayObject):void
		{
			controlsContainer.addElement(control);
			hasControls = controlsContainer.numChildren > 0;
			
			if (hasControls && !contains(controlsContainer))
				addChild(controlsContainer);
		
		}
		
		protected function removeControl(control:DisplayObject):void
		{
			controlsContainer.removeElement(control);
			hasControls = controlsContainer.numChildren > 0;
			
			if (!hasControls && contains(controlsContainer))
				removeChild(controlsContainer);
		}
		
		protected function addParameter(newParam:Parameter, addGUI:Boolean = true, tooltip:String = ""):Parameter // TODO :  checker la classe de newParam  ?
		{
			parameters.push(newParam);
			
			if (addGUI)
				parameterContainer.addElement(newParam.gui);
			if (tooltip != "")
				Tooltip.addTarget(newParam.gui, tooltip);
			
			hasParameters = parameterContainer.numChildren > 0
			if (hasParameters && !contains(parameterContainer))
				addChild(parameterContainer);
			
			return newParam;
		}

        protected function removeAllParameters():void
        {
            while (parameters.length > 0)
            {
                removeParameter(parameters[0]);
            }
        }

		protected function removeParameter(targetParam:Parameter, autoHeight:Boolean = true):void
		{
			var index:int = parameters.indexOf(targetParam);
			trace("remove param", targetParam.id);
			
			if (index < 0)
				throw new Error("Can't delete Parameter " + targetParam.label + " because it doesn't belong to Module " + moduleName);
			
			parameterContainer.removeElement(targetParam.gui);
			
			parameters.splice(index, 1);
			
			hasParameters = parameterContainer.numChildren > 0;
			
			if (!hasParameters && contains(parameterContainer))
				removeChild(parameterContainer);
		
		}
		
		protected function draw():void
		{
			if (opened)
			{
				bgWidth = moduleWidth;
			}
			else
			{
				bgWidth = moduleWidth;
				bgHeight = headerHeight;
			}
			
			var curY:Number = headerHeight;
			
			if (contains(controlsContainer))
			{
				controlsContainer.x = 10;
				controlsContainer.width = moduleWidth - 20;
				controlsContainer.y = curY;
				curY = controlsContainer.y + controlsContainer.height + 5;
			}
			
			if (contains(parameterContainer))
			{
				parameterContainer.x = 10;
				parameterContainer.width = moduleWidth - 20;
				
				parameterContainer.y = curY;
				curY = parameterContainer.y + parameterContainer.height + 5;
			}
			
			if (contains(contentContainer))
			{
				contentContainer.y = curY;
				curY = contentContainer.y + contentContainer.height + 5;
			}
			
			removeBT.x = moduleWidth - removeBT.width - 5;
			removeBT.y = 5;
			
			enableBT.x = removeBT.x - enableBT.width - 5;
			enableBT.y = 5;
			
			switcherBT.x = (moduleWidth - switcherBT.width) / 2;
			switcherBT.y = 5;
			
			moduleHeight = curY + 10;
			bgHeight = moduleHeight;
			
			drawBG();
		}
		
		public function drawBG():void
		{
			if (bgWidth == 0 || bgHeight == 0 || isNaN(bgWidth) || isNaN(bgHeight)) return;
			bg.graphics.clear();
			bg.graphics.beginFill(Style.UI_SUBPANEL);
			bg.graphics.drawRoundRect(0, 0, bgWidth, bgHeight, 5, 5);
			bg.graphics.endFill();
		}
		
		private function contextMenuSelect(e:Event):void
		{
			var item:NativeMenuItem = e.target as NativeMenuItem;
			switch (item.label)
			{
				case "Renommer":
					//renameModule();
					break;
					
				case "Supprimer": 
					askForRemove();
					break;
				
				case "Mini Mode": 
					openItem.checked = !openItem.checked;
					opened = !openItem.checked;
					break;
					
			}
		}
		
		private function switcherBTHandler(e:UIEvent):void
		{
			opened = !opened;
		}
		
		private function removeBTHandler(e:UIEvent):void
		{
			
			askForRemove();
		}
		
		private function enableBTHandler(e:UIEvent):void 
		{
			enabled = enableBT.selected;
		}
		
		private function askForRemove():void
		{
			Alert.show(stage, "Voulez-vous vraiment supprimer ce module ?", true, confirmRemove);
		}
		
		private function confirmRemove():void
		{
			dispatchEvent(new ModuleEvent(ModuleEvent.REMOVE_MODULE));
		}
		
		private function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			draw();
		
		}
		
		protected function showElement(element:DisplayObject, elemX:Number = NaN, elemY:Number = NaN, duration:Number = 0.5):void
		{
			addToChildren(element);
			TweenLite.to(element, duration, {x: isNaN(elemX) ? element.x : elemX, y: isNaN(elemY) ? element.y : elemY, alpha: 1, ease: Sine.easeInOut});
		}
		
		protected function hideElement(element:DisplayObject, duration:Number = 0.5):void
		{
			TweenLite.to(element, duration, {x: 0, alpha: 0, ease: Sine.easeInOut, onComplete: removeFromChildren, onCompleteParams: [element]});
		}
		
		protected function addToChildren(child:DisplayObject):void
		{
			
			if (!contentContainer.contains(child))
				contentContainer.addChild(child);
		
		}
		
		protected function removeFromChildren(child:DisplayObject):void
		{
			if (contentContainer.contains(child))
				contentContainer.removeChild(child);
		}
		
		public function clean():void
		{
			//trace("Module CLEAN !");
			if (contextMenu != null)
			{
				contextMenu.removeEventListener(Event.SELECT, contextMenuSelect);
				contextMenu = null;
			}
			removeBT.removeEventListener(UIEvent.BUTTON_SELECTED, removeBTHandler);
		}
		
		public function closeComplete():void
		{
			dispatchEvent(new UIEvent(UIEvent.PANEL_CLOSED));
			if (contains(contentContainer))
				removeChild(contentContainer);
			if (contains(parameterContainer))
				removeChild(parameterContainer);
			if (contains(controlsContainer))
				removeChild(controlsContainer);
		}
		
		//Project save XML
		public function getXML():XML
		{
			var xml:XML =  <module></module>;
			xml.@id = id;
			xml.@name = moduleName;
			xml.@type = moduleType.type;
			xml.@opened = opened;
			xml.appendChild(<parameters></parameters>);
			
			//var pIndex:int = 0;
			for each(var p:Parameter in parameters)
			{
				var pXML:XML = p.getXML();
				xml.parameters[0].appendChild(pXML);
					//pXML.@parameterIndex = pIndex;
					//pIndex++;
			}
			
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			moduleName = data.@name;
			id = data.@id;
			
			for each (var pXML:XML in data.parameters[0].parameter)
			{
				try 
				{
					getParameterByID(pXML.@id).loadXML(pXML);
				} catch (e:Error)
				{
					trace(e.getStackTrace());
					
				}
					// TODO : mettre une protection en cas de sauvegarde foireuse
			}
			
			opened = (data.@opened == "true");
		}
		
		private function getParameterByID(id:String):Parameter
		{
			for each (var p:Parameter in parameters)
			{
				if (p.id == id)
					return p;
			}
			
			return null;
		}
		
		// GETTER / SETTER
		
		public function get moduleName():String
		{
			return _moduleName;
		}
		
		public function set moduleName(value:String):void
		{
			_moduleName = value;
			if(titleTF != null) titleTF.text = value;
		}
		
		public function get moduleType():ModuleType
		{
			return _moduleType;
		}
		
		override public function get width():Number
		{
			return moduleWidth;
		}
		
		override public function set width(value:Number):void
		{
			if (moduleWidth == value)
				return;
			moduleWidth = value;
			bgWidth = value;
			draw();
		}
		
		override public function get height():Number
		{
			return opened ? moduleHeight : headerHeight;
		}
		
		override public function set height(value:Number):void
		{
			if (moduleHeight == value)
				return;
			moduleHeight = value;
			bgHeight = opened ? value : headerHeight;
			draw();
		}
		
		public function get opened():Boolean
		{
			return _opened;
		}
		
		public function set opened(value:Boolean):void
		{
			_opened = value;
			
			if (value)
			{
				dispatchEvent(new UIEvent(UIEvent.PANEL_OPENED));
				
				TweenLite.to(this, .2, {bgHeight: moduleHeight, onUpdate: drawBG});
				addChild(contentContainer);
				if (hasParameters)
					addChild(parameterContainer);
				if (hasControls)
					addChild(controlsContainer);
				//contentContainer.alpha = 1;
				TweenLite.to([parameterContainer, contentContainer, controlsContainer], .2, {delay: .2, alpha: 1});
				
			}
			else
			{
				TweenLite.to([parameterContainer, contentContainer, controlsContainer], .2, {alpha: 0});
				TweenLite.to(this, .2, {delay: .2, bgHeight: headerHeight, onUpdate: drawBG, onComplete: closeComplete});
			}
		}
		
		public function get enabled():Boolean 
		{
			return _enabled;
		}
		
		public function set enabled(value:Boolean):void 
		{
			_enabled = value;
			draw();
		}
	
	}
}