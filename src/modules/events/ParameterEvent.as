package modules.events 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class ParameterEvent extends Event 
	{
		public static const VALUE_CHANGE:String = "valueChange";
		
		public function ParameterEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ParameterEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ParameterEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}