package modules.events 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class MeasureEvent extends Event 
	{
		public static const VALUE_CHANGE:String = "valueChange";
		static public const LABEL_CHANGED:String = "labelChanged";
		
		public function MeasureEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new MeasureEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("MeasureEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}