package modules.events 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ModuleEvent extends Event 
	{
		
		public static const REMOVE_MODULE:String = "removeModule";
		
		
		static public const MODULE_ADDED:String = "moduleAdded";
		static public const MODULE_REMOVED:String = "moduleRemoved";
		
		static public const MEASURE_ADDED:String = "measureAdded";
		static public const MEASURE_REMOVED:String = "measureRemoved";
		
		public function ModuleEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ModuleEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ModuleEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}