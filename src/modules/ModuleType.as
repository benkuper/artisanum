package modules 
{
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ModuleType 
	{
		
		public var name:String;
		public var type:String;
		public var typeClass:Class;
		public var flow:String;
		public var maxAllowed:int;
		
		//constants
		public static var INPUT:String = "input";
		public static var OUTPUT:String = "output";
		static public const SPECIAL:String = "special";
		
		public function ModuleType(name:String, type:String, typeClass:Class,flow:String,maxAllowed:int = 1)
		{
			
			this.name = name;
			this.type = type;
			this.typeClass = typeClass;
			this.flow = flow;
			this.maxAllowed = maxAllowed;
			
		}
		
	}

}