﻿package modules 
{
import modules.input.ciboulette.CibouletteInputModule;
import modules.input.micro.MicrophoneInputModule;
import modules.input.myo.MyOSCInputModule;
import modules.input.serial.MakeyKitInputModule;
import modules.input.serial.arduinokit.ArduinoKitInputModule;
import modules.output.resolume.ResolumeSimpleOuputModule;

import statemachine.behaviors.modules.BehaviorInputModule;
import statemachine.modules.StateMachineInputModule;
import statemachine.modules.StateMachineOutputModule;

import timemachine.modules.TimeMachineInputModule;
import timemachine.modules.TimeMachineOutputModule;

/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ModuleTypes 
	{
		public static var inputTypes:Vector.<ModuleType>;
		public static var outputTypes:Vector.<ModuleType>;
		public static var specialTypes:Vector.<ModuleType>;
		
		public static function init():void
		{
			inputTypes = new Vector.<ModuleType>;
			inputTypes.push(
			  
				//new ModuleType("Chant", "voiceInputWorkers", VoiceInputWithWorkersModule, ModuleType.INPUT, 1)
				new ModuleType("Micro", "microphone", MicrophoneInputModule, ModuleType.INPUT, 1)
				, new ModuleType("Makey", "MakeyKit", MakeyKitInputModule, ModuleType.INPUT, 0)
				, new ModuleType("Arduino", "Arduinokit", ArduinoKitInputModule, ModuleType.INPUT, 0)
				, new ModuleType("Ciboulette Chant", "Ciboulette", CibouletteInputModule, ModuleType.INPUT, 1)
				//, new ModuleType("OSC", "osc", OSCInputModule, ModuleType.INPUT, 1)
				//, new ModuleType("Myo (OSC)", "MyOSC", MyOSCInputModule, ModuleType.INPUT, 1)
				//, new ModuleType("Test", "test", TestInputModule, ModuleType.INPUT, 0)
				//ajouter les modules d'entrée ici
				
//				, new ModuleType("GloveServer","gloveServer",GloveInputModule,ModuleType.INPUT,1)
//				, new ModuleType("Looper","looper",LooperInputModule,ModuleType.INPUT,1)
//				, new ModuleType("Spat","spat",SpatInputModule,ModuleType.INPUT,1)
//				, new ModuleType("Origamis", "origamis", OrigamiInputModule, ModuleType.INPUT)
//				, new ModuleType("iPad / iPhone(touchOSC)","ipad",TouchOSCInputModule, ModuleType.INPUT)
			);
			
			
			outputTypes = new Vector.<ModuleType>;
			outputTypes.push(
				new ModuleType("Resolume", "simpleResolume",ResolumeSimpleOuputModule, ModuleType.OUTPUT)
				//, new ModuleType("Open OSC", "openOSC",OpenOSC, ModuleType.OUTPUT,0)
				//, new ModuleType("OSC", "osc", OutputModule, ModuleType.OUTPUT)
			//	, new ModuleType("Ableton Live", "live", OutputModule, ModuleType.OUTPUT)
			//	, new ModuleType("DMX", "dmx", OutputModule, ModuleType.OUTPUT)
				//, new ModuleType("Demo 24", "demo24", DemoOutputModule, ModuleType.OUTPUT)
				//,
				//ajouter les modules de sorties ici
				
//				, new ModuleType("GloveServer","gloveServer",GloveServerOutputModule,ModuleType.OUTPUT,1)
//				, new ModuleType("Looper","looper",LooperOutputModule,ModuleType.OUTPUT,1)
//				, new ModuleType("Spat","spat",SpatOutputModule,ModuleType.OUTPUT,1)
//				, new ModuleType("D-Light","dLight",DLightOutputModule,ModuleType.OUTPUT,1)
			);
			
			
			specialTypes = new Vector.<ModuleType>;
			specialTypes.push(
				new ModuleType("Ce comportement", "behaviorInput", BehaviorInputModule, ModuleType.SPECIAL),
				new ModuleType("Machine à état", "stateMachineInput", StateMachineInputModule, ModuleType.SPECIAL),
				new ModuleType("Machine à état", "stateMachineOutput", StateMachineOutputModule, ModuleType.SPECIAL),
				new ModuleType("Machine à temps", "timeMachineInput", TimeMachineInputModule, ModuleType.SPECIAL),
				new ModuleType("Machine à temps", "timeMachineOutput", TimeMachineOutputModule, ModuleType.SPECIAL)
			);
			
			
		}		
		
		static public function getTypeForModule(module:Module):ModuleType
		{
			var targetClass:Class =  Object(module).constructor;
			var mt:ModuleType;
			for each(mt in inputTypes)
			{
				if (mt.typeClass == targetClass) return mt;
			}
			
			for each(mt in outputTypes)
			{
				if (mt.typeClass == targetClass) return mt;
			}
			
			
			for each(mt in specialTypes)
			{
				if (mt.typeClass == targetClass) return mt;
			}
			
			return null;
		}
		
		static public function getList(flow:String):Array
		{
			
			//trace("get List ", flow);
			var target:Vector.<ModuleType>;
			switch(flow)
			{
				case ModuleType.INPUT:
					target = inputTypes.filter(checkAuthorizedInputs);
					break;
					
				case ModuleType.OUTPUT:
					target = outputTypes.filter(checkAuthorizedOutputs);
					break;
					
				case ModuleType.SPECIAL:
					target = specialTypes.filter(checkAuthorizedSpecials);
					break;
					
				default:
					//trace("flow not found ! " + flow);
					return null;
					break;
			}
			
			var list:Array = new Array();
			for each(var mt:ModuleType in target)
			{
				list.push( { label:mt.name, value:mt } );
			}
			
			return list;
		}

		static private function checkAuthorizedInputs(element:ModuleType, index:int, arr:Vector.<ModuleType>):Boolean
		{
			return String(MODULES::inputs).indexOf(element.type) != -1 || String(MODULES::inputs).indexOf("*") != -1;
			//return true;
		}
		
		static private function checkAuthorizedOutputs(element:*, index:int, arr:Vector.<ModuleType>):Boolean
		{
			return String(MODULES::outputs).indexOf(element.type) != -1 || String(MODULES::outputs).indexOf("*") != -1;
			//return true
		}
		
		static private function checkAuthorizedSpecials(element:*, index:int, arr:Vector.<ModuleType>):Boolean
		{
			return String(MODULES::specials).indexOf(element.type) != -1 || String(MODULES::specials).indexOf("*") != -1;
			//return true;
		}
		
		static public function getInputTypeForType(type:String):ModuleType 
		{
			trace("Search input type :" + type);
			for each(var mt:ModuleType in inputTypes)
			{
				if (mt.type == type) return mt;
			}
			
			return null;
		}
		
		static public function getOutputTypeForType(type:String):ModuleType 
		{
			trace("Search output type :" + type);
			for each(var mt:ModuleType in outputTypes)
			{
				if (mt.type == type) return mt;
			}
			
			return null;
		}
		
		static public function getSpecialTypeForType(type:String):ModuleType 
		{
			trace("Search special type :" + type);
			for each(var mt:ModuleType in specialTypes)
			{
				if (mt.type == type) return mt;
			}
			
			return null;
		}
		
	}

}