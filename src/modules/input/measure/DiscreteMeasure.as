package modules.input.measure 
{
	import flash.display.Sprite;
	import modules.events.MeasureEvent;
	import modules.input.measure.comparators.*;
	import ui.components.TextBox;
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class DiscreteMeasure extends Measure // this class is "abstract", it should not be directly implemented
	{
		protected var feedBack:TextBox;
		
		public var referenceValues:Vector.<ReferenceValue>;
		private var _currentReferenceValue:ReferenceValue;
		private var _currentIndex:int;
		
		
		public function DiscreteMeasure(id:String, label:String, enabled:Boolean, dispatchOnChangeOnly:Boolean)
		{
			super(id, label, enabled, dispatchOnChangeOnly);
			
			referenceValues = new Vector.<ReferenceValue>;
			
			feedBack = new TextBox(label, (currentReferenceValue == null)?"no ref":currentReferenceValue.label);
			gui.addChild(feedBack);
			
			_guiWidth = feedBack.width;
			_guiHeight = 10;
			
			
			addComparatorTypes(
				new ComparatorType("=","=", EnumComparator,{} )
				);
		}
		
		// Modify value
		public function jumpToReferenceValue(index:int):void
		{
			index = Math.min(referenceValues.length - 1, Math.max(0, index));	
			currentReferenceValue = referenceValues[index];
		}
		
		
		// UI
		override protected function updateGUI():void
		{
			feedBack.width = guiWidth;
			feedBack.height = guiHeight;
		}
		
		// Getters & setters
		public function get currentReferenceValue():ReferenceValue 
		{
			return _currentReferenceValue;
		}
		
		public function set currentReferenceValue(value:ReferenceValue):void 
		{
			//if (currentReferenceValue != null)
			//{
				//trace("Set currentReference Value :", value.label, currentReferenceValue.label, value == currentReferenceValue, dispatchOnChangeOnly);
			//}
			
			if (currentReferenceValue == value && dispatchOnChangeOnly) return;
			
			if (value == null)
			{
				trace("[DiscreteMeasure", label,"] ERROR : new current reference value null !");
				return;
			}
			_currentReferenceValue = value;
			
			if (feedBack != null)
				feedBack.text = value.label;
				
			if (!enabled) return;
			dispatchEvent(new MeasureEvent(MeasureEvent.VALUE_CHANGE));
		}
		
		public function get currentIndex():int
		{
			return referenceValues.indexOf(currentReferenceValue);
		}
		
		public function getReferenceValuesList():Array
		{
			var a:Array = new Array();
			for each(var r:ReferenceValue in referenceValues)
			{
				a.push( {id:r.id, label:r.label, value:r } );
			}
			return a;
		}
		
		
		override public function set label(value:String):void 
		{
			super.label = value;
			feedBack.label = label;
		}
	}
}