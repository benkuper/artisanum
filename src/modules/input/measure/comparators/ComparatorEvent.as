package modules.input.measure.comparators 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ComparatorEvent extends Event 
	{
		static public const RESULT_CHANGED:String = "resultChanged";
		
		public function ComparatorEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new ComparatorEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("ComparatorEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}