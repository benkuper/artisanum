package modules.input.measure.comparators 
{
	import modules.events.MeasureEvent;
	import modules.events.ParameterEvent;
	import modules.input.measure.ContinuousMeasure;
	import modules.input.measure.Measure;
	import modules.input.measure.ReferenceValue;
	import modules.parameters.ParameterFloat;
	import ui.components.Slider;
	import ui.components.UIEvent;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class NumberComparator extends Comparator
	{
		
		private var compareFunc:Function;
		
		private var currentValue:Number;
		private var checkVal:ParameterFloat;
		private var refVal:Number;
		
		public function NumberComparator(measure:Measure,fromComparator:Comparator = null) 
		{
			super(measure,fromComparator);
			var cm:ContinuousMeasure = measure as ContinuousMeasure;
			checkVal = addParameter(new ParameterFloat("compareVal","", cm.defaultValue, cm.minimumValue, cm.maximumValue), "Valeur à comparer", true) as ParameterFloat;
			checkVal.addEventListener(ParameterEvent.VALUE_CHANGE, checkValChange);
			
			checkVal.slider.useRef = true;
			currentValue = checkVal.value;
			
			
			checkVal.guiWidth = 100;
			
			if (fromComparator is NumberComparator)
			{
				checkVal.value = (fromComparator as NumberComparator).currentValue;
			}
			
		}
		
		
		//override
		
		override protected function updateGUI():void 
		{
			super.updateGUI();
			checkVal.guiWidth = guiWidth;
		}
		
		override protected function compare():Boolean 
		{
			if (compareFunc == null) return false;
			
			
			var res:Boolean = compareFunc.apply(this, [refVal]);
			//trace("Number comparator, compare", refVal, checkVal.value, res);
			
			return res;
		}
		
		
		override protected function setProp(propName:String, value:*):void 
		{
			super.setProp(propName, value);
			
			switch(propName)
			{
				case "compareFunc":
					switch(value as String)
					{
						case "=":
							compareFunc = isEqualTo;
							break;
							
						case "~":
							compareFunc = isEqualToWithTolerance;
							checkVal.slider.useTolerance = true;
							break;
							
						case ">":
							compareFunc = isGreaterThan;
							break;
							
						case ">=":
							compareFunc = isGreaterThanOrEqualTo;
							break;
							
						case "<":
							compareFunc = isLessThan;
							break;
							
						case "<=":
							compareFunc = isLessThanOrEqualTo;
							break;
					}
					break;
			}
			
			
			compare();
		}
		
		
		//handlers
		override protected function measureValueChanged(e:MeasureEvent):void 
		{
			//to override too
			refVal = (measure as ContinuousMeasure).currentValue;
			super.measureValueChanged(e); //super method includes compare();
			checkVal.slider.refValue = refVal;
		}
		
		
		private function checkValChange(e:ParameterEvent):void 
		{
			currentValue = checkVal.value;
			result = compare();
		};
		
		// base comparators
		public function isEqualTo(value:Number):Boolean
		{
			return (value == currentValue);
		}
		
		public function isEqualToWithTolerance(value:Number):Boolean
		{
			return (value >= currentValue-checkVal.slider.tolerance/2 && value <= currentValue+checkVal.slider.tolerance/2);
		}
		
		public function isGreaterThan(value:Number):Boolean
		{
			return (value > currentValue);
		}
		
		public function isGreaterThanOrEqualTo(value:Number):Boolean
		{
			return (value >= currentValue);
		}
		
		public function isLessThan(value:Number):Boolean
		{
			return (value < currentValue);
		}
		
		public function isLessThanOrEqualTo(value:Number):Boolean
		{
			return (value <= currentValue);
		}
		
		
		//override load / save
		//public function getXML():XML 
		//{
			//var xml:XML = <comparator></comparator>;
			//var paramsXML:XML = <parameters></parameters>;
			//
			////var pIndex:int = 0;
			//for each(var p:Parameter in parameters)
			//{
				//var pXML:XML = p.getXML();
				////trace("Get parameter XML !", pXML.toXMLString());
				////pXML.@parameterIndex = pIndex;
				//paramsXML.appendChild(pXML);
				////pIndex++;
			//}
			//xml.appendChild(paramsXML);
			//
			//return xml;
		//}
		//
		//public function loadXML(data:XML):void
		//{
			//super.loadXML(data)
			//for each(var pXML:XML in data.parameters[0].parameter)
			//{
				//getParameterByID(pXML.@id).loadXML(pXML);
			//}
		//}
		
		//clean
		override public function clean():void 
		{
			checkVal.removeEventListener(ParameterEvent.VALUE_CHANGE, checkValChange);
			checkVal = null;
			super.clean();
		}
		
	}

}