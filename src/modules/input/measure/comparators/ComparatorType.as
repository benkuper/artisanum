package modules.input.measure.comparators 
{
	import modules.input.measure.Measure;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ComparatorType 
	{
		public var id:String;
		public var label:String;
		public var typeClass:Class;
		public var props:Object;
		
		public function ComparatorType(id:String, label:String, typeClass:Class, props:Object) 
		{
			this.id = id;
			this.label = label;
			this.typeClass = typeClass;
			this.props = props;
		}
		
		public function toString():String
		{
			return "[ComparatorType, labe="+label+", typeClass = " + typeClass+"]";
		}
		
		public function createComparator(measure:Measure, fromComparator:Comparator = null):Comparator
		{
			var c:Comparator = new typeClass(measure,fromComparator) as Comparator;
			c.setProps(props);
			return c;
		}
		
	}

}