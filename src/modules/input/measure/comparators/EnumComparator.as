package modules.input.measure.comparators 
{
	import modules.events.MeasureEvent;
	import modules.events.ParameterEvent;
	import modules.input.measure.DiscreteMeasure;
	import modules.input.measure.Measure;
	import modules.input.measure.ReferenceValue;
	import modules.parameters.ParameterEnum;
	import ui.Toaster;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class EnumComparator extends Comparator 
	{
		
		protected var checkValBox:ParameterEnum;
		protected var checkVal:ReferenceValue;
		protected var currentRefValue:ReferenceValue;
		
		
		public function EnumComparator(measure:Measure, fromComparator:Comparator = null) 
		{
			super(measure, fromComparator);
			checkValBox = addParameter(new ParameterEnum("reference","[Reference]", (measure as DiscreteMeasure).getReferenceValuesList()),"Valeur à comparer",true) as ParameterEnum;
			checkValBox.addEventListener(ParameterEvent.VALUE_CHANGE, checkValChanged);
			checkValBox.guiWidth = 30;
			checkValBox.guiHeight = 20;
			
			currentRefValue = (measure as DiscreteMeasure).currentReferenceValue;
			checkValBox.selectedIndex = 0;
		}
		
		override protected function setProp(propName:String, value:*):void 
		{
			switch(propName)
			{
				case "label":
					checkValBox.label = value as String;
					break;
			}
		}
		
		//handlers
		override protected function measureValueChanged(e:MeasureEvent):void 
		{
			//to override too
			currentRefValue = (measure as DiscreteMeasure).currentReferenceValue;
			super.measureValueChanged(e); //super method includes compare();
			
		}
		
		private function checkValChanged(e:ParameterEvent):void 
		{
			checkVal = checkValBox.selectedItem.value as ReferenceValue;
			result = compare();
		}
		
		override protected function compare():Boolean
		{			
			if (currentRefValue == null || checkVal == null) return false;
			
			return currentRefValue.value == checkVal.value;
		}
		
		override public function clean():void 
		{
			checkValBox.removeEventListener(ParameterEvent.VALUE_CHANGE, checkValChanged);
			checkValBox = null;
			
			super.clean();
		}
	}

}