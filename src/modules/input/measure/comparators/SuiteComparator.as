package modules.input.measure.comparators 
{
	import modules.events.MeasureEvent;
	import modules.events.ParameterEvent;
	import modules.input.measure.DiscreteMeasure;
	import modules.input.measure.Measure;
	import modules.input.measure.ReferenceValue;
	import modules.parameters.ParameterString;
	import ui.components.TextBox;
	import ui.Toaster;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class SuiteComparator extends Comparator 
	{
		
		public var suiteBox:ParameterString;
		public var curIndex:int;
		public var suiteValues:Array;
		public var curRefVal:ReferenceValue;
		
		public function SuiteComparator(measure:Measure,fromComparator:Comparator = null) 
		{
			
			super(measure,fromComparator );
			
			suiteBox = addParameter(new ParameterString("suite","Suite", ""), "Suite séparée par des virgules", true) as ParameterString;
			suiteBox.addEventListener(ParameterEvent.VALUE_CHANGE, suiteBoxChanged);
			
			suiteValues = new Array();
			
			
		}
		
		//handlers
		override protected function measureValueChanged(e:MeasureEvent):void 
		{
			//to override too
			curRefVal = (measure as DiscreteMeasure).currentReferenceValue;
			super.measureValueChanged(e); //super method includes compare();
		}
		
		private function suiteBoxChanged(e:ParameterEvent):void 
		{
			suiteValues = suiteBox.value.split(",");
			reset();
			result = compare();
			
		}
		
		
		override protected function compare():Boolean 
		{
			if (curRefVal == null) return false;
			if (suiteValues.length == 0) return false;
			if (curIndex >= suiteValues.length) return false;
			
			var curSuiteVal:String = suiteValues[curIndex];
			
			//trace("compare !",Math.random() );
			
			if (curRefVal.label as String == curSuiteVal)
			{
                // Toaster.info("note n°" + curIndex + " identifiée :" + curSuiteVal);
				curIndex++;
				if (curIndex == suiteValues.length) curIndex = 0;
				return true;
			}
			
			return false;
		}
		
		override public function reset():void 
		{
			super.reset();
			curIndex = 0;
		}
		
		override public function clean():void 
		{
			suiteBox.removeEventListener(ParameterEvent.VALUE_CHANGE, suiteBoxChanged);
			suiteBox = null;
			super.clean();
		}
		
	}

}