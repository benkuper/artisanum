package modules.input.measure.comparators 
{
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import modules.events.MeasureEvent;
	import modules.input.measure.Measure;
	import modules.input.measure.ReferenceValue;
	import modules.parameters.Parameter;
	import ui.Tooltip;
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Comparator extends EventDispatcher
	{
		
		protected var measure:Measure;
		
		public var gui:Sprite;
		protected var _guiWidth:Number;
		protected var _guiHeight:Number;
		
		private var _result:Boolean;
		
		
		protected var parameters:Vector.<Parameter>;
		
		
		public function Comparator(measure:Measure,fromComparator:Comparator = null)  //fromComparator is only used to keep value if possible
		{
			
			this.measure = measure;
			measure.addEventListener(MeasureEvent.VALUE_CHANGE, measureValueChanged);
			parameters = new Vector.<Parameter>();
			gui = new Sprite();
			
			reset();
			
		}
		//
		//protected function isCompatible(fromComparator:Comparator):Boolean 
		//{
			////to override
			//return typeof(fromComparator) == typeof(this);
		//}
		
		protected function compare():Boolean
		{
			//to be overriden
			return false;
		}
		
		public function reset():void
		{
			//to be overriden
		}
		
		public function clean():void
		{
			//to be overriden wuthout forgetting super.clean(); at end of function for proper cleaning per class
			
			measure.removeEventListener(MeasureEvent.VALUE_CHANGE, measureValueChanged);
			measure = null;
			removeAllParameters();
			
		}
		
		protected function measureValueChanged(e:MeasureEvent):void 
		{
			//to override too
			result = compare();
		}
		
		
		public function setProps(props:Object):void
		{
			for (var propName:String in props)
			{
				setProp(propName, props[propName]);
			}
			
			result = compare();
			//trace("set Props, compare result = ", result);
		}
		
		protected function setProp(propName:String, value:*):void 
		{
			//to be overriden
		}
		
		
		protected function updateGUI():void 
		{
			//to be overriden
			//trace("Comparator update GUI :", guiWidth);
		}
		
		
		//Parameters
		
		protected function addParameter(newParam:Parameter, tooltip:String = "", addToChildren:Boolean = false):Parameter
		{
			parameters.push(newParam);
			
			if (addToChildren)
			{
				gui.addChild(newParam.gui);
				gui.setChildIndex(newParam.gui, 0);
			}
			if (tooltip != "") Tooltip.addTarget(newParam.gui, tooltip);
			
			return newParam;
		}
		
		protected function removeParameter(targetParam:Parameter):void
		{
			var index:int = parameters.indexOf(targetParam);
			
			if (index < 0) throw new Error("Can't delete Parameter " + targetParam.label + " because it doesn't belong to this command");
		
			if (gui.contains(targetParam.gui)) gui.removeChild(targetParam.gui);
			
			parameters.splice(index, 1);
		}
		
		protected function removeAllParameters():void
		{
			while (parameters.length > 0)
			{
				removeParameter(parameters[0]);
			}
		}
		
		//Saving and Loading
		
		public function getXML():XML 
		{
			var xml:XML = <comparator></comparator>;
			var paramsXML:XML = <parameters></parameters>;
			
			//var pIndex:int = 0;
			for each(var p:Parameter in parameters)
			{
				var pXML:XML = p.getXML();
				//trace("Get parameter XML !", pXML.toXMLString());
				//pXML.@parameterIndex = pIndex;
				paramsXML.appendChild(pXML);
				//pIndex++;
			}
			xml.appendChild(paramsXML);
			
			return xml;
		}
		
		public function loadXML(data:XML):void
		{
			for each(var pXML:XML in data.parameters[0].parameter)
			{
				getParameterByID(pXML.@id).loadXML(pXML);
			}
		}
		
		private function getParameterByID(id:String):Parameter
		{
			for each(var p:Parameter in parameters)
			{
				if (p.id == id) return p;
			}
			
			return null;
		}
		
		// Getters & setters
		public function get guiWidth():Number 
		{
			return _guiWidth;
		}
		
		public function set guiWidth(value:Number):void 
		{
			_guiWidth = value;
			if(!isNaN(guiWidth)) updateGUI();
		}
		
		public function get guiHeight():Number 
		{
			return _guiHeight;
		}
		
		public function set guiHeight(value:Number):void 
		{
			_guiHeight = value;
			if(!isNaN(guiHeight)) updateGUI();
		}
		
		public function get result():Boolean 
		{
			return _result;
		}
		
		public function set result(value:Boolean):void 
		{
			if (result == value) return;
			
			_result = value;
			
			dispatchEvent(new ComparatorEvent(ComparatorEvent.RESULT_CHANGED));
		}
	}

}