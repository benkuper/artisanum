package modules.input.measure 
{
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import modules.events.MeasureEvent;
	import modules.input.measure.comparators.Comparator;
	import modules.input.measure.comparators.ComparatorType;
	/**
	 * ...
	 * @author Antoine Costes
	 * 
	 * Une Measure est une "mesure du monde exterieur" utilisable par Artisanum, 
	 * c'est-a-dire une grandeur issue d'un capteur ramenee a une seule valeur continue.
	 * Il peut s'agir d'une donnee brute, ou du resultat d'un certain traitement des donnees du capteur (ce traitement est effectue par l'InputModule).
	 * 
	 * Une Measure ne calcule pas sa valeur courante, qui est donnee par l'InputModule. Elle la compare a des valeurs de reference.
	 * Elle expose ses propres comparateurs et ses propres valeurs de reference.
	 * Selon son type, ses valeurs de references sont les bornes d'un intervalle continu ou bien un ensemble de valeurs discretes.
	 * 
	 */
	
	public class Measure extends EventDispatcher// this class is "abstract", it should not be directly implemented
	{
		public var id:String;
		private var _label:String;
		public var gui:Sprite;
		//layout //[Ben] Added guiWidth and guiHeight for width/height override
		protected var _guiWidth:Number;
		protected var _guiHeight:Number;

        protected var dispatchOnChangeOnly:Boolean;

		private var _enabled:Boolean;
		
		private var comparatorTypes:Vector.<ComparatorType>;
		
		
		public function Measure(id:String, label:String, enabled:Boolean = true, dispatchOnChangeOnly:Boolean = true)
		{
			this.id = id;
			this._label = label;
			gui = new Sprite();
			this._enabled = enabled;
			
			dispatchOnChangeOnly = true;
			
			comparatorTypes = new Vector.<ComparatorType>();
		}
		
		public function getXML():XML
		{
			var mXML:XML = <measure></measure>;
			mXML.@id = id;
			mXML.@enabled = enabled;
			return mXML;
		}
		
		public function loadXML(data:XML):void
		{
			this.id = data.@id;
			this._enabled = data.@enabled == "true";
		}
		
		
		//comparators
		
		protected function addComparatorTypes(...arguments):void 
		{
			for each(var a:* in arguments)
			{
				comparatorTypes.push(a as ComparatorType);
			}
		}
		
		public function getComparatorsList():Array
		{
			var a:Array = new Array();
			for each(var ct:ComparatorType in comparatorTypes)
			{
				a.push( { id:ct.id, label:ct.label, value:ct } );
			}
			
			return a;
		}
		
		//[Ben] Added updateGUI so gui can be resized safely width guiWidth & guiHeight properties
		//ui
		protected function updateGUI():void 
		{
			//to be overriden
		}
		
		//getter / setter
		
		public function get guiWidth():Number 
		{
			return _guiWidth;
		}
		
		public function set guiWidth(value:Number):void 
		{
			_guiWidth = value;
			updateGUI();
		}
		
		public function get guiHeight():Number 
		{
			return _guiHeight;
		}
		
		public function set guiHeight(value:Number):void 
		{
			_guiHeight = value;
			updateGUI();
		}
		
		public function get enabled():Boolean 
		{
			return _enabled;
		}
		
		public function set enabled(value:Boolean):void 
		{
			_enabled = value;
		}
		
		public function get label():String 
		{
			return _label;
		}
		
		public function set label(value:String):void 
		{
			if (label == value) return;
			_label = value;
			dispatchEvent(new MeasureEvent(MeasureEvent.LABEL_CHANGED));
		}
		
	}

}