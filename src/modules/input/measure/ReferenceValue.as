package modules.input.measure 
{
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class ReferenceValue 
	{
		public var value:*;
		public var label:String;
		public var id:String;
		
		public function ReferenceValue(label:String, value:* ) 
		{
			this.label = label;
			this.value = value;
			
			//to change
			this.id = label;
		}
		
	}

}