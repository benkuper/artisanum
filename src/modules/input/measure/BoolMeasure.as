package modules.input.measure 
{
	import ui.components.CheckBox;
	import ui.components.StatusBox;
	/**
	 * ...
	 * @author ...
	 */
	public class BoolMeasure extends DiscreteMeasure 
	{
		protected var _value:Boolean;
		protected var feedBack2:CheckBox;
		
		public function BoolMeasure(id:String, label:String, enabled:Boolean = true) 
		{
			super(id, label, enabled, false);

			referenceValues.push(new ReferenceValue("oui", true));
            referenceValues.push(new ReferenceValue("non", false));
			
			gui.removeChild(feedBack);
			feedBack2 = new CheckBox(label);
			feedBack2.readOnly = true;
			gui.addChild(feedBack2);
		}
		
		public function get value():Boolean 
		{
			return _value;
		}
		
		public function set value(value:Boolean):void 
		{
			_value = value;
			currentReferenceValue = referenceValues[value?0:1];
			feedBack2.selected = value;
		}
		
		override public function getXML():XML
		{
			trace("bool get xml");
			var mXML:XML = <measure></measure>;
			mXML.@id = id;
			mXML.@enabled = enabled;
			return mXML;
		}
		
		override public function loadXML(data:XML):void
		{
			this.id = data.@id;
			//this._enabled = data.@enabled == "true";
			
			trace("bool load xml");
			trace(data);
		}
		
		override public function set label(value:String):void 
		{
			super.label = value;
			feedBack.label = value;
			feedBack2.label = value;
		}
	}

}