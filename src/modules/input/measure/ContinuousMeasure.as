package modules.input.measure 
{
	import com.greensock.TweenLite;
	import flash.display.Sprite;
	import modules.events.MeasureEvent;
	import modules.input.measure.comparators.ComparatorType;
	import modules.input.measure.comparators.NumberComparator;

import ui.Style;
import ui.components.Slider;
	import ui.Toaster;
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class ContinuousMeasure extends Measure
	{
		//ui
		private var feedBack:Slider;
		
		private var _feedBackColor:uint // TEMP, en attendant le refactor sur les custom GUI dans les measures et parameters
		
		//data
		protected var _currentValue:Number;
		protected var _minimumValue:Number;
		protected var _maximumValue:Number;
		protected var _smoothedValue:Number;
		public var defaultValue:Number;
		
		public var smoothing:Number; // TO DO : A REVOIR !!
		
		public function ContinuousMeasure(id:String, label:String, minValue:Number = 0, maxValue:Number = 1, initialValue:Number = 0) 
		{
			super(id, label);
			_currentValue = initialValue;
			minimumValue = minValue;
			maximumValue = maxValue;
			_smoothedValue = initialValue;
			defaultValue = initialValue;
			
			this.smoothing = 0; //smooth value if needed
			
			feedBack = new Slider(label, currentValue, minimumValue, maximumValue);
			feedBack.readOnly = true;
			feedBack.width = 200;
			gui.addChild(feedBack);
			
			_guiWidth = feedBack.width;
			_guiHeight = 10;
			
			addComparatorTypes(
				new ComparatorType("=", "=", NumberComparator, { compareFunc:"=" } ),
				new ComparatorType("~","~", NumberComparator, { compareFunc:"~" } ),
				
				new ComparatorType(">",">", NumberComparator, { compareFunc:">" } ),
				new ComparatorType(">=",">=", NumberComparator, { compareFunc:">=" } ),
				new ComparatorType("<","<", NumberComparator, { compareFunc:"<" } ),
				new ComparatorType("<=","<=", NumberComparator, { compareFunc:"<=" } )
				);
		}
		
		//ui
		override protected function updateGUI():void
		{
			//trace("updateGui :", guiWidth, guiHeight);
			feedBack.width = guiWidth;
			feedBack.height = guiHeight;
		}
		
		// getters & setters
		public function get currentValue():Number 
		{
			return smoothing?_smoothedValue:_currentValue;
		}
		
		public function set currentValue(value:Number):void 
		{
			if (_currentValue == value && dispatchOnChangeOnly) return;

			//if (value > maximumValue) value = maximumValue;
			//else if (value < minimumValue) value = minimumValue;

			//plus propre :
			value = Math.min(Math.max(value, minimumValue), maximumValue);
			
			_currentValue = value;
			
			if (feedBack != null)
			{
				if (smoothing == 0) 
				{
					feedBack.value = currentValue;
					dispatchValueChange();
				}else
				{
					TweenLite.to(this, smoothing, { smoothedValue:value} );
				}
			}
		}

		private function dispatchValueChange():void
		{
			if (!enabled) return;
			dispatchEvent(new MeasureEvent(MeasureEvent.VALUE_CHANGE));
		}
		
		public function get minimumValue():Number 
		{
			return _minimumValue;
		}
		
		public function set minimumValue(value:Number):void 
		{
			_minimumValue = value;
		}
		
		public function get maximumValue():Number 
		{
			return _maximumValue;
		}
		
		public function set maximumValue(value:Number):void 
		{
			_maximumValue = value;
		}
		
		public function get feedBackColor():uint 
		{
			return _feedBackColor;
		}
		
		public function set feedBackColor(value:uint):void 
		{
			if (feedBackColor == value) return;
			_feedBackColor = value;
			feedBack.color = value;
		}
		
		public function get smoothedValue():Number 
		{
			return _smoothedValue;
		}
		
		public function set smoothedValue(value:Number):void 
		{
			_smoothedValue = value;
			dispatchValueChange();
			feedBack.value = value;
		}
		
		override public function set label(value:String):void 
		{
			super.label = value;
			feedBack.label = label;
		}
		
	}

}