package modules.input.ciboulette.measures
{
	import modules.input.measure.comparators.ComparatorType;
	import modules.input.measure.comparators.SuiteComparator;
	import modules.input.measure.DiscreteMeasure;
	import modules.input.measure.ReferenceValue;
	
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class IntervalMeasure extends DiscreteMeasure
	{
		private const INTERVALS:Array = new Array("-",  "0", "1/2", "1", "1+1/2", "2", "2+1/2", "3", "3+1/2", "4", "4+1/2", "5", "5+1/2", "6");

		public function IntervalMeasure(id:String, label:String, enabled:Boolean = true)
		{
			super(id, label, enabled, false);
			
			for (var i:int = 0 ; i < 14 ; i++)
				referenceValues.push(new ReferenceValue(INTERVALS[i], (i-1)*.5));
		}

		public function shut():void
		{
			currentReferenceValue = referenceValues[0];
		}

		public function setValueWithIndex(index:int):void
		{
            if (index < -1 && index > 11) return;
            currentReferenceValue = referenceValues[index + 1];
		}
		
		public function setValueForNbTons(nbTons:Number):void
		{
            nbTons = Math.abs(nbTons)%6;

			switch (nbTons)
            {
                case 0:
                case .5:
                case 1:
                case 1.5:
                case 2:
                case 2.5:
                case 3:
                case 3.5:
                case 4:
                case 4.5:
                case 5:
                case 5.5:
                case 6:
                    currentReferenceValue = referenceValues[1 + 2*nbTons];
                    //trace("set interval", 1 + 2*nbTons);
                    break;

                default:
                    trace("wrong interval", nbTons);
                    currentReferenceValue = referenceValues[0];
                    break;
            }
		}
	}
}