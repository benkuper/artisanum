package modules.input.ciboulette 
{
	import benkuper.util.osc.OSCMaster;
	import com.greensock.TweenLite;
    import flash.events.Event;
    import flash.utils.getTimer;
	import modules.input.InputModule;
    import modules.input.ciboulette.measures.IntervalMeasure;
    import modules.input.measure.ContinuousMeasure;
    import modules.input.measure.Measure;
    import modules.input.micro.measures.NoteMeasure;
	import modules.input.micro.measures.OctaveMeasure;
	import modules.parameters.ParameterFloat;
	import org.tuio.osc.IOSCListener;
	import org.tuio.osc.OSCMessage;
	import ui.components.TextBox;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class CibouletteInputModule extends InputModule implements IOSCListener
	{
		private const notes:Array = new Array("-",  "Do", "Do#", "Ré", "Mib", "Mi", "Fa", "Fa#", "Sol", "Sol#", "La", "Sib", "Si");
		
		private var thresholdParam:ParameterFloat;
		
		// measures
		protected var volume:ContinuousMeasure;
		protected var cleanVolume:ContinuousMeasure;
		protected var pitch:ContinuousMeasure;
		protected var note:NoteMeasure;
		protected var noteOctave:OctaveMeasure;
        protected var noteDuration:ContinuousMeasure;
        protected var noteTimer:Number;
        protected var interval:IntervalMeasure;

		// ui
		protected var prevNote:TextBox;
		
		// additional data
        protected var lastPitch:Number;
        protected var lastNote:String;
        private var noteDetected:Boolean
		private var modulationHistory:Vector.<Number>;
		private var modulationNumSamples:int;
		private var modulation:ContinuousMeasure;
		
		public function CibouletteInputModule() 
		{
			super();
			OSCMaster.register("pitch", this);
			
			thresholdParam = addParameter(new ParameterFloat("threshold", "seuil", 50, 0, 90)) as ParameterFloat;
			parameterContainer.height = 80;
			
			volume = addMeasure(new ContinuousMeasure("volume", "Volume", 0, 100), "le volume mesuré par le micro") as ContinuousMeasure;
			cleanVolume = addMeasure(new ContinuousMeasure("intensité", "Intensity", 0, 100), "") as ContinuousMeasure;
			
			volume.smoothing = .15;
			pitch = addMeasure(new ContinuousMeasure("pitch","Hauteur de note", 0, 120), "la note chantée (notation MIDI)") as ContinuousMeasure;
            note = addMeasure(new NoteMeasure("note","Note")) as NoteMeasure;
			
			prevNote = new TextBox("Prev Note", "-");
            lastNote = "-";
			measureContainer.addChild(prevNote);
			
			noteOctave = addMeasure(new OctaveMeasure("octave", "Octave")) as OctaveMeasure;
            noteDuration = addMeasure(new ContinuousMeasure("noteDuration", "Tenue", 0, 20), "le temps durant lequel la note est chantée") as ContinuousMeasure;
            noteTimer = getTimer();
            addEventListener(Event.ENTER_FRAME, enterFrame)

            interval = addMeasure(new IntervalMeasure("interval","Intervalle")) as IntervalMeasure;

			//Modulation
			modulation = addMeasure(new ContinuousMeasure("modulation","Modulation", -1, 1, 0)) as ContinuousMeasure;
			modulationHistory = new Vector.<Number>();
			modulationNumSamples = 4;
			
			measureContainer.autoLayout = true;
			
			draw();
		}

        private function enterFrame(event:Event):void
        {
            noteDuration.currentValue = (getTimer() - noteTimer)/1000;
        }
		
		/* INTERFACE org.tuio.osc.IOSCListener */
		
		public function acceptOSCMessage(msg:OSCMessage):void 
		{
			
			var type:String = msg.address.split("/")[2];
			switch(type)
			{
				case "env":
					volume.currentValue = msg.arguments[0] as Number;
					
					if (volume.currentValue < thresholdParam.value) cleanVolume.currentValue = 0;
					else cleanVolume.currentValue = 100 * (volume.currentValue - thresholdParam.value) / (90 - thresholdParam.value);
					break;
					
				case "pitch":
					if (volume.currentValue < thresholdParam.value) return;
					var pitchValue:Number = msg.arguments[0] as Number;

                    if (pitchValue == 0 )
                    {
                        noteTimer = getTimer();
                        noteDetected = false;
                        TweenLite.killDelayedCallsTo(gotNewNote);
						TweenLite.delayedCall(0.3, gotNewNote, [pitchValue, nbHalfTons]);
                    } else
                    {
                        var nbHalfTons:int = Math.round(pitchValue) - Math.round(pitch.currentValue);
						
                        if (nbHalfTons != 0 || !noteDetected) // new note
                        {
                            noteDetected = true;
                            TweenLite.delayedCall(0.1, gotNewNote, [pitchValue, nbHalfTons]);
                        } else // maintained note
                        {
                            //trace("maintained")
                            TweenLite.killDelayedCallsTo(shutFeedbacks);
                            TweenLite.delayedCall(0.05, shutFeedbacks);
                        }
                    }
					break;
			}
		}

        private function gotNewNote(pitchValue:Number, nbHalfTons:int):void
        {
            //trace("new note", pitchValue);

            noteTimer = getTimer();

            TweenLite.killDelayedCallsTo(gotNewNote);
            TweenLite.killDelayedCallsTo(shutFeedbacks);

            lastPitch = pitch.currentValue;
            pitch.currentValue = pitchValue;

            var notePitch:int = Math.round(pitchValue) - 60;

            note.setValueWithPitch(notePitch);
            prevNote.text = lastNote;
            noteOctave.setValueForPitch(notePitch);

            if (lastPitch != 0 && pitchValue != 0)
            {
                modulation.currentValue = (pitchValue - lastPitch ) / 20;

                interval.setValueForNbTons(nbHalfTons * .5);

                //trace("new interval !", Math.round(pitchValue), Math.round(lastPitch), nbHalfTons);
            }
            TweenLite.delayedCall(0.05, shutFeedbacks);
        }
		private function shutFeedbacks():void
		{
			//note.shut();

            if (note.currentReferenceValue != null && note.currentIndex != 0)
                lastNote = note.currentReferenceValue.label;

            modulation.currentValue = 0;
            interval.setValueWithIndex(-1);
            note.setValueWithIndex(-1);
            noteOctave.setValueForFrequency(-1);
		}
		
		override public function clean():void
		{
			super.clean();
			OSCMaster.unregister("pitch", this);
            removeEventListener(Event.ENTER_FRAME, enterFrame);
		}
		
		
		override protected function draw():void
		{
			super.draw();
			thresholdParam.guiWidth = measureContainer.width - 20;

            for each (var loopMeasure:Measure in measures) loopMeasure.gui.x = (measureContainer.width - loopMeasure.gui.width ) *.5;

            note.gui.x = 25;
			prevNote.x = note.gui.x + 80;
			prevNote.y = note.gui.y + 50;
		}

    }

}