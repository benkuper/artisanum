package modules.input.serial
{
import flash.utils.ByteArray;

import modules.input.measure.BoolMeasure;

import ui.Style;
import ui.Toaster;
import ui.components.Button;
import ui.components.Slider;
import ui.components.TextBox;
import ui.components.UIEvent;

/**
 * ...
 * @author To
 */
public class MakeyKitInputModule extends SerialInputModule
{
    protected var pads:Vector.<BoolMeasure>;
    protected var batteryValues:Vector.<int>;
    protected var batteryFeedback:TextBox;
    //protected var refreshBT:Button;
    protected var resetBT:Button;

    public function MakeyKitInputModule(bufferHandling:String = BYTE255, register:Boolean = true)
    {
        super(bufferHandling, register);
        pads = new Vector.<BoolMeasure>();
        batteryValues = new Vector.<int>();
        batteryFeedback = new TextBox("batterie");
        addControl(batteryFeedback);

        pingCheckDelay = 20; // Hack Ciboulette

        /*
         refreshBT = new Button("Rafraichir");
         addControl(refreshBT);
         refreshBT.addEventListener(UIEvent.BUTTON_SELECTED, BTHandler);


         resetBT = new Button("Reset mesures");
         addControl(resetBT);
         resetBT.addEventListener(UIEvent.BUTTON_SELECTED, BTHandler);
         */
    }

    override protected function BTHandler(e:UIEvent):void
    {
        super.BTHandler(e);
        var targetBT:Button = e.target as Button;

        switch (targetBT)
        {
            /*
             case resetBT:
             removeAllMeasures();
             break;
             /*
             case refreshBT:
             if (currentPort != null && currentPort.isOpened) currentPort.write("r");
             else Toaster.warning("Il faut se connecter d'abord !");
             break;
             */
        }
    }

    override protected function handleBuffer(buffer:ByteArray):void
    {
        var message:Array = new Array;
        while (buffer.bytesAvailable) message.push(buffer.readByte());

        //trace("acceptMessage :", message);

        if (message.length != 2)
        {
            trace("MESSAGE ERROR", message);
            return;
        }

        var command:int = message.shift() as int;
        var value:int = message.shift() as int;

        switch (command)
        {
            case 98: // 'b'
                var level:String = String(value);
                batteryFeedback.text = (level != null)?level:"-";
                /*
                 if (value < 100)
                 {

                 value = (value - 60)/ (70 - 60) * 100;
                 batteryFeedback.value = value;
                 batteryFeedback.color = Style.BLUE;
                 }
                 else
                 {
                 value = (value - 170)/ (190 - 170) * 100;
                 batteryFeedback.value = value;
                 batteryFeedback.color = Style.GREEN;
                 }
                 */
                break;

            case 116:  // 't'
            case 114: //'r'
                var targetPad:BoolMeasure = getPadMeasure(value);
                if (targetPad == null)
                {
                    addPad(value);
                    targetPad = getPadMeasure(value);
                }
                targetPad.value = command == 116 ? true : false;
                break;
        }
    }

    //protected function getBatteryAverageValue(newValue:int):int
    //{
    //batteryValues.push(newValue);
    //var average:Number = 0;
    //
    //for each (var loopVal:int in batteryValues) average += loopVal;
    //average /= batteryValues.length;
    //
    //if (batteryValues.length > 10) batteryValues.shift();
    //
    //return average;
    //
    //}

    protected function addPad(index:int):void
    {
        //trace("add pad", index);
        var newPad:BoolMeasure = new BoolMeasure("pad" + index, "Toucher " + (pads.length + 1));
        pads.push(newPad);
        addMeasure(newPad);
        draw();
    }

    protected function getPadMeasure(index:int):BoolMeasure
    {
        for each (var loopPad:BoolMeasure in pads)
            if (loopPad.id == "pad" + index)
                return loopPad;

        return null;
    }

    // override
    override protected function closeCurrentPort(warning:String = null):void
    {
        super.closeCurrentPort(warning);

        if (batteryFeedback != null)
        {
            batteryFeedback.text = "";
            //batteryFeedback.value = 0;
            //batteryFeedback.color = Style.UI_NORMAL;
        }
    }

    override protected function removeAllMeasures():void
    {
        super.removeAllMeasures();
        pads = new Vector.<BoolMeasure>();
    }

    override protected function draw():void
    {
        super.draw()

        //batteryFeedback.width = controlsContainer.width - 20;


        var curY:Number = 0;
        for (var i:int = 0; i < pads.length; i++)
        {
            var pad:BoolMeasure  = pads[i];
            pad.gui.x = (i % 2 == 0)?20:120;
            pad.gui.y = curY;
            curY += (i % 2 == 0)?0:pad.gui.height + 5;
        }
        //measureContainer.bgHeight = curY + 30;
        //trace(measureContainer.title);
    }

    protected function getIndexFromId(id:String):int
    {
        return int(id.slice(3)); // id = "pad" + index
    }

    override  public function loadXML(data:XML):void
    {
        trace("makey load measures");
        super.loadXML(data);
        removeAllMeasures();
        for each(var mXML:XML in data.measures[0].measure)
        {
            addPad(getIndexFromId(mXML.@id));
        }
    }
}
}