/**
 * Created by Antoine on 09/02/2015.
 */
package modules.input.serial.arduinokit
{

import modules.events.ParameterEvent;
import modules.input.measure.ContinuousMeasure;
import modules.input.serial.*;
import modules.input.serial.arduinokit.kit.ArduinoKitUI;
import modules.input.serial.arduinokit.kit.PlugEvent;
import modules.parameters.Parameter;
import modules.parameters.ParameterEnum;
import modules.parameters.ParameterInt;
import org.tuio.osc.OSCMessage;
import ui.Toaster;
import ui.components.Button;
import ui.components.UIEvent;

public class ArduinoKitInputModule extends SLIPInputModule
{
    // controls
    protected var resetCapas:Button;

    // parameters
    protected var kitTypeParameter:ParameterEnum;
    protected var currentKitType:String;

    protected var kitParameters:Vector.<ParameterInt>;
    protected var touchParameter:ParameterInt;
    protected var releaseParameter:ParameterInt;
    protected var timeOutParameter:ParameterInt;
    protected var _currentIndex:int;

    private var kit:ArduinoKitUI;
    protected var capas:Vector.<ContinuousMeasure>;

    protected var _isWaitingForData:Boolean;

    public function ArduinoKitInputModule(bufferHandling:String = SLIP, register:Boolean = true)
    {
        super(bufferHandling, register);

        // variables
        kitParameters = new Vector.<ParameterInt>();

        // controls
        resetCapas = new Button("Réinitialiser");
        addControl(resetCapas);
        resetCapas.addEventListener(UIEvent.BUTTON_SELECTED, buttonHandler);

        // parameters
        kitTypeParameter = addParameter(ParameterEnum.create("kitType", "Type de Kit", {id: ArduinoKitAPI.KIT_ID, label: "Kit Standard", value: ArduinoKitAPI.KIT_ID}, {id: ArduinoKitAPI.BT_KIT_ID, label: "Kit Sans Fil", value: ArduinoKitAPI.BT_KIT_ID}, {id: ArduinoKitAPI.RD_KIT_ID, label: "Kit R&D", value: ArduinoKitAPI.RD_KIT_ID}), true, "Choisir le type de kit") as ParameterEnum;
        kitTypeParameter.addEventListener(ParameterEvent.VALUE_CHANGE, parameterChanged);
        kitTypeParameter.selectedIndex = 0;

        measureContainer.autoLayout = false;
    }

    // handlers
    private function plugSelectHandler(event:PlugEvent):void
    {
        //trace(event);
        selectPlugForCalibration(event.plugIndex, event.type == PlugEvent.SELECTED);
    }

    private function buttonHandler(e:UIEvent):void
    {
        var targetButton:Button = e.target as Button;

        switch (targetButton)
        {
            case resetCapas:
                resetArduinoParameters();
                break;
        }
    }

    protected function parameterChanged(e:ParameterEvent):void
    {
        var targetParam:Parameter = e.target as Parameter;

        switch (targetParam)
        {
            case kitTypeParameter:
                initKit(kitTypeParameter.selectedItem.value);
                break;

            case touchParameter:
                if (isWaitingForData || currentIndex < 0)
                    return;
                setArduinoParameters(currentIndex, 1, touchParameter.value);
                break;

            case releaseParameter:
                if (isWaitingForData || currentIndex < 0)
                    return;
                setArduinoParameters(currentIndex, 0, releaseParameter.value);
                break;

            case timeOutParameter:
                if (isWaitingForData)
                    return;
                //sendCommand(ArduinoKitAPI.TIMEOUT_VALUE, timeOutParameter.value);
                break;
        }
    }

    // communication
    override public function acceptOSCMessage(msg:OSCMessage):void
    {
        var type:String;
        var value:Number;
        var index:int;
        trace("msg from arduino", msg.address, msg.argumentsToString());
        var kitID:String = msg.address.substring(1);

        if (kitID != kitTypeParameter.selectedItem.value)
        {
            trace("[ArduinoKit] OSCMessage received from unknown address ! address :", msg.address);

            if (kitID == ArduinoKitAPI.BT_KIT_ID || kitID == ArduinoKitAPI.KIT_ID || kitID == ArduinoKitAPI.RD_KIT_ID)
            {
                closeCurrentPort("Mauvais type de kit, veuillez vous reconnecter");
                kitTypeParameter.setSelectedItemByValue(kitID);
            }
        }
        else
        {
            var command:String = msg.arguments.shift() as String;
            //if (command != ArduinoKitAPI.PING && command != ArduinoKitAPI.HANDSHAKE) trace(kitTypeParameter.selectedItem.value, "receive command :", command);

            switch (command)
            {
                case ArduinoKitAPI.HANDSHAKE:
                    sendCommand(ArduinoKitAPI.HANDSHAKE);
                    break;

                case ArduinoKitAPI.PING: // inutile ?
                    if (!connected)
                    {
                        trace("---- PING HANDSHAKE ----")
                        sendCommand(ArduinoKitAPI.HANDSHAKE);
                    }
                    break;

                case ArduinoKitAPI.PARAM_VALUES:
                    index = msg.arguments.shift() as int;
                    var touchVal:Number = msg.arguments.shift() as Number;
                    var relVal:Number = msg.arguments.shift() as Number;
                    //trace("got param values", index, touchVal, relVal);

                    if (index != currentIndex)
                    {
                        //selectPlugForCalibration(index, true, true);
                        trace("ERROR ON CURRENT INDEX", isWaitingForData);
                        Toaster.error("[ArduinoKit] Problème de communication !");
                        break;
                    }
                    if (!isWaitingForData)
                    {
                        trace("ERROR DATA RECEIVED")
                        _isWaitingForData = false;
                    }

                    touchParameter.value = touchVal;
                    releaseParameter.value = relVal;

                    isWaitingForData = false;
                    Toaster.success("[ArduinoKit Données reçues !");
                    break;

                case ArduinoKitAPI.UPDATE_VALUES:
                    type = msg.arguments.shift() as String;
                    while (msg.arguments.length > 1)
                    {
                        index = msg.arguments.shift() as int;
                        value = msg.arguments.shift() as Number;
                    }
                    if (index >= 0 && !isNaN(value))
                    {
                        switch (type)
                        {
                            case ArduinoKitAPI.CAPA:

                                if (index >= capas.length)
                                    return;
                                kit.setPlugActive(ArduinoKitAPI.CAPA, index, true);
                                capas[index].enabled = true;

                                if (kitTypeParameter.selectedItem.value == ArduinoKitAPI.RD_KIT_ID)
                                {
                                    kit.setPlugValue(ArduinoKitAPI.CAPA, index, value / 1023);
                                    capas[index].currentValue = value / 1023;
                                    //trace(value, capas[index].currentValue);
                                }
                                else
                                {
                                    kit.setPlugValue(ArduinoKitAPI.CAPA, index, value?1:0);
                                    capas[index].currentValue = value ? 1 : 0;
                                }
                                break;
                        }
                        break;
                    }
                    break;

                //case ArduinoKitAPI.TIMEOUT_VALUE:
                //break;

                default:
                    trace("command unknown :" + command);
                    break;
            }
        }
    }


    // kit
    private function initKit(kitId:String = ""):void
    {
        currentKitType = kitId;
        cleanKit(); // clean

        // init parameters according to kit type
        var nbCapas:int = 0;
        switch (kitId)
        {
            case ArduinoKitAPI.RD_KIT_ID: // R&D kit
                timeOutParameter = addParameter(new ParameterInt("timeOutParam", "calibration", 10, 1, 70)) as ParameterInt;
                kitParameters.push(timeOutParameter);
                timeOutParameter.addEventListener(ParameterEvent.VALUE_CHANGE, parameterChanged);
                nbCapas = 6;
                break;

            case ArduinoKitAPI.KIT_ID: // arduinoKit
            case ArduinoKitAPI.BT_KIT_ID: // BT kit
                if (kitId == ArduinoKitAPI.KIT_ID)
                {
                    nbCapas = ArduinoKitAPI.KIT_NB_CAPAS;
                }
                else if (kitId == ArduinoKitAPI.BT_KIT_ID)
                {
                    nbCapas = ArduinoKitAPI.BT_KIT_NB_CAPAS;
                } else if (kitId == ArduinoKitAPI.RD_KIT_ID)
                {
                    nbCapas = ArduinoKitAPI.RD_KIT_NB_CAPAS;
                } else Toaster.error("[ArduinoKitInputModule] Nombre de capas inconnu !")

                touchParameter = addParameter(new ParameterInt("touchParam", "sensibilité toucher", 10, 1, 100)) as ParameterInt;
                kitParameters.push(touchParameter);
                touchParameter.addEventListener(ParameterEvent.VALUE_CHANGE, parameterChanged);

                releaseParameter = addParameter(new ParameterInt("releaseParam", "sensibilité relâche", 10, 1, 100)) as ParameterInt;
                kitParameters.push(releaseParameter);
                releaseParameter.addEventListener(ParameterEvent.VALUE_CHANGE, parameterChanged);
                break;

            default:
                Toaster.error("Erreur : l'identifiant du kit n'est pas reconnu !");
                break;
        }

        // init kit
        kit = new ArduinoKitUI(0, 0, nbCapas);
        kit.x = 25;
        kit.y = measureContainer.headerHeight;
        measureContainer.addChild(kit);
        kit.addEventListener(PlugEvent.SELECTED, plugSelectHandler);
        kit.addEventListener(PlugEvent.DESELECTED, plugSelectHandler);

        // init measures
        capas = new Vector.<ContinuousMeasure>();
        for (var i:int = 0; i < nbCapas; i++)
        {
            var capaName:String = "Capacitif " + (capas.length + 1);
            var newCapa:ContinuousMeasure = new ContinuousMeasure(capaName, capaName);
            capas.push(newCapa);
            addMeasure(newCapa, "", false);
        }

        selectPlugForCalibration(0, false);

        draw();
    }

    // calibration
    private function selectPlugForCalibration(index:int, value:Boolean):void
    {
        if (index >= 0 && value)
        {
            currentIndex = index;
            touchParameter.enabled = true;
            releaseParameter.enabled = true;
            getArduinoParameters(index);
        } else
        {
            currentIndex = -1;
            touchParameter.value = 1;
            releaseParameter.value = 1;
            touchParameter.enabled = false;
            releaseParameter.enabled = false;
        }
    }

    protected function getArduinoParameters(index:int):void
    {
        isWaitingForData = true;
        Toaster.info("Récupération de données en cours...");
        sendCommand(ArduinoKitAPI.GET_PARAMS, index);
    }

    // todo : faire un truc plus lisible pour le type (touch ou release, là c'est 1 ou 0)
    protected function setArduinoParameters(index:int, type:int, value:int):void
    {
        isWaitingForData = true;
        Toaster.info("Calibration en cours...");
        sendCommand(ArduinoKitAPI.SET_PARAM, index, type, value);
    }

    protected function resetArduinoParameters():void
    {
        sendCommand(ArduinoKitAPI.RESET_PARAMS);
        sendCommand(ArduinoKitAPI.GET_PARAMS, 0);
    }

    //ui
    override protected function draw():void
    {
        super.draw();

        // controls
        resetCapas.x = 10;
        resetCapas.y = -10;
        resetCapas.width = 80;
        controlsContainer.height = 100;

        var paramHeight:int = -20; // parameterContainer.headerHeight;
        var paramHeightStep:int = 25;

        kitTypeParameter.gui.x = (parameterContainer.width - parameterContainer.x - kitTypeParameter.guiWidth) / 2;
        ;
        kitTypeParameter.gui.y = paramHeight;
        paramHeight += paramHeightStep;

        if (parameters.indexOf(touchParameter) >= 0)
        {
            touchParameter.gui.x = 10;
            touchParameter.gui.y = paramHeight;
            touchParameter.guiWidth = moduleWidth - 30;
            paramHeight += paramHeightStep;
        }
        if (parameters.indexOf(releaseParameter) >= 0)
        {
            releaseParameter.gui.x = 10;
            releaseParameter.gui.y = paramHeight;
            releaseParameter.guiWidth = moduleWidth - 30;
            paramHeight += paramHeightStep;
        }
        if (parameters.indexOf(timeOutParameter) >= 0)
        {
            timeOutParameter.gui.x = 10;
            timeOutParameter.gui.y = paramHeight;
            timeOutParameter.guiWidth = moduleWidth - 30;
            paramHeight += paramHeightStep;
        }
        parameterContainer.height = paramHeight + 60;

        contentContainer.y = parameterContainer.y + parameterContainer.height + 5; // todo : à virer quand les prob de draw seront réglés
        measureContainer.height = kit.height + 80;

        // todo : il faudrait un updateDraw
        // ce hack règle bien la dimension du module mais le bouton ajouter n'est pas bien placé
        //moduleHeight = 465;// headerHeight + 5 + controlsContainer.height + 5 + parameterContainer.height + 5 + measureContainer.height + 10;
        //bgHeight = moduleHeight;
        //drawBG();
    }


    // getters & setters
    override public function set connected(value:Boolean):void
    {
        super.connected = value;
        if (kit != null) kit.setAllPlugsActive(value);
    }

    public function get currentIndex():int
    {
        return _currentIndex;
    }

    public function set currentIndex(value:int):void
    {
        _currentIndex = value;
        //currentCapaConfig.value = _currentIndex + 1;
    }

    public function get isWaitingForData():Boolean
    {
        return _isWaitingForData;
    }

    public function set isWaitingForData(value:Boolean):void
    {
        _isWaitingForData = value;
        touchParameter.enabled = !value;
        releaseParameter.enabled = !value;
    }

    // clean
    override public function clean():void
    {
        super.clean();
        //TweenMax.killDelayedCallsTo(noPing); in connected = false
        kitTypeParameter.removeEventListener(ParameterEvent.VALUE_CHANGE, parameterChanged);
        cleanKit();
    }

    protected function cleanKit():void
    {

        if (kit != null)
        {
            kit.removeEventListener(PlugEvent.SELECTED, plugSelectHandler);
            kit.removeEventListener(PlugEvent.DESELECTED, plugSelectHandler);
            if (measureContainer.contains(kit))
                measureContainer.removeChild(kit);
        }

        removeAllMeasures();
        for each (var param:ParameterInt in kitParameters)
        {
            param.removeEventListener(ParameterEvent.VALUE_CHANGE, parameterChanged);
            removeParameter(param);
        }
        kitParameters = new Vector.<ParameterInt>();
    }
}
}
