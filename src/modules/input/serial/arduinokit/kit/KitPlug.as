package modules.input.serial.arduinokit.kit
{
	import com.greensock.TweenMax;
	import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

import modules.input.serial.arduinokit.ArduinoKitAPI;

import ui.fonts.Fonts;
	import ui.Style;

import utils.display.addChild;

/**
	 * ...
	 * @author Ben Kuper
	 */
	public class KitPlug extends Sprite
	{
		
		private var _active:Boolean;
		private var valueTF:TextField;
		
		private var _value:Number;
		
		private var radius:Number = 15;
		
		private var type:String;
		private var index:int;
		
		private var calibMenu:NativeMenu;
		private var calibContextItem:NativeMenuItem;
		private var _calibrating:Boolean;

        private var _selected:Boolean;
		
		public function KitPlug(type:String, index:int)
		{
			super();
			this.index = index;
			this.type = type;
			
			valueTF = Fonts.createTF("val", Fonts.smallGreyTF);
			valueTF.selectable = false;
			addChild(valueTF);
			
			value = 0;
			
			active = false;
            selected = false;
			
			if (type == ArduinoKitAPI.CAPA)
			{
				calibMenu = new NativeMenu();
				calibContextItem = new NativeMenuItem("Calibrer");
				calibMenu.addItem(calibContextItem);
				calibContextItem.addEventListener(Event.SELECT, calibMenuSelected);
			}
		}
		
		private function calibMenuSelected(e:Event):void
		{
			calibrating = !calibrating;
		}
		
		private function draw():void
		{
			graphics.clear();
            graphics.beginFill(active ? selected? calibrating ? Style.YELLOW : Style.RED : Style.ORANGE : Style.UI_NORMAL);
			//active ? graphics.beginFill(selected ? calibrating ?  Style.YELLOW : Style.BLUE : Style.ORANGE ):graphics.beginFill(Style.UI_NORMAL);
			graphics.drawCircle(0, 0, radius);
			graphics.endFill();
			
			//drawArc(1, Style.UI_NORMAL);
			drawArc(value?1:0, Style.GREEN);
			
			valueTF.y = -valueTF.textHeight / 2 - 3;
			valueTF.x = -valueTF.textWidth / 2 - 1;
			
			addEventListener(MouseEvent.CLICK, switchActiveMouseHandler);
		
		}
		
		private function switchActiveMouseHandler(e:MouseEvent):void
		{
			//active = !active;
			TweenMax.to(this, .15, {scaleX: active ? .8 : .65, scaleY: active ? .8 : .65, yoyo: true, repeat: 1});
			dispatchEvent(new PlugEvent(active ? PlugEvent.MANUAL_DEACTIVATION : PlugEvent.MANUAL_ACTIVATION, type, index));
            selected = !selected;
		}
		
		private function drawArc(val:Number, color:uint):void
		{
			var start:Number = 100;
			var range:Number = 340;
			var end:Number = start + val * range;
			var lineWidth:Number = 3;
			var innerRadius:Number = radius;
			graphics.lineStyle(lineWidth, color); // , 1, false, "normal", CapsStyle.SQUARE);
			graphics.moveTo(Math.cos(start * Math.PI / 180) * innerRadius, Math.sin(start * Math.PI / 180) * innerRadius);
			for (var i:int = start; i < end; i++)
			{
				graphics.lineTo(Math.cos(i * Math.PI / 180) * innerRadius, Math.sin(i * Math.PI / 180) * innerRadius);
			}
		}
		
		public function get active():Boolean
		{
			return _active;
		}
		
		public function set active(activity:Boolean):void
		{
			//if (active == value) return;
			//if (active != value)
			dispatchEvent(new PlugEvent(active ? PlugEvent.ACTIVATED : PlugEvent.DEACTIVATED, type, index));
			
			_active = activity;
			TweenMax.to(valueTF, .3, {alpha: active ? 1 : 0});
			if (!activity) TweenMax.to(this, .3, {value: 0});
			TweenMax.to(this, .3, {scaleX: active ? 1 : .8, scaleY: active ? 1 : .8});
			draw();
			
			if (active)
			{
				contextMenu = calibMenu;
			}
			else
			{
				contextMenu = null;
			}
		
		}
		
		public function get value():Number
		{
			return _value;
		}
		
		public function set value(value:Number):void
		{
			_value = value;
			valueTF.text = String(int(value*100)/100);
			draw();
		}
		
		public function get calibrating():Boolean
		{
			return _calibrating;
		}
		
		public function set calibrating(val:Boolean):void
		{
			if (calibrating == val)
				return;
			_calibrating = val;
			dispatchEvent(new PlugEvent(calibrating ? PlugEvent.CALIBRATION_START : PlugEvent.CALIBRATION_END, type, index));
			calibContextItem.label = calibrating ? "Terminer la calibration" : "Calibrer";
			//calibContextItem.checked = calibrating;
		}


        public function get selected():Boolean
        {
            return _selected;
        }

        public function set selected(val:Boolean):void
        {
            if (selected == val) return;
            _selected = val;
            dispatchEvent(new PlugEvent(val?PlugEvent.SELECTED:PlugEvent.DESELECTED, type, index));
            draw();
        }
	
	}

}