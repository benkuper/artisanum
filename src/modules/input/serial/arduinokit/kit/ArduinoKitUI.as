package modules.input.serial.arduinokit.kit
{
	import flash.display.Sprite;

import modules.input.serial.arduinokit.ArduinoKitAPI;

/**
	 * ...
	 * @author Ben Kuper
	 */
	public class ArduinoKitUI extends Sprite
	{
		public static const CAPA_COLOR:uint = 0x0099CC;
		public static const RESISTANCE_COLOR:uint = 0xFFD700;
		public static const BUTTON_COLOR:uint = 0xB3425F;
		
		private var bg:Sprite;
		private var bPlugs:Vector.<KitPlug>;
		private var rPlugs:Vector.<KitPlug>;
		private var cPlugs:Vector.<KitPlug>;
		
		private var plugContainer:Sprite;
		
		private var baseWidth:Number;
		private var baseHeight:Number;
		
		public function ArduinoKitUI(numButtons:int = 4, numRes:int = 4, numCapas:int = 4)
		{
			super();
			
			bg = new Sprite();
			addChild(bg);
			
			plugContainer = new Sprite();
			addChild(plugContainer);
			
			bPlugs = new Vector.<KitPlug>();
			rPlugs = new Vector.<KitPlug>();
			cPlugs = new Vector.<KitPlug>();
			
			var i:int;
			for (i = 0; i < numButtons; i++)
			{
				var bPlug:KitPlug = new KitPlug(ArduinoKitAPI.BUTTON, i);
				bPlugs.push(bPlug);
				plugContainer.addChild(bPlug);
				//bPlug.addEventListener(PlugEvent.MANUAL_DEACTIVATION, selectHandler);
				bPlug.addEventListener(PlugEvent.SELECTED, selectHandler);
					//bPlug.addEventListener(PlugEvent.DESELECTED, selectHandler);
			}
			
			for (i = 0; i < numRes; i++)
			{
				var rPlug:KitPlug = new KitPlug(ArduinoKitAPI.POT, i);
				rPlugs.push(rPlug);
				plugContainer.addChild(rPlug);
				//rPlug.addEventListener(PlugEvent.MANUAL_DEACTIVATION, selectHandler);
				rPlug.addEventListener(PlugEvent.SELECTED, selectHandler);
					//rPlug.addEventListener(PlugEvent.DESELECTED, selectHandler);
			}
			
			for (i = 0; i < numCapas; i++)
			{
				var cPlug:KitPlug = new KitPlug(ArduinoKitAPI.CAPA, i);
				cPlugs.push(cPlug);
				plugContainer.addChild(cPlug);
				//cPlug.addEventListener(PlugEvent.MANUAL_DEACTIVATION, selectHandler);
				cPlug.addEventListener(PlugEvent.SELECTED, selectHandler);
					//cPlug.addEventListener(PlugEvent.DESELECTED, selectHandler);
			}
			
			//width = 10 + 30 * int(1 + (cPlugs.length - 1) / 6);
			baseWidth = 250;
			baseHeight = 10 + 30 * int(1 + (cPlugs.length - 1) / 6);
			
			draw();
		}
		
		private function selectHandler(event:PlugEvent):void
		{
			var targetPlug:KitPlug = event.target as KitPlug;
			var plug:KitPlug;
			for each (plug in bPlugs)
				if (plug != targetPlug)
					plug.selected = false;
			for each (plug in rPlugs)
				if (plug != targetPlug)
					plug.selected = false;
			for each (plug in cPlugs)
				if (plug != targetPlug)
					plug.selected = false;
		
			// inutile car bubbling
			//if (event.type == PlugEvent.SELECTED || event.type == PlugEvent.DESELECTED)
			//dispatchEvent(event);
		}
		
		//ui
		private function draw():void
		{
			bg.graphics.clear();
			var i:int;
			
			if (bPlugs.length > 0)
			{
				bg.graphics.beginFill(BUTTON_COLOR);
				bg.graphics.drawRoundRect(0, 50, 10 + Math.min(bPlugs.length, 6) * 30, 10 + 30 * int(1 + (bPlugs.length - 1) / 6), 10, 10);
				bg.graphics.endFill();
				
				for (i = 0; i < bPlugs.length; i++)
				{
					bPlugs[i].x = 20 + 30 * i;
					bPlugs[i].y = 120;
				}
			}
			
			if (rPlugs.length > 0)
			{
				bg.graphics.beginFill(RESISTANCE_COLOR);
				bg.graphics.drawRoundRect(0, 50, 10 + Math.min(rPlugs.length, 6) * 30, 10 + 30 * int(1 + (rPlugs.length - 1) / 6), 10, 10);
				bg.graphics.endFill();
				
				for (i = 0; i < rPlugs.length; i++)
				{
					rPlugs[i].x = 20 + 30 * i;
					rPlugs[i].y = 70;
				}
			}
			
			if (cPlugs.length > 0)
			{
				bg.graphics.beginFill(CAPA_COLOR);
				bg.graphics.drawRoundRect(0, 0, 10 + Math.min(cPlugs.length, 6) * 30, 10 + 30 * int(1 + (cPlugs.length - 1) / 6), 10, 10);
				bg.graphics.endFill();
				
				for (i = 0; i < cPlugs.length; i++)
				{
					cPlugs[i].x = 20 + 30 * (i % 6);
					cPlugs[i].y = 20 + 30 * int(i / 6);
				}
			}
		}
		
		//data
		public function setPlugActive(type:String, index:int, active:Boolean):void
		{
			//if (active && !getPlug(type, index).active) Toaster.info("is active");
			//if (!active && getPlug(type, index).active) Toaster.info("is inactive");
			var plug:KitPlug = getPlug(type, index);
			if (plug != null)
				plug.active = active;
		}
		
		public function setPlugValue(type:String, index:int, value:Number):void
		{
			var plug:KitPlug = getPlug(type, index);
			if (plug != null)
				plug.value = value;
			//trace("setPlug", type, index, value);
		}
		
		public function setAllPlugsActive(active:Boolean):void
		{
			var i:int;
			for (i = 0; i < bPlugs.length; i++)
			{
				bPlugs[i].active = active;
			}
			for (i = 0; i < rPlugs.length; i++)
			{
				rPlugs[i].active = active;
			}
			for (i = 0; i < cPlugs.length; i++)
			{
				cPlugs[i].active = active;
			}
		}
		
		public function isPlugCalibrating(type:String, index:int):Boolean
		{
			return getPlug(type, index).calibrating;
		}
		
		private function getPlug(type:String, index:int):KitPlug
		{
			var target:Vector.<KitPlug>;
			
			switch (type)
			{
				case ArduinoKitAPI.CAPA: 
					target = cPlugs;
					break;
				
				case ArduinoKitAPI.POT: 
					target = rPlugs;
					break;
				
				case ArduinoKitAPI.BUTTON: 
					target = bPlugs;
					break;
				
				default: 
					trace("Kit not found :", type);
					break;
			}
			
			if (index >= target.length)
			{
				trace("error getting plug", type, index);
				return null;
			}
			else
				return target[index];
		}
		
		//override
		override public function get width():Number
		{
			return baseWidth;
		}
		
		override public function set width(value:Number):void
		{
			baseWidth = value;
			draw();
		}
		
		override public function get height():Number
		{
			return baseHeight;
		}
		
		override public function set height(value:Number):void
		{
			baseHeight = value;
			draw();
		}
	}

}