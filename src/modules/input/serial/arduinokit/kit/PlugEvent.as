package modules.input.serial.arduinokit.kit
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class PlugEvent extends Event 
	{

		static public const MANUAL_ACTIVATION:String = "manualActivation";
		static public const MANUAL_DEACTIVATION:String = "manualDesactivation";

		static public const ACTIVATED:String = "activated";
		static public const DEACTIVATED:String = "deactivated";

		static public const CALIBRATION_START:String = "calibrationStart";
		static public const CALIBRATION_END:String = "calibrationEnd";

        static public const SELECTED:String = "selected";
        static public const DESELECTED:String = "deselected";

		public var plugType:String;
		public var plugIndex:int;

		public function PlugEvent(type:String, plugType:String = null, plugIndex:int = -1, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.plugType = plugType;
			this.plugIndex = plugIndex;
		}

		public override function clone():Event
		{
			return new PlugEvent(type, plugType,plugIndex, bubbles, cancelable);
		}

		public override function toString():String
		{
			return formatToString("PlugEvent", "type","plugType", "plugIndex", "bubbles", "cancelable", "eventPhase");
		}

	}
	
}