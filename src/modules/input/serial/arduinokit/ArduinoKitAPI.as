package modules.input.serial.arduinokit
{
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class ArduinoKitAPI 
	{	
		// common
		public static const CAPA:String = "capa";
		public static const POT:String = "potar";
		public static const BUTTON:String = "button";
		
		static public const KIT_ID:String = "Kit";
		static public const BT_KIT_ID:String = "BTKit";
		static public const RD_KIT_ID:String = "RDKit";
		
		static public const KIT_PORT:int = 9000;
		static public const BT_KIT_PORT:int = 10000;
		static public const RD_KIT_PORT:int = 8000;
		
		static public const KIT_NB_CAPAS:int = 12;
		static public const BT_KIT_NB_CAPAS:int = 6;
		static public const RD_KIT_NB_CAPAS:int = 6;
		
		// from arduinoKit
		static public const ADDRESS:String = "arduinoKit";
		static public const PING:String = "ping";
		static public const HANDSHAKE:String = "handshake";
		//static public const UNKNOWN_COMMAND:String = "unknownCommand";
		//static public const INVALID_MESSAGE:String = "invalidMessage";
		//static public const OK:String = "ok";
		static public const UPDATE_VALUES:String = "updateValues";
		static public const PARAM_VALUES:String = "paramValues";
		
		// to arduinoKit
		static public const ARTSANUM_ADDRESS:String = "artisanum";
		static public const RESET_PARAMS:String = "resetParams";
		static public const SET_PARAM:String = "setParam";
		static public const GET_PARAMS:String = "getParams";
		
		public function ArduinoKitAPI() 
		{
			
		}
		
	}

}