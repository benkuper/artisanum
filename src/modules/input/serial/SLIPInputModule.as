/**
 * Created by Antoine on 31/01/15.
 */
package modules.input.serial
{
import benkuper.nativeExtensions.SerialEvent;
import benkuper.nativeExtensions.SerialPort;

import flash.errors.EOFError;

import flash.utils.ByteArray;
import flash.utils.getDefinitionByName;
import flash.utils.getQualifiedClassName;

import modules.input.serial.arduinokit.ArduinoKitAPI;

import org.tuio.osc.OSCMessage;

public class SLIPInputModule extends SerialInputModule
{
    static public const SLIP_EOT:int = -64;
    static public const SLIP_ESC:int = -37;
    static public const SLIP_ESC_END:int = -36;
    static public const SLIP_ESC_ESC:int = -35;

    private var messageBuffer:ByteArray;

    public function SLIPInputModule(bufferHandling:String = SLIP, register:Boolean = true)
    {
        super(bufferHandling, register);

        messageBuffer = new ByteArray();
    }

    override protected function handleBuffer(buffer:ByteArray):void
    {
        // decode SLIP
        buffer.position = 0;
        var prevByte:int = 0;
        while (buffer.bytesAvailable)
        {
            var incByte:int = buffer.readByte();

            if (prevByte == SLIP_ESC)
            {
                if (incByte == SLIP_ESC_END) messageBuffer.writeByte(SLIP_EOT);
                else if (incByte == SLIP_ESC_ESC) messageBuffer.writeByte(SLIP_ESC);
            }
            else if (incByte == SLIP_EOT)
            {
                if (messageBuffer.length > 0)
                {
                    messageBuffer.position = 0;
                    try
                    {
                        var oscmsg:OSCMessage = new OSCMessage(messageBuffer);
                        //trace("got SLIP message :", oscmsg.address, oscmsg.argumentsToString());
                        acceptOSCMessage(oscmsg)
                    } catch (e:EOFError )
                    {
                        trace("---- END OF FILE ----", messageBuffer);
                    }
                }
                messageBuffer = new ByteArray();
            }
            else messageBuffer.writeByte(incByte);
            prevByte = incByte;
        }
    }

    public function acceptOSCMessage(msg:OSCMessage):void
    {
        // to override
    }

    protected function sendCommand(command:String, ... values):void
    {
        if (currentPort == null) return;

        trace("send command", command, values)

        // First build an oscMessage

        var message:OSCMessage = new OSCMessage();
        message.address = "/" + ArduinoKitAPI.ARTSANUM_ADDRESS;

        // build arrays for arguments
        var args:Array = new Array();
        args.push(command);
        var types:String = "s"; // command is a string
        for each (var v:*in values)
        {
            if (v is int)
                types += "i";
            else if (v is Number)
                types += "f";
            else if (v is String)
                types += "s";
            else
            {
                trace("problem is osc types !", v, getDefinitionByName(getQualifiedClassName(v)));
                continue;
            }
            args.push(v);
        }
        message.addArguments(types, args);

        // Now we can convert it to a byte array
        var packet:ByteArray = message.getBytes();
        var buffer:ByteArray = new ByteArray();

        // Encode SLIP
        packet.position = 0;
        while (packet.bytesAvailable > 0)
        {
            var incByte:int = packet.readByte();

            if (incByte == SLIP_EOT)
            {
                buffer.writeByte(SLIP_ESC);
                buffer.writeByte(SLIP_ESC_END);
            }
            else if (incByte == SLIP_ESC)
            {
                buffer.writeByte(SLIP_ESC);
                buffer.writeByte(SLIP_ESC_END);
            }
            else buffer.writeByte(incByte);
        }
        buffer.writeByte(SLIP_EOT);
        currentPort.writeBytes(buffer);
    }




}
}
