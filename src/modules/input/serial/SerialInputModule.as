/**
 * Created by Antoine on 29/11/14.
 */
package modules.input.serial
{
import benkuper.nativeExtensions.NativeSerial;
import benkuper.nativeExtensions.SerialEvent;
import benkuper.nativeExtensions.SerialPort;

import com.greensock.TweenMax;

import flash.system.Capabilities;
import flash.utils.ByteArray;

import modules.events.ParameterEvent;
import modules.input.InputModule;
import modules.parameters.Parameter;
import modules.parameters.ParameterBool;
import modules.parameters.ParameterEnum;
import modules.parameters.ParameterInt;

import ui.Toaster;
import ui.components.Button;
import ui.components.StatusBox;
import ui.components.UIEvent;

public class SerialInputModule extends InputModule
{
    // constants
    static public const BYTE255:String = 'byte255';
    static public const SLIP:String = 'slip';
    protected var portMode:String;
    static public const BAUD_RATE:int = 38400;

    // data
    protected var currentPort:SerialPort;

    // delays
    protected var pingCheckDelay:Number = 4;

    // controls
    private var connectionWitness:StatusBox;
    private var pingWitness:StatusBox;
    private var pingTween:TweenMax;
    protected var connectBT:Button;

    // parameters
    protected var arduinoId:ParameterInt;
    private var port:ParameterEnum;
    protected var filterPorts:ParameterBool;

    protected var _connected:Boolean;

    public function SerialInputModule(portMode:String, register:Boolean)
    {
        super(register);
        this.portMode = portMode;

        // serial
        NativeSerial.init();
        NativeSerial.instance.addEventListener(SerialEvent.PORT_ADDED, updatePorts);
        NativeSerial.instance.addEventListener(SerialEvent.PORT_REMOVED, updatePorts);
        _connected = false;

        // parameters
        arduinoId = addParameter(new ParameterInt("kitIDParam", "n° carte", 0, 0, 100), false) as ParameterInt;
        port = addParameter(new ParameterEnum("serialPort","Port série"), true, "le port USB ou Bluetooth du kit") as ParameterEnum;
        port.gui.addEventListener(UIEvent.DROPDOWN_OPEN, dropDownOpen);
        port.gui.x = 10;
        port.addEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
        filterPorts = addParameter(new ParameterBool("filterPorts","Filtrer les ports", true)) as ParameterBool;
        filterPorts.addEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);

        // ui
        connectionWitness = new StatusBox("Connexion");
        connectionWitness.shape = StatusBox.CIRCLE;
        connectionWitness.radius = 5;
        addControl(connectionWitness);

        pingWitness = new StatusBox("Signal ");
        pingWitness.shape = StatusBox.CIRCLE;
        pingWitness.radius = 5;
        addControl(pingWitness);

        connectBT = new Button("Connexion");
        addControl(connectBT);
        connectBT.addEventListener(UIEvent.BUTTON_SELECTED, BTHandler);

        controlsContainer.autoLayout = true;
        parameterContainer.autoLayout = true;
        measureContainer.autoLayout = true;

        updatePorts();
    }

    // handlers
    private function dropDownOpen(e:UIEvent):void
    {
        if (e.currentTarget == port.gui) parameterContainer.bringElementToFront(port.gui);
    }

    private function updatePorts(e:SerialEvent = null):void
    {
        //trace("update ports");
        var formerPort:SerialPort = currentPort;

        var serialPorts:Vector.<SerialPort> = NativeSerial.ports;
        var availablePorts:Array = new Array();
        var changePort:Boolean = true;
        for (var i:int = 0; i < serialPorts.length; i++)
        {
            // filter ports on mac
            if (Capabilities.os.toLowerCase().indexOf("mac") >= 0 && filterPorts.value)
            {
                if (serialPorts[i].name.indexOf("Artisanum") < 0 && serialPorts[i].name.indexOf("usb") < 0) continue;
            }

            availablePorts.push( { id:serialPorts[i].COMID, label:serialPorts[i].name, value:serialPorts[i].COMID } );

            if (serialPorts[i] == formerPort)
            {
                changePort = false;
                port.selectedIndex = i;
            }
        }

        if (availablePorts.length == 0) availablePorts.push( {id:"none", label:"Aucun port détecté", value:"" } );
        port.setOptions(availablePorts);

        if (changePort)
        {
            port.selectedIndex = 0;
            closeCurrentPort("Le kit a été débranché !")
        }
    }

    private function paramChanged(e:ParameterEvent):void
    {
        var targetParam:Parameter = e.target as Parameter;

        switch (targetParam)
        {
            case filterPorts:
                updatePorts();
                break;
        }
    }

    protected function BTHandler(e:UIEvent):void
    {
        var targetBT:Button = e.target as Button;

        switch (targetBT)
        {
            case connectBT:
                connectionWitness.status = StatusBox.NORMAL;
                if (port.selectedItem == null) Toaster.warning("Sélectionnez d'abord un port !")
                else connect(port.selectedItem.id);
                break;
        }
    }

    // data
    private function connect(portID:String):void
    {
        trace("connect to ", portID)
        if (currentPort != null && currentPort.COMID != portID)
        {
            trace("currentPort : ", currentPort.name, "try to connect to :", portID)
            closeCurrentPort();
        }

        currentPort = NativeSerial.getPort(portID);

        if (currentPort == null) Toaster.error("Connexion non identifiée !");
        else
        {
            //trace("connect to ", currentPort.COMID, currentPort.name, currentPort.fullName);
            Toaster.info(currentPort.isOpened?"Reconnexion...":"Connexion...");
            TweenMax.delayedCall(.1, tryToOpenPort);
        }
    }

    private function tryToOpenPort():void
    {
        currentPort.mode = (portMode == BYTE255)?SerialPort.MODE_BYTE255:SerialPort.MODE_RAW;

        if (currentPort.open(BAUD_RATE))
        {
            trace("--- CONNEXION SUCCEED");

            var name:String = currentPort.name;
            var id:String = name.charAt(name.indexOf("Kit") + 4); // Kit_i
            arduinoId.value = int(id);

            connected = true;

            currentPort.addEventListener(SerialEvent.DATA, serialData);
            currentPort.addEventListener(SerialEvent.DATA_255, serialData);
        } else
        {
            trace("--- CONNEXION FAILED");
            connected = false;
        }
    }

    private function serialData(e:SerialEvent):void
    {
        if (e.port as SerialPort != currentPort)
        {
            trace("ERROR : DATA FROM OTHER PORT :");
            trace("currentPort :", currentPort.COMID);
            trace(e.port.COMID);
        }

        TweenMax.killDelayedCallsTo(closeCurrentPort);
        if (connected) TweenMax.delayedCall(pingCheckDelay, closeCurrentPort, ["Signal perdu !"]);
        else trace("PROBLEM : DATA RECEIVED BUT NO CONNECTED !");
        connected = true;

        flashPing(); // TO DO : faire une fonction flash(color) dans StatusBox

        // trace("data received :", e.data.length);
        var newBytes:ByteArray = e.data;
        handleBuffer(newBytes);
    }

    protected function handleBuffer(buffer:ByteArray):void
    {
        // to override
        trace("handle buffer :", buffer);
    }

    private function flashPing():void
    {
        pingWitness.status = StatusBox.OK;
        TweenMax.delayedCall(0.05, setPingToNormal);
    }

    private function setPingToNormal():void
    {
        TweenMax.killDelayedCallsTo(setPingToNormal);
        pingWitness.status = StatusBox.NORMAL;
    }

    protected function closeCurrentPort(warning:String = null):void
    {
        trace("----------- CLOSE CURRENT PORT");
        TweenMax.killDelayedCallsTo(closeCurrentPort);
        connected = false;
        //trace("close current with warning :", warning);
        if (warning != null) Toaster.warning(warning);

        if (currentPort == null)
        {
            trace("currentport is null !");
            return;
        }
        if (currentPort.hasEventListener(SerialEvent.DATA)) currentPort.removeEventListener(SerialEvent.DATA, serialData);
        if (currentPort.hasEventListener(SerialEvent.DATA_255)) currentPort.removeEventListener(SerialEvent.DATA_255, serialData);
        if (currentPort.isOpened)
        {
            currentPort.close();
            if (warning == null) Toaster.info("Le port "+currentPort.COMID + " a été fermé");
        } else
        {
            trace("currentport n'était pas ouvert !")
        }
    }

    // getters & setters
    public function get connected():Boolean
    {
        return _connected;
    }

    public function set connected(value:Boolean):void
    {
        if (_connected && value) return;

        _connected = value;

        if (value)
        {
            connectionWitness.status = StatusBox.OK;
            Toaster.success("Connexion série à " + currentPort.COMID + " établie !");
        } else
        {
            connectionWitness.status = StatusBox.ERROR;
            Toaster.warning("Connexion impossible !")
        }
    }
    //clean
    override public function clean():void
    {
        super.clean();

        NativeSerial.instance.removeEventListener(SerialEvent.PORT_ADDED, updatePorts);
        NativeSerial.instance.removeEventListener(SerialEvent.PORT_REMOVED, updatePorts);

        port.gui.removeEventListener(UIEvent.DROPDOWN_OPEN, dropDownOpen);
        port.removeEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
        connectBT.removeEventListener(UIEvent.BUTTON_SELECTED, BTHandler);

        closeCurrentPort();

    }

    // ui
    override protected function draw():void
    {
        super.draw();

        connectionWitness.x = 5; // controlsContainer.width - connectionWitness.width - 5;
        connectionWitness.y = 0;
        connectionWitness.height = 20;

        pingWitness.x = connectionWitness.x;
        pingWitness.y = connectionWitness.y + 20;
        pingWitness.height = 20;

        connectBT.x = 10;

        controlsContainer.height = 80;

        port.guiWidth = parameterContainer.width - 20;
    }
}
}
