package modules.input 
{
	import modules.events.MeasureEvent;
	import modules.events.ModuleEvent;
	import modules.input.measure.Measure;
	import modules.Module;
	import modules.parameters.Parameter;
	import ui.components.Panel;
	import ui.components.UIEvent;
	import ui.Tooltip;
	
	/**
	 * ...
	 * @author Antoine Costes
	 * 
	 * Un InputModule permet de fournir a Artisanum des informations provenant d'un capteur specifique.
	 * Il expose une liste de Measures dont il met a jour la currentValue (suivant le fonctionnement du capteur).
	 * Il expose une liste de Parameters
	 */
	
	public class InputModule extends Module 
	{
		public static var moduleList:Vector.<InputModule>;
		protected var measures:Vector.<Measure>;
		
		//ui
		protected var measureContainer:Panel;
		protected var hasMeasures:Boolean;
		
		public function InputModule(register:Boolean = true)  //temp variable for specialModules handling, will be gone with common IMeasureProvider interface
		{
			if (moduleList == null) moduleList = new Vector.<InputModule>;
			if(register) moduleList.push(this);
			
			measureContainer = new Panel("Mesures");
			//measureContainer.autoLayout = true;
			contentContainer.addChild(measureContainer);
			
			
			measures = new Vector.<Measure>;
			
			moduleHeight = 800;
		}
		
		
		protected function addMeasure(newMeasure:Measure, tooltip:String = "", addGUI:Boolean = true):Measure // TODO :  checker la classe de newMeasure  ?
		{
			measures.push(newMeasure);
			if (addGUI) measureContainer.addElement(newMeasure.gui);
			
			if (tooltip != "") Tooltip.addTarget(newMeasure.gui, tooltip);
			
			hasMeasures = (measureContainer.numChildren > 0);
			if (hasMeasures && !contentContainer.contains(measureContainer)) contentContainer.addChild(measureContainer);
			
			newMeasure.addEventListener(MeasureEvent.LABEL_CHANGED, measureLabelChanged);
			
			dispatchEvent(new ModuleEvent(ModuleEvent.MEASURE_ADDED));
			return newMeasure;
		}
		
		
		 
		protected function removeMeasure(targetMeasure:Measure, autoHeight:Boolean = true):void
		{
			var index:int = measures.indexOf(targetMeasure);
			
			if (index < 0) 
			{
				throw new Error("Can't delete Measure " + targetMeasure.label + " because it doesn't belong to Module " + moduleName);
			}
			
			measureContainer.removeElement(targetMeasure.gui);
			
			hasMeasures = (measureContainer.numChildren > 0);
			if (!hasMeasures && contentContainer.contains(measureContainer)) contentContainer.removeChild(measureContainer);
			
			measures.splice(index, 1);
			
			targetMeasure.removeEventListener(MeasureEvent.LABEL_CHANGED, measureLabelChanged);
			
			dispatchEvent(new ModuleEvent(ModuleEvent.MEASURE_REMOVED));
		}
		
		protected function removeAllMeasures():void
		{
			while (measures.length > 0)
			{
				removeMeasure(measures[0]);
			}
		}
		
		//override draw
		override protected function draw():void
		{
			super.draw();
			measureContainer.x = 10;
			measureContainer.width = moduleWidth - 20;
			measureContainer.height = moduleHeight;
		}
		
		
		//handlers
		private function measureLabelChanged(e:MeasureEvent):void 
		{
			dispatchEvent(e.clone());
		}
		
		//list providing
		public function getMeasureList():Array
		{
			var a:Array = new Array();
			for each(var m:Measure in measures)
			{
				a.push( {id:m.id, label:m.label, value:m } );
			}
			return a;
		}
		
		public function getMeasureByID(id:String):Measure
		{
			for each(var m:Measure in measures)
			{
				if (m.id == id) return m;
			}
			
			return null;
		}
		
		override public function clean():void
		{
			super.clean();
			moduleList.splice(moduleList.indexOf(this), 1);
		}
		
		//static module list providing
		public static function getModuleList():Array
		{
			var a:Array = new Array();
			for each(var m:InputModule in moduleList)
			{
				a.push( {id:m.id, label:m.moduleName, value:m } );
			}
			return a;
		}
		
		static public function getModuleById(moduleId:String):Module
		{
			for each(var m:InputModule in moduleList)
			{
				if (m.id == moduleId) return m;
			}
			return null;
		}
		
		
		//XML project saving
		override public function getXML():XML 
		{
			var xml:XML = super.getXML();
			xml.appendChild(<measures></measures>);
			
			//var mIndex:int = 0;
			for each(var m:Measure in measures)
			{
				var mXML:XML = m.getXML();
				//mXML.@measureIndex = mIndex;
				xml.measures[0].appendChild(mXML);
				//mIndex++;
			}
			return xml;
		}
		
		override  public function loadXML(data:XML):void
		{
			super.loadXML(data);
			
			for each(var mXML:XML in data.measures[0].measure)
			{
				try
				{
					getMeasureByID(mXML.@id).loadXML(mXML);
				} catch (e:Error)
				{
                    trace("can't load measure :", mXML.@id, "because it does not exists");
					//trace(e.getStackTrace());
				}
			}
		}
		
		//util
		
		//util
		private function getUniqueNameFor(m:InputModule, sourceName:String):String 
		{
			for each(var tm:InputModule in moduleList)
			{
				if (tm == m) continue;
				
				if (tm.moduleName == sourceName)
				{
					var nextName:String;
					var lastN:Number = Number(sourceName.slice(sourceName.length - 1));
					if (!isNaN(lastN))
					{
						nextName = sourceName.slice(0, sourceName.length - 1) + (lastN + 1);
					}else
					{
						nextName = sourceName+" 2";
					}
					
					return getUniqueNameFor(m, nextName);
				}
			}
			
			
			return sourceName;
		}
		
		
		// overrides
		override public function set moduleName(value:String):void
		{
			super.moduleName = getUniqueNameFor(this,value);
		}
	}

}