package modules.input.osc 
{
	import benkuper.util.osc.OSCMaster;
	import modules.events.ParameterEvent;
	import modules.input.InputModule;
	import modules.parameters.ParameterInt;
	import modules.parameters.ParameterString;
	import org.tuio.osc.IOSCListener;
	import org.tuio.osc.OSCMessage;
	
	/**
	 * ...
	 * @author ...
	 */
	public class OSCInputModule extends InputModule implements IOSCListener
	{
		protected var ipParameter:ParameterString;
		protected var portParameter:ParameterInt;
		protected var verbose:Boolean;
		
		
		public function OSCInputModule() 
		{
			super();
			
			ipParameter = addParameter(new ParameterString("ip", "IP cible", "127.0.0.1"), false) as ParameterString;
			portParameter = addParameter(new ParameterInt("port", "port", 9000), false) as ParameterInt;
		}
		
		protected function register(id:String):void
		{
			OSCMaster.register(id, this);
		}
		
		protected function unregister(id:String):void
		{
			OSCMaster.unregister(id, this);
		}
		
		
		//data
		public function acceptOSCMessage(msg:OSCMessage):void
		{
			trace("received OSC", msg.address, " / ", msg.arguments);
			
			var targetInfos:String = msg.address.split("#")[1]; // exemple : arduinoKit#BTKit@127.0.0.1:7000
			var targetId:String = targetInfos.split("@")[0] as String;
			var targetIP:String = targetInfos.split("@")[1].split(":")[0] as String;
			var targetPort:int = int(targetInfos.split("@")[1].split(":")[1]);
			
			trace(targetId, targetIP, targetPort);
			
			if (targetIP != ipParameter.value) ipParameter.value = targetIP;
			if (targetPort != portParameter.value) portParameter.value = targetPort;
		}
		
		public function sendMessage(address:String, types:String, args:Array):void
		{
			var message:OSCMessage = new OSCMessage();
			message.address = address;
			message.addArguments(types, args);
			
			OSCMaster.sendTo(message, ipParameter.value, portParameter.value, verbose);
		}
		
		public function sendOSCMessage(oscmsg:OSCMessage):void
		{
			OSCMaster.sendTo(oscmsg, ipParameter.value, portParameter.value, verbose);
		}

		//clean
		override public function clean():void
		{
			super.clean();
			//ipParameter.removeEventListener(ParameterEvent.VALUE_CHANGE, initOSC);
			//portParameter.removeEventListener(ParameterEvent.VALUE_CHANGE, initOSC);
		}
	}
}