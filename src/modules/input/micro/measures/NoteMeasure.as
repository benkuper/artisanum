package modules.input.micro.measures 
{
	import modules.input.measure.comparators.ComparatorType;
	import modules.input.measure.comparators.SuiteComparator;
	import modules.input.measure.DiscreteMeasure;
	import modules.input.measure.ReferenceValue;
	
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class NoteMeasure extends DiscreteMeasure 
	{
        public static const NOTE_FREQS:Array = [0, 130.81, 138.59, 146.83, 155.56, 164.81, 174.61, 185, 196, 207.66, 220, 233.08, 246.94];
		public static const NOTE_NAMES:Array = new Array("-",  "Do", "Do#", "Ré", "Mib", "Mi", "Fa", "Fa#", "Sol", "Sol#", "La", "Sib", "Si");
		private const nbNotes:int = NOTE_NAMES.length;
		
		private const MIN_FREQ:Number = NOTE_FREQS[1];
		private const MAX_FREQ:Number = NOTE_FREQS[nbNotes - 1];
		
		public var distanceToNote:Number;
		
		public function NoteMeasure(id:String, label:String, enabled:Boolean = true)
		{
			super(id, label, enabled, false);
			
			for (var i:int = 0 ; i < nbNotes ; i++)
				referenceValues.push(new ReferenceValue(NOTE_NAMES[i], i-1));
			
			
			addComparatorTypes(new ComparatorType("~","~", NoteComparator, { } ) );
			addComparatorTypes(new ComparatorType("suite","Suite", SuiteComparator, { } ) );
		}
		
		public function shut():void
		{
			currentReferenceValue = referenceValues[0];
		}
		
		public function setValueWithIndex(index:int):void
		{
			if (index < -1 && index > 11) return;
			currentReferenceValue = referenceValues[index + 1];
		}

        public function setValueWithPitch(pitch:int):void
        {
            while (pitch < 0) pitch += 12;
            while (pitch > 11) pitch -= 12;
            currentReferenceValue = referenceValues[pitch + 1];
        }
		
		public function setValueForFrequency(freq:Number):void
		{
			if (freq <= 0)
			{
				//trace("[NoteMeasure] setValueForFrequency :: ERROR : frequency must be strictly positive !");
				currentReferenceValue = referenceValues[0];
				distanceToNote = 0;
			} else
			{
			
				// normalize frequency
				var normalizedFreq:Number = freq;
				while (normalizedFreq < MIN_FREQ)
				{
					normalizedFreq = normalizedFreq << 1;
				}
				while (normalizedFreq > MAX_FREQ)
				{
					normalizedFreq = normalizedFreq >> 1;
				}
				
				// get closest note
				var minDist:Number = Number.MAX_VALUE;
				var distance:Number;
				var index:int = 0;
				var n:int = NOTE_FREQS.length;
				
				for (var i:int = 1 ; i < n ; i++)
				{
					if (Math.abs(NOTE_FREQS[i] - normalizedFreq) < minDist)
					{
						distance = normalizedFreq - NOTE_FREQS[i];
						minDist = Math.abs(distance);
						index = i;
					}
				}
				
				currentReferenceValue = referenceValues[index];
				
				if (distance > 0)
				{
					distanceToNote = 2*distance / ( NOTE_FREQS[index + 1] - NOTE_FREQS[index] );
				} else
				{
					distanceToNote = 2*distance / ( NOTE_FREQS[index] - NOTE_FREQS[index - 1] );
				}
			}
		}
	}
}