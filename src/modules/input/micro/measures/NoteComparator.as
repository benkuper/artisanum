package modules.input.micro.measures 
{
	import modules.events.ParameterEvent;
	import modules.input.measure.comparators.Comparator;
	import modules.input.measure.comparators.EnumComparator;
	import modules.input.measure.Measure;
	import modules.parameters.ParameterInt;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class NoteComparator extends EnumComparator 
	{
		
		private var numDemiTons:int = 12; //ref for loop
		
		public var toleranceParam:ParameterInt;
		public var tolerance:int;
		
		public function NoteComparator(measure:Measure, fromComparator:Comparator=null) 
		{
			super(measure, fromComparator);
			toleranceParam = addParameter(new ParameterInt("tolerance","+/-",0,0,12),"Tolérance (demi-tons)",true) as ParameterInt;
			toleranceParam.addEventListener(ParameterEvent.VALUE_CHANGE, toleranceChanged);
			
			toleranceParam.guiWidth = 150;
			toleranceParam.gui.x = checkValBox.gui.x+checkValBox.guiWidth+10;
			toleranceParam.gui.y = checkValBox.gui.y;
			
		}
		
		private function toleranceChanged(e:ParameterEvent):void 
		{
			tolerance = toleranceParam.value;
			result = compare();
		}
		
		override protected function compare():Boolean
		{			
			if (currentRefValue == null || checkVal == null) return false;
			if (currentRefValue.value == -1) return false; //no note detected
			
			var lowVal:int = (checkVal.value- tolerance +numDemiTons ) % numDemiTons;
			var highVal:int = lowVal + 2 * tolerance;
			var refVal:int = currentRefValue.value;
			if (refVal < lowVal) refVal += numDemiTons;
			
			return  refVal >=  lowVal && refVal <= highVal;
		}
	}

}