package modules.input.micro.measures 
{
	import modules.input.measure.DiscreteMeasure;
	import modules.input.measure.Measure;
	import modules.input.measure.ReferenceValue;
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class OctaveMeasure extends DiscreteMeasure
	{
		private const MIN_FREQ:Number = 130.81;
		private const MAX_FREQ:Number = 246.94;
		
		private const MIN_OCTAVE:Number = -2;
		private const MAX_OCTAVE:Number = 5;
		
		public function OctaveMeasure(id:String, label:String, enabled:Boolean = true)
		{
			super(id, label, enabled, false);
			
			referenceValues.push(new ReferenceValue("-", -99));

			for (var i:int = MIN_OCTAVE ; i <= MAX_OCTAVE; i++)
				referenceValues.push(new ReferenceValue(i.toString(), i));
				
		}
		
		public function setValue(val:int):void
		{
			currentReferenceValue = referenceValues[val + 3];
		}

        public function setValueForPitch(pitch:int):void
        {
            var octaveIndex:int = Math.abs(MIN_OCTAVE) + 1; // index for octave = 1

            while (pitch < 0)
            {
                pitch += 12;
                octaveIndex--;
            }
            while (pitch > 11)
            {
                pitch -= 12;
                octaveIndex++;
            }
            if (octaveIndex < 0 || octaveIndex > referenceValues.length - 1)
            {
                currentReferenceValue = referenceValues[0];
            } else
            {
                currentReferenceValue = referenceValues[octaveIndex];
            }
        }

		public function setValueForFrequency(freq:Number):void
		{
			if (freq <= 0)
			{
				//trace("[OctaveMeasure] getOctaveForFrequency :: ERROR : frequency must be strictly positive !");
				currentReferenceValue = referenceValues[0];
			} else
			{
				var octaveIndex:int = Math.abs(MIN_OCTAVE) + 1; // index for octave = 1
				
				while (freq < MIN_FREQ)
				{
					freq = freq << 1;
					octaveIndex--;
				}
				while (freq > MAX_FREQ)
				{
					freq = freq >> 1;
					octaveIndex++;
				}
				if (octaveIndex < 0 || octaveIndex > referenceValues.length - 1)
				{
					currentReferenceValue = referenceValues[0];
				} else
				{
					currentReferenceValue = referenceValues[octaveIndex];
				}
			}
		}
	}
}