﻿package modules.input.micro
{
	import modules.input.micro.filter.LowpassFilter;
	import com.greensock.TweenLite;
	import flash.events.SampleDataEvent;
	import flash.media.Microphone;
	import flash.system.MessageChannel;
	import flash.system.Worker;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	import flash.utils.getTimer;
	import modules.events.ParameterEvent;
	import modules.input.InputModule;
	import modules.input.measure.ContinuousMeasure;
	import modules.input.micro.measures.NoteMeasure;
	import modules.input.micro.measures.OctaveMeasure;
	import modules.parameters.Parameter;
	import modules.parameters.ParameterEnum;
	import modules.parameters.ParameterFloat;
	import modules.parameters.ParameterInt;
	import ui.components.Button;
	import ui.components.UIEvent;
	import ui.Style;
	import ui.Toaster;
	
	/**
	 * ...
	 * @author To
	 */
	public class MicrophoneInputModule extends InputModule
	{
		protected var micro:Microphone;
		
		// ui
		protected var updateMicsBT:Button;
		protected var setSilenceLevelBT:Button;
		
		// sound analysis
		private static var PITCH_FACTOR:Number = 14.96598;
		private static var INTENSITY_FACTOR:Number = 0.01;
		private var datf:DATF;
		private var hopSize:uint = 1470; // 2048;
		private var frameSize:uint = 2 * hopSize;
		private var soundBytes:ByteArray;
		
		// parameters
		protected var microphones:ParameterEnum;
		protected var cutoff:ParameterFloat;
		protected var useCutoff:Boolean;
		//protected var resonance:ParameterFloat;
		protected var gain:ParameterFloat;
		protected var gate:ParameterInt;
		protected var gateStartTime:int;
		protected var gateCurrTime:int;
		
		// measures
		protected var volume:ContinuousMeasure;
		protected var frequency:ContinuousMeasure;
		protected var note:NoteMeasure;
		protected var noteOctave:OctaveMeasure;
		
		// additional data
		private var modulationHistory:Vector.<Number>;
		private var modulationNumSamples:int;
		private var modulation:ContinuousMeasure;
		
		private var lowpass:LowpassFilter;
		private var _correctValues:Boolean;
		private var verbose:Boolean;
		
		public function MicrophoneInputModule()
		{
			
			// ui
			updateMicsBT = new Button("Repérer les micros");
			addControl(updateMicsBT);
			updateMicsBT.addEventListener(UIEvent.BUTTON_SELECTED, updateMicrophones);
			setSilenceLevelBT = new Button("Calibrer le micro");
			addControl(setSilenceLevelBT);
			setSilenceLevelBT.addEventListener(UIEvent.BUTTON_SELECTED, setMicSilenceLevel);
			this.hasControls = true;
			
			// parameters
			microphones = addParameter(new ParameterEnum("micro","Micro"), true, "choisir le micro dans lequel on chante") as ParameterEnum;
			microphones.gui.x = 10;
			
			microphones.gui.addEventListener(UIEvent.DROPDOWN_OPEN, dropDownOpen);
			microphones.addEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			cutoff = addParameter(new ParameterFloat("cutoff", "coupure (kHz)", 20, 5, 20)) as ParameterFloat;
			cutoff.addEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			useCutoff = true;
			//resonance = addParameter(new ParameterFloat("resonance", 1.5, 0, 5)) as ParameterFloat;
			//resonance.addEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			gain = addParameter(new ParameterFloat("microGain","gain du micro", 50, 0, 100),true, "le gain du micro doit être suffisant pour analyser le son") as ParameterFloat;
			gain.addEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			gate = addParameter(new ParameterInt("gate","temps d'analyse", 30, 0, 70),true, "nombre de milisecondes pendant lequel le chant est analysé") as ParameterInt;
			gate.addEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			gate.value = 30; // TODO : Bug ! la valeur par defaut n'est pas mise à jour !
			gateStartTime = 0;
			gateCurrTime = 0;
			
			// Measures
			volume = addMeasure(new ContinuousMeasure("volume","Volume", 0, 100), "le volume mesuré par le micro") as ContinuousMeasure;
			//volume.smoothing = .15;
			frequency = addMeasure(new ContinuousMeasure("frequence","Fréquence", 0, 1500), "la fréquence de la voix") as ContinuousMeasure;
			note = addMeasure(new NoteMeasure("note", "Note")) as NoteMeasure;
			note.setValueWithIndex(0);
			noteOctave = addMeasure(new OctaveMeasure("octave","Octave")) as OctaveMeasure;
			noteOctave.setValue(0);
			
			// sound analysis
			datf = new DATF(hopSize, false, frameSize, 0);
			lowpass = new LowpassFilter();
			lowpass.cutoffFrequency = cutoff.value*1000;
			//resonance.value = lowpass.resonance;
			
			//Modulation
			modulation = addMeasure(new ContinuousMeasure("modulation","Modulation", -1, 1, 0)) as ContinuousMeasure;
			modulationHistory = new Vector.<Number>();
			modulationNumSamples = 4;
			
			updateMicrophones();
			microphones.selectedIndex = 0;
			verbose = false;
		}
		
		private function dropDownOpen(e:UIEvent):void 
		{
			if (e.currentTarget == microphones.gui) parameterContainer.bringElementToFront(microphones.gui);
			//if (e.currentTarget == baudRate.gui) parameterContainer.bringElementToFront(baudRate.gui);
		}
		
		private function updateMicrophones(e:UIEvent = null):void
		{
			
			var mics:Array = Microphone.names;
			var currentMicrophones:Array = new Array();
			for (var i:int = 0; i < mics.length; i++)
			{
				currentMicrophones.push({id:mics[i],label: mics[i], value: i});
			}
			
			microphones.setOptions(currentMicrophones);
		}
		
		private function setMicSilenceLevel(e:UIEvent = null):void
		{
			if (micro != null)
			{
				micro.setSilenceLevel(volume.currentValue);
				Toaster.success("[Module Micro] Micro calibré !");
			}
		}
		
		private function paramChanged(e:ParameterEvent):void
		{
			var targetParam:Parameter = e.target as Parameter;
			
			switch (targetParam)
			{
				case microphones: 
					if (microphones.selectedItem != null) trace("[MicroInputModule] microphone changed !", microphones.selectedItem.value);
					if (microphones.selectedIndex >= 0)
					{
						Toaster.success("[Module Micro] Connexion au micro réussie !");
						micro = Microphone.getMicrophone(microphones.selectedIndex); //i);
						micro.rate = 22;
						//micro.gain = gain.value;
						//micro.setSilenceLevel(0.0);
						micro.addEventListener(SampleDataEvent.SAMPLE_DATA, sampleData);
					}
					else
					{
						Toaster.error("[Module Micro] Aucun micro n'est connecté !");
					}
					break;
					
				case gain:
					if (micro != null) micro.gain = gain.value;
					break;
					
					
				//case resonance:
					//lowpass.resonance = resonance.value;
					//break;
					//
				case cutoff:
					useCutoff = cutoff.value < 20;
					lowpass.cutoffFrequency = cutoff.value*1000;
					break;
			}
		}
		
		private function sampleData(e:SampleDataEvent):void
		{
			soundBytes = new ByteArray();
			soundBytes.endian = Endian.LITTLE_ENDIAN; // Set endianess for data processed in CPP
			
			while (e.data.bytesAvailable)
			{
				var sample:Number = e.data.readFloat();
				soundBytes.writeFloat(sample);
				soundBytes.writeFloat(sample);
			}
			
			soundBytes.position = 0;
			datf.resetAll();
			datf.setFrame(soundBytes, "float");
			
			var intensity:Number = datf.getIntensity() * INTENSITY_FACTOR;
			var pitch:Number = datf.getPitch() * PITCH_FACTOR;
			var bandWidth:Number = datf.getBandwidth();
			
			var prevNote:int = note.currentIndex as int;
			
			// reset values : 
			//trace(micro.activityLevel);
			volume.currentValue = micro.activityLevel;// intensity;	// we always want the volume
			//frequency.currentValue = 0;	we want to see the last frequency
			
			if (!areValuesCorrect(pitch, intensity, bandWidth))
			{
				note.setValueForFrequency(0);
				noteOctave.setValueForFrequency(0);
				modulation.currentValue = 0;
				correctValues = false;
			} else
			{
				correctValues = true;
				var currGate:int = getTimer() - gateStartTime;
				
				if ( currGate < gate.value)
				{
					//trace("gate too low", currGate, gate.value);
					return;
				}
				
				frequency.currentValue = useCutoff?lowpass.process(pitch):pitch;
				//volume.currentValue = micro.activityLevel;// intensity;
				note.setValueForFrequency(pitch);
				noteOctave.setValueForFrequency(pitch);

				//modulation
				if (pitch != 0) computeModulation(pitch);
			}
		}
		
		public function areValuesCorrect(pitch:Number, intensity:Number, bandWidth:Number):Boolean
		{
			if (isNaN(pitch))
			{
				if (verbose)  trace("nan pitch")
				return false;
			}
			else if (isNaN(intensity))
			{
				if (verbose) trace("nan intensity")
				return false;
			}
			else if (pitch % 128 == 0)
			{
				if (verbose) trace("bad pitch :", pitch);
				return false;
			}
			else if (intensity < 2)
			{
				//trace("low vol");
				return false;
			}
			else if (pitch > frequency.maximumValue)
			{
				//if (verbose) trace("f**cking high pitch :", pitch);
				return false;
			}
			else if (bandWidth < 10)
			{
				//if (verbose) trace("low bandw");
				return false;
			}
			return true;
		}
		
		public function computeModulation(newFreq:Number):void
		{
			if (modulationHistory.length >= modulationNumSamples * 2) modulationHistory.shift();
			modulationHistory.push(newFreq);
			//Toaster.info(modulationHistory.length + " <> " + modulationNumSamples * 2);
			if (modulationHistory.length == modulationNumSamples * 2)
			{
				var oldMod:Number = 0;
				var newMod:Number = 0;
				var i:int;
				for (i = 0; i < modulationNumSamples; i++)
					oldMod += modulationHistory[i];
				for (i = modulationNumSamples; i < modulationNumSamples * 2; i++)
					newMod += modulationHistory[i];
				oldMod /= modulationNumSamples;
				newMod /= modulationNumSamples;
				
				modulation.currentValue = (newMod - oldMod) / 300; //arbitrary
			}
		}
		
		override protected function draw():void
		{
			
			super.draw();
			
			// controls
			updateMicsBT.x = 10;
			updateMicsBT.y = -10;
			updateMicsBT.width = (controlsContainer.width - 30) / 2;
			
			setSilenceLevelBT.x = controlsContainer.width / 2 + 5;
			setSilenceLevelBT.y = -10;
			setSilenceLevelBT.width = (controlsContainer.width - 30) / 2;
			
			controlsContainer.height = 80;
			
			//[Ben] Added parameter.guiWidth && parameter.guiHeight since ui component are not accessible through here anymore
			//volume.guiWidth = moduleWidth - 20;
			volume.gui.x = 10;
			volume.gui.y = -10;
			volume.guiWidth = measureContainer.width - 20;
			
			frequency.gui.x = 10;
			frequency.gui.y = 20;
			frequency.guiWidth = measureContainer.width - 20;
			
			note.gui.x = (measureContainer.width - noteOctave.gui.width) * .3;
			note.gui.y = 50;
			
			noteOctave.gui.x = (measureContainer.width - noteOctave.gui.width) * .7;
			noteOctave.gui.y = 50;
			
			modulation.gui.x = 10;
			modulation.gui.y = 90;
			
			measureContainer.height = 180;
			
			// parameters
			microphones.gui.y = -10;
			microphones.guiWidth = this.parameterContainer.width - 20;
			
			gain.gui.x = 10;
			gain.gui.y = 35;
			gain.guiWidth = measureContainer.width - 20;
			
			gate.gui.x = 10;
			gate.gui.y = 65;
			gate.guiWidth = measureContainer.width - 20;
			
			cutoff.gui.x = 10;
			cutoff.gui.y = 95;
			cutoff.guiWidth = measureContainer.width - 20;
			
			parameterContainer.height = 180;
		
		}
		
		override public function clean():void
		{
			super.clean();
			
			updateMicsBT.removeEventListener(UIEvent.BUTTON_SELECTED, updateMicrophones);
			setSilenceLevelBT.removeEventListener(UIEvent.BUTTON_SELECTED, setMicSilenceLevel);
			microphones.gui.removeEventListener(UIEvent.DROPDOWN_OPEN, dropDownOpen);
			microphones.removeEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			gain.removeEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			gate.removeEventListener(ParameterEvent.VALUE_CHANGE, paramChanged);
			if (micro != null) micro.removeEventListener(SampleDataEvent.SAMPLE_DATA, sampleData);

		}
		
		public function get correctValues():Boolean
		{
			return _correctValues;
		}
		
		public function set correctValues(value:Boolean):void
		{
			if (value == _correctValues) return;
			_correctValues = value;
			frequency.feedBackColor = value?Style.GREEN:Style.UI_OVER;
			gateStartTime = getTimer();
		}
	
	}

}