package modules.input.test
{
	import benkuper.util.osc.OSCMaster;
	import com.greensock.TweenLite;
    import flash.events.Event;
    import flash.utils.getTimer;
	import modules.input.InputModule;
    import modules.input.ciboulette.measures.IntervalMeasure;
import modules.input.measure.BoolMeasure;
import modules.input.measure.ContinuousMeasure;
    import modules.input.measure.Measure;
    import modules.input.micro.measures.NoteMeasure;
	import modules.input.micro.measures.OctaveMeasure;
	import modules.parameters.ParameterFloat;
	import org.tuio.osc.IOSCListener;
	import org.tuio.osc.OSCMessage;

import ui.components.Button;
import ui.components.TextBox;
import ui.components.UIEvent;

/**
	 * ...
	 * @author Ben Kuper
	 */
	public class TestInputModule extends InputModule
	{
		
		// measures
		protected var boolmeasure:BoolMeasure;
        protected var notemeasure:NoteMeasure;


        // buttons
        protected var testBT:Button;
        protected var testBT2:Button;
        protected var noteIndex:int;

		
		public function TestInputModule()
		{
			super();
            boolmeasure = addMeasure(new BoolMeasure("bool test", "test bool measure")) as BoolMeasure;
            notemeasure = addMeasure(new NoteMeasure("note test", "test note measure")) as NoteMeasure;

            noteIndex = 0;

            testBT = new Button("Bool Test");
            addControl(testBT);
            testBT.addEventListener(UIEvent.BUTTON_SELECTED, BTHandler);

            testBT2 = new Button("Note Test");
            addControl(testBT2);
            testBT2.addEventListener(UIEvent.BUTTON_SELECTED, BTHandler);
		}


        protected function BTHandler(e:UIEvent):void
        {
            var targetBT:Button = e.target as Button;

            switch (targetBT)
            {
                case testBT:
                        boolmeasure.value = !boolmeasure.value;
                        trace(boolmeasure.value);
                    break;

                case testBT2:
                    notemeasure.setValueWithIndex(noteIndex);
                    noteIndex++;
                    if (noteIndex > 10) noteIndex = 0;
                    break;
            }
        }


    override protected function draw():void
    {
        super.draw();

        testBT2.x = 100;
        notemeasure.gui.x = 100;

    }

        //clean
        override public function clean():void
        {
            super.clean();
            testBT.removeEventListener(UIEvent.BUTTON_SELECTED, BTHandler);

        }
    }

}