package modules.input.myo 
{
	import benkuper.util.osc.OSCMaster;
	import modules.input.InputModule;
	import modules.input.measure.ContinuousMeasure;
	import org.tuio.osc.IOSCListener;
	import org.tuio.osc.OSCMessage;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class MyOSCInputModule extends InputModule implements IOSCListener
	{
		private var yawMeasure:ContinuousMeasure;
		private var pitchMeasure:ContinuousMeasure;
		private var rollMeasure:ContinuousMeasure;
		
		public function MyOSCInputModule() 
		{
			super();
			OSCMaster.register("myo", this);
			
			yawMeasure = addMeasure(new ContinuousMeasure("yaw","Yaw", -Math.PI,Math.PI), "Yaw", true) as ContinuousMeasure;
			pitchMeasure = addMeasure(new ContinuousMeasure("pitch","Pitch", -Math.PI/2, Math.PI/2), "Pitch", true) as ContinuousMeasure;
			rollMeasure = addMeasure(new ContinuousMeasure("roll", "Roll", -Math.PI,Math.PI), "Roll", true) as ContinuousMeasure;
			
			measureContainer.autoLayout = true;
			
			draw();
		}
		
		/* INTERFACE org.tuio.osc.IOSCListener */
		
		public function acceptOSCMessage(msg:OSCMessage):void 
		{
			var type:String = msg.address.split("/")[2];
			switch(type)
			{
				case "orientation":
					yawMeasure.currentValue = msg.arguments[3] as Number;
					pitchMeasure.currentValue = msg.arguments[2] as Number;
					rollMeasure.currentValue = msg.arguments[1] as Number;
					break;
					
				case "pose":
					break;
			}
		}
		
		override public function clean():void
		{
			super.clean();
			OSCMaster.unregister("myo", this);
		}
	}

}