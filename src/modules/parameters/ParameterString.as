package modules.parameters 
{
	import flash.display.Sprite;
	import modules.events.ParameterEvent;
	import ui.components.TextBox;
	import ui.components.UIEvent;
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class ParameterString extends Parameter
	{
		private var _value:String;
		private var textBox:TextBox;
		
		public function ParameterString(id:String, label:String, value:String) 
		{
			super(id, label, Parameter.PARAM_TYPE_STRING);
			
			textBox = new TextBox(label, (value == "")?"empty string param":value);
			textBox.editable = true;
			textBox.addEventListener(UIEvent.TEXTBOX_CHANGE, textBox_textBoxChange);
			gui.addChild(textBox);
			
			this._value = value;
			
			_guiWidth = textBox.width;
			_guiHeight = 20;
		}
		
		private function textBox_textBoxChange(e:UIEvent):void 
		{
			value = String(textBox.text);
		}
		
		//ui
		override protected function updateGUI():void
		{
			textBox.width = guiWidth;
			textBox.height = guiHeight;
		}
		
		//Project save
		override public function getXML():XML
		{
			var xml:XML = super.getXML();
			xml.@value = value;
			
			return xml;
		}
		
		// getters and setters
		public function get value():String 
		{
			return _value;
		}
		
		public function set value(value:String):void 
		{
			_value = value;
			textBox.text = value;
			dispatchEvent(new ParameterEvent(ParameterEvent.VALUE_CHANGE));
		}
		 
	}
}