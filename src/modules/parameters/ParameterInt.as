package modules.parameters 
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import modules.events.ParameterEvent;
	import ui.components.Slider;
	import ui.components.TextBox;
	import ui.components.UIEvent;
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class ParameterInt  extends Parameter
	{
		private var _value:int;
		private var minimumValue:int;
		private var maximumValue:int;
		private var slider:Slider;
		private var textBox:TextBox;
		
		public static const SLIDER_GUI:String = "sliderGui";
		public static const TEXTBOX_GUI:String = "sliderGui";
		private var guiType:String;
		
		public function ParameterInt(id:String, label:String, value:int, minimumValue:int = 0, maximumValue:int = 1, guiType:String = SLIDER_GUI) 
		{
			super(id, label, Parameter.PARAM_TYPE_INT);
			
			this.minimumValue = minimumValue;
			this.maximumValue = maximumValue;
			this.guiType = guiType;
			
			switch (guiType)
			{
				case SLIDER_GUI:
					slider = new Slider(label, value, minimumValue, maximumValue);
					gui.addChild(slider);
					slider.addEventListener(UIEvent.SLIDER_SET, slider_sliderSet);
					slider.steps = -1;
					slider.width = 150;
					slider.noDecimals = true;
					slider.value = value;
					_guiWidth = slider.width;
					_guiHeight = 10;
					break;
					
				case TEXTBOX_GUI:
					textBox = new TextBox(label, value.toString());
					textBox.editable = true;
					textBox.addEventListener(UIEvent.TEXTBOX_CHANGE, textBoxChange);
					gui.addChild(textBox);
					
					_guiWidth = textBox.width;
					_guiHeight = textBox.height;
					break;
			}
			
			this._value = value;
			
		}
		
		private function slider_sliderSet(e:UIEvent):void 
		{
			value = slider.value;
		}
		
		private function textBoxChange(e:UIEvent):void 
		{
			var newValue:int = int(textBox.text);
			
			if (isNaN(newValue))
			{
				textBox.text = value.toString();
			} else
			{
				value = newValue;
			}
		}
		
		//ui
		override protected function updateGUI():void
		{
			switch(guiType)
			{
				case SLIDER_GUI:
					slider.width = guiWidth;
					slider.height = guiHeight;
					break;
					
				case TEXTBOX_GUI:
					textBox.width = guiWidth;
					textBox.height = guiHeight;
					break;
			}
		}
		
		//Project save
		override public function getXML():XML
		{
			var xml:XML = super.getXML();
			xml.@value = value;
			return xml;
		}
		
		// getters and setters
		public function get value():int 
		{
			return _value;
		}
		
		public function set value(value:int):void 
		{
			
			_value = value;
			slider.value = value;
			dispatchEvent(new ParameterEvent(ParameterEvent.VALUE_CHANGE));
		}
		
		public function setSliderSteps(nbSteps:int):void
		{
			if (guiType == SLIDER_GUI)
			{
				slider.steps = nbSteps;
			} else 
			{
				//trace("[ParameterInt] setSliderSteps :: ERROR : this parameter has no slider gui !");
			}
			
		}
	}
}