package modules.parameters 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import modules.events.ParameterEvent;
	import ui.components.Slider;
	import ui.components.UIEvent;
	import ui.Toaster;
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class ParameterFloat extends Parameter
	{
		private var _value:Number;
		private var minimumValue:Number;
		private var maximumValue:Number;
		private var _slider:Slider;
		
		public function ParameterFloat(id:String, label:String, value:Number, minimumValue:Number = 0, maximumValue:Number = 1) 
		{
			super(id, label, Parameter.PARAM_TYPE_FLOAT);
			
			this.minimumValue = minimumValue;
			this.maximumValue = maximumValue;
			
			_slider = new Slider(label, value, minimumValue, maximumValue);
			gui.addChild(slider);
			slider.addEventListener(UIEvent.SLIDER_SET, slider_sliderSet);
			slider.width = 150;
			
			_guiWidth = slider.width;
			_guiHeight = 10;
			
			this._value = value;
		}
		
		private function slider_sliderSet(e:UIEvent):void 
		{
			value = slider.value;
		}
		
		
		//ui
		override protected function updateGUI():void
		{
			if (isNaN(guiWidth) || isNaN(guiHeight)) return;
			
			slider.width = guiWidth;
			slider.height = guiHeight;
		}
		
		//Project save
		override public function getXML():XML
		{
			var xml:XML = super.getXML();
			xml.@value = value;
			if (slider.useTolerance) xml.@tolerance = slider.tolerance;
			return xml;
		}
		
		
		override public function loadXML(data:XML):void
		{
			super.loadXML(data);
			if (slider.useTolerance) slider.tolerance = Number(data.@tolerance);
		}
		
		// getters and setters
		public function get value():Number 
		{
			return _value;
		}
		
		public function set value(value:Number):void 
		{
			_value = value;
			slider.value = value;
			dispatchEvent(new ParameterEvent(ParameterEvent.VALUE_CHANGE));
		}
		
		public function get slider():Slider 
		{
			return _slider;
		}
	}

}