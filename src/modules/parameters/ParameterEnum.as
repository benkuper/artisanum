package modules.parameters 
{
	import flash.display.Sprite;
	import modules.events.ParameterEvent;
	import modules.input.measure.ReferenceValue;
	import ui.components.DropDown;
	import ui.components.DropDownItem;
	import ui.components.UIEvent;
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class ParameterEnum extends Parameter 
	{
		public var dropDown:DropDown;
		private var _selectedItem:Object;
		
		public function ParameterEnum(id:String, label:String, options:Array = null, initialValueIndex:int = -1) 
		{
			super(id, label, Parameter.PARAM_TYPE_ENUM);
			
			//if (options== null) trace("[ParameterEnum] Error :: options should not be null !!");
		
			dropDown = new DropDown(options, label);
			gui.addChild(dropDown);
			dropDown.addEventListener(UIEvent.DROPDOWN_CHANGE, dropDownChange);
			
			if (initialValueIndex >= 0) dropDown.selectedIndex = initialValueIndex;
			
			_guiWidth = dropDown.width;
			_guiHeight = dropDown.height;
		}
		
		private function dropDownChange(e:UIEvent):void 
		{
			dispatchEvent(new ParameterEvent(ParameterEvent.VALUE_CHANGE));
		}
		
		
		public static function create(id:String, label:String, ...options):ParameterEnum
		{
			var enumOptions:Array = new Array();
			for (var i:int = 0; i < options.length; i++)
			{
				enumOptions.push( { id:options[i].id, label:options[i].label, value:options[i].value } );
			}
			
			return new ParameterEnum(id, label, enumOptions);
		}
		
		
		public function setOptions(options:Array, setIndex:int = -1):void
		{
			dropDown.setOptions(options);
			if(setIndex > -1) selectedIndex = setIndex;
		}
		
		// UI
		override protected function updateGUI():void
		{
			dropDown.width = guiWidth;
			dropDown.height = guiHeight;
			
		}
		
		// Clean
		override public function clean():void
		{
			dropDown.removeEventListener(UIEvent.DROPDOWN_CHANGE, dropDownChange);
		}
		
		// Project save
		override public function getXML():XML
		{
			var xml:XML = super.getXML();
			//xml.@valueIndex = dropDown.selectedIndex;
			if (dropDown.selectedOption != null) 
			{
				//trace("Dropdown getXML:",dropDown.selectedOption);
				xml.@valueId = dropDown.selectedOption.id;
				//xml.@value = dropDown.selectedOption.value;
			}
			return xml;
		}
		
		override public function loadXML(data:XML):void
		{
			dropDown.setSelectedOption(data.@valueId);
		}
		
		// Set values
		
		public function setSelectedItem(id:String):void 
		{
			dropDown.setSelectedOption(id);
		}
		
		public function setSelectedItemByValue(value:*):void
		{
			dropDown.setSelectedOptionWithValue(value);
		}
		
		public function setSelectedItemByLabel(label:String):void
		{
			dropDown.setSelectedOptionWithLabel(label);
		}
		
		
		
		// Getters & setters
		
		public function get selectedItem():Object 
		{
			return dropDown.selectedOption;
		}
		
		public function get selectedIndex():int
		{
			return dropDown.selectedIndex;
		}
		
		public function set selectedIndex(value:int):void
		{
			dropDown.selectedIndex = value;
		}
	}
}