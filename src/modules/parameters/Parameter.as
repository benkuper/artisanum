package modules.parameters
{
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	import ui.components.Slider;
	import ui.components.UIComponent;
	
	/**
	 * ...
	 * @author Ben Kuper
	 */
	public class Parameter extends EventDispatcher
	{
		//constants
		public static const PARAM_TYPE_BOOL:String = "bool";
		public static const PARAM_TYPE_INT:String = "int";
		public static const PARAM_TYPE_FLOAT:String = "float";
		public static const PARAM_TYPE_STRING:String = "string";
		public static const PARAM_TYPE_ENUM:String = "enum";
		
		//data
		public var id:String;
		public var label:String;
		public var type:String;
		
		private var _enabled:Boolean;
		
		
		//ui
		public var gui:Sprite;
		
		//layout : Ben] Added guiWidth and guiHeight for width/height override
		protected var _guiWidth:Number;
		protected var _guiHeight:Number;
		
		public function Parameter(id:String, label:String, type:String)
		{
			this.id = id;
			this.label = label;
			this.type = type;
			this._enabled = true;
			
			gui = new Sprite();
		}
		
		public function getXML():XML
		{
			var xml:XML = <parameter></parameter>;
			xml.@id = id;
			xml.@name = label;
			return xml;
			
		}
		
		//cleaning
		public function clean():void
		{
			//to be overriden
		}
		
		public function loadXML(pXML:XML):void  //TO OVERRIDE IN EACH CLASS
		{
			//trace("set pXML", pXML.toXMLString());
			switch(type)
			{
				case PARAM_TYPE_BOOL:
					(this as ParameterBool).value = (pXML.@value == "true");
					break;
				case PARAM_TYPE_INT:
					(this as ParameterInt).value = int(Number(pXML.@value));
					break;
				case PARAM_TYPE_FLOAT:
					(this as ParameterFloat).value = Number(pXML.@value);
					
					break;
				case PARAM_TYPE_STRING:
					(this as ParameterString).value = pXML.@value;
					break;
				case PARAM_TYPE_ENUM:
					(this as ParameterEnum).setSelectedItem(pXML.@valueId);
					break;
			}
			
		}
		
		//[Ben] Added updateGUI so gui can be resized safely width guiWidth & guiHeight properties
		//ui
		protected function updateGUI():void 
		{
			
		}	
		
		//getter / setter
		
		public function get guiWidth():Number 
		{
			return _guiWidth;
		}
		
		public function set guiWidth(value:Number):void 
		{
			_guiWidth = value;
			updateGUI();
		}
		
		public function get guiHeight():Number 
		{
			return _guiHeight;
		}
		
		public function set guiHeight(value:Number):void 
		{
			_guiHeight = value;
			updateGUI();
		}
		
		public function get enabled():Boolean 
		{
			return _enabled;
		}
		
		public function set enabled(value:Boolean):void 
		{
			_enabled = value;
			//gui.alpha = value?1:.5;
			gui.mouseEnabled = value;
			
			for (var i:int = 0; i < gui.numChildren; i++) 
			{
				var child:UIComponent = gui.getChildAt(i) as UIComponent;
				if(child != null) child.enabled = value;
			}
		}
		
	}

}