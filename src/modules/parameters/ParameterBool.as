package modules.parameters 
{
	import flash.display.Sprite;
	import modules.events.ParameterEvent;
	import ui.components.CheckBox;
	import ui.components.UIEvent;
	import ui.Toaster;
	/**
	 * ...
	 * @author Antoine Costes
	 */
	public class ParameterBool extends Parameter
	{
		private var _value:Boolean;
		private var checkBox:CheckBox;
		
		public function ParameterBool(id:String, label:String, value:Boolean) 
		{
			super(id, label, Parameter.PARAM_TYPE_BOOL);
			this._value = value;
			
			checkBox = new CheckBox(label, true, value);
			gui.addChild(checkBox);
			checkBox.addEventListener(UIEvent.CHECKBOX_DESELECTED, gui_handler);
			checkBox.addEventListener(UIEvent.CHECKBOX_SELECTED, gui_handler);
			
			
			_guiWidth = checkBox.width;
			_guiHeight = checkBox.height;
		}
		
		private function gui_handler(e:UIEvent):void 
		{
			value = (e.type == UIEvent.CHECKBOX_SELECTED);
		}
		
		
		//ui
		override protected function updateGUI():void
		{
			checkBox.width = guiWidth;
			checkBox.height = guiHeight;
		}
		
		//Project save
		override public function getXML():XML
		{
			var xml:XML = super.getXML();
			xml.@value = value;
			return xml;
		}
		
		
		// getters and setters
		public function set value(value:Boolean):void 
		{
			if (_value == value) return;
			
			_value = value;
			checkBox.selected = value;
			dispatchEvent(new ParameterEvent(ParameterEvent.VALUE_CHANGE));
			
		}
		
		public function get value():Boolean 
		{
			return _value;
		}
	}

}