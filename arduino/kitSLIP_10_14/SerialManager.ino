OSCMessage buildMessage()
{
  OSCMessage msg(KIT_ADDRESS);
  return msg;
}

OSCMessage buildMessage(char* command)
{
  OSCMessage msg = buildMessage();
  msg.add(command);
  return msg;
}

void sendMessage(char* command)
{
  OSCMessage msg = buildMessage(command);
  sendMessage(&msg);
}

void sendMessage(OSCMessage* msg)
{
  SLIPSerial.beginPacket();  
  (*msg).send(SLIPSerial); // send the bytes to the SLIP stream
  SLIPSerial.endPacket(); // mark the end of the OSC Packet
  (*msg).empty(); // free space occupied by message
  delay(20);
}

void read()
{
  OSCMessage msgIN;
  int size;

  while(!SLIPSerial.endofPacket() && SLIPSerial.available() > 0)
    if( (size =SLIPSerial.available()) > 0)
      while(size--)
        msgIN.fill(SLIPSerial.read());

  if(!msgIN.hasError())
    msgIN.dispatch(ARTISANUM_ADDRESS, receiveInstruction);
}

void receiveInstruction(OSCMessage &msg)
{
  //sendMessage("got message");

  if(msg.isString(0))
  {
    int length=msg.getDataLength(0);
    char command[length];
    msg.getString(0,command,length);

    // sendMessage(command);

    if(strcmp(command, HANDSHAKE) == 0)
    {
      // stop sending handshakes
     active = true; 
    } 
    else if(strcmp(command, SET_PARAM) == 0)
    {
      int index = msg.getInt(1);
      int isTouch = msg.getInt(2);
      int val = msg.getInt(3);

      storeCapaValue(val, index, isTouch > 0);
      
      OSCMessage paramMsg = buildMessage(PARAM_VALUES);
      paramMsg.add(index);
      paramMsg.add(getCapaValue(index, true));
      paramMsg.add(getCapaValue(index, false));
      sendMessage(&paramMsg);
    } 
    else if(strcmp(command, GET_PARAMS) == 0)
    {
      int index = msg.getInt(1);
      OSCMessage paramMsg = buildMessage(PARAM_VALUES);
      paramMsg.add(index);
      paramMsg.add(getCapaValue(index, true));
      paramMsg.add(getCapaValue(index, false));
      sendMessage(&paramMsg);
    } 
    else if(strcmp(command, RESET_PARAMS) == 0)
    {
      resetCapaValues();
    } 
    else if (!msg.hasError())
    {
      sendMessage("command unknown");
      sendMessage(command);
    }
  } 
  else
  {
    sendMessage("msg must begin with a string");
  }
}

/*
  switch (instruction)
 {
 case 'b':  // handshake
 active = true;
 sendParams();
 break;
 
 case 'e':  // end
 active = false;
 break;
 
 case 't':  // set touch threshold
 //if (messageLength < 3) return;
 value = incomingBuffer[readIndex++] - 48;
 index = incomingBuffer[readIndex++] - 48;
 storeCapaValue(value, index, true);
 calibrate();
 
 startMessage();
 printValue(TOUCH_VALUE_CHAR, index, getCapaValue(index, true), 1);
 endMessage();
 break;
 
 case 'r':  // set release threshold
 //if (messageLength < 3) return;
 value = incomingBuffer[readIndex++] - 48;
 index = incomingBuffer[readIndex++] - 48;
 storeCapaValue(value, index, false);
 calibrate();
 
 startMessage();
 printValue(RELEASE_VALUE_CHAR, index, getCapaValue(index, false), 1);
 endMessage();
 break;
 
 case 'g':  // get threshold values
 //if (messageLength < 3) return;
 index = incomingBuffer[readIndex++] - 48;
 
 startMessage();
 printValue(TOUCH_VALUE_CHAR, index, getCapaValue(index, true), 1);
 endMessage();
 startMessage();
 printValue(RELEASE_VALUE_CHAR, index, getCapaValue(index, false), 1);
 endMessage();
 break;
 }
 }
 
 void sendParams()
 {
 startMessage();
 printValue(ID_CHAR, id, id, 1);
 endMessage();
 startMessage();
 printValue(NB_SENSORS_CHAR, 0, NB_CAPAS, 1);
 endMessage();
 for (int i = 0 ; i < NB_CAPAS ; i++)
 {
 startMessage();
 printValue(TOUCH_VALUE_CHAR, i, getCapaValue(i, true), 1);
 endMessage();
 startMessage();
 printValue(RELEASE_VALUE_CHAR, i, getCapaValue(i, false), 1);
 endMessage();
 }
 startMessage();
 for (int i = 0 ; i < NB_CAPAS ; i++)  printValue(CAPA_VALUE_CHAR, i, 0, 8);
 endMessage();
 }
 */

