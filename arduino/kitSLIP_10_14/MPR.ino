void readTouchInputs()
{
  if(!checkInterrupt())
  {
    //read the touch state from the MPR121
    Wire.requestFrom(0x5A,2); 
    byte LSB = Wire.read();
    byte MSB = Wire.read();
    uint16_t touched = ((MSB << 8) | LSB); //16bits that make up the touch states
    for (int i=0; i < NB_CAPAS; i++)
    { 
      if(touched & (1<<i))
      {
        if(touchStates[i] == 0)
        {
          OSCMessage valMsg = buildMessage(UPDATE_VALUES);
          valMsg.add(CAPA);
          valMsg.add(i);
          valMsg.add(1);
          sendMessage(&valMsg);
        }
        else if(touchStates[i] == 1)
        {
          //pin i is still being touched
        }  

        touchStates[i] = 1;      
      }
      else{
        if(touchStates[i] == 1)
        {
          OSCMessage valMsg = buildMessage(UPDATE_VALUES);
          valMsg.add(CAPA);
          valMsg.add(i);
          valMsg.add(0);
          sendMessage(&valMsg);
        }
        touchStates[i] = 0;
      }
    }
  }
}

void resetCapaValues()
{
    for (int i=0; i < NB_CAPAS; i++)
  {
      storeCapaValue(0x06, i, true);
      storeCapaValue(0x06, i, false);
  }
}

void calibrate()
{
  set_register(0x5A, ELE_CFG, 0x00); 

  // Section A - Controls filtering when data is > baseline.
  set_register(0x5A, MHD_R, 0x01);
  set_register(0x5A, NHD_R, 0x01);
  set_register(0x5A, NCL_R, 0x00);
  set_register(0x5A, FDL_R, 0x00);

  // Section B - Controls filtering when data is < baseline.
  set_register(0x5A, MHD_F, 0x01);
  set_register(0x5A, NHD_F, 0x01);
  set_register(0x5A, NCL_F, 0xFF);
  set_register(0x5A, FDL_F, 0x02);

  // Section C - Sets touch and release thresholds for each electrode
  set_register(0x5A, ELE0_T, getCapaValue(0, true));
  set_register(0x5A, ELE0_R, getCapaValue(0, false));

  set_register(0x5A, ELE1_T, getCapaValue(1, true));
  set_register(0x5A, ELE1_R, getCapaValue(1, false));

  set_register(0x5A, ELE2_T, getCapaValue(2, true));
  set_register(0x5A, ELE2_R, getCapaValue(2, false));

  set_register(0x5A, ELE3_T, getCapaValue(3, true));
  set_register(0x5A, ELE3_R, getCapaValue(3, false));

  set_register(0x5A, ELE4_T, getCapaValue(4, true));
  set_register(0x5A, ELE4_R, getCapaValue(4, false));

  set_register(0x5A, ELE5_T, getCapaValue(5, true));
  set_register(0x5A, ELE5_R, getCapaValue(5, false));

  set_register(0x5A, ELE6_T, getCapaValue(6, true));
  set_register(0x5A, ELE6_R, getCapaValue(6, false));

  set_register(0x5A, ELE7_T, getCapaValue(7, true));
  set_register(0x5A, ELE7_R, getCapaValue(7, false));

  set_register(0x5A, ELE8_T, getCapaValue(8, true));
  set_register(0x5A, ELE8_R, getCapaValue(8, false));

  set_register(0x5A, ELE9_T, getCapaValue(9, true));
  set_register(0x5A, ELE9_R, getCapaValue(9, false));

  set_register(0x5A, ELE10_T, getCapaValue(10, true));
  set_register(0x5A, ELE10_R, getCapaValue(10, false));

  set_register(0x5A, ELE11_T, getCapaValue(11, true));
  set_register(0x5A, ELE11_R, getCapaValue(11, false));

  // Section D
  // Set the Filter Configuration
  // Set ESI2
  set_register(0x5A, FIL_CFG, 0x04);

  // Section E
  // Electrode Configuration
  // Set ELE_CFG to 0x00 to return to standby mode
  set_register(0x5A, ELE_CFG, 0x0C);  // Enables all 12 Electrodes

  // Section F
  // Enable Auto Config and auto Reconfig
  /*set_register(0x5A, ATO_CFG0, 0x0B);
   set_register(0x5A, ATO_CFGU, 0xC9);  // USL = (Vdd-0.7)/vdd*256 = 0xC9 @3.3V   set_register(0x5A, ATO_CFGL, 0x82);  // LSL = 0.65*USL = 0x82 @3.3V
   set_register(0x5A, ATO_CFGT, 0xB5);*/  // Target = 0.9*USL = 0xB5 @3.3V
  //set_register(0x5A, ELE_CFG, 0x0C);
}


void enableAutoConfig()
{
  set_register(0x5A, ELE_CFG, 0x00); // Stop the touch momentarily
  set_register(0x5A, ATO_CFG0, 0x0B);
  set_register(0x5A, ATO_CFGU, 0xC9);  // USL = (Vdd-0.7)/vdd*256 = 0xC9 @3.3V   set_register(0x5A, ATO_CFGL, 0x82);  // LSL = 0.65*USL = 0x82 @3.3V
  set_register(0x5A, ATO_CFGT, 0xB5);
  set_register(0x5A, ELE_CFG, 0x0C); // Start Touch again
}

boolean checkInterrupt(void)
{
  return digitalRead(irqpin);
}

void set_register(int address, unsigned char r, unsigned char v)
{
  Wire.beginTransmission(address);
  Wire.write(r);
  Wire.write(v);
  Wire.endTransmission();
}
