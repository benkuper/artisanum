#include "mpr121.h"
#include <Wire.h>
#include <EEPROM.h>
#include <OSCMessage.h>
#include <OSCBundle.h>

#include <SLIPEncodedSerial.h>
SLIPEncodedSerial SLIPSerial(Serial);

// GLOBAL
int ledPin = 13;
boolean active = false;
boolean arduinoDebug = false;
unsigned long sanityDelay = 100;
int handshakeTimeInMs = 1000;
int pingTimeInMs = 500;
unsigned long lastPingTime = 0;

// from artisanum
char* ARTISANUM_ADDRESS = "/artisanum";
char* UNKNOWN_COMMAND = "unknownCommand";
char* INVALID_MESSAGE = "invalidMessage";
char* OK = "ok";
char* SET_PARAM = "setParam";
char* GET_PARAMS = "getParams";
char* RESET_PARAMS = "resetParams";

// to artisanum
char *KIT_ADDRESS = "/BTKit";//"/Kit";    // ID DU KIT - NE PAS OUBLIER LE /
char *PING = "ping";
char *HANDSHAKE = "handshake";
char *CAPA = "capa";
char *PARAM_VALUES = "paramValues";
char *UPDATE_VALUES = "updateValues";

// MPR
int NB_CAPAS = 6;//12;                      // NB CAPAS
int irqpin = 3;  // Digital 2
boolean touchStates[12]; //to keep track of the previous touch states

//EEPROM
int eepromStartAdress = 100;
int nbStoredValues = 12*2;

void setup()
{
  pinMode(ledPin, OUTPUT);
  
  pinMode(irqpin, INPUT);
  digitalWrite(irqpin, HIGH); //enable pullup resistor

  SLIPSerial.begin(38400); 
  Wire.begin();
  
  //resetCapaValues();
  calibrate();
  //enableAutoConfig(); 
}

void loop()
{
  digitalWrite(ledPin, active);
  unsigned long time = millis();
  
  // Receive serial instructions
  read();
     
  // Send values only if active and if a value changed
  if (active)
  {
    readAndSendValues();
    
    if ((unsigned long)(millis() - lastPingTime) > pingTimeInMs)
    {
      sendMessage(PING);
       lastPingTime = millis();
    }
    
  } // else send handshake to get activated
  else if ((unsigned long)(millis() - lastPingTime) > handshakeTimeInMs)
  {
    sendMessage(HANDSHAKE);
     lastPingTime = millis();
  }
  
  // Sanity delay
  time = millis() - time;
  unsigned long delayMs = 0;
  if (time > 0 && time < sanityDelay) delayMs = sanityDelay - time;
  delay(delayMs);
}

void readAndSendValues()
{
  if(!checkInterrupt())
  {
    readTouchInputs();
  }
}


