void storeCapaValue(byte value, int index, boolean isTouch)
{
  if (isTouch) index = 2*index;
  else index = 2*index + 1;
   EEPROM.write( eepromStartAdress + index, value);
}

byte getCapaValue(int index, boolean isTouch)
{
  if (isTouch) index = 2*index;
  else index = 2*index + 1;
   return EEPROM.read( eepromStartAdress + index);
}

void clearEEPROM()
{
  for (int i = 0 ; i < nbStoredValues ; i++)
   EEPROM.write( eepromStartAdress + i, 0);
}
