void initCapacitive()
{
  if (arduinoDebug)
  {
    Serial.println("---- INIT CAPACITIVE ----");
  }
  clearCapacitive();
  fCount = 0;
  capacitiveActive = true;
  
  addCapa();
  addCapa();
  addCapa();
   addCapa();
    addCapa();
}

void clearCapacitive()
{
  memset(capas,0,sizeof(capas));
  nbCapas = 0;
}

void addCapa()
{
  capas[nbCapas] = CapacitiveSensor(SEND_PIN, capasPins[nbCapas]);
  capas[nbCapas].set_CS_Timeout_Millis(globalTimeOut); 
  nbCapas++;
}

boolean readCapacitiveValues()
{ 
  if (fCount > averagePeriod)
  {
    // reset capacitive values
    for (int i = 0 ; i < nbCapas ; i++) capasValues[i] = 0;
    fCount = 0;
  }
  
  for (int i = 0 ; i < nbCapas ; i++)
  {
    int tempVal = capas[i].capacitiveSensor(samples);
    if (tempVal < 0 || tempVal > globalTimeOut) tempVal = globalTimeOut;
    capasValues[i] += tempVal;
  }
  
  fCount++;
  
  return (fCount > averagePeriod);
}


void sendCapacitiveValues()
{
  if (nbCapas == 0) return;
  
  
  for (int i = 0 ; i < nbCapas ; i++)
  {
  OSCMessage valMsg = buildMessage(UPDATE_VALUES);
  valMsg.add(CAPA);
  valMsg.add(i);
  valMsg.add(capasValues[i]);///fCount);
  sendMessage(&valMsg);
  }
}

