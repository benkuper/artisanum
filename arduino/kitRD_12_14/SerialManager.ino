OSCMessage buildMessage()
{
  OSCMessage msg(KIT_ADDRESS);
  return msg;
}

OSCMessage buildMessage(char* command)
{
  OSCMessage msg = buildMessage();
  msg.add(command);
  return msg;
}

void sendMessage(char* command)
{
  OSCMessage msg = buildMessage(command);
  sendMessage(&msg);
}

void sendMessage(OSCMessage* msg)
{
  SLIPSerial.beginPacket();  
  (*msg).send(SLIPSerial); // send the bytes to the SLIP stream
  SLIPSerial.endPacket(); // mark the end of the OSC Packet
  (*msg).empty(); // free space occupied by message
  delay(20);
}

void read()
{
  OSCMessage msgIN;
  int size;

  while(!SLIPSerial.endofPacket() && SLIPSerial.available() > 0)
    if( (size =SLIPSerial.available()) > 0)
      while(size--)
        msgIN.fill(SLIPSerial.read());

  if(!msgIN.hasError())
    msgIN.dispatch(ARTISANUM_ADDRESS, receiveInstruction);
}

void receiveInstruction(OSCMessage &msg)
{
  //sendMessage("got message");

  if(msg.isString(0))
  {
    int length=msg.getDataLength(0);
    char command[length];
    msg.getString(0,command,length);

    // sendMessage(command);

    if(strcmp(command, HANDSHAKE) == 0)
    {
      // stop sending handshakes
     active = true; 
          initCapacitive();
    } 
    else if(strcmp(command, SET_PARAM) == 0)
    {
      int val = msg.getInt(1);
      
      // TODO : EEPOEM STORAGE
      //storeCapaValue(val, index, isTouch > 0);
      globalTimeOut = val*TIME_OUT_FACTOR;

      OSCMessage paramMsg = buildMessage(PARAM_VALUES);
      paramMsg.add(globalTimeOut/TIME_OUT_FACTOR);
      sendMessage(&paramMsg);
    } 
    else if(strcmp(command, GET_PARAMS) == 0)
    {
      OSCMessage paramMsg = buildMessage(PARAM_VALUES);
      paramMsg.add(globalTimeOut/TIME_OUT_FACTOR);
      sendMessage(&paramMsg);
    } 
    else if(strcmp(command, RESET_PARAMS) == 0)
    {
      globalTimeOut = 600;
      
      OSCMessage paramMsg = buildMessage(PARAM_VALUES);
      paramMsg.add(globalTimeOut/TIME_OUT_FACTOR);
      sendMessage(&paramMsg);
    } 
    else if (!msg.hasError())
    {
      sendMessage("command unknown");
      sendMessage(command);
    }
  } 
  else
  {
    sendMessage("msg must begin with a string");
  }
}

