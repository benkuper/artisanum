#include <CapacitiveSensor.h>
#include <Wire.h>
#include <EEPROM.h>
#include <OSCMessage.h>
#include <OSCBundle.h>

#include <SLIPEncodedSerial.h>
SLIPEncodedSerial SLIPSerial(Serial);

// GLOBAL
int ledPin = 13;
boolean active = false;
boolean arduinoDebug = false;
unsigned long sanityDelay = 100;
int handshakeTimeInMs = 1000;
int pingTimeInMs = 500;
unsigned long lastPingTime = 0;

// from artisanum
char* ARTISANUM_ADDRESS = "/artisanum";
char* UNKNOWN_COMMAND = "unknownCommand";
char* INVALID_MESSAGE = "invalidMessage";
char* OK = "ok";
char* SET_PARAM = "setParam";
char* GET_PARAMS = "getParams";
char* RESET_PARAMS = "resetParams";

// to artisanum
char *KIT_ADDRESS = "/RDKit";//"/Kit";    // ID DU KIT - NE PAS OUBLIER LE /
char *PING = "ping";
char *HANDSHAKE = "handshake";
char *CAPA = "capa";
char *PARAM_VALUES = "paramValues";
char *TIMEOUT_VALUE = "timeoutValue";
char *UPDATE_VALUES = "updateValues";

// CAPACITIF
boolean capacitiveActive;

#define NUM_MAX_CAPAS 6
int capasPins[] = {11, 10, 9, 8, 7, 6};
#define SEND_PIN 12
CapacitiveSensor capas[NUM_MAX_CAPAS] = {
CapacitiveSensor(SEND_PIN,capasPins[0]), 
CapacitiveSensor(SEND_PIN,capasPins[1]), 
CapacitiveSensor(SEND_PIN,capasPins[2]),
CapacitiveSensor(SEND_PIN,capasPins[3]),
CapacitiveSensor(SEND_PIN,capasPins[4]),
CapacitiveSensor(SEND_PIN,capasPins[5])};
int capasValues[NUM_MAX_CAPAS];
int nbCapas;

int samples = 10;  // A QUOI CA SERT ?
#define TIME_OUT_FACTOR 20 // multiply the received value to reduce needed bytes. With 20 we get the 2000 default value with a received value of 100
int globalTimeOut = 600;
int fCount;
int averagePeriod = 1;


//EEPROM
int eepromStartAdress = 100;
int nbStoredValues = 12*2;

void setup()
{
  pinMode(ledPin, OUTPUT);

  SLIPSerial.begin(38400); 
  Wire.begin();
}

void loop()
{
  digitalWrite(ledPin, active);
  unsigned long time = millis();
  
  // Receive serial instructions
  read();
     
  // Send values only if active and if a value changed
  if (active)
  {
    readAndSendValues();
    
    if ((unsigned long)(millis() - lastPingTime) > pingTimeInMs)
    {
      sendMessage(PING);
       lastPingTime = millis();
    }
    
  } // else send handshake to get activated
  else if ((unsigned long)(millis() - lastPingTime) > handshakeTimeInMs)
  {
    sendMessage(HANDSHAKE);
     lastPingTime = millis();
  }
  
  // Sanity delay
  time = millis() - time;
  unsigned long delayMs = 0;
  if (time > 0 && time < sanityDelay) delayMs = sanityDelay - time;
  delay(delayMs);
}

void readAndSendValues()
{
    if (capacitiveActive && readCapacitiveValues())
    {
        sendCapacitiveValues();
    }
}


