
// PING
int pingTimeInMs = 1000;
unsigned long lastPingTime = 0;
unsigned long sanityDelay = 100;

///////////////////////////
// FUNCTIONS //////////////
///////////////////////////
void initializeArduino();
void initializeInputs();
void updateMeasurementBuffers();
void updateBufferSums();
void updateBufferIndex();
void updateInputStates();
void sendMouseButtonEvents();
void sendMouseMovementEvents();
void addDelay();
void cycleLEDs();
void danceLeds();
void updateOutLEDs();


//void sendKey(int pin, boolean pressed);

//////////////////////
// SETUP /////////////
//////////////////////
void setup() 
{
  initializeArduino();
  initializeInputs();
  danceLeds();
}

////////////////////
// MAIN LOOP ///////
////////////////////
void loop() 
{
  updateMeasurementBuffers();
  updateBufferSums();
  updateBufferIndex();
  //updateInputStates();
  //sendMouseButtonEvents();
  //sendMouseMovementEvents();
  
  readInputs();
  
  cycleLEDs();
  updateOutLEDs();
  addDelay();
  
  // Send Ping
  unsigned long time = millis();
  if ((unsigned long)(millis() - lastPingTime) > pingTimeInMs)
  {
  Serial.write(byte('b'));
  Serial.write(byte(50));
  Serial.write(byte(255));
      lastPingTime = millis();
  }
}

/* SERIAL DEBUG
void sendKey(int pin, boolean pressed)
{
  if (pressed)
  {
    Serial.print(pin);
    Serial.println(" pressed");
  } else
  {
    Serial.print(pin);
    Serial.println(" relased");
  }
}
*/

void sendKey(int pin, boolean pressed)
{
  Serial.write(byte(pressed?'t':'r'));
  Serial.write(byte(pin));
  Serial.write(byte(255));
}
    
    
