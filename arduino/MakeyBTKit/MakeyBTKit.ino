
#include <SoftwareSerial.h>
#include "makeyMate.h"

// PING
int pingTimeInMs = 1000;
unsigned long lastPingTime = 0;

// BATTERY
long batteryValSum = 0;
int nbValues = 0;

///////////////////////////
// Bluetooth Mate Stuff ///
///////////////////////////
makeyMateClass makeyMate;  // create a makeyMateClass object named makeyMate
// This defines the name of the Bluetooth Mate. This string
// can be up to 20 characters long, but it must be terminated
// with a \r character.
char makeyMateName[] = "Artisanum_MaKeyKit_2\r"; // ARTISANUM ID
boolean configMode = true;


// button logger stuff:
const byte SEQUENCE_LENGTH = 6;
int buttonSequence[SEQUENCE_LENGTH] = {
  0, 0, 0, 0, 0, 0};
int sequenceIndex = 0;
int sequenceInterval = 1000;  // in ms
int resetSequence[SEQUENCE_LENGTH] = {
  0, 1, 2, 3, 4, 5};  // u->d->l->r->space->click

///////////////////////////
// FUNCTIONS //////////////
///////////////////////////
void initializeArduino();
void initializeInputs();
void updateMeasurementBuffers();
void updateBufferSums();
void updateBufferIndex();
void updateInputStates();
void sendMouseButtonEvents();
void sendMouseMovementEvents();
void addDelay();
void cycleLEDs();
void danceLeds();
void updateOutLEDs();

// SPP hack
void softBegin();
void readInputs();
void sendKey(int pin, boolean pressed);
void sendBatteryLevel(int level);
void flushBuffer();
void readBluetooth();
void sendAllStates();


//////////////////////
// SETUP /////////////
//////////////////////

void setup() 
{
  delay(1000);
  
  initializeArduino();
  initializeInputs();
  danceLeds();

  Serial.begin(9600);
  
  if (configMode)
  {
    makeyMate.begin(makeyMateName);  // Initialize the bluetooth mate
  }
  else softBegin();
  
  
  makeyMate.connect();  // Attempt to connect to a stored remote address
}

////////////////////
// MAIN LOOP ///////
////////////////////
void loop() 
{
  updateMeasurementBuffers();  // Step 1: read inputs, update measurementBuffer
  updateBufferSums();  // Step 2: update bufferSum, remove old measruement, add new
  updateBufferIndex();  // Step 3: update bitCounter and byteCounter
  //updateInputStates();  // Step 4: check/update pressed/released states, send button presses/releases
  //sendMouseButtonEvents();  // Step 5: Send mouse button click/releases
  //sendMouseMovementEvents(); // Step 6: Send mouse movement

  readInputs();

  cycleLEDs();  // Step 7: Update U/D/L/R/Space/Click LEDs
  updateOutLEDs();  // Step 8: Update output LEDs (K/M)
  addDelay();
  
  batteryValSum += readVcc()/50;
  nbValues++;
  
  // Send Ping
  unsigned long time = millis();
  if ((unsigned long)(millis() - lastPingTime) > pingTimeInMs)
  {
    int level = int(batteryValSum)/nbValues;
    batteryValSum = 0;
    nbValues = 0;
    sendBatteryLevel(level);
    lastPingTime = millis();
  }
  
  //readBluetooth();
  
  // Wait for transmit buffers to be emptied
  //Serial.flush();
  //flushBuffer();
}

long readVcc()
{
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif  
 
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring
 
  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
  uint8_t high = ADCH; // unlocks both
 
  long result = (high<<8) | low;
 
  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}
